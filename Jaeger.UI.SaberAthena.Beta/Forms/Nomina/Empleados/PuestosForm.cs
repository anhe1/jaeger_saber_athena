﻿namespace Jaeger.UI.Forms.Nomina.Empleados {
    internal class PuestosForm : UI.Nomina.Forms.Empleados.PuestosForm {
        protected internal Domain.Base.Abstractions.UIMenuElement menuElement;

        public PuestosForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Text = "Empresa: Áreas";
            this.menuElement = menuElement;
            this.Load += PuestosForm_Load;
        }

        private void PuestosForm_Load(object sender, System.EventArgs e) {
            this.Service = new Aplication.Nomina.Adapter.Services.PuestoService();
            this.TPuesto.Permisos = this.menuElement.Action;
        }
    }
}
