﻿namespace Jaeger.UI.Forms.Nomina.Empleados {
    internal class DepartamentosForm : UI.Nomina.Forms.Empleados.DepartamentosForm {
        protected Domain.Base.Abstractions.UIMenuElement menuElement;
        public DepartamentosForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Text = "Empresa: Departamentos";
            this.menuElement = menuElement;
            this.Load += DepartamentosForm_Load;   
        }

        private void DepartamentosForm_Load(object sender, System.EventArgs e) {
            this.Service = new Aplication.Nomina.Adapter.Services.DepartamentoService();
            this.TDepartamento.Permisos = this.menuElement.Action;
        }
    }
}
