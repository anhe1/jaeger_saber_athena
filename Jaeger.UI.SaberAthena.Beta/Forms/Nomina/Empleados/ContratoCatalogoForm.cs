﻿namespace Jaeger.UI.Forms.Nomina.Empleados {
    /// <summary>
    /// catalogo de contratos
    /// </summary>
    internal class ContratoCatalogoForm : UI.Nomina.Forms.Empleados.ContratoCatalogoForm {
        public ContratoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Load += ContratoCatalogoForm_Load;
        }

        private void ContratoCatalogoForm_Load(object sender, System.EventArgs e) {
            this.service = new Aplication.Nomina.Adapter.Services.ContratoService();
            this.departamentoService = new Aplication.Nomina.Adapter.Services.DepartamentoService();
            this.puestoService = new Aplication.Nomina.Adapter.Services.PuestoService();
        }
    }
}
