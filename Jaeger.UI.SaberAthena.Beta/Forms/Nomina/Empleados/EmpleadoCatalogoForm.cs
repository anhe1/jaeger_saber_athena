﻿namespace Jaeger.UI.Forms.Nomina.Empleados {
    /// <summary>
    /// catalogo de empleados
    /// </summary>
    internal class EmpleadoCatalogoForm : UI.Nomina.Forms.Empleados.EmpleadoCatalogoForm {
        /// <summary>
        /// constructor
        /// </summary>
        public EmpleadoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Load += EmpleadoCatalogoForm_Load;
        }

        private void EmpleadoCatalogoForm_Load(object sender, System.EventArgs e) {
            this.Service = new Aplication.Nomina.Adapter.Services.EmpleadoService();
            this.ServiceContrato = new Aplication.Nomina.Adapter.Services.ContratoService();
        }
    }
}
