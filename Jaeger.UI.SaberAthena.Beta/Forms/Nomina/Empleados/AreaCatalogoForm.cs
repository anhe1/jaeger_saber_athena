﻿namespace Jaeger.UI.Forms.Nomina.Empleados {
    internal class AreaCatalogoForm : UI.Nomina.Forms.Empleados.AreaCatalogoForm {
        protected internal Domain.Base.Abstractions.UIMenuElement menuElement;

        public AreaCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Text = "Empresa: Áreas";
            this.menuElement = menuElement;
            this.Load += AreaCatalogoForm_Load;
        }

        private void AreaCatalogoForm_Load(object sender, System.EventArgs e) {
            this._AreaService = new Aplication.Nomina.Adapter.Services.AreaService();
            this.TArea.Permisos = this.menuElement.Action;
        }
    }
}
