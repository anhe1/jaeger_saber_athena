﻿namespace Jaeger.UI.Forms.Nomina.Movimientos {
    internal class PeriodoCalcularForm : UI.Nomina.Forms.Procesos.PeriodoCalcularForm {
        public PeriodoCalcularForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Load += PeriodoCalcularForm_Load;
        }

        private void PeriodoCalcularForm_Load(object sender, System.EventArgs e) {
            this.Text = "Movimientos: Período";
        }

        protected override void OnLoad(System.EventArgs e) {
            base.OnLoad(e);
            this.service = new Aplication.Nomina.Adapter.Services.CalculoService();
            this.periodoService = new Aplication.Nomina.Adapter.Services.PeriodoService();
        }
    }
}
