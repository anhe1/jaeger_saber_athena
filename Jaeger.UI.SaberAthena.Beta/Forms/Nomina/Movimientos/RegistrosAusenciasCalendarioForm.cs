﻿namespace Jaeger.UI.Forms.Nomina.Movimientos {
    internal class RegistrosAusenciasCalendarioForm : UI.Nomina.Forms.Procesos.RegistrosAusenciasCalendarioForm {
        public RegistrosAusenciasCalendarioForm(Domain.Base.Abstractions.UIMenuElement uIMenuElement) : base() {
            this.Load += RegistrosAusenciasCalendarioForm_Load;
        }

        private void RegistrosAusenciasCalendarioForm_Load(object sender, System.EventArgs e) {
            this.Text = "Movimientos: Registro de Ausencias";
            this._Service = new Aplication.Nomina.Adapter.Services.RegistroAusenciasService();
        }
    }
}
