﻿namespace Jaeger.UI.Forms.Nomina.Movimientos {
    public class HorasExtrasForm : UI.Nomina.Forms.Procesos.HorasExtrasForm {
        public HorasExtrasForm(Domain.Base.Abstractions.UIMenuElement uIMenuElement) : base() {
            this.Load += HorasExtrasForm_Load;
        }

        private void HorasExtrasForm_Load(object sender, System.EventArgs e) {
            this.Text = "Movimientos: Horas Extra";
        }
    }
}
