﻿namespace Jaeger.UI.Forms.Nomina.Movimientos {
    internal class RegistrosVacacionesForm : UI.Nomina.Forms.Empleados.VacacionesForm {
        public RegistrosVacacionesForm(Domain.Base.Abstractions.UIMenuElement uIMenuElement) : base() {
            this._Service = new Aplication.Nomina.Adapter.Services.RegistroAusenciasService();
            this.Load += RegistrosVacacionesForm_Load;
        }

        private void RegistrosVacacionesForm_Load(object sender, System.EventArgs e) {
            this.Text = "Movimientos: Registro de Vacaciones";
        }
    }
}
