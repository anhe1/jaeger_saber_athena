﻿namespace Jaeger.UI.Forms.Nomina.Movimientos {
    internal class AguinaldoForm : UI.Nomina.Forms.Procesos.AguinaldoForm {
        public AguinaldoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.Service = new Aplication.Nomina.Adapter.Services.AguinaldoService();
            this.Load += AguinaldoForm_Load;
        }

        private void AguinaldoForm_Load(object sender, System.EventArgs e) {
            this.Text = "Movimientos: Aguinaldo";
        }
    }
}
