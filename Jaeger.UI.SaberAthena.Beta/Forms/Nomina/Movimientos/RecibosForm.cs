﻿namespace Jaeger.UI.Forms.Nomina.Movimientos {
    internal class RecibosForm : UI.Nomina.Forms.ComprobanteNominaForm {

        public RecibosForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this._Service = new Aplication.Nomina.Services.ControlService();
            this.Load += RecibosForm_Load;
        }

        private void RecibosForm_Load(object sender, System.EventArgs e) {
            this.Text = "Movimientos: Consulta Recibos";
        }
    }
}
