﻿using Jaeger.Aplication.Nomina.Adapter.Services;

namespace Jaeger.UI.Forms.Nomina.Parametros {
    internal class TablaISRForm : UI.Nomina.Forms.Parametros.TablaISRForm {
        public TablaISRForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.service = new TablaISRService();
        }
    }
}
