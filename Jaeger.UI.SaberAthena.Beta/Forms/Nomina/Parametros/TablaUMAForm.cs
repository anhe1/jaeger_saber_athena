﻿namespace Jaeger.UI.Forms.Nomina.Parametros {
    internal class TablaUMAForm : UI.Nomina.Forms.Parametros.TablaUMAForm {
        public TablaUMAForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.service = new Aplication.Nomina.Adapter.Services.UMAService();
        }
    }
}
