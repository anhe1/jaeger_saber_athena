﻿namespace Jaeger.UI.Forms.Nomina.Parametros {
    internal class TablaSalarioMinimoForm : UI.Nomina.Forms.Parametros.TablaSalarioMinimoForm {
        public TablaSalarioMinimoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.service = new Aplication.Nomina.Adapter.Services.SalarioMinimoService();
        }
    }
}
