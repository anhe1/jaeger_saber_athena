﻿using Jaeger.Aplication.Nomina.Adapter.Services;

namespace Jaeger.UI.Forms.Nomina.Parametros {
    internal class ParametrosForm : UI.Nomina.Forms.Parametros.ParametrosForm {
        public ParametrosForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.service = new ParametrosService();
            this.serviceISR = new TablaISRService();
            this.serviceSubsidio = new SubsidioAlEmpleoService();
        }
    }
}
