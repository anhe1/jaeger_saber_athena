﻿namespace Jaeger.UI.Forms.Nomina.Parametros {
    internal class TablaSubsidioForm : UI.Nomina.Forms.Parametros.TablaSubsidioForm {
        public TablaSubsidioForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base(menuElement) {
            this.service = new Aplication.Nomina.Adapter.Services.SubsidioAlEmpleoService();
        }
    }
}
