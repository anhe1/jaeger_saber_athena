﻿namespace Jaeger.UI.Forms.Nomina.Parametros {
    internal class ConceptoCatalogoForm : UI.Nomina.Forms.Procesos.ConceptoCatalogoForm {
        protected internal Domain.Base.ValueObjects.UIAction _Permisos;
        public ConceptoCatalogoForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this._Permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
            this.Load += ConceptoNominaCatalogoForm_Load;
        }

        private void ConceptoNominaCatalogoForm_Load(object sender, System.EventArgs e) {
            this.Text = "Parametros: Conceptos";
            this.TConcepto.Permisos = this._Permisos;
        }

        protected override void OnLoad(System.EventArgs e) {
            base.OnLoad(e);
            this.service = new Aplication.Nomina.Adapter.Services.ConceptoService();
        }
    }
}
