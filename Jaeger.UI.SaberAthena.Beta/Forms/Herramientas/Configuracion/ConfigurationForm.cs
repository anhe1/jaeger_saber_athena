﻿using System;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms.Herramientas.Configuracion {
    internal class ConfigurationForm : Empresa.Forms.ConfigurationForm {
        private Telerik.WinControls.UI.RadPageViewPage pageNomina = new Telerik.WinControls.UI.RadPageViewPage() { Text = "| Nómina" };

        public ConfigurationForm(Domain.Base.Abstractions.UIMenuElement menuElement) : base() {
            this.Load += ConfigurationForm_Load;
        }

        private void ConfigurationForm_Load(object sender, EventArgs e) {
            this.Title.Text = $"|| Configuración de la empresa registrada: {ConfigService.Synapsis.Empresa.RFC}";
            this.SynapsisProperty.SelectedObject = ConfigService.Synapsis;
            var d0 = new UI.Nomina.Forms.Parametros.ConfigurationControl();
            pageNomina.Controls.Add(d0);
            this.TabsControl.Pages.Add(pageNomina);
        }
    }
}
