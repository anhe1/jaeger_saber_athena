﻿namespace Jaeger.UI.Forms {
    partial class MainRibbonForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRibbonForm));
            this.BEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.RadDock = new Telerik.WinControls.UI.Docking.RadDock();
            this.RadContainer = new Telerik.WinControls.UI.Docking.DocumentContainer();
            this.visualStudio2012LightTheme1 = new Telerik.WinControls.Themes.VisualStudio2012LightTheme();
            this.MenuRibbonBar = new Telerik.WinControls.UI.RadRibbonBar();
            this.adm_grp_nomina = new Telerik.WinControls.UI.RibbonTab();
            this.adm_gnom_empleado = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gnom_empexp = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_empcont = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_gmov = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gnom_periodo = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_sgmov = new Telerik.WinControls.UI.RadRibbonBarButtonGroup();
            this.adm_gnom_movvac = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_movfal = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_movacu = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_aguina = new Telerik.WinControls.UI.RadButtonElement();
            this.adm_gnom_recibo = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gnom_reccon = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_recres = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_parame = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.adm_gnom_gconcep = new Telerik.WinControls.UI.RadButtonElement();
            this.nom_gpar_empresa = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.nom_gpar_areas = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gpar_deptos = new Telerik.WinControls.UI.RadMenuItem();
            this.nom_gpar_puesto = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_tablas = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.adm_gnom_tisr = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_tsbe = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_tsm = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_tuma = new Telerik.WinControls.UI.RadMenuItem();
            this.adm_gnom_conf = new Telerik.WinControls.UI.RadButtonElement();
            this.TTools = new Telerik.WinControls.UI.RibbonTab();
            this.dsk_grp_config = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gconfig_param = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gconfig_emisor = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_avanzado = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_menu = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_usuario = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_perfil = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gconfig_cert = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gconfig_series = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_tools = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gtools_validador = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_validarfc = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_validacedula = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_gtools_actualiza = new Telerik.WinControls.UI.RadDropDownButtonElement();
            this.dsk_gtools_catsat = new Telerik.WinControls.UI.RadMenuItem();
            this.dsk_grp_theme = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_gtheme_2010Black = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Blue = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_gtheme_2010Silver = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_grp_about = new Telerik.WinControls.UI.RadRibbonBarGroup();
            this.dsk_btn_manual = new Telerik.WinControls.UI.RadButtonElement();
            this.dsk_btn_about = new Telerik.WinControls.UI.RadButtonElement();
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).BeginInit();
            this.RadDock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // BEstado
            // 
            this.BEstado.Location = new System.Drawing.Point(0, 603);
            this.BEstado.Name = "BEstado";
            this.BEstado.Size = new System.Drawing.Size(1108, 26);
            this.BEstado.SizingGrip = false;
            this.BEstado.TabIndex = 1;
            // 
            // RadDock
            // 
            this.RadDock.AutoDetectMdiChildren = true;
            this.RadDock.Controls.Add(this.RadContainer);
            this.RadDock.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RadDock.IsCleanUpTarget = true;
            this.RadDock.Location = new System.Drawing.Point(0, 162);
            this.RadDock.MainDocumentContainer = this.RadContainer;
            this.RadDock.Name = "RadDock";
            // 
            // 
            // 
            this.RadDock.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadDock.Size = new System.Drawing.Size(1108, 441);
            this.RadDock.TabIndex = 6;
            this.RadDock.TabStop = false;
            // 
            // RadContainer
            // 
            this.RadContainer.Name = "RadContainer";
            // 
            // 
            // 
            this.RadContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.RadContainer.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Fill;
            // 
            // MenuRibbonBar
            // 
            this.MenuRibbonBar.CommandTabs.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_grp_nomina,
            this.TTools});
            // 
            // 
            // 
            this.MenuRibbonBar.ExitButton.Text = "Exit";
            this.MenuRibbonBar.ExitButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.MenuRibbonBar.Location = new System.Drawing.Point(0, 0);
            this.MenuRibbonBar.Name = "MenuRibbonBar";
            // 
            // 
            // 
            this.MenuRibbonBar.OptionsButton.Text = "Options";
            this.MenuRibbonBar.OptionsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            // 
            // 
            // 
            this.MenuRibbonBar.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.MenuRibbonBar.Size = new System.Drawing.Size(1108, 162);
            this.MenuRibbonBar.StartButtonImage = global::Jaeger.UI.Properties.Resources.worker_30px;
            this.MenuRibbonBar.TabIndex = 0;
            this.MenuRibbonBar.Text = "MainRibbonForm";
            this.MenuRibbonBar.Visible = false;
            // 
            // adm_grp_nomina
            // 
            this.adm_grp_nomina.IsSelected = true;
            this.adm_grp_nomina.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_empleado,
            this.adm_gnom_gmov,
            this.adm_gnom_parame});
            this.adm_grp_nomina.Name = "adm_grp_nomina";
            this.adm_grp_nomina.Text = "TNómina";
            this.adm_grp_nomina.UseMnemonic = false;
            // 
            // adm_gnom_empleado
            // 
            this.adm_gnom_empleado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_empexp,
            this.adm_gnom_empcont});
            this.adm_gnom_empleado.Name = "adm_gnom_empleado";
            this.adm_gnom_empleado.Text = "gEmpleados";
            // 
            // adm_gnom_empexp
            // 
            this.adm_gnom_empexp.Image = global::Jaeger.UI.Properties.Resources.workers_30px;
            this.adm_gnom_empexp.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_empexp.Name = "adm_gnom_empexp";
            this.adm_gnom_empexp.Text = "bExpediente";
            this.adm_gnom_empexp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gnom_empexp.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_empcont
            // 
            this.adm_gnom_empcont.Image = global::Jaeger.UI.Properties.Resources.contract_30px;
            this.adm_gnom_empcont.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_empcont.Name = "adm_gnom_empcont";
            this.adm_gnom_empcont.Text = "bContratos";
            this.adm_gnom_empcont.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gnom_empcont.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_gmov
            // 
            this.adm_gnom_gmov.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_periodo,
            this.adm_gnom_sgmov,
            this.adm_gnom_aguina,
            this.adm_gnom_recibo});
            this.adm_gnom_gmov.Name = "adm_gnom_gmov";
            this.adm_gnom_gmov.Text = "gMovimientos";
            // 
            // adm_gnom_periodo
            // 
            this.adm_gnom_periodo.Image = global::Jaeger.UI.Properties.Resources.accounting_30px;
            this.adm_gnom_periodo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_periodo.Name = "adm_gnom_periodo";
            this.adm_gnom_periodo.Text = "bPeriodo";
            this.adm_gnom_periodo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gnom_periodo.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_sgmov
            // 
            this.adm_gnom_sgmov.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_movvac,
            this.adm_gnom_movfal,
            this.adm_gnom_movacu});
            this.adm_gnom_sgmov.MinSize = new System.Drawing.Size(22, 22);
            this.adm_gnom_sgmov.Name = "adm_gnom_sgmov";
            this.adm_gnom_sgmov.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.adm_gnom_sgmov.Text = "radRibbonBarButtonGroup1";
            this.adm_gnom_sgmov.TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // adm_gnom_movvac
            // 
            this.adm_gnom_movvac.Image = global::Jaeger.UI.Properties.Resources.beach_16px;
            this.adm_gnom_movvac.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_movvac.Name = "adm_gnom_movvac";
            this.adm_gnom_movvac.ShowBorder = false;
            this.adm_gnom_movvac.Text = "bVacaciones";
            this.adm_gnom_movvac.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gnom_movvac.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.adm_gnom_movvac.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_movfal
            // 
            this.adm_gnom_movfal.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_movfal.Name = "adm_gnom_movfal";
            this.adm_gnom_movfal.ShowBorder = false;
            this.adm_gnom_movfal.Text = "bFaltas";
            this.adm_gnom_movfal.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gnom_movfal.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_movacu
            // 
            this.adm_gnom_movacu.MinSize = new System.Drawing.Size(0, 20);
            this.adm_gnom_movacu.Name = "adm_gnom_movacu";
            this.adm_gnom_movacu.ShowBorder = false;
            this.adm_gnom_movacu.Text = "Acumulados";
            this.adm_gnom_movacu.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.adm_gnom_movacu.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_aguina
            // 
            this.adm_gnom_aguina.Image = global::Jaeger.UI.Properties.Resources.Dollar_Circled_30px;
            this.adm_gnom_aguina.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_aguina.Name = "adm_gnom_aguina";
            this.adm_gnom_aguina.Text = "bAguinaldo";
            this.adm_gnom_aguina.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gnom_aguina.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_recibo
            // 
            this.adm_gnom_recibo.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gnom_recibo.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gnom_recibo.ExpandArrowButton = false;
            this.adm_gnom_recibo.Image = global::Jaeger.UI.Properties.Resources.money_transfer_30px;
            this.adm_gnom_recibo.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_recibo.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_reccon,
            this.adm_gnom_recres});
            this.adm_gnom_recibo.Name = "adm_gnom_recibo";
            this.adm_gnom_recibo.Text = "bRecibos";
            this.adm_gnom_recibo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gnom_reccon
            // 
            this.adm_gnom_reccon.Name = "adm_gnom_reccon";
            this.adm_gnom_reccon.Text = "bConsulta";
            this.adm_gnom_reccon.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_recres
            // 
            this.adm_gnom_recres.Name = "adm_gnom_recres";
            this.adm_gnom_recres.Text = "Resumen";
            this.adm_gnom_recres.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_parame
            // 
            this.adm_gnom_parame.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_gpar_empresa,
            this.adm_gnom_gconcep,
            this.adm_gnom_tablas,
            this.adm_gnom_conf});
            this.adm_gnom_parame.Name = "adm_gnom_parame";
            this.adm_gnom_parame.Text = "gParametros";
            // 
            // adm_gnom_gconcep
            // 
            this.adm_gnom_gconcep.AutoSize = true;
            this.adm_gnom_gconcep.Image = global::Jaeger.UI.Properties.Resources.tax_30px;
            this.adm_gnom_gconcep.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_gconcep.Name = "adm_gnom_gconcep";
            this.adm_gnom_gconcep.Text = "b.Conceptos";
            this.adm_gnom_gconcep.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gnom_gconcep.TextWrap = true;
            this.adm_gnom_gconcep.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gpar_empresa
            // 
            this.nom_gpar_empresa.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.nom_gpar_empresa.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.nom_gpar_empresa.ExpandArrowButton = false;
            this.nom_gpar_empresa.Image = global::Jaeger.UI.Properties.Resources.organization_30px;
            this.nom_gpar_empresa.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.nom_gpar_empresa.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.nom_gpar_areas,
            this.nom_gpar_deptos,
            this.nom_gpar_puesto});
            this.nom_gpar_empresa.Name = "nom_gpar_empresa";
            this.nom_gpar_empresa.Text = "b.Organización";
            this.nom_gpar_empresa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // nom_gpar_areas
            // 
            this.nom_gpar_areas.Name = "nom_gpar_areas";
            this.nom_gpar_areas.Text = "b.Areas";
            this.nom_gpar_areas.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gpar_deptos
            // 
            this.nom_gpar_deptos.Name = "nom_gpar_deptos";
            this.nom_gpar_deptos.Text = "b.Departamentos";
            this.nom_gpar_deptos.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // nom_gpar_puesto
            // 
            this.nom_gpar_puesto.Name = "nom_gpar_puesto";
            this.nom_gpar_puesto.Text = "b.Puestos";
            this.nom_gpar_puesto.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_tablas
            // 
            this.adm_gnom_tablas.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.adm_gnom_tablas.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.Auto;
            this.adm_gnom_tablas.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.adm_gnom_tablas.ExpandArrowButton = false;
            this.adm_gnom_tablas.FitToSizeMode = Telerik.WinControls.RadFitToSizeMode.FitToParentContent;
            this.adm_gnom_tablas.FlipText = false;
            this.adm_gnom_tablas.Image = global::Jaeger.UI.Properties.Resources.administrative_tools_30px;
            this.adm_gnom_tablas.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_tablas.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.adm_gnom_tisr,
            this.adm_gnom_tsbe,
            this.adm_gnom_tsm,
            this.adm_gnom_tuma});
            this.adm_gnom_tablas.Name = "adm_gnom_tablas";
            this.adm_gnom_tablas.Text = "bTablas";
            this.adm_gnom_tablas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // adm_gnom_tisr
            // 
            this.adm_gnom_tisr.Name = "adm_gnom_tisr";
            this.adm_gnom_tisr.Text = "bTabla ISR";
            this.adm_gnom_tisr.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_tsbe
            // 
            this.adm_gnom_tsbe.Name = "adm_gnom_tsbe";
            this.adm_gnom_tsbe.Text = "bSubsidio";
            this.adm_gnom_tsbe.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_tsm
            // 
            this.adm_gnom_tsm.Name = "adm_gnom_tsm";
            this.adm_gnom_tsm.Text = "b.Salario Mínimo";
            this.adm_gnom_tsm.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_tuma
            // 
            this.adm_gnom_tuma.Name = "adm_gnom_tuma";
            this.adm_gnom_tuma.Text = "b.UMA";
            this.adm_gnom_tuma.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // adm_gnom_conf
            // 
            this.adm_gnom_conf.Image = global::Jaeger.UI.Properties.Resources.settings_30px;
            this.adm_gnom_conf.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.adm_gnom_conf.Name = "adm_gnom_conf";
            this.adm_gnom_conf.Text = "bConfiguracion";
            this.adm_gnom_conf.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.adm_gnom_conf.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // TTools
            // 
            this.TTools.IsSelected = false;
            this.TTools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_grp_config,
            this.dsk_grp_tools,
            this.dsk_grp_theme,
            this.dsk_grp_about});
            this.TTools.Name = "TTools";
            this.TTools.Text = "Herramientas";
            this.TTools.UseMnemonic = false;
            // 
            // dsk_grp_config
            // 
            this.dsk_grp_config.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_param,
            this.dsk_gconfig_cert,
            this.dsk_gconfig_series});
            this.dsk_grp_config.Name = "dsk_grp_config";
            this.dsk_grp_config.Text = "bConfiguración";
            // 
            // dsk_gconfig_param
            // 
            this.dsk_gconfig_param.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gconfig_param.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gconfig_param.ExpandArrowButton = false;
            this.dsk_gconfig_param.Image = global::Jaeger.UI.Properties.Resources.control_panel_30px;
            this.dsk_gconfig_param.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_param.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gconfig_emisor,
            this.dsk_gconfig_avanzado,
            this.dsk_gconfig_menu,
            this.dsk_gconfig_usuario,
            this.dsk_gconfig_perfil});
            this.dsk_gconfig_param.Name = "dsk_gconfig_param";
            this.dsk_gconfig_param.Text = "b.Parametros";
            this.dsk_gconfig_param.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gconfig_emisor
            // 
            this.dsk_gconfig_emisor.Name = "dsk_gconfig_emisor";
            this.dsk_gconfig_emisor.Text = "b.Emisor";
            this.dsk_gconfig_emisor.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_avanzado
            // 
            this.dsk_gconfig_avanzado.Name = "dsk_gconfig_avanzado";
            this.dsk_gconfig_avanzado.Text = "bAvanzado";
            this.dsk_gconfig_avanzado.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_menu
            // 
            this.dsk_gconfig_menu.Name = "dsk_gconfig_menu";
            this.dsk_gconfig_menu.Text = "bMenus";
            this.dsk_gconfig_menu.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_usuario
            // 
            this.dsk_gconfig_usuario.Name = "dsk_gconfig_usuario";
            this.dsk_gconfig_usuario.Text = "bUsuarios";
            this.dsk_gconfig_usuario.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_perfil
            // 
            this.dsk_gconfig_perfil.Name = "dsk_gconfig_perfil";
            this.dsk_gconfig_perfil.Text = "bPerfil";
            this.dsk_gconfig_perfil.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_cert
            // 
            this.dsk_gconfig_cert.Image = global::Jaeger.UI.Properties.Resources.certificate_32px;
            this.dsk_gconfig_cert.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_cert.Name = "dsk_gconfig_cert";
            this.dsk_gconfig_cert.Text = "b.Certificado";
            this.dsk_gconfig_cert.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dsk_gconfig_cert.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gconfig_series
            // 
            this.dsk_gconfig_series.Image = global::Jaeger.UI.Properties.Resources.counter_30px;
            this.dsk_gconfig_series.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gconfig_series.Name = "dsk_gconfig_series";
            this.dsk_gconfig_series.Text = "b.Series";
            this.dsk_gconfig_series.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dsk_gconfig_series.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_grp_tools
            // 
            this.dsk_grp_tools.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_validador,
            this.dsk_gtools_actualiza});
            this.dsk_grp_tools.Name = "dsk_grp_tools";
            this.dsk_grp_tools.Text = "b.Herramientas";
            // 
            // dsk_gtools_validador
            // 
            this.dsk_gtools_validador.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_validador.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_validador.ExpandArrowButton = false;
            this.dsk_gtools_validador.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtools_validador.Image")));
            this.dsk_gtools_validador.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_validador.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_validarfc,
            this.dsk_gtools_validacedula});
            this.dsk_gtools_validador.Name = "dsk_gtools_validador";
            this.dsk_gtools_validador.Text = "b.Validador";
            this.dsk_gtools_validador.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_validarfc
            // 
            this.dsk_gtools_validarfc.Name = "dsk_gtools_validarfc";
            this.dsk_gtools_validarfc.Text = "b.Validación de la clave en el RFC";
            this.dsk_gtools_validarfc.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_validacedula
            // 
            this.dsk_gtools_validacedula.Name = "dsk_gtools_validacedula";
            this.dsk_gtools_validacedula.Text = " b.Cedula de Identificación Fiscal";
            this.dsk_gtools_validacedula.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_gtools_actualiza
            // 
            this.dsk_gtools_actualiza.ArrowButtonMinSize = new System.Drawing.Size(12, 12);
            this.dsk_gtools_actualiza.DropDownDirection = Telerik.WinControls.UI.RadDirection.Down;
            this.dsk_gtools_actualiza.ExpandArrowButton = false;
            this.dsk_gtools_actualiza.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtools_actualiza.Image")));
            this.dsk_gtools_actualiza.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_gtools_actualiza.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtools_catsat});
            this.dsk_gtools_actualiza.Name = "dsk_gtools_actualiza";
            this.dsk_gtools_actualiza.Text = "b.Actualizar";
            this.dsk_gtools_actualiza.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_gtools_catsat
            // 
            this.dsk_gtools_catsat.Name = "dsk_gtools_catsat";
            this.dsk_gtools_catsat.Text = "b.Catálogos SAT";
            this.dsk_gtools_catsat.Click += new System.EventHandler(this.Menu_Main_Click);
            // 
            // dsk_grp_theme
            // 
            this.dsk_grp_theme.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_gtheme_2010Black,
            this.dsk_gtheme_2010Blue,
            this.dsk_gtheme_2010Silver});
            this.dsk_grp_theme.Name = "dsk_grp_theme";
            this.dsk_grp_theme.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.dsk_grp_theme.Text = "g.Theme";
            // 
            // dsk_gtheme_2010Black
            // 
            this.dsk_gtheme_2010Black.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Black.Image")));
            this.dsk_gtheme_2010Black.Name = "dsk_gtheme_2010Black";
            this.dsk_gtheme_2010Black.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Black.Tag = "Office2010Black";
            this.dsk_gtheme_2010Black.Text = "Office 2010 Black";
            this.dsk_gtheme_2010Black.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Black.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Black.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Blue
            // 
            this.dsk_gtheme_2010Blue.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Blue.Image")));
            this.dsk_gtheme_2010Blue.Name = "dsk_gtheme_2010Blue";
            this.dsk_gtheme_2010Blue.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Blue.Tag = "Office2010Blue";
            this.dsk_gtheme_2010Blue.Text = "Office 2010 Blue";
            this.dsk_gtheme_2010Blue.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Blue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Blue.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_gtheme_2010Silver
            // 
            this.dsk_gtheme_2010Silver.Image = ((System.Drawing.Image)(resources.GetObject("dsk_gtheme_2010Silver.Image")));
            this.dsk_gtheme_2010Silver.Name = "dsk_gtheme_2010Silver";
            this.dsk_gtheme_2010Silver.Padding = new System.Windows.Forms.Padding(1);
            this.dsk_gtheme_2010Silver.Tag = "Office2010Silver";
            this.dsk_gtheme_2010Silver.Text = "Office 2010 Silver";
            this.dsk_gtheme_2010Silver.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.dsk_gtheme_2010Silver.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.dsk_gtheme_2010Silver.Click += new System.EventHandler(this.Menu_UI_AplicarTema_Click);
            // 
            // dsk_grp_about
            // 
            this.dsk_grp_about.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.dsk_btn_manual,
            this.dsk_btn_about});
            this.dsk_grp_about.Name = "dsk_grp_about";
            this.dsk_grp_about.Text = "gAyuda";
            // 
            // dsk_btn_manual
            // 
            this.dsk_btn_manual.Image = global::Jaeger.UI.Properties.Resources.user_manual_30px;
            this.dsk_btn_manual.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_btn_manual.Name = "dsk_btn_manual";
            this.dsk_btn_manual.Text = "b.Manual";
            this.dsk_btn_manual.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // dsk_btn_about
            // 
            this.dsk_btn_about.Image = global::Jaeger.UI.Properties.Resources.about_30px;
            this.dsk_btn_about.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.dsk_btn_about.Name = "dsk_btn_about";
            this.dsk_btn_about.Text = "bAcercaDe";
            this.dsk_btn_about.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.dsk_btn_about.Click += new System.EventHandler(this.Menu_About_Click);
            // 
            // MainRibbonForm
            // 
            this.AllowAero = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 629);
            this.Controls.Add(this.RadDock);
            this.Controls.Add(this.BEstado);
            this.Controls.Add(this.MenuRibbonBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = null;
            this.Name = "MainRibbonForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "MainRibbonForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainRibbonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RadDock)).EndInit();
            this.RadDock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RadContainer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MenuRibbonBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadRibbonBar MenuRibbonBar;
        private Telerik.WinControls.UI.RadStatusStrip BEstado;
        private Telerik.WinControls.UI.RibbonTab adm_grp_nomina;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_gnom_empleado;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_empexp;
        private Telerik.WinControls.UI.RibbonTab TTools;
        private Telerik.WinControls.UI.Docking.RadDock RadDock;
        private Telerik.WinControls.UI.Docking.DocumentContainer RadContainer;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_empcont;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_gconcep;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_gnom_gmov;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_periodo;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gnom_recibo;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_aguina;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_config;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gconfig_param;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_avanzado;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_usuario;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_menu;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_perfil;
        private Telerik.WinControls.UI.RadRibbonBarButtonGroup adm_gnom_sgmov;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_movvac;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_movfal;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_movacu;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tisr;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tsm;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tuma;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_tsbe;
        private Telerik.WinControls.UI.RadRibbonBarGroup adm_gnom_parame;
        private Telerik.WinControls.UI.RadDropDownButtonElement adm_gnom_tablas;
        private Telerik.WinControls.Themes.VisualStudio2012LightTheme visualStudio2012LightTheme1;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_about;
        private Telerik.WinControls.UI.RadButtonElement dsk_btn_about;
        private Telerik.WinControls.UI.RadButtonElement adm_gnom_conf;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_reccon;
        private Telerik.WinControls.UI.RadMenuItem adm_gnom_recres;
        private Telerik.WinControls.UI.RadMenuItem dsk_gconfig_emisor;
        private Telerik.WinControls.UI.RadDropDownButtonElement nom_gpar_empresa;
        private Telerik.WinControls.UI.RadMenuItem nom_gpar_areas;
        private Telerik.WinControls.UI.RadMenuItem nom_gpar_deptos;
        private Telerik.WinControls.UI.RadMenuItem nom_gpar_puesto;
        private Telerik.WinControls.UI.RadButtonElement dsk_btn_manual;
        private Telerik.WinControls.UI.RadButtonElement dsk_gconfig_cert;
        private Telerik.WinControls.UI.RadButtonElement dsk_gconfig_series;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_tools;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_actualiza;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_catsat;
        private Telerik.WinControls.UI.RadDropDownButtonElement dsk_gtools_validador;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validarfc;
        private Telerik.WinControls.UI.RadMenuItem dsk_gtools_validacedula;
        private Telerik.WinControls.UI.RadRibbonBarGroup dsk_grp_theme;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Black;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Blue;
        private Telerik.WinControls.UI.RadButtonElement dsk_gtheme_2010Silver;
    }
}
