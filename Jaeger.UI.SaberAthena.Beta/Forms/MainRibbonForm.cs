﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Localization;
using Jaeger.UI.Common.Builder;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI.Forms {
    /// <summary>
    /// Clase principal de la aplicación
    /// </summary>
    public partial class MainRibbonForm : Telerik.WinControls.UI.RadRibbonForm {
        #region declaracion de controles
        protected bool IsReady = false;
        protected BackgroundWorker descargarLogo;
        #endregion

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public MainRibbonForm() {
            InitializeComponent();
        }

        #region formulario
        private void MainRibbonForm_Load(object sender, EventArgs e) {
            CheckForIllegalCrossThreadCalls = false;
            RadGridLocalizationProvider.CurrentProvider = new LocalizationProviderCustomGrid();
            RadMessageLocalizationProvider.CurrentProvider = new LocalizationProviderCustomerMessageBox();

            this.descargarLogo = new BackgroundWorker();
            this.descargarLogo.DoWork += DescargarLogo_DoWork;
            this.descargarLogo.RunWorkerCompleted += DescargarLogo_RunWorkerCompleted;
            this.descargarLogo.RunWorkerAsync();

            using (var _espera = new Common.Forms.Waiting1Form(this.SetConfiguration)) {
                _espera.Text = "Esperando respuesta del servidor ...";
                _espera.ShowDialog(this);
            }

            // todo listo?
            if (this.IsReady == false) {
                RadMessageBox.Show(this, "No se pudo cargar la configuración de la empresa.", "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Error);
                if (ConfigService.Piloto != null) {
                    if (ConfigService.Piloto.Clave.ToLower() == "anhe1") {
                        Herramientas.Configuracion.ConfigurationForm _parametros = new Herramientas.Configuracion.ConfigurationForm(null);
                        _parametros.ShowDialog(this);
                    }
                }
                this.Close();
                return;
            }

            this.BEstado.Items.Add(new ButtonEmpresaBuilder()
                .Usuario(ConfigService.Piloto.Clave)
                .Version(Application.ProductVersion.ToString())
                .Empresa(ConfigService.Synapsis.Empresa.RFC)
                .DataBase(Aplication.Nomina.Services.GeneralService.Configuration.DataBase.Database)
                .Build());

            // es modo de demostracion?
            ConfigService.IsTesting();
            this.Text = ConfigService.Titulo();
            this.MenuRibbonBar.SetDefaultTab();
            this.MenuRibbonBar.Visible = true;
        }

        private void Menu_Main_Click(object sender, EventArgs e) {
            var _item = (RadItem)sender;
            var _menuElement = ConfigService.GeMenuElement(_item.Name);

            if (_menuElement != null) {
                var localAssembly = new object() as Assembly;
                if (_menuElement.Assembly == "Jaeger.UI")
                    localAssembly = UIMenuRibbonHelper.GetAssembly("SaberAthena.Beta");
                else
                    localAssembly = UIMenuRibbonHelper.GetAssembly(_menuElement.Assembly);

                if (localAssembly != null) {
                    try {
                        Type type = localAssembly.GetType(string.Format("{0}.{1}", _menuElement.Assembly, _menuElement.Form), true);
                        this.Activar_Form(type, _menuElement);
                    } catch (Exception ex) {
                        RadMessageBox.Show(this, "Main: " + ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
                    }
                } else {
                    RadMessageBox.Show(this, "No se definio ensamblado.", "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Error);
                }
            }
        }

        private void Menu_About_Click(object sender, EventArgs e) {
            var _about = new Herramientas.Ayuda.AboutBoxForm();
            _about.ShowDialog(this);
        }

        private void Activar_Form(Type type, Domain.Base.Abstractions.UIMenuElement sender) {
            try {
                Form _form = (Form)Activator.CreateInstance(type, sender);
                if (_form.WindowState == FormWindowState.Maximized) {
                    _form.MdiParent = this;
                    _form.WindowState = FormWindowState.Maximized;
                    _form.Show();
                    this.RadDock.ActivateMdiChild(_form);
                } else {
                    _form.StartPosition = FormStartPosition.CenterParent;
                    _form.ShowDialog(this);
                }
            } catch (Exception ex) {
                RadMessageBox.Show(this, ex.Message, "Jaeger.UI", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            }
        }

        private void Menu_UI_AplicarTema_Click(object sender, EventArgs e) {
            this.Change_Theme(((RadButtonElement)sender).Tag.ToString());
        }
        #endregion

        #region metodos privados
        private void SetConfiguration() {
            // configuracion de la empresa
            Aplication.Empresa.Contracts.IEmpresaService empresa = new Aplication.Empresa.Service.ConfigurationService();
            ConfigService.Synapsis.Empresa = empresa.GetEmpresa();
            ConfigService.Synapsis.Empresa.RazonSocial = "Impostores Profesionales SA de CV";

            this.IsReady = Aplication.Nomina.Services.GeneralService.Validate();
            if (this.IsReady) {
                Aplication.Kaiju.Contracts.IProfileToService service = new Aplication.Nomina.Adapter.Services.ProfileService();
                ConfigService.Piloto = service.CreateProfile(ConfigService.Piloto);
                ConfigService.Menus = service.CreateMenus(ConfigService.Piloto.Id).ToList();

                var MenuPermissions = new Domain.Base.ValueObjects.UIMenuItemPermission(Domain.Base.ValueObjects.UIPermissionEnum.Invisible);
                MenuPermissions.Load(ConfigService.Menus);
                UIMenuUtility.SetPermission(this, ConfigService.Piloto, MenuPermissions);

                this.MenuRibbonBar.Refresh();
            }
        }

        private void DescargarLogo_DoWork(object sender, DoWorkEventArgs e) {
            string logo = ConfigService.UrlLogo();
            string archivo = ManagerPathService.JaegerPath(Domain.Base.ValueObjects.PathsEnum.Media, string.Format("logo-{0}.png", ConfigService.Synapsis.Empresa.Clave));
            Util.FileService.DownloadFile(logo, archivo);
        }

        private void DescargarLogo_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            this.Text = ConfigService.Titulo();
        }

        /// <summary>
        /// cambiar tema
        /// </summary>
        private void Change_Theme(string nameTheme) {
            ThemeResolutionService.ApplicationThemeName = nameTheme;
        }
        #endregion
    }
}
