﻿namespace Jaeger.UI.Forms {
    /// <summary>
    /// Formulario personalizado de Login
    /// </summary>
    internal class LoginForm : Login.Forms.LoginForm {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        public LoginForm(string[] args) : base(args) {
            this.Text = "EDITA Nómina Beta";
            this.LogoTipo = Properties.Resources.engineer_60px;
            this.LogoPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LogoPicture.Padding = new System.Windows.Forms.Padding(20, 20, 20, 20);
            this.LogoPicture.ClientSize = new System.Drawing.Size(120, 120);
            this.LogoPicture.Location = new System.Drawing.Point(90, 50);
            this.lblInformacion.Text = "EDITA Nómina Beta";
            this.ShowInTaskbar = true;
        }
    }
}
