﻿using System;
using System.Windows.Forms;
using Jaeger.Aplication.Base.Services;

namespace Jaeger.UI {
    internal static class Program {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main(string[] args) {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ManagerPathService.CreatePaths();
            
            // formulario para login 
            using (var login = new Forms.LoginForm(args) { ShowInTaskbar = true }) {
                login.ShowDialog();
            }

            if (ConfigService.Piloto != null && ConfigService.Synapsis != null)
                Application.Run(new Forms.MainRibbonForm());
        }
    }
}
