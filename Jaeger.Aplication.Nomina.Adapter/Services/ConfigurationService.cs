﻿using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Empresa.Entities;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    /// <summary>
    /// Servicio de configuración de la aplicación
    /// </summary>
    public class ConfigurationService : Nomina.Services.ConfigurationService, Empresa.Contracts.IConfigurationService, Nomina.Contracts.IConfigurationService {
        /// <summary>
        /// obtener o establecer repositorio de parametros de configuracion
        /// </summary>
        protected internal ISqlParametroRepository localParametrosRepository { get; protected set; }

        /// <summary>
        /// constructor
        /// </summary>
        public ConfigurationService() : base() { }

        protected override void OnLoad() {
            base.OnLoad();
            this.localParametrosRepository = new DataAccess.FB.Empresa.Repositories.SqlFbParametroRepository(GeneralService.Configuration.DataBase);
        }

        public override EmpresaGeneral GetEmpresa() {
            var local = base.GetEmpresa();
            return local;
        }
    }
}
