﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class RegistroAusenciasService : Aplication.Nomina.Services.RegistroAusenciasService, IRegistroAusenciasService {
        public RegistroAusenciasService() : base() { }

        public override void OnLoad() {
            this.registroDiasRepository = new SqlRegistroAusenciasRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.diaFestivoRepository= new SqlDiaFestivoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.empleadoRepository = new SqlEmpleadoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
