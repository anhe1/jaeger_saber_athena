﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.DataAccess.FB.Empresa.Repositories;


namespace Jaeger.Aplication.Nomina.Adapter.Services {
    /// <summary>
    /// Departamento service
    /// </summary>
    public class DepartamentoService : Nomina.Services.DepartamentoService, IDepartamentoService {
        public DepartamentoService() { }

        protected override void OnLoad() {
            this.departamentoRepository = new SqlFbDepartamentoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.areaRepository = new SqlFbAreaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
