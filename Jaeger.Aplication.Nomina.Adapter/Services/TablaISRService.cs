﻿using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class TablaISRService : ITablaISRService {
        protected ISqlTablaImpuestoSobreRentaRepository isrRepository;

        public TablaISRService() { 
            this.isrRepository = new SqlTablaImpuestoSobreRentaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public BindingList<TablaImpuestoSobreRentaModel> GetList() {
            return new BindingList<TablaImpuestoSobreRentaModel>(this.isrRepository.GetList().ToList());
        }

        public BindingList<TablaImpuestoSobreRentaModel> GetList(List<Conditional> conditionals) {
            throw new System.NotImplementedException();
        }

        public BindingList<TablaImpuestoSobreRentaModel> Save(BindingList<TablaImpuestoSobreRentaModel> tabla) {
            return this.isrRepository.Save(tabla);
        }

        public void Prueba() {
            this.isrRepository.Delete(0);
        }
    }
}
