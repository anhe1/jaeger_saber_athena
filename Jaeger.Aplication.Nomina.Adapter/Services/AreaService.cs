﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.DataAccess.FB.Empresa.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class AreaService : Nomina.Services.AreaService, Contracts.IAreaService {
        
        public AreaService() : base() { }

        protected override void OnLoad() {
            this.areaRepository = new SqlFbAreaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
