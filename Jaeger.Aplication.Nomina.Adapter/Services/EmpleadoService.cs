﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    /// <summary>
    /// servicio para control de empleados
    /// </summary>
    public class EmpleadoService : Nomina.Services.EmpleadoService, IEmpleadoService {
        /// <summary>
        /// constructor
        /// </summary>
        public EmpleadoService() : base() { }

        protected override void OnLoad() {
            this.empleadoRepository = new SqlEmpleadoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.contratoRepository = new SqlEmpleadoContratoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.cuentaBancariaRepository = new SqlEmpleadoCuentaBancariaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.conceptoNominaRepository = new SqlEmpleadoConceptoNominaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.nominaConceptoRepository = new SqlNominaConceptoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
