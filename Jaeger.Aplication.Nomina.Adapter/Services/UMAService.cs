﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Aplication.Base.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class UMAService : IUMAService {
        protected ISqlTablaUMARepository tablaUMARepository;

        public UMAService() {
            this.tablaUMARepository = new SqlTablaUMARepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public BindingList<UMAModel> GetList() {
            return new BindingList<UMAModel>(this.tablaUMARepository.GetList().ToList());
        }

        public BindingList<UMAModel> Save(BindingList<UMAModel> models) {
            for (int i = 0; i < models.Count; i++) {
                models[i] = this.tablaUMARepository.Save(models[i]);
            }
            return models;
        }
    }
}
