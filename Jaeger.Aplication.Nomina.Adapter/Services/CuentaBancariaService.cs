﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.ValueObjects;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class CuentaBancariaService : ICuentaBancariaService {
        protected ISqlEmpleadoCuentaBancariaRepository cuentaBancariaRepository;

        public CuentaBancariaService() {
            this.cuentaBancariaRepository = new SqlEmpleadoCuentaBancariaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
        public EmpleadoCuentaBancariaModel GetById(int index) {
            return this.cuentaBancariaRepository.GetById(index);
        }

        public BindingList<EmpleadoCuentaBancariaModel> GetList(bool onlyActive) {
            throw new NotImplementedException();
        }

        public EmpleadoCuentaBancariaModel Save(EmpleadoCuentaBancariaModel model) {
            return this.cuentaBancariaRepository.Save(model);
        }

        public List<CuentaBancariaTipoModel> GetTipos() {
            return ((CuentaBancariaTipoEnum[])Enum.GetValues(typeof(CuentaBancariaTipoEnum)))
                .Select(c => new CuentaBancariaTipoModel() {
                    Id = (int)c,
                    Descripcion = c.GetType().GetMember(c.ToString()).FirstOrDefault().GetCustomAttribute<DescriptionAttribute>().Description
                }).ToList();
        }

        public List<MonedaModel> GetMonedas() {
            var monedas = new List<MonedaModel>();
            monedas.Add(new MonedaModel("MXN", "Peso Mexicano"));
            monedas.Add(new MonedaModel("USD", "Dolar Americano"));
            return monedas;
        }
    }
}
