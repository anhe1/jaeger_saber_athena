﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class SalarioMinimoService : ISalarioMinimoService {
        protected ISqlTablaSalarioMinimoRepository salarioMinimoRepository;

        public SalarioMinimoService() {
            this.salarioMinimoRepository = new SqlTablaSalarioMinimoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public BindingList<SalarioMinimoModel> GetList() {
            return new BindingList<SalarioMinimoModel>(this.salarioMinimoRepository.GetList().ToList());
        }

        public BindingList<SalarioMinimoModel> Save(BindingList<SalarioMinimoModel> models) {
            for (int i = 0; i < models.Count; i++) {
                models[i] = this.salarioMinimoRepository.Save(models[i]);
            }
            return models;
        }
    }
}
