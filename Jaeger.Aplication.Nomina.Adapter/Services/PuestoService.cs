﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class PuestoService : Nomina.Services.PuestoService, IPuestoService {

        public PuestoService() : base() { }

        protected override void OnLoad() {
            this.puestoRepository = new SqlPuestoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
