﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class ConceptoService : IConceptoService {
        protected ISqlNominaConceptoRepository conceptoRepository;

        public ConceptoService() {
            this.conceptoRepository = new SqlNominaConceptoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.conceptoRepository.GetList<T1>(conditionals);
        }

        public BindingList<INominaConceptoDetailModel> GetList(bool onlyActive) {
            return new BindingList<INominaConceptoDetailModel>(this.conceptoRepository.GetList<NominaConceptoDetailModel>(new List<IConditional> { new Conditional("NMCNP_ID", "0", ConditionalTypeEnum.GreaterThan) }).ToList<INominaConceptoDetailModel>());
        }

        public INominaConceptoDetailModel Save(INominaConceptoDetailModel model) {
            return this.conceptoRepository.Save(model);
        }
    }
}
