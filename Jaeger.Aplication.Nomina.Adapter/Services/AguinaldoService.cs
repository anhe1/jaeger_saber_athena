﻿using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Empresa.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.DataAccess.FB.Nomina.Repositories;
using Jaeger.DataAccess.FB.Empresa.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class AguinaldoService : IAguinaldoService {
        protected ISqlEmpleadoRepository empleadoRepository;
        protected ISqlEmpleadoContratoRepository contratoRepository;
        protected ISqlParametroRepository configuracionRepository;
        protected ISqlTablaImpuestoSobreRentaRepository isrRepository;
        protected ISqlTablaSubsidioAlEmpleoRepository subsidioAlEmpleoRepository;
        protected ISqlTablaUMARepository umaRepository;
        protected ISqlTablaSalarioMinimoRepository salarioMinimoRepository;
        protected ISqlEmpleadoCuentaBancariaRepository cuentaBancariaRepository;

        public AguinaldoService() {
            this.empleadoRepository = new SqlEmpleadoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.contratoRepository = new SqlEmpleadoContratoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.configuracionRepository = new SqlFbParametroRepository(GeneralService.Configuration.DataBase);
            this.subsidioAlEmpleoRepository = new SqlTablaSubsidioAlEmpleoRepository(GeneralService.Configuration.DataBase);
            this.isrRepository = new SqlTablaImpuestoSobreRentaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.umaRepository = new SqlTablaUMARepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.salarioMinimoRepository = new SqlTablaSalarioMinimoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.cuentaBancariaRepository = new SqlEmpleadoCuentaBancariaRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public BindingList<IEmpleadoCalcularAguinaldo> GetAguinaldos(int year) {
            var _empleados = this.contratoRepository.GetList<ContratoDetailModel>(new List<Domain.Base.Builder.IConditional> { new Domain.Base.Entities.Conditional("ctemp_a", "1") });
            var _tablaISR = this.isrRepository.GetById(year, 30);
            var _tablaSubsidio = this.subsidioAlEmpleoRepository.GetById(year, 30);
            var _uma = this.umaRepository.GetByYear(year);
            var _response = new BindingList<IEmpleadoCalcularAguinaldo>();
            var _salarioMinimo = this.salarioMinimoRepository.GetByEjercicio(year);

            foreach (var item in _empleados) {
                var _empleado = new EmpleadoCalcularAguinaldo(item.IdEmpleado, item.Clave, item.FullName, item.SalarioDiario, _tablaISR, _tablaSubsidio, _uma.Diario);
                _empleado.FecInicioRelLaboral = item.FecInicioRelLaboral;
                _empleado.SalarioMinimoGeneral = _salarioMinimo.Valor;
                _empleado.DiasAguinaldo = 15;
                if (item.FecInicioRelLaboral.Value.Year == System.DateTime.Now.Year) {
                    _empleado.DiasLaborados = System.DateTime.Now.Subtract(_empleado.FecInicioRelLaboral.Value).Days;
                } else {
                    var _fecha = new System.DateTime(System.DateTime.Now.Year, 1, 1);
                    _empleado.DiasLaborados = System.DateTime.Now.Subtract(_fecha).Days;
                }
                _response.Add(_empleado);
            }
            return _response;
        }
    }
}
