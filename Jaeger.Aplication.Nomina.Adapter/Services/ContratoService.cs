﻿using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class ContratoService : IContratoService {
        protected ISqlEmpleadoContratoRepository contratoRepository;

        public ContratoService() {
            this.contratoRepository = new SqlEmpleadoContratoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public BindingList<IContratoModel> GetList() {
            return new BindingList<IContratoModel>(this.contratoRepository.GetList().ToList<IContratoModel>());
        }

        public BindingList<IContratoDetailModel> GetList(bool onlyActive) {
            return new BindingList<IContratoDetailModel>(this.contratoRepository.GetList<ContratoDetailModel>(new List<IConditional> { new Conditional("CTEMPC_A", "1") }).ToList<IContratoDetailModel>());
        }

        public IContratoModel GetById(int id) {
            return this.contratoRepository.GetById(id);
        }

        public bool Remove(int id) {
            return this.contratoRepository.Remove(id);
        }

        public IContratoModel Save(IContratoModel model) {
            model = this.contratoRepository.Save(model as ContratoModel);
            return model;
        }
    }
}
