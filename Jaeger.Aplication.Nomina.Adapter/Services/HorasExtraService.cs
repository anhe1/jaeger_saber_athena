﻿using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Builder;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class HorasExtraService : IHorasExtraService {
        protected internal ISqlNominaHorasExtraRepository SqlNominaHorasExtraRepository;
        public HorasExtraService() {
            this.OnLoad();
        }

        public virtual void OnLoad() {
            this.SqlNominaHorasExtraRepository = new SqlNominaHorasExtraRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public List<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            return this.SqlNominaHorasExtraRepository.GetList<T1>(conditionals).ToList();
        }

        public virtual IRegistroHoraExtraModel Save(IRegistroHoraExtraModel model) {
            return this.SqlNominaHorasExtraRepository.Save(model);
        }

        public static IHorasExtraQueryBuilder Query() {
            return new HorasExtraQueryBuilder();
        }
    }
}
