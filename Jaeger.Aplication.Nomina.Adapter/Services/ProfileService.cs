﻿using System.Collections.Generic;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Services;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    /// <summary>
    /// servicio de perfiles de sistema
    /// </summary>
    public class ProfileService : Kaiju.Services.ProfileService, Kaiju.Contracts.IProfileService {
        /// <summary>
        /// constructor
        /// </summary>
        public ProfileService() : base() { }

        protected override void OnLoad() {
            base.OnLoad();
            this._ProfileRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUIProfileRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this._UserRolRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUserRolRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this._RelacionRepository = new DataAccess.FB.Kaiju.Repositories.SqlFbUserRolRelacionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public override List<Domain.Base.Abstractions.UIMenuElement> GetMenus() {
            return this._MenuRepository.GetNominaBeta().ToList();
        }
    }
}
