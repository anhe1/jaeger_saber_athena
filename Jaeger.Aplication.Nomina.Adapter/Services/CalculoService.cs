﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Builder;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class CalculoService : ICalculoService {
        protected ISqlNominaCalRepository nominaCalRepository;
        protected ISqlNominaPeriodoRepository nominaPeriodoRepository;
        protected ISqlPeriodoConceptoRepository conceptoRepository;
        protected ISqlNominaHorasExtraRepository horasExtraRepository;
        protected ISqlRegistroAusenciasRepository registroDiasRepository;

        public CalculoService() {
            this.nominaCalRepository = new SqlNominaCalRepository(GeneralService.Configuration.DataBase);
            this.nominaPeriodoRepository = new SqlNominaPeriodoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.conceptoRepository = new SqlPeriodoConceptoRepository(GeneralService.Configuration.DataBase    );
            this.horasExtraRepository = new SqlNominaHorasExtraRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
            this.registroDiasRepository = new SqlRegistroAusenciasRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public BindingList<INominaPeriodoEmpleadoDetailModel> GetList(int idPeriodo) {
            return new BindingList<INominaPeriodoEmpleadoDetailModel>(this.nominaCalRepository.GetList(new List<IConditional> {
                new Conditional("NMCAL_NOM_ID", idPeriodo.ToString(), ConditionalTypeEnum.Equal) }).ToList<INominaPeriodoEmpleadoDetailModel>());
        }

        public INominaPeriodoDetailModel GetPeriodo(int idPeriodo) {
            var _response = this.nominaPeriodoRepository.GetList<NominaPeriodoDetailModel>(new List<IConditional> {new Conditional("NMPRD_ID", idPeriodo.ToString())}).FirstOrDefault();
            if (_response != null) {
                _response.Empleados = this.GetList(idPeriodo);
                if (_response.Empleados != null) {
                    if (_response.Empleados.Count > 0) {
                        var _conceptos = this.conceptoRepository.GetList(new List<Conditional> { new Conditional("NMEMP_NMCAL_ID", idPeriodo.ToString()) });
                        for (int i = 0; i < _response.Empleados.Count; i++) {
                            _response.Empleados[i].Conceptos = new BindingList<IPeriodoConceptoDetailModel>(_conceptos.Where(it => it.IdEmpleado == _response.Empleados[i].IdEmpleado).ToList<IPeriodoConceptoDetailModel>());
                        }
                        var horasExtra = this.horasExtraRepository.GetList<RegistroHoraExtraModel>(new List<IConditional> { new Conditional("NMHRE_NOM_ID", idPeriodo.ToString()) });
                        for (int i = 0; i < _response.Empleados.Count; i++) {
                            _response.Empleados[i].HorasExtra = new BindingList<IRegistroHoraExtraModel>(horasExtra.Where(it => it.IdEmpleado == _response.Empleados[i].IdEmpleado).ToList<IRegistroHoraExtraModel>());
                        }
                        var dias = this.registroDiasRepository.GetList<RegistroAusenciasModel>(new List<IConditional> { new Conditional("NMRD_NMPRD_ID", idPeriodo.ToString()) });
                        for (int i = 0; i < _response.Empleados.Count; i++) {
                            _response.Empleados[i].Ausencias = new BindingList<IRegistroAusenciasModel>(dias.Where(it => it.IdEmpleado == _response.Empleados[i].IdEmpleado).ToList<IRegistroAusenciasModel>());
                        }
                    }
                }
            }
            return _response;
        }

        public INominaPeriodoDetailModel GetLast() {
            var response = this.nominaPeriodoRepository.GetLast<NominaPeriodoDetailModel>().FirstOrDefault();
            return response;
        }

        public bool CalcularPeriodo(INominaPeriodoDetailModel model) {
            return this.nominaPeriodoRepository.Calcular(model) > 0;
        }

        public INominaPeriodoDetailModel Save(INominaPeriodoDetailModel currentPeriodo) {
            this.nominaPeriodoRepository.Save(currentPeriodo);
            if (currentPeriodo.Empleados != null) {
                if (currentPeriodo.Empleados.Count > 0) {
                    for (int i = 0; i < currentPeriodo.Empleados.Count; i++) {
                        if (currentPeriodo.Empleados[i].IsChange) {
                            Console.WriteLine("si guardar");
                            currentPeriodo.Empleados[i] = this.nominaCalRepository.Save(currentPeriodo.Empleados[i]);
                            if (currentPeriodo.Empleados[i].HorasExtra != null) {
                                for (int h = 0; h < currentPeriodo.Empleados[i].HorasExtra.Count; h++) {
                                    currentPeriodo.Empleados[i].HorasExtra[h] = this.horasExtraRepository.Save(currentPeriodo.Empleados[i].HorasExtra[h]);
                                }
                            }
                            if (currentPeriodo.Empleados[i].Ausencias != null) {
                                for (int j = 0; j < currentPeriodo.Empleados[i].Ausencias.Count; j++) {
                                    currentPeriodo.Empleados[i].Ausencias[j] = this.registroDiasRepository.Save(currentPeriodo.Empleados[i].Ausencias[j]);
                                }
                            }
                            currentPeriodo.Empleados[i].IsChange = false;
                        }
                    }
                }
            }
            return currentPeriodo;
        }

        public static INominaPeriodoQueryBuilder Query() {
            return new NominaPeriodoQueryBuilder();
        }
    }
}
