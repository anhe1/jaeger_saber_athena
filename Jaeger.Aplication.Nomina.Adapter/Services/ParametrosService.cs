﻿using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class ParametrosService : Nomina.Services.ParametrosService, IParametrosService {
        public ParametrosService() : base() {
        }

        protected override void OnLoad() {
            this.configuracionRepository = new SqlNominaConfiguracionRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }
    }
}
