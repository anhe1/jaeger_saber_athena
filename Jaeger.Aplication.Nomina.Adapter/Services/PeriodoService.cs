﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using Jaeger.Aplication.Base.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FB.Nomina.Repositories;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class PeriodoService : IPeriodoService {
        protected ISqlNominaPeriodoRepository periodoRepository;

        public PeriodoService() {
            this.periodoRepository = new SqlNominaPeriodoRepository(GeneralService.Configuration.DataBase, ConfigService.Piloto.Clave);
        }

        public BindingList<NominaPeriodoModel> GetList(bool onlyActive) {
            return new BindingList<NominaPeriodoModel>(this.periodoRepository.GetList().ToList());
        }

        public BindingList<NominaPeriodoModel> GetList(int month, int year) {
            var condiciones = new List<IConditional> { new Conditional("EXTRACT(YEAR FROM NMPRD_FCINI)", year.ToString()) };

            if (month > 0) {
                condiciones.Add(new Conditional("EXTRACT(MONTH FROM NMPRD_FCINI)", month.ToString()));
            }

            return new BindingList<NominaPeriodoModel>(this.periodoRepository.GetList<NominaPeriodoModel>(condiciones).ToList());
        }

        public INominaPeriodoDetailModel Save(INominaPeriodoDetailModel model) {
            var _result = this.periodoRepository.Save(model);
            if (_result != null) {
                if (_result.IdPeriodo > 0) {
                    var a = this.periodoRepository.CrearPeriodo(model);
                }
            }
            return _result;
        }
    }
}
