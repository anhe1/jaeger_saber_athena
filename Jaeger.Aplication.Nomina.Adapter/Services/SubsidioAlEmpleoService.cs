﻿using System.Linq;
using System.ComponentModel;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.DataAccess.FB.Nomina.Repositories;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Services;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public class SubsidioAlEmpleoService : ISubsidioAlEmpleoService {
        protected ISqlTablaSubsidioAlEmpleoRepository subsidioAlEmpleoRepository;

        public SubsidioAlEmpleoService() {
            this.subsidioAlEmpleoRepository = new SqlTablaSubsidioAlEmpleoRepository(GeneralService.Configuration.DataBase);
        }
        public BindingList<TablaSubsidioAlEmpleoModel> GetList() {
            return new BindingList<TablaSubsidioAlEmpleoModel>(this.subsidioAlEmpleoRepository.GetList().ToList());
        }

        public BindingList<TablaSubsidioAlEmpleoModel> Save(BindingList<TablaSubsidioAlEmpleoModel> models) {
            throw new System.NotImplementedException();
        }
    }
}
