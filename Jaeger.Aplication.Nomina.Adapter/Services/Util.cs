﻿using System;

namespace Jaeger.Aplication.Nomina.Adapter.Services {
    public static class Util {
        public static string CalcularAntiguedad(DateTime? FechaInicioRelLab, DateTime FechaFinPago, string Antiguedad) {
            string antiguedad;
            TimeSpan timeSpan;
            if ((string.IsNullOrEmpty(Antiguedad) || FechaInicioRelLab.Value == null ? false : FechaFinPago != null)) {
                try {
                    string str = "P";
                    DateTime dateTime = FechaInicioRelLab.Value;
                    DateTime dateTime1 = FechaFinPago;
                    if (!Antiguedad.Contains("W")) {
                        dateTime1 = dateTime1.AddDays(1);
                        int year = dateTime1.Year - dateTime.Year;
                        if (dateTime.AddYears(year) > dateTime1) {
                            year--;
                        }
                        dateTime = dateTime.AddYears(year);
                        int month = dateTime1.Month - dateTime.Month;
                        if (month == 0) {
                            if (dateTime.Year < dateTime1.Year) {
                                if (!(dateTime1 <= dateTime)) {
                                    month--;
                                } else {
                                    month = 12;
                                    if (year > 0) {
                                        year--;
                                        dateTime = dateTime.AddYears(-1);
                                    }
                                }
                            }
                        }
                        if (month < 0) {
                            month += 12;
                        }
                        if (dateTime.AddMonths(month) > dateTime1) {
                            month--;
                        }
                        dateTime = dateTime.AddMonths(month);
                        if ((year <= 0 ? false : month == 0)) {
                            year--;
                            month = 12;
                        }
                        timeSpan = dateTime1 - dateTime;
                        int num = Convert.ToInt16(timeSpan.Days);
                        if (year > 0) {
                            str = string.Concat(str, year.ToString(), "Y");
                        }
                        if (month > 0) {
                            str = string.Concat(str, month.ToString(), "M");
                        }
                        str = string.Concat(str, num.ToString(), "D");
                    } else {
                        timeSpan = dateTime1 - dateTime;
                        double totalDays = timeSpan.TotalDays + 1;
                        double num1 = Math.Floor(totalDays / 7);
                        str = string.Concat(str, num1.ToString(), "W");
                    }
                    Antiguedad = str;
                } catch {
                }
                antiguedad = Antiguedad;
            } else {
                antiguedad = "";
            }
            return antiguedad;
        }
    }
}
