CREATE TABLE NMCAL
(
  NMCAL_ID Integer NOT NULL,
  NMCAL_NOM_ID Integer,
  EMP_ID Integer,
  EMP_CLV Varchar(10),
  FCC Timestamp DEFAULT 'NOW',
  FIRL Date DEFAULT 'NOW',
  BPP Smallint DEFAULT 1,
  HRJR Numeric(18,4) DEFAULT 48 NOT NULL,
  HRLJ Numeric(18,4) DEFAULT 48 NOT NULL,
  HRAT Numeric(18,4) DEFAULT 0 NOT NULL,
  EFI Numeric(18,4) DEFAULT 1,
  INC Numeric(18,4) DEFAULT 0,
  AUS Numeric(18,4) DEFAULT 0,
  VAC Smallint DEFAULT 0 NOT NULL,
  INF Numeric(18,4) DEFAULT 0 NOT NULL,
  DASL Numeric(18,4) DEFAULT 0 NOT NULL,
  SD Numeric(18,4) DEFAULT 0,
  SDIP Numeric(18,4) DEFAULT 0,
  JT Numeric(18,4) DEFAULT 7,
  HD Integer DEFAULT 8,
  DP Numeric(18,4) DEFAULT 7,
  DXA Numeric(18,4) DEFAULT 365,
  DAG Numeric(18,4) DEFAULT 15,
  PV Numeric(18,4) DEFAULT .25,
  FPPP Numeric(18,4) DEFAULT .10 NOT NULL,
  FFDA Numeric(18,4) DEFAULT .13 NOT NULL,
  FVDD Numeric(18,4) DEFAULT .25 NOT NULL,
  FFXC Smallint DEFAULT 0,
  PPRD_ID Integer DEFAULT 0 NOT NULL,
  NMCAL_PRD_ID Integer,
  SM Numeric(18,4),
  UMA Numeric(18,4),
  TISR_ID Integer,
  TSBD_ID Integer,
  CONSTRAINT PK_NMCAL_ID PRIMARY KEY (NMCAL_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de periodos'  where RDB$FIELD_NAME = 'NMCAL_NOM_ID' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del catalogo de empleados'  where RDB$FIELD_NAME = 'EMP_ID' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del empleado'  where RDB$FIELD_NAME = 'EMP_CLV' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de pago'  where RDB$FIELD_NAME = 'FCC' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de alta'  where RDB$FIELD_NAME = 'FIRL' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'bandera: 1 = indica aplicar premio por puntualidad y asistencia'  where RDB$FIELD_NAME = 'BPP' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas de la jornada'  where RDB$FIELD_NAME = 'HRJR' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas de reloj'  where RDB$FIELD_NAME = 'HRLJ' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas extra autorizadas'  where RDB$FIELD_NAME = 'HRAT' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaje para el premio de producion'  where RDB$FIELD_NAME = 'EFI' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de dias de incapacidad'  where RDB$FIELD_NAME = 'INC' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de dias de ausencia'  where RDB$FIELD_NAME = 'AUS' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de vacaciones tomadas por el trabajador'  where RDB$FIELD_NAME = 'VAC' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descuento diario por pago de credito de vivienda (infonavit)'  where RDB$FIELD_NAME = 'INF' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descuento por anticipo a salarios'  where RDB$FIELD_NAME = 'DASL' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario diario'  where RDB$FIELD_NAME = 'SD' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario diario integrado personalizado'  where RDB$FIELD_NAME = 'SDIP' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'jornadas de trabajo'  where RDB$FIELD_NAME = 'JT' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas del dia'  where RDB$FIELD_NAME = 'HD' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias del periodo'  where RDB$FIELD_NAME = 'DP' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias del anio'  where RDB$FIELD_NAME = 'DXA' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de aguinaldo'  where RDB$FIELD_NAME = 'DAG' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '% prima vacacional'  where RDB$FIELD_NAME = 'PV' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaj aplicable para la percepcion de premio por puntualidad y premio por asistencia'  where RDB$FIELD_NAME = 'FPPP' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaje de vales de despensa'  where RDB$FIELD_NAME = 'FFDA' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaje aplicable para la percepcion de vales de despensa'  where RDB$FIELD_NAME = 'FVDD' and RDB$RELATION_NAME = 'NMCAL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla aplicable para produccion'  where RDB$FIELD_NAME = 'PPRD_ID' and RDB$RELATION_NAME = 'NMCAL';
ALTER TABLE NMCAL ADD SM3 COMPUTED BY (
    CAST ((SM * 3)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMCAL ADD UMA3 COMPUTED BY (
    CAST ((UMA * 3)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMCAL ADD HREXT COMPUTED BY (
    (CASE WHEN ABS(HRLJ -HRJR)>HRAT THEN HRAT 
        ELSE ABS(HRLJ-HRJR) END) 
        );
ALTER TABLE NMCAL ADD ADS COMPUTED BY (
    CAST(
        DATEDIFF(DAY FROM FIRL TO FCC)/DXA
    AS NUMERIC(18,4)
    ));
ALTER TABLE NMCAL ADD DVC COMPUTED BY (
    CAST(
        CASE WHEN (ADS < 1 ) THEN 6 
            WHEN (ADS < 2 ) THEN 8 
            WHEN (ADS < 3 ) THEN 10 
            WHEN (ADS < 4 ) THEN 12 
            WHEN (ADS < 9 ) THEN 14 
            WHEN (ADS < 14 ) THEN 16 
            WHEN (ADS < 19 ) THEN 18 
            WHEN (ADS < 24 ) THEN 20 
            WHEN (ADS < 29 ) THEN 22 
            WHEN (ADS < 34 ) THEN 24 
    END AS NUMERIC(18,4)
    ));
ALTER TABLE NMCAL ADD FSDI COMPUTED BY (
    CAST(
        (DXA + DAG + (DVC * PV))/DXA AS NUMERIC(18,4) 
    ));
ALTER TABLE NMCAL ADD SDI COMPUTED BY ( 
    CAST( 
        CASE WHEN (SDIP = 0) THEN
                (SD * FSDI)
            ELSE
                SDIP
            END
    AS NUMERIC(18,2) 
    ));
ALTER TABLE NMCAL ADD PSLD COMPUTED BY (
    CAST(
        CAST(DP - (TRUNC((DP / CAST(JT AS NUMERIC(18,4)))) * (INC + AUS) * TRUNC(DP / CAST(JT AS NUMERIC(18, 4)))) AS NUMERIC(18, 4)) * SD 
    AS NUMERIC(18,4) 
    ));
ALTER TABLE NMCAL ADD PFDA COMPUTED BY (
    CAST(
        CASE WHEN CEIL(INC + AUS) > 0 THEN 
            ((DP-(CAST((DP/JT) AS NUMERIC(18,4)) *(INC+AUS))) * SD *FFDA) 
        ELSE (DP * SD) * FFDA END 
    AS NUMERIC(18,4)
    ));
ALTER TABLE NMCAL ADD POPA COMPUTED BY (
    CAST(
        CASE WHEN (BPP=1 ) THEN 
            (SDI * FPPP) * DP
        ELSE 0 
        END 
    AS NUMERIC(18,4) 
    ));
ALTER TABLE NMCAL ADD PPPP COMPUTED BY (
    CAST(
        CASE WHEN (BPP = 1) THEN 
            (SDI * FPPP) * DP
        ELSE 0 
        END 
        AS NUMERIC(18,4)
        ));
ALTER TABLE NMCAL ADD PVDD COMPUTED BY (
    CAST(
        CASE WHEN PSLD > 0 THEN 
            (SM*(DP-(DP/JT* CEIL(INC+AUS))) * FVDD) 
        ELSE 0 
        END 
        AS NUMERIC(18,4)
    ));
ALTER TABLE NMCAL ADD PHET COMPUTED BY ( 
    CAST(
        (CASE WHEN PSLD > 0 THEN 
            (CASE WHEN HREXT > 9 THEN SD/8*2*9 
                ELSE 
                    (CASE WHEN HREXT < 1 THEN 0 
                        ELSE ((SD/8*2)*HRAT) END)
                END)
        ELSE 0 END) 
    AS NUMERIC(18,4)));
ALTER TABLE NMCAL ADD PPVC COMPUTED BY (
    CAST (
        CASE WHEN ((VAC > 1) AND ((SD*VAC) > (SM*DAG))) 
            THEN (SM * DAG) 
            ELSE (VAC * SD) * 1 END
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMCAL ADD PPPV COMPUTED BY (
    CAST (
        CASE WHEN ((VAC > 1) AND ((SD*VAC) > (SM*DAG))) 
            THEN (SM * DAG)
            ELSE ((VAC * SD) * PV) END
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMCAL ADD PCMS COMPUTED BY (
    CAST(
        CASE WHEN PSLD > 0 THEN 
            CASE WHEN (HRJR+HREXT)<(HRJR+9) OR (HRJR+HREXT)=(HRJR+9) THEN 0
                ELSE ((HRJR+HREXT)-(HRJR+9))*(SD/8*2) END
            ELSE 0 END
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMCAL ADD PGRV COMPUTED BY (
    CAST(
        (PSLD + PCMS)
        AS NUMERIC(18, 4)
));
ALTER TABLE NMCAL ADD DINF COMPUTED BY (
    CAST (
        (CASE WHEN (JT > 0)
            THEN ((DP/JT) * (JT-INC-AUS)) * INF
            ELSE 0 END)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMCAL ADD DFDA COMPUTED BY (
    CAST(
        CASE WHEN CEIL(INC + AUS) > 0 THEN ((DP-(CEIL(DP/JT)*(INC+AUS)))*SD*FFDA) 
            ELSE (DP * SD) * FFDA END 
    AS NUMERIC(18,4)
    ));
ALTER TABLE NMCAL ADD EXC_E COMPUTED BY (
        CAST(
            CASE WHEN (FFXC = 0) THEN
                CASE WHEN (SDI > (SM3) AND (INC+AUS)> 0) THEN
                    (SDI-SM3)*((JT-CEIL((INC+AUS)/JT)*JT)*.0040)
                ELSE
                    CASE WHEN (SDI>(SM3) AND (INC+AUS)=0) THEN
                        (SDI-(SM3)) * DP * (.0040)
                    ELSE 0 END
                END 
            ELSE
                CASE WHEN (SDI > (UMA3) AND (INC+AUS)> 0) THEN
                    (SDI-UMA3)*((JT-CEIL((INC+AUS)/JT)*JT)*.0040)
                ELSE
                    CASE WHEN (SDI>(UMA3) AND (INC+AUS)=0) THEN
                        (SDI-(UMA3)) * DP * (.0040)
                    ELSE 0 END
                END 
            END AS Numeric(18,4)
        ) 
    );
ALTER TABLE NMCAL ADD PD_E COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*(JT-CEIL((INC+AUS)/JT)*JT)*0.0025)
            ELSE
                (SDI*DP*0.0025) END
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD GMP_E COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*(JT-(CEIL(INC+AUS)/JT)*JT)*0.00375)
            ELSE
                (SDI*DP*0.00375) END
            AS NUMERIC(18,4)
        ) 
    );
ALTER TABLE NMCAL ADD IV_E COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                SDI*(JT-(CEIL(INC+AUS)/JT)*JT)*0.00625
            ELSE
                (SDI*DP*0.00625) END
            AS NUMERIC(18,4)
        ) 
    );
ALTER TABLE NMCAL ADD CESA_E COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*(JT-(CEIL(INC+AUS)/JT)*JT)*0.01125)
            ELSE
                (SDI*DP*0.01125) END
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD R_T_P COMPUTED BY (
CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*(JT-CEIL((INC+AUS)/JT )*JT)*0.025984)
            ELSE
                (SDI*DP*0.025984)
            END
            AS NUMERIC(18,4)
        ) 
    );
ALTER TABLE NMCAL ADD C_F_P COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                ((SM)*(JT-CAST((INC+AUS)/JT AS NUMERIC(18,4))*JT)*0.204)
            ELSE
                ((SM)*(DP*0.204))
            END
            AS NUMERIC(18,4)
        ) 
    );
ALTER TABLE NMCAL ADD EXC_P COMPUTED BY (
    CAST (
        CASE WHEN (SDI > SM3 AND (INC + AUS) > 0) THEN
            (SDI-SM3)*((JT - CEIL(INC + AUS)/JT * JT) * 0.011)
        ELSE
            CASE WHEN (SDI>SM3 AND (INC + AUS)=0) THEN
                (SDI - SM3) * DP * (0.011)
            ELSE
                0
            END
        END
        AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD PD_P COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                SDI * (JT-CEIL((INC + AUS)/JT) * JT) * 0.007
            ELSE
                (SDI * DP * 0.007)
            END
            AS NUMERIC(18,4)
        ) 
    );
ALTER TABLE NMCAL ADD GMP_P COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*CEIL(JT-((INC+AUS)/JT)*JT)*0.0105)
            ELSE
                (SDI*DP*0.0105)
            END
            AS NUMERIC(18,4)
        ) 
    );
ALTER TABLE NMCAL ADD IV_P COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*(JT - CAST(((INC+AUS)/JT) AS NUMERIC(18,4)) *JT)*0.0175)
            ELSE
                (SDI*DP*0.0175)
            END
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD GPS_P COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                SDI * (JT - CAST(((INC + AUS)/JT) AS NUMERIC(18,4)) * JT) * 0.01
            ELSE
                (SDI * DP * 0.01)
            END
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD CESA_P COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*(JT-CAST((INC+AUS)/JT AS NUMERIC(18,4)) * JT)*0.0315)
            ELSE
                (SDI * DP * 0.0315)
            END
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD SAR_P COMPUTED BY (
        CAST (
            CASE WHEN ((INC > 0) OR (AUS >0)) THEN
                (SDI*(JT-CAST((INC+AUS)/JT AS NUMERIC(18,4))*JT)*0.02)
            ELSE
                (SDI*DP*0.02)
            END
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD IMSS_P COMPUTED BY (
        CAST (
            (R_T_P + C_F_P + EXC_P + PD_P + GMP_P + IV_P + GPS_P + CESA_P + SAR_P) 
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD IMSS_E COMPUTED BY (
        CAST (
            (EXC_E + PD_E + GMP_E + IV_E + CESA_E) 
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD DSES COMPUTED BY (
        CAST (
            (EXC_E + PD_E + GMP_E + IV_E + CESA_E) 
            AS NUMERIC(18,4)
        )
    );
ALTER TABLE NMCAL ADD P003 COMPUTED BY ( 
    CAST(
        (CASE WHEN PSLD > 0 THEN 
            (CASE WHEN HREXT > 9 THEN ((SD/HD)*2)*9
                ELSE 
                    (CASE WHEN HREXT < 1 THEN 0 
                        ELSE ((SD/HD*2)*HRAT) END)
                END)
        ELSE 0 END) 
    AS NUMERIC(18,4)));
ALTER TABLE NMCAL ADD P005 COMPUTED BY (
    CAST(
        CASE WHEN PSLD > 0 THEN 
            CASE WHEN (HRJR+HREXT)<(HRJR+9) OR (HRJR+HREXT)=(HRJR+9) THEN 0
                ELSE ((HRJR+HREXT)-(HRJR+9))*(SD/HD*3) END
            ELSE 0 END
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMCAL ADD DISR COMPUTED BY (
    CAST (
        (SELECT (((PGRV) - ISRC_LI) * (ISRC_EXC / 100)) + ISRC_CUOTA 
         FROM ISR, ISRC 
         WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
             AND ISR_ID = TISR_ID
             AND (PGRV) >= ISRC_LI 
             AND (PGRV) <= ISRC_LS) AS NUMERIC(18,4)
        ));
ALTER TABLE NMCAL ADD PSBE COMPUTED BY (
    CAST(
        (SELECT SBSD_CNTDD FROM SBSD 
        LEFT JOIN ISR ON SBSD_ISR_ID = ISR_ID 
        WHERE (PGRV >= SBSD_PARA 
            AND PGRV <= SBSD_HASTA) 
            AND ISR_ID = TSBD_ID)
        AS NUMERIC(18, 4)
    ));
CREATE INDEX IDX_NMCAL_EMP_ID ON NMCAL (EMP_ID);
CREATE INDEX IDX_NMCAL_NOM_ID ON NMCAL (NMCAL_NOM_ID);
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMCAL TO  SYSDBA WITH GRANT OPTION;

