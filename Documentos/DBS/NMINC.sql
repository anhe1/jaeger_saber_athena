CREATE TABLE NMINC
(
  NMCAL_ID Integer NOT NULL,
  NMTP_ID Integer,
  NMCAL_NOM_ID Integer,
  EMP_ID Integer,
  EMP_CLV Varchar(10),
  FCC Timestamp DEFAULT 'NOW',
  FIRL Date DEFAULT 'NOW',
  BPP Smallint DEFAULT 1,
  HRJR Numeric(18,4) DEFAULT 48 NOT NULL,
  HRLJ Numeric(18,4) DEFAULT 48 NOT NULL,
  HRAT Numeric(18,4) DEFAULT 0 NOT NULL,
  EFI Numeric(18,4) DEFAULT 1,
  INC Numeric(18,4) DEFAULT 0,
  AUS Numeric(18,4) DEFAULT 0,
  VAC Smallint DEFAULT 0 NOT NULL,
  INF Numeric(18,4) DEFAULT 0 NOT NULL,
  DASL Numeric(18,4) DEFAULT 0 NOT NULL,
  SD Numeric(18,4) DEFAULT 0,
  SDIP Numeric(18,4) DEFAULT 0,
  JT Numeric(18,4) DEFAULT 7,
  HD Integer DEFAULT 8,
  DP Numeric(18,4) DEFAULT 7,
  DXA Numeric(18,4) DEFAULT 365,
  DAG Numeric(18,4) DEFAULT 15,
  PV Numeric(18,4) DEFAULT .25,
  FPPP Numeric(18,4) DEFAULT .10 NOT NULL,
  FFDA Numeric(18,4) DEFAULT .13 NOT NULL,
  FVDD Numeric(18,4) DEFAULT .25 NOT NULL,
  FFXC Smallint DEFAULT 0,
  PPRD_ID Integer DEFAULT 0 NOT NULL,
  NMCAL_PRD_ID Integer,
  SM Numeric(18,4),
  UMA Numeric(18,4),
  TISR_ID Integer,
  TSBD_ID Integer,
  TPERC Numeric(18,4),
  TPGRV Numeric(18,4),
  TPEXE Numeric(18,4),
  TDEDU Numeric(18,4),
  CONSTRAINT PK_NMINC_ID PRIMARY KEY (NMCAL_ID)
);

UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de nomina'  where RDB$FIELD_NAME = 'NMTP_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de periodos'  where RDB$FIELD_NAME = 'NMCAL_NOM_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del catalogo de empleados'  where RDB$FIELD_NAME = 'EMP_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del empleado'  where RDB$FIELD_NAME = 'EMP_CLV' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de pago'  where RDB$FIELD_NAME = 'FCC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de alta'  where RDB$FIELD_NAME = 'FIRL' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'bandera: 1 = indica aplicar premio por puntualidad y asistencia'  where RDB$FIELD_NAME = 'BPP' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas de la jornada'  where RDB$FIELD_NAME = 'HRJR' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas de reloj'  where RDB$FIELD_NAME = 'HRLJ' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas extra autorizadas'  where RDB$FIELD_NAME = 'HRAT' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaje para el premio de producion'  where RDB$FIELD_NAME = 'EFI' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de dias de incapacidad'  where RDB$FIELD_NAME = 'INC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de dias de ausencia'  where RDB$FIELD_NAME = 'AUS' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de vacaciones tomadas por el trabajador'  where RDB$FIELD_NAME = 'VAC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descuento diario por pago de credito de vivienda (infonavit)'  where RDB$FIELD_NAME = 'INF' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descuento por anticipo a salarios'  where RDB$FIELD_NAME = 'DASL' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario diario'  where RDB$FIELD_NAME = 'SD' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario diario integrado personalizado'  where RDB$FIELD_NAME = 'SDIP' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'jornadas de trabajo'  where RDB$FIELD_NAME = 'JT' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas del dia'  where RDB$FIELD_NAME = 'HD' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias del periodo'  where RDB$FIELD_NAME = 'DP' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias del anio'  where RDB$FIELD_NAME = 'DXA' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de aguinaldo'  where RDB$FIELD_NAME = 'DAG' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '% prima vacacional'  where RDB$FIELD_NAME = 'PV' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaj aplicable para la percepcion de premio por puntualidad y premio por asistencia'  where RDB$FIELD_NAME = 'FPPP' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaje de vales de despensa'  where RDB$FIELD_NAME = 'FFDA' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'porcentaje aplicable para la percepcion de vales de despensa'  where RDB$FIELD_NAME = 'FVDD' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla aplicable para produccion'  where RDB$FIELD_NAME = 'PPRD_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de produccion asociada al empleado'  where RDB$FIELD_NAME = 'NMCAL_PRD_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario minimo vigente (SM)'  where RDB$FIELD_NAME = 'SM' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'unidad de medida actualizada vigente (UMA)'  where RDB$FIELD_NAME = 'UMA' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla ISR'  where RDB$FIELD_NAME = 'TISR_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla subsidio al empleo asociada al periodo'  where RDB$FIELD_NAME = 'TSBD_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de percepciones'  where RDB$FIELD_NAME = 'TPERC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de percepciones gravadas'  where RDB$FIELD_NAME = 'TPGRV' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de deducciones exentas'  where RDB$FIELD_NAME = 'TPEXE' and RDB$RELATION_NAME = 'NMINC';
ALTER TABLE NMINC ADD SM3 COMPUTED BY (
    CAST ((SM * 3)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMINC ADD UMA3 COMPUTED BY (
    CAST ((UMA * 3)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMINC ADD HREXT COMPUTED BY (
    (CASE WHEN ABS(HRLJ -HRJR)>HRAT THEN HRAT 
        ELSE ABS(HRLJ-HRJR) END) 
        );
ALTER TABLE NMINC ADD ADS COMPUTED BY (
    CAST(
        DATEDIFF(DAY FROM FIRL TO FCC)/DXA
    AS NUMERIC(18,4)
    ));
ALTER TABLE NMINC ADD DVC COMPUTED BY (
    CAST(
        CASE WHEN (ADS < 1 ) THEN 6 
            WHEN (ADS < 2 ) THEN 8 
            WHEN (ADS < 3 ) THEN 10 
            WHEN (ADS < 4 ) THEN 12 
            WHEN (ADS < 9 ) THEN 14 
            WHEN (ADS < 14 ) THEN 16 
            WHEN (ADS < 19 ) THEN 18 
            WHEN (ADS < 24 ) THEN 20 
            WHEN (ADS < 29 ) THEN 22 
            WHEN (ADS < 34 ) THEN 24 
    END AS NUMERIC(18,4)
    ));
ALTER TABLE NMINC ADD FSDI COMPUTED BY (
    CAST(
        (DXA + DAG + (DVC * PV))/DXA AS NUMERIC(18,4) 
    ));
ALTER TABLE NMINC ADD SDI COMPUTED BY ( 
    CAST( 
        CASE WHEN (SDIP = 0) THEN
                (SD * FSDI)
            ELSE
                SDIP
            END
    AS NUMERIC(18,2) 
    ));
ALTER TABLE NMINC ADD ISPT COMPUTED BY (
    CAST (
        (SELECT (((TPGRV) - ISRC_LI) * (ISRC_EXC / 100)) + ISRC_CUOTA 
         FROM ISR, ISRC 
         WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
             AND ISR_ID = TISR_ID
             AND (TPGRV) >= ISRC_LI 
             AND (TPGRV) <= ISRC_LS) AS NUMERIC(18,4)
        ));
ALTER TABLE NMINC ADD SUBE COMPUTED BY (
    CAST(
        (SELECT SBSD_CNTDD FROM SBSD 
        LEFT JOIN ISR ON SBSD_ISR_ID = ISR_ID 
        WHERE (TPGRV >= SBSD_PARA 
            AND TPGRV <= SBSD_HASTA) 
            AND ISR_ID = TSBD_ID)
        AS NUMERIC(18, 4)
    ));
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMINC TO  SYSDBA WITH GRANT OPTION;

