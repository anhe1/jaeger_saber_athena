/********************* ROLES **********************/

/********************* UDFS ***********************/

/****************** GENERATORS ********************/

CREATE GENERATOR GEN_CONF_ID;
CREATE GENERATOR GEN_CTDPTP_ID;
CREATE GENERATOR GEN_CTDPT_ID;
CREATE GENERATOR GEN_CTEMPB_ID;
CREATE GENERATOR GEN_CTEMPC_ID;
CREATE GENERATOR GEN_CTEMPD_ID;
CREATE GENERATOR GEN_CTEMP_ID;
CREATE GENERATOR GEN_ISRC_ID;
CREATE GENERATOR GEN_ISR_ID;
CREATE GENERATOR GEN_NMCNF_ID;
CREATE GENERATOR GEN_NMCNP_ID;
CREATE GENERATOR GEN_NMEMP_ID;
CREATE GENERATOR GEN_NMHRE_ID;
CREATE GENERATOR GEN_NMINC_ID;
CREATE GENERATOR GEN_NMPRD_ID;
CREATE GENERATOR GEN_RLSUSR_ID;
CREATE GENERATOR GEN_RLUSRL_ID;
CREATE GENERATOR GEN_SBSD_ID;
CREATE GENERATOR GEN_UIPERFIL_ID;
CREATE GENERATOR GEN_UMA_ID;
/******************** DOMAINS *********************/

/******************* PROCEDURES ******************/

SET TERM ^ ;
CREATE PROCEDURE CALCULAR_ISR (
    IMPORTE Numeric(18,4),
    PERIODO Integer,
    EJERCICIO Integer )
RETURNS (
    BGRAVABLE Numeric(18,4),
    LINFERIOR Numeric(18,4),
    EXCLINFERIOR Decimal(18,4),
    PORCENTAJE Numeric(18,4),
    CUOTAFIJA Numeric(18,4),
    IMPSINCUOTA Numeric(18,4),
    IMPUESTO Numeric(18,4) )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE CALCULAR_SBE (
    IMPORTE Numeric(18,4),
    PERIODO Smallint,
    EJERCICIO Integer )
RETURNS (
    BGRAVABLE Numeric(18,4),
    PARA Numeric(18,4),
    HASTA Numeric(18,4),
    SUBSIDIO Numeric(18,4) )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE NME_CAL (
    NOM_IDX Integer )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE NME_CALPD (
    NOM_IDX Integer,
    NMCNP_ID Integer,
    CTEMP_ID Integer,
    FORMULA Varchar(200) )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE NME_CALPD_INC (
    NOM_IDX Integer,
    CTEMP_ID Integer,
    NMCNP_ID Integer,
    NMCNP_TP_ID Integer,
    FORMULA Varchar(200),
    NMCNP_ISR_BF Integer,
    NMCNP_ISR_FORM Varchar(200) )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE NME_CAL_INC (
    NOM_IDX Integer )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE NME_CREAR_NMINC (
    NMCNF_ANIO Integer,
    NMCAL_NOM_ID Integer,
    NMTP_ID Integer )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE NME_DEL_INC (
    NOM_IDX Integer )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

SET TERM ^ ;
CREATE PROCEDURE NM_UPDATE_CONF (
    NMCNF_ANIO Integer,
    NMCAL_NOM_ID Integer )
AS
BEGIN SUSPEND; END^
SET TERM ; ^

/******************** TABLES **********************/

CREATE TABLE CLDF
(
  FECHA Date NOT NULL,
  DESCRIPCION Varchar(255),
  CONSTRAINT PK_CLDF_ID PRIMARY KEY (FECHA)
);
CREATE TABLE CONF
(
  CONF_KEY Integer DEFAULT 0 NOT NULL,
  CONF_GRP Integer DEFAULT 0 NOT NULL,
  CONF_DATA Varchar(500),
  CONF_NOTA Varchar(50) DEFAULT '',
  CONF_FM Timestamp DEFAULT CURRENT_TIMESTAMP       NOT NULL,
  CONSTRAINT PK_CONF_1 PRIMARY KEY (CONF_KEY,CONF_GRP)
);
CREATE TABLE CTDPT
(
  CTDPT_ID Integer NOT NULL,
  CTDPT_NOM Varchar(40),
  CTDPT_CTCTBL Varchar(30),
  CTDPT_CLSF Varchar(10),
  CTDPT_CCST Varchar(3),
  CTDPT_FN Timestamp NOT NULL,
  CTDPT_USR_N Varchar(10),
  CTDPT_FM Timestamp,
  CTDPT_USR_M Varchar(10),
  CONSTRAINT PK_CTDPT_ID PRIMARY KEY (CTDPT_ID)
);
CREATE TABLE CTDPTP
(
  CTDPTP_ID Integer NOT NULL,
  CTDPTP_A Smallint DEFAULT 1,
  CTDPTP_NOM Varchar(100),
  CTDPTP_SLDSG Numeric(18,4),
  CTDPTP_SLDMX Numeric(18,4),
  CTDPTP_RSGPST Smallint,
  CTDPTP_FN Timestamp DEFAULT 'Now' NOT NULL,
  CTDPTP_USR_N Varchar(10),
  CTDPTP_FM Timestamp,
  CTDPTP_USR_M Varchar(10),
  CONSTRAINT PK_CTDPTP_ID PRIMARY KEY (CTDPTP_ID)
);
CREATE TABLE CTEMP
(
  CTEMP_ID Integer NOT NULL,
  CTEMP_A Smallint NOT NULL,
  CTEMP_DRCTR_ID Integer DEFAULT NULL,
  CTEMP_CLV Varchar(10) NOT NULL,
  CTEMP_RFC Varchar(14) DEFAULT NULL,
  CTEMP_NSS Varchar(16) DEFAULT NULL,
  CTEMP_UMF Varchar(16) DEFAULT NULL,
  CTEMP_FONAC Varchar(16) DEFAULT NULL,
  CTEMP_CURP Varchar(20) DEFAULT NULL,
  CTEMP_AFORE Varchar(20) DEFAULT NULL,
  CTEMP_ENTFED Integer DEFAULT NULL,
  CTEMP_LGNAC Varchar(32) DEFAULT NULL,
  CTEMP_NAC Varchar(32) DEFAULT NULL,
  CTEMP_NOM Varchar(80) NOT NULL,
  CTEMP_PAPE Varchar(80) NOT NULL,
  CTEMP_SAPE Varchar(80) NOT NULL,
  CTEMP_MAIL Varchar(80) DEFAULT NULL,
  CTEMP_SITIO Varchar(80) DEFAULT NULL,
  CTEMP_FNAC Date DEFAULT NULL,
  CTEMP_SEXO Integer DEFAULT NULL,
  CTEMP_ECVL Integer DEFAULT NULL,
  CTEMP_TLFN Varchar(20) DEFAULT NULL,
  CTEMP_TLFN2 Varchar(20) DEFAULT NULL,
  CTEMP_DOMFIS Varchar(5),
  CTEMP_RGFSC Varchar(3),
  CTEMP_IMG Blob sub_type 0 DEFAULT NULL,
  CTEMP_VSAT Smallint,
  CTEMP_FN Timestamp NOT NULL,
  CTEMP_USR_N Varchar(10) DEFAULT NULL,
  CTEMP_FM Timestamp DEFAULT NULL,
  CTEMP_USR_M Varchar(10) DEFAULT NULL,
  PRIMARY KEY (CTEMP_ID),
  CONSTRAINT UNQ_CTEMP_RFC UNIQUE (CTEMP_RFC)
);
CREATE TABLE CTEMPB
(
  CTEMPB_ID Numeric(10,0) NOT NULL,
  CTEMPB_A Smallint NOT NULL,
  CTEMPB_CTEMP_ID Numeric(10,0) NOT NULL,
  CTEMPB_VRFCD Smallint NOT NULL,
  CTEMPB_TIPO Smallint NOT NULL,
  CTEMPB_CRGMX Numeric(14,2),
  CTEMPB_CLV Varchar(4),
  CTEMPB_REFNUM Varchar(7),
  CTEMPB_MND Varchar(10),
  CTEMPB_SCRSL Varchar(20),
  CTEMPB_REFALF Varchar(20),
  CTEMPB_NMCTA Varchar(20),
  CTEMPB_CLABE Varchar(25),
  CTEMPB_NMCRT Varchar(20),
  CTEMPB_BANCO Varchar(30),
  CTEMPB_FN Timestamp DEFAULT 'Now' NOT NULL,
  CTEMPB_USR_N Varchar(10),
  CTEMPB_FM Timestamp,
  CTEMPB_USR_M Varchar(10),
  CTEMPB_BENEF Varchar(120),
  CTEMPB_BRFC Varchar(15),
  CONSTRAINT PK_CTEMPB_ID PRIMARY KEY (CTEMPB_ID)
);
CREATE TABLE CTEMPC
(
  CTEMPC_ID Integer NOT NULL,
  CTEMPC_A Smallint NOT NULL,
  CTEMPC_CTEMP_ID Integer,
  CTEMPC_DEPTOID Integer DEFAULT NULL,
  CTEMPC_PSTID Integer DEFAULT NULL,
  CTEMPC_PPRD_ID Integer NOT NULL,
  CTEMPC_TPREG Varchar(2) DEFAULT NULL,
  CTEMPC_RSGPST Varchar(3) DEFAULT NULL,
  CTEMPC_PRDDPG Varchar(3) DEFAULT NULL,
  CTEMPC_MTDPG Integer DEFAULT NULL,
  CTEMPC_TPCNTR Varchar(3) DEFAULT NULL,
  CTEMPC_TPJRN Varchar(3) DEFAULT NULL,
  CTEMPC_CLVBNC Varchar(3) DEFAULT NULL,
  CTEMPC_CTABAN Varchar(16) DEFAULT NULL,
  CTEMPC_CLABE Varchar(18) DEFAULT NULL,
  CTEMPC_CTAVAL Varchar(18) DEFAULT NULL,
  CTEMPC_SCRSL Varchar(16) DEFAULT NULL,
  CTEMPC_TPCTA Bigint DEFAULT NULL,
  CTEMPC_RGP Varchar(20) DEFAULT NULL,
  CTEMPC_DEPTO Varchar(32) DEFAULT NULL,
  CTEMPC_PUESTO Varchar(32) DEFAULT NULL,
  CTEMPC_FCHREL Date NOT NULL,
  CTEMPC_FCHTERLAB Date DEFAULT NULL,
  CTEMPC_BSCOT Integer DEFAULT NULL,
  CTEMPC_SB Numeric(10,4) DEFAULT NULL,
  CTEMPC_SBC Numeric(10,4) DEFAULT NULL,
  CTEMPC_SDI Numeric(10,4) DEFAULT NULL,
  CTEMPC_SD Numeric(10,4) DEFAULT NULL,
  CTEMPC_JRNTR Integer DEFAULT NULL,
  CTEMPC_JRNHRS Numeric(10,0) DEFAULT NULL,
  CTEMPC_FN Timestamp NOT NULL,
  CTEMPC_USR_N Varchar(10) DEFAULT NULL,
  CTEMPC_FM Timestamp DEFAULT NULL,
  CTEMPC_USR_M Varchar(10) DEFAULT NULL,
  PRIMARY KEY (CTEMPC_ID)
);
CREATE TABLE CTEMPD
(
  CTEMPD_ID Numeric(10,0) NOT NULL,
  CTEMPD_A Integer NOT NULL,
  CTEMPD_CTEMP_ID Numeric(10,0) NOT NULL,
  CTEMPD_NMCNP_ID Numeric(10,0) NOT NULL,
  CTEMPD_TBL_ID Numeric(10,0),
  CTEMPD_IMPFJ Decimal(11,4),
  CONSTRAINT PK_CTEMPD_ID PRIMARY KEY (CTEMPD_ID)
);
CREATE TABLE ISR
(
  ISR_ID Integer NOT NULL,
  ISR_ANIO Integer NOT NULL,
  ISR_PRD Integer,
  PRIMARY KEY (ISR_ID)
);
CREATE TABLE ISRC
(
  ISRC_ID Integer NOT NULL,
  ISRC_ISR_ID Integer NOT NULL,
  ISRC_LI Numeric(18,4),
  ISRC_LS Numeric(18,4),
  ISRC_CUOTA Numeric(18,4),
  ISRC_EXC Numeric(18,4),
  PRIMARY KEY (ISRC_ID)
);
CREATE TABLE NMAGU
(
  NOM_ID Integer,
  EMP_ID Integer,
  EMP_CLV Varchar(10),
  INC Smallint DEFAULT 0,
  SD Numeric(18,4) DEFAULT 0,
  FCC Timestamp DEFAULT 'NOW',
  FCA Timestamp DEFAULT 'NOW',
  DXA Numeric(18,4) DEFAULT 365,
  DXM Numeric(18,4) DEFAULT 30.4,
  DAG Smallint NOT NULL
);
CREATE TABLE NMCNF
(
  NMCNF_ID Integer NOT NULL,
  NMCNF_ANIO Integer NOT NULL,
  NMCNF_TISR_ID Integer,
  NMCNF_TSBD_ID Smallint,
  NMCNF_PRD_ID Smallint,
  NMCNF_DXA Smallint NOT NULL,
  NMCNF_DXS Smallint,
  NMCNF_HD Integer NOT NULL,
  NMCNF_DP Numeric(18,4),
  NMCNF_SMV Numeric(18,4),
  NMCNF_UMAV Numeric(18,4),
  NMCNF_USR_N Varchar(10),
  NMCNF_FN Timestamp NOT NULL,
  NMCNF_USR_M Varchar(10),
  NMCNF_FM Timestamp,
  NMCNF_EEX_E Numeric(18,6),
  NMCNF_PED_E Numeric(18,6),
  NMCNF_PYB_E Numeric(18,6),
  NMCNF_IYV_E Numeric(18,6),
  NMCNF_CYV_E Numeric(18,6),
  NMCNF_ECF_P Numeric(18,6),
  NMCNF_EEX_P Numeric(18,6),
  NMCNF_PED_P Numeric(18,6),
  NMCNF_PYB_P Numeric(18,6),
  NMCNF_IYV_P Numeric(18,6),
  NMCNF_RDT_P Numeric(18,6),
  NMCNF_GPS_P Numeric(18,6),
  NMCNF_SAR_P Numeric(18,6),
  NMCNF_CYV_P Numeric(18,6),
  NMCNF_INF_P Numeric(18,6),
  CONSTRAINT PK_NMCNF_ID PRIMARY KEY (NMCNF_ID)
);
CREATE TABLE NMCNP
(
  NMCNP_ID Numeric(10,0) NOT NULL,
  NMCNP_A Integer DEFAULT 1 NOT NULL,
  NMCNP_TP_ID Smallint DEFAULT 1 NOT NULL,
  NMCNP_TNM_ID Smallint DEFAULT 1 NOT NULL,
  NMCNP_APL_ID Smallint DEFAULT 1 NOT NULL,
  NMCNP_TBL_ID Integer,
  NMCNP_FRMPG_ID Smallint,
  NMCNP_VIS Integer DEFAULT 1 NOT NULL,
  NMCNP_CLV Varchar(4),
  NMCNP_CLVSAT Varchar(3),
  NMCNP_NOM Varchar(100),
  NMCNP_FORM Varchar(150),
  NMCNP_ISR_BF Smallint,
  NMCNP_ISR_FORM Varchar(150),
  NMCNP_IMS_BF Smallint,
  NMCNP_IMS_FORM Varchar(150),
  NMCNP_USR_N Varchar(10),
  NMCNP_FN Timestamp DEFAULT 'Now' NOT NULL,
  NMCNP_USR_M Varchar(10),
  NMCNP_FM Timestamp,
  CONSTRAINT PK_NMCNP_ID PRIMARY KEY (NMCNP_ID)
);
CREATE TABLE NMEMP
(
  NMEMP_ID Integer NOT NULL,
  NMEMP_CTEMP_ID Integer,
  NMEMP_NMCNP_ID Integer,
  NMEMP_CNP_TP_ID Smallint,
  NMEMP_NMCNP_ISR_BF Smallint,
  NMEMP_NMCAL_ID Integer,
  NMEMP_TOTAL Numeric(18,4) NOT NULL,
  NMEMP_TPGRA Numeric(18,4),
  NMEMP_TPEXE Numeric(18,4),
  NMEMP_TDEDU Numeric(18,4),
  CONSTRAINT PK_NMEMP_ID PRIMARY KEY (NMEMP_ID)
);
CREATE TABLE NMHRE
(
  NMHRE_ID Integer NOT NULL,
  NMHRE_A Smallint,
  NMHRE_NMCNP_ID Integer,
  NMHRE_EMP_ID Integer,
  NMHRE_NOM_ID Integer,
  NMHRE_TP Integer,
  NMHRE_FEC Date,
  NMHRE_CANT Smallint,
  NMHRE_NOTA Varchar(150),
  NMHRE_USR_N Varchar(10),
  NMHRE_FN Timestamp DEFAULT 'Now' NOT NULL,
  NMHRE_USR_M Varchar(10),
  NMHRE_FM Timestamp,
  CONSTRAINT PK_NMHRE_ID PRIMARY KEY (NMHRE_ID)
);
CREATE TABLE NMINC
(
  NMCAL_ID Integer NOT NULL,
  NMTP_ID Integer,
  NMCAL_NOM_ID Integer,
  EMP_ID Integer,
  EMP_CLV Varchar(10),
  FCC Timestamp DEFAULT 'NOW',
  FIRL Date DEFAULT 'NOW',
  BPP Smallint DEFAULT 1,
  HRJR Numeric(18,4) DEFAULT 48 NOT NULL,
  HRLJ Numeric(18,4) DEFAULT 48 NOT NULL,
  HRAT Numeric(18,4) DEFAULT 0 NOT NULL,
  INC Numeric(18,4) DEFAULT 0,
  AUS Numeric(18,4) DEFAULT 0,
  VAC Smallint DEFAULT 0 NOT NULL,
  INF Numeric(18,4) DEFAULT 0 NOT NULL,
  DASL Numeric(18,4) DEFAULT 0 NOT NULL,
  SD Numeric(18,4) DEFAULT 0,
  SDIP Numeric(18,4) DEFAULT 0,
  SBC Numeric(18,8),
  JT Numeric(18,4) DEFAULT 7,
  HD Integer DEFAULT 8,
  DP Numeric(18,4) DEFAULT 7,
  DXA Numeric(18,4) DEFAULT 365,
  DAG Numeric(18,4) DEFAULT 15,
  PV Numeric(18,4) DEFAULT .25,
  FFXC Smallint DEFAULT 0,
  SM Numeric(18,4),
  UMA Numeric(18,4),
  TISR_ID Integer,
  TSBD_ID Integer,
  FEEX_E Numeric(18,6),
  FPED_E Numeric(18,6),
  FPYB_E Numeric(18,6),
  FIYV_E Numeric(18,6),
  FCYV_E Numeric(18,6),
  FECF_P Numeric(18,6),
  FEEX_P Numeric(18,6),
  FPED_P Numeric(18,6),
  FPYB_P Numeric(18,6),
  FIYV_P Numeric(18,6),
  FRDT_P Numeric(18,6),
  FGPS_P Numeric(18,6),
  FSAR_P Numeric(18,6),
  FCYV_P Numeric(18,6),
  FINF_P Numeric(18,6),
  CONSTRAINT PK_NMINC_ID PRIMARY KEY (NMCAL_ID)
);
CREATE TABLE NMPRD
(
  NMPRD_ID Integer NOT NULL,
  NMPRD_A Integer NOT NULL,
  NMPRD_STTS_ID Numeric(10,0) NOT NULL,
  NMPRD_NOM Varchar(65),
  NMPRD_FCINI Timestamp,
  NMPRD_FCFIN Timestamp,
  NMPRD_FCPGO Timestamp,
  NMPRD_FCAUT Timestamp,
  NMPRD_DSPRD Numeric(10,0) NOT NULL,
  NMPRD_TPPRD Numeric(10,0) NOT NULL,
  NMPRD_TP_ID Integer NOT NULL,
  NMPRD_TPER Decimal(10,0),
  NMPRD_TDED Decimal(10,4),
  NMPRD_TFDA Decimal(10,4),
  NMPRD_TVDD Decimal(10,4),
  NMPRD_USR_A Varchar(10),
  NMPRD_USR_C Varchar(10),
  NMPRD_USR_N Varchar(10),
  NMPRD_FN Timestamp,
  NMPRD_USR_M Varchar(10),
  NMPRD_FM Timestamp,
  CONSTRAINT PK_NMPRD_ID PRIMARY KEY (NMPRD_ID)
);
CREATE TABLE NMRD
(
  NMRD_NMCNP_ID Integer DEFAULT 0 NOT NULL,
  NMRD_EMP_ID Integer DEFAULT 0 NOT NULL,
  NMRD_NMPRD_ID Integer DEFAULT 0 NOT NULL,
  NMRD_CTTP_ID Integer DEFAULT 0 NOT NULL,
  NMRD_FEC Date,
  NMRD_NOTA Varchar(150),
  NMRD_USR_N Varchar(10),
  NMRD_FN Timestamp DEFAULT 'Now' NOT NULL,
  CONSTRAINT PK_NMRD_ID PRIMARY KEY (NMRD_NMCNP_ID,NMRD_EMP_ID,NMRD_NMPRD_ID,NMRD_CTTP_ID)
);
CREATE TABLE RLSUSR
(
  RLSUSR_ID Smallint NOT NULL,
  RLSUSR_A Smallint DEFAULT 1 NOT NULL,
  RLSUSR_M Smallint DEFAULT 0 NOT NULL,
  RLSUSR_CLV Varchar(100),
  RLSUSR_NMBR Varchar(100),
  RLSUSR_FN Timestamp NOT NULL,
  RLSUSR_USR_N Varchar(50),
  RLSUSR_FM Timestamp,
  RLSUSR_USR_M Varchar(50),
  CONSTRAINT PK_RLSUSR PRIMARY KEY (RLSUSR_ID)
);
CREATE TABLE RLUSRL
(
  RLUSRL_ID Integer NOT NULL,
  RLUSRL_A Smallint NOT NULL,
  RLUSRL_USR_ID Integer,
  RLUSRL_RLSUSR_ID Integer,
  CONSTRAINT RLUSRL_ID_PK PRIMARY KEY (RLUSRL_ID)
);
CREATE TABLE SBSD
(
  SBSD_ID Integer NOT NULL,
  SBSD_ISR_ID Integer NOT NULL,
  SBSD_PARA Numeric(18,4),
  SBSD_HASTA Numeric(18,4),
  SBSD_CNTDD Numeric(18,4),
  PRIMARY KEY (SBSD_ID)
);
CREATE TABLE SM
(
  SM_ANIO Integer NOT NULL,
  SM_SALARIO Numeric(18,4)
);
CREATE TABLE UIPERFIL
(
  UIPERFIL_ID Integer NOT NULL,
  UIPERFIL_A Smallint NOT NULL,
  UIPERFIL_UIMENU_ID Integer,
  UIPERFIL_RLSUSR_ID Integer,
  UIPERFIL_ACTION Varchar(10) DEFAULT 0000000000,
  UIPERFIL_KEY Varchar(25),
  UIPERFIL_FN Timestamp NOT NULL,
  UIPERFIL_USR_N Varchar(10),
  UIPERFIL_FM Timestamp,
  UIPERFIL_USR_M Varchar(10),
  CONSTRAINT UIPERFIL_PK PRIMARY KEY (UIPERFIL_ID)
);
CREATE TABLE UMA
(
  UMA_ID Integer NOT NULL,
  UMA_ANIO Integer NOT NULL,
  UMA_DIARIO Numeric(18,4),
  UMA_MENSUAL Numeric(18,4),
  UMA_ANUAL Numeric(18,4),
  PRIMARY KEY (UMA_ID)
);
/********************* VIEWS **********************/

/******************* EXCEPTIONS *******************/

/******************** TRIGGERS ********************/

SET TERM ^ ;
CREATE TRIGGER CTDPTP_BI FOR CTDPTP ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 17122021 1415
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTDPTP_ID IS NULL) THEN
    NEW.CTDPTP_ID = GEN_ID(GEN_CTDPTP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTDPTP_ID, 0);
    if (tmp < new.CTDPTP_ID) then
      tmp = GEN_ID(GEN_CTDPTP_ID, new.CTDPTP_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER CTDPT_BI FOR CTDPT ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 17122021 1415
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTDPT_ID IS NULL) THEN
    NEW.CTDPT_ID = GEN_ID(GEN_CTDPT_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTDPT_ID, 0);
    if (tmp < new.CTDPT_ID) then
      tmp = GEN_ID(GEN_CTDPT_ID, new.CTDPT_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER CTEMPB_BI FOR CTEMPB ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 17122021 1415
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTEMPB_ID IS NULL) THEN
    NEW.CTEMPB_ID = GEN_ID(GEN_CTEMPB_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTEMPB_ID, 0);
    if (tmp < new.CTEMPB_ID) then
      tmp = GEN_ID(GEN_CTEMPB_ID, new.CTEMPB_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER CTEMPC_BI FOR CTEMPC ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 28112021 0026
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTEMPC_ID IS NULL) THEN
    NEW.CTEMPC_ID = GEN_ID(GEN_CTEMPC_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTEMPC_ID, 0);
    if (tmp < new.CTEMPC_ID) then
      tmp = GEN_ID(GEN_CTEMPC_ID, new.CTEMPC_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER CTEMPD_BI FOR CTEMPD ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 040120222313
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTEMPD_ID IS NULL) THEN
    NEW.CTEMPD_ID = GEN_ID(GEN_CTEMPD_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTEMPD_ID, 0);
    if (tmp < new.CTEMPD_ID) then
      tmp = GEN_ID(GEN_CTEMPD_ID, new.CTEMPD_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER CTEMP_BI FOR CTEMP ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 28112021 0026
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.CTEMP_ID IS NULL) THEN
    NEW.CTEMP_ID = GEN_ID(GEN_CTEMP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_CTEMP_ID, 0);
    if (tmp < new.CTEMP_ID) then
      tmp = GEN_ID(GEN_CTEMP_ID, new.CTEMP_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER ISRC_BI FOR ISRC ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  IF (NEW.ISRC_ID IS NULL) THEN
    NEW.ISRC_ID = GEN_ID(GEN_ISRC_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_ISRC_ID, 0);
    if (tmp < new.ISRC_ID) then
      tmp = GEN_ID(GEN_ISRC_ID, new.ISRC_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER ISR_BI FOR ISR ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  IF (NEW.ISR_ID IS NULL) THEN
    NEW.ISR_ID = GEN_ID(GEN_ISR_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_ISR_ID, 0);
    if (tmp < new.ISR_ID) then
      tmp = GEN_ID(GEN_ISR_ID, new.ISR_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER NMCNF_BI FOR NMCNF ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 06012022 1228
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------ */
  IF (NEW.NMCNF_ID IS NULL) THEN
    NEW.NMCNF_ID = GEN_ID(GEN_NMCNF_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_NMCNF_ID, 0);
    if (tmp < new.NMCNF_ID) then
      tmp = GEN_ID(GEN_NMCNF_ID, new.NMCNF_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER NMCNP_BI FOR NMCNP ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  IF (NEW.NMCNP_ID IS NULL) THEN
    NEW.NMCNP_ID = GEN_ID(GEN_NMCNP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_NMCNP_ID, 0);
    if (tmp < new.NMCNP_ID) then
      tmp = GEN_ID(GEN_NMCNP_ID, new.NMCNP_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER NMEMP_BI FOR NMEMP ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  IF (NEW.NMEMP_ID IS NULL) THEN
    NEW.NMEMP_ID = GEN_ID(GEN_NMEMP_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_NMEMP_ID, 0);
    if (tmp < new.NMEMP_ID) then
      tmp = GEN_ID(GEN_NMEMP_ID, new.NMEMP_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER NMHRE_BI FOR NMHRE ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1 12122023 2054
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.NMHRE_ID IS NULL) THEN
    NEW.NMHRE_ID = GEN_ID(GEN_NMHRE_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_NMHRE_ID, 0);
    if (tmp < new.NMHRE_ID) then
      tmp = GEN_ID(GEN_NMHRE_ID, new.NMHRE_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER NMINC_BI FOR NMINC ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  IF (NEW.NMCAL_ID IS NULL) THEN
    NEW.NMCAL_ID = GEN_ID(GEN_NMINC_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_NMINC_ID, 0);
    if (tmp < new.NMCAL_ID) then
      tmp = GEN_ID(GEN_NMINC_ID, new.NMCAL_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER NMPRD_BI FOR NMPRD ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 040120222313
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------ */
  IF (NEW.NMPRD_ID IS NULL) THEN
    NEW.NMPRD_ID = GEN_ID(GEN_NMPRD_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_NMPRD_ID, 0);
    if (tmp < new.NMPRD_ID) then
      tmp = GEN_ID(GEN_NMPRD_ID, new.NMPRD_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER RLSUSR_BI FOR RLSUSR ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* **************************************************************************************************************
    Develop: ANHE1-29/09/2021 16:55
    Purpose: Disparador para indice incremental 
************************************************************************************************************** */
  IF (NEW.RLSUSR_ID IS NULL) THEN
    NEW.RLSUSR_ID = GEN_ID(GEN_RLSUSR_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RLSUSR_ID, 0);
    if (tmp < new.RLSUSR_ID) then
      tmp = GEN_ID(GEN_RLSUSR_ID, new.RLSUSR_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER RLUSRL_BI FOR RLUSRL ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1-29/09/2021 16:54
    Purpose: Disparador para indice incremental 
------------------------------------------------------------------------------------------------------------- */
  IF (NEW.RLUSRL_ID IS NULL) THEN
    NEW.RLUSRL_ID = GEN_ID(GEN_RLUSRL_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_RLUSRL_ID, 0);
    if (tmp < new.RLUSRL_ID) then
      tmp = GEN_ID(GEN_RLUSRL_ID, new.RLUSRL_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER SBSD_BI FOR SBSD ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  IF (NEW.SBSD_ID IS NULL) THEN
    NEW.SBSD_ID = GEN_ID(GEN_SBSD_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_SBSD_ID, 0);
    if (tmp < new.SBSD_ID) then
      tmp = GEN_ID(GEN_SBSD_ID, new.SBSD_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER UIPERFIL_BI FOR UIPERFIL ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
/* **************************************************************************************************************
    Develop: ANHE1-29/09/2021 16:54
    Purpose: Disparador para indice incremental 
************************************************************************************************************** */
  IF (NEW.UIPERFIL_ID IS NULL) THEN
    NEW.UIPERFIL_ID = GEN_ID(GEN_UIPERFIL_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_UIPERFIL_ID, 0);
    if (tmp < new.UIPERFIL_ID) then
      tmp = GEN_ID(GEN_UIPERFIL_ID, new.UIPERFIL_ID-tmp);
  END
END^
SET TERM ; ^
SET TERM ^ ;
CREATE TRIGGER UMA_BI FOR UMA ACTIVE
BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  IF (NEW.UMA_ID IS NULL) THEN
    NEW.UMA_ID = GEN_ID(GEN_UMA_ID, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_UMA_ID, 0);
    if (tmp < new.UMA_ID) then
      tmp = GEN_ID(GEN_UMA_ID, new.UMA_ID-tmp);
  END
END^
SET TERM ; ^

SET TERM ^ ;
ALTER PROCEDURE CALCULAR_ISR (
    IMPORTE Numeric(18,4),
    PERIODO Integer,
    EJERCICIO Integer )
RETURNS (
    BGRAVABLE Numeric(18,4),
    LINFERIOR Numeric(18,4),
    EXCLINFERIOR Decimal(18,4),
    PORCENTAJE Numeric(18,4),
    CUOTAFIJA Numeric(18,4),
    IMPSINCUOTA Numeric(18,4),
    IMPUESTO Numeric(18,4) )
AS
DECLARE VARIABLE LIMITE_INFERIOR NUMERIC(18, 4);
DECLARE VARIABLE LIMITE_SUPERIOR NUMERIC(18, 4);
DECLARE VARIABLE CUOTA_FIJA NUMERIC(18, 4);
DECLARE VARIABLE EXCEDENTE_LIMITE_INFERIOR NUMERIC(18, 4);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
  Purpose: datos del calculo de ISR
  Develop: ANHE 151220210333
------------------------------------------------------------------------------------------------------------- */
    SELECT ISRC_LI, ISRC_LS, ISRC_CUOTA, ISRC_EXC
    FROM ISR, ISRC
    WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
        AND ISR_ANIO = :EJERCICIO
        AND ISR_PRD = :PERIODO
        AND (:IMPORTE) >= ISRC_LI 
        AND (:IMPORTE) <= ISRC_LS
    INTO :LIMITE_INFERIOR, :LIMITE_SUPERIOR, :CUOTA_FIJA, :EXCEDENTE_LIMITE_INFERIOR;
    
    LINFERIOR = :LIMITE_INFERIOR;
    EXCLINFERIOR = IMPORTE - :LIMITE_INFERIOR;
    CUOTAFIJA = CUOTA_FIJA;
    PORCENTAJE = EXCEDENTE_LIMITE_INFERIOR / 100;
    IMPSINCUOTA = EXCLINFERIOR * PORCENTAJE;
    IMPUESTO = IMPSINCUOTA + CUOTAFIJA;
    BGRAVABLE = IMPORTE;
    SUSPEND;
    
END^
SET TERM ; ^


SET TERM ^ ;
ALTER PROCEDURE CALCULAR_SBE (
    IMPORTE Numeric(18,4),
    PERIODO Smallint,
    EJERCICIO Integer )
RETURNS (
    BGRAVABLE Numeric(18,4),
    PARA Numeric(18,4),
    HASTA Numeric(18,4),
    SUBSIDIO Numeric(18,4) )
AS
DECLARE VARIABLE IMPORTE_SUBSIDIO NUMERIC(18, 4);
BEGIN
/* -------------------------------------------------------------------------------------------------------------
  Purpose: datos del calculo de SUBSIDIO AL EMPLEO
  Develop: ANHE 151220210333
------------------------------------------------------------------------------------------------------------- */
    SELECT SBSD_CNTDD, SBSD_PARA, SBSD_HASTA
    FROM SBSD 
    LEFT JOIN ISR ON SBSD_ISR_ID = ISR_ID 
    WHERE (:IMPORTE >= SBSD_PARA 
        AND :IMPORTE <= SBSD_HASTA) 
        AND ISR_ANIO = :EJERCICIO
        AND ISR_PRD = :PERIODO
    INTO :IMPORTE_SUBSIDIO, :PARA, :HASTA;

    IF (:PARA IS NULL OR :PARA = 0) THEN
        BEGIN
            PARA = 0;
        END
    
    IF (:HASTA IS NULL OR :HASTA = 0) THEN
        BEGIN
            HASTA = 0;
        END

    IF (:IMPORTE_SUBSIDIO IS NULL OR IMPORTE_SUBSIDIO = 0) THEN
        BEGIN
            IMPORTE_SUBSIDIO = 0;
        END

    BGRAVABLE = :IMPORTE;
    SUBSIDIO = :IMPORTE_SUBSIDIO;
    SUSPEND;
END^
SET TERM ; ^


SET TERM ^ ;
ALTER PROCEDURE NME_CAL (
    NOM_IDX Integer )
AS
DECLARE VARIABLE CTEMP_ID integer;
DECLARE VARIABLE NMCAL_ID integer;
DECLARE VARIABLE NMCNP_ID integer;
DECLARE VARIABLE FORMULA VARCHAR(200);
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 09012022 0119
    Purpose: para calcular periodo de nomina
------------------------------------------------------------------------------------------------ */
  FOR
    SELECT NMEMP_CTEMP_ID, NMEMP_NMCAL_ID, NMEMP_NMCNP_ID, NMCNP_FORM
    FROM NMEMP, NMCNP
    WHERE NMEMP_NMCNP_ID = NMCNP_ID AND NMEMP_NMCAL_ID = :NOM_IDX
    ORDER BY NMEMP_CTEMP_ID, NMEMP_NMCNP_ID ASC
    INTO :CTEMP_ID, :NMCAL_ID, :NMCNP_ID, :FORMULA 
  DO
    begin
        execute procedure NME_CALPD(:NOM_IDX, :NMCNP_ID, :CTEMP_ID, :FORMULA);
    end
END^
SET TERM ; ^

UPDATE RDB$PROCEDURES set
  RDB$DESCRIPTION = 'Nomina: calcular periodo de nomina'
  where RDB$PROCEDURE_NAME = 'NME_CAL';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice de la nomina'
  where RDB$PARAMETER_NAME = 'NOM_IDX' AND RDB$PROCEDURE_NAME = 'NME_CAL';

SET TERM ^ ;
ALTER PROCEDURE NME_CALPD (
    NOM_IDX Integer,
    NMCNP_ID Integer,
    CTEMP_ID Integer,
    FORMULA Varchar(200) )
AS
DECLARE VARIABLE VAL NUMERIC(18,4);
DECLARE VARIABLE SQL VARCHAR(1000);
DECLARE VARIABLE IDX INTEGER;
DECLARE VARIABLE IDX2 INTEGER;
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 09012022 0030
    Purpose: procedimiento para el calculo de los conceptos de nomina
------------------------------------------------------------------------------------------------ */
    -- comprobar formula valida
  IF (:FORMULA <> '' AND :FORMULA IS NOT NULL) THEN
    BEGIN
        -- seleccionamos el registro de empleado de la tabla temporal nomcal
        SQL = 'Select NMCAL_ID, cast(' || :FORMULA || ' as numeric(18,4)) From NMCAL Where NMCAL_NOM_ID = ' || :NOM_IDX || ' AND EMP_ID = ' || :CTEMP_ID;
        -- ejecutar y recorrer el resultado
        For 
            EXECUTE STATEMENT SQL
            Into :IDX, :VAL -- obtener el resultado
        Do
            BEGIN
                -- actualizar tabla de resultados
                SQL = 'UPDATE NMEMP SET NMEMP_RESULT =' || :VAL;
                SQL = SQL || ' Where NMEMP_NMCAL_ID = ' || :NOM_IDX  || ' AND NMEMP_NMCNP_ID = ' || :NMCNP_ID || ' AND NMEMP_CTEMP_ID = ' || :CTEMP_ID;

                EXECUTE STATEMENT SQL;
                SUSPEND;
            END
    END
END^
SET TERM ; ^

UPDATE RDB$PROCEDURES set
  RDB$DESCRIPTION = 'Nomina: calcular concepto de nomina asociados al empleado (Percepcion o Deduccion)'
  where RDB$PROCEDURE_NAME = 'NME_CALPD';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice de nomina'
  where RDB$PARAMETER_NAME = 'NOM_IDX' AND RDB$PROCEDURE_NAME = 'NME_CALPD';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice del concepto (percepcion o deduccion)'
  where RDB$PARAMETER_NAME = 'NMCNP_ID' AND RDB$PROCEDURE_NAME = 'NME_CALPD';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice del empleado'
  where RDB$PARAMETER_NAME = 'CTEMP_ID' AND RDB$PROCEDURE_NAME = 'NME_CALPD';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'formula de la percepcion o deduccion'
  where RDB$PARAMETER_NAME = 'FORMULA' AND RDB$PROCEDURE_NAME = 'NME_CALPD';

SET TERM ^ ;
ALTER PROCEDURE NME_CALPD_INC (
    NOM_IDX Integer,
    CTEMP_ID Integer,
    NMCNP_ID Integer,
    NMCNP_TP_ID Integer,
    FORMULA Varchar(200),
    NMCNP_ISR_BF Integer,
    NMCNP_ISR_FORM Varchar(200) )
AS
DECLARE VARIABLE TOTAL NUMERIC(18,4);
DECLARE VARIABLE TGRAV NUMERIC(18,4);
DECLARE VARIABLE SQL VARCHAR(1000);
DECLARE VARIABLE IDX INTEGER;
DECLARE VARIABLE IDX2 INTEGER;
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 09012022 0030
    Purpose: procedimiento para el calculo de los conceptos de nomina
------------------------------------------------------------------------------------------------ */
    -- comprobar formula valida
  IF (:FORMULA <> '' AND :FORMULA IS NOT NULL) THEN
    BEGIN
        -- seleccionamos el registro de empleado de la tabla temporal nomcal
        SQL = 'Select NMCAL_ID, cast(' || :FORMULA || ' as numeric(18,4)), 0 From NMINC Where NMCAL_NOM_ID = ' || :NOM_IDX || ' AND EMP_ID = ' || :CTEMP_ID;
        
        -- si la formular es valida para ISR
        IF (:NMCNP_ISR_FORM <> '' AND :NMCNP_ISR_FORM IS NOT NULL) THEN
            BEGIN
                SQL = 'Select NMCAL_ID, cast(' || :FORMULA || ' as numeric(18,4)), cast(' || NMCNP_ISR_FORM || ' as numeric(18,4)) From NMINC Where NMCAL_NOM_ID = ' || :NOM_IDX || ' AND EMP_ID = ' || :CTEMP_ID;
            END 
        -- ejecutar y recorrer el resultado
        For 
            EXECUTE STATEMENT SQL
            Into :IDX, :TOTAL, :TGRAV -- obtener el resultado
        Do
            BEGIN
                IF (TOTAL IS NULL) THEN
                    BEGIN 
                        TOTAL = 0;
                    END
                IF (TGRAV IS NULL) THEN 
                    BEGIN 
                        TGRAV = 0;
                    END 
                -- actualizar tabla de resultados
                SQL = 'UPDATE NMEMP SET NMEMP_TOTAL =' || :TOTAL;
                -- cuando es una percepcion
                IF (:NMCNP_TP_ID = 1) THEN 
                    BEGIN 
                        -- gravable
                        IF (:NMCNP_ISR_BF = 1 AND :TGRAV IS NOT NULL) THEN
                            BEGIN
                                SQL = SQL || ' ,NMEMP_TPGRA = ' || :TOTAL;
                            END
                        -- cuando grava parcialmente
                        IF (:NMCNP_ISR_BF = 2 AND TGRAV IS NOT NULL) THEN
                            BEGIN
                                SQL = SQL || ' ,NMEMP_TPGRA = ' || :TGRAV || ' ,NMEMP_TPEXE = ' || (:TOTAL - :TGRAV);
                            END
                        -- cuando exenta
                        IF (:NMCNP_ISR_BF = 3 AND :TGRAV IS NOT NULL) THEN
                            BEGIN
                                SQL = SQL || ' ,NMEMP_TPEXE = ' || :TOTAL;
                            END
                        -- cuando exenta parcialmente
                        IF (:NMCNP_ISR_BF = 4 AND :TGRAV IS NOT NULL) THEN
                            BEGIN
                                SQL = SQL || ' ,NMEMP_TPEXE = ' || :TGRAV;
                            END
                    END 
                    
                -- cuando es una deduccion entonces actualizamos el campo de NMEMP_TDEDU
                IF (:NMCNP_TP_ID = 2) THEN 
                    BEGIN 
                        SQL = SQL || ',NMEMP_TDEDU = ' || :TOTAL;
                    END 
                
                SQL = SQL || ' Where NMEMP_NMCAL_ID = ' || :NOM_IDX  || ' AND NMEMP_NMCNP_ID = ' || :NMCNP_ID || ' AND NMEMP_CTEMP_ID = ' || :CTEMP_ID;

                EXECUTE STATEMENT SQL;
                SUSPEND;
            END
    END
END^
SET TERM ; ^

UPDATE RDB$PROCEDURES set
  RDB$DESCRIPTION = 'Nomina: calcular concepto de nomina asociados al empleado (Percepcion o Deduccion)'
  where RDB$PROCEDURE_NAME = 'NME_CALPD_INC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice de nomina'
  where RDB$PARAMETER_NAME = 'NOM_IDX' AND RDB$PROCEDURE_NAME = 'NME_CALPD_INC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice del empleado'
  where RDB$PARAMETER_NAME = 'CTEMP_ID' AND RDB$PROCEDURE_NAME = 'NME_CALPD_INC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice del concepto (percepcion o deduccion)'
  where RDB$PARAMETER_NAME = 'NMCNP_ID' AND RDB$PROCEDURE_NAME = 'NME_CALPD_INC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'formula de la percepcion o deduccion'
  where RDB$PARAMETER_NAME = 'FORMULA' AND RDB$PROCEDURE_NAME = 'NME_CALPD_INC';

SET TERM ^ ;
ALTER PROCEDURE NME_CAL_INC (
    NOM_IDX Integer )
AS
DECLARE VARIABLE CTEMP_ID integer;
DECLARE VARIABLE NMCAL_ID integer;
DECLARE VARIABLE NMCNP_ID integer;
DECLARE VARIABLE NMCNP_TP_ID integer;
DECLARE VARIABLE NMCNP_ISR_BF integer;
DECLARE VARIABLE FORMULA VARCHAR(200);
DECLARE VARIABLE FORMULA2 VARCHAR(200);
DECLARE VARIABLE BGRAVABLE NUMERIC(18, 4);
DECLARE VARIABLE BEXENTO NUMERIC(18, 4);
DECLARE VARIABLE BTOTAL NUMERIC(18, 4);
DECLARE VARIABLE DEDUCCION NUMERIC(18, 4);
DECLARE VARIABLE SQL VARCHAR(200);
DECLARE VARIABLE SQLR VARCHAR(2000);
DECLARE VARIABLE IMPFIJO integer;
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 09012022 0119
    Purpose: para calcular periodo de nomina
------------------------------------------------------------------------------------------------ */
    --- CALCULAR PERCEPCIONES
  FOR
    SELECT NMEMP_CTEMP_ID, NMEMP_NMCAL_ID, NMEMP_NMCNP_ID, NMCNP_FORM, NMEMP_NMCNP_ISR_BF, NMCNP_ISR_FORM, NMCNP_TP_ID
    FROM NMEMP, NMCNP
    WHERE NMEMP_NMCNP_ID = NMCNP_ID AND NMEMP_NMCAL_ID = :NOM_IDX AND NMCNP_TP_ID = 1
    ORDER BY NMEMP_CTEMP_ID, NMEMP_NMCNP_ID ASC
    INTO :CTEMP_ID, :NMCAL_ID, :NMCNP_ID, :FORMULA , :NMCNP_ISR_BF, :FORMULA2, :NMCNP_TP_ID
  DO
    begin
        BEGIN
            execute procedure NME_CALPD_INC(:NOM_IDX, :CTEMP_ID, :NMCNP_ID, :NMCNP_TP_ID, :FORMULA, :NMCNP_ISR_BF, :FORMULA2);
        END
    end
    --- CALCULAR DEDUCCIONES
  FOR
    SELECT NMEMP_CTEMP_ID, NMEMP_NMCAL_ID, NMEMP_NMCNP_ID, NMCNP_FORM, NMEMP_NMCNP_ISR_BF, NMCNP_ISR_FORM, NMCNP_TP_ID, CTEMPD.CTEMPD_IMPFJ
    FROM NMEMP, NMCNP
    LEFT JOIN CTEMPD ON CTEMPD.CTEMPD_NMCNP_ID = NMCNP.NMCNP_ID AND CTEMPD.CTEMPD_A = 1
    WHERE NMEMP_NMCNP_ID = NMCNP_ID AND NMEMP_NMCAL_ID = :NOM_IDX AND NMCNP_TP_ID = 2
    ORDER BY NMEMP_CTEMP_ID, NMEMP_NMCNP_ID ASC
    INTO :CTEMP_ID, :NMCAL_ID, :NMCNP_ID, :FORMULA , :NMCNP_ISR_BF, :FORMULA2, :NMCNP_TP_ID, :IMPFIJO
  DO
    begin
        BEGIN
            IF (:IMPFIJO IS NOT NULL AND :IMPFIJO > 0) THEN
                BEGIN
                    FORMULA = IMPFIJO;
                END
            execute procedure NME_CALPD_INC(:NOM_IDX, :CTEMP_ID, :NMCNP_ID, :NMCNP_TP_ID, :FORMULA, :NMCNP_ISR_BF, :FORMULA2);
        END
    end
END^
SET TERM ; ^


SET TERM ^ ;
ALTER PROCEDURE NME_CREAR_NMINC (
    NMCNF_ANIO Integer,
    NMCAL_NOM_ID Integer,
    NMTP_ID Integer )
AS
DECLARE VARIABLE CNF_SMV Numeric(18,4); 
DECLARE VARIABLE CNF_UMAV Numeric(18,4);
DECLARE VARIABLE CNF_EEX_E Numeric(18,6);
DECLARE VARIABLE CNF_PED_E Numeric(18,6);
DECLARE VARIABLE CNF_PYB_E Numeric(18,6);
DECLARE VARIABLE CNF_IYV_E Numeric(18,6);
DECLARE VARIABLE CNF_CYV_E Numeric(18,6);
DECLARE VARIABLE CNF_ECF_P Numeric(18,6);
DECLARE VARIABLE CNF_EEX_P Numeric(18,6);
DECLARE VARIABLE CNF_PED_P Numeric(18,6);
DECLARE VARIABLE CNF_PYB_P Numeric(18,6);
DECLARE VARIABLE CNF_IYV_P Numeric(18,6);
DECLARE VARIABLE CNF_RDT_P Numeric(18,6);
DECLARE VARIABLE CNF_GPS_P Numeric(18,6);
DECLARE VARIABLE CNF_SAR_P Numeric(18,6);
DECLARE VARIABLE CNF_CYV_P Numeric(18,6);
DECLARE VARIABLE CNF_INF_P Numeric(18,6);
DECLARE VARIABLE CNF_TISR_ID integer;
DECLARE VARIABLE CNF_TSBD_ID integer;
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1-15/06/2022 21:03
    Purpose: creacion de la tabla temporal de trabajo
------------------------------------------------------------------------------------------------------------- */
    --- configuracion del periodo 
    SELECT  NMCNF_SMV,NMCNF_UMAV,NMCNF_TISR_ID, NMCNF_TSBD_ID, NMCNF_EEX_E, NMCNF_PED_E, NMCNF_PYB_E, NMCNF_IYV_E, NMCNF_CYV_E, NMCNF_ECF_P, NMCNF_EEX_P, NMCNF_PED_P, NMCNF_PYB_P, NMCNF_IYV_P, NMCNF_RDT_P, NMCNF_GPS_P, NMCNF_SAR_P, NMCNF_CYV_P, NMCNF_INF_P
    FROM NMCNF WHERE NMCNF_ANIO = :NMCNF_ANIO
        INTO :CNF_SMV, :CNF_UMAV, :CNF_TISR_ID,  :CNF_TSBD_ID,  :CNF_EEX_E,  :CNF_PED_E,  :CNF_PYB_E,  :CNF_IYV_E,  :CNF_CYV_E,  :CNF_ECF_P,  :CNF_EEX_P,  :CNF_PED_P,  :CNF_PYB_P,  :CNF_IYV_P,  :CNF_RDT_P,  :CNF_GPS_P,  :CNF_SAR_P,  :CNF_CYV_P,  :CNF_INF_P;

    --- agregar la lista de empleados a la tabla nminc (nomina incidencias)
    INSERT INTO NMINC (  EMP_ID,   EMP_CLV,          FIRL, BPP,        SD,       SDIP,        SBC,           JT, VAC, INF, DASL, NMCAL_ID, NMCAL_NOM_ID,        UMA,       SM,       TISR_ID,      TSBD_ID,  NMTP_ID,     FEEX_E,      FPED_E,      FPYB_E,      FIYV_E,      FCYV_E,      FECF_P,      FEEX_P,      FPED_P,      FPYB_P,      FIYV_P,      FRDT_P,      FGPS_P,     FSAR_P,       FCYV_P,      FINF_P) 
                SELECT CTEMP_ID, CTEMP_CLV, CTEMPC_FCHREL,   1, CTEMPC_SD, CTEMPC_SDI, CTEMPC_SBC, CTEMPC_JRNTR,   0,   0,    0,     NULL,  :NMCAL_NOM_ID, :CNF_UMAV, :CNF_SMV, :CNF_TISR_ID, :CNF_TSBD_ID, :NMTP_ID, :CNF_EEX_E,  :CNF_PED_E,  :CNF_PYB_E,  :CNF_IYV_E,  :CNF_CYV_E,  :CNF_ECF_P,  :CNF_EEX_P,  :CNF_PED_P,  :CNF_PYB_P,  :CNF_IYV_P,  :CNF_RDT_P,  :CNF_GPS_P,  :CNF_SAR_P,  :CNF_CYV_P,  :CNF_INF_P
                FROM CTEMP
                LEFT JOIN CTEMPC ON CTEMP_ID = CTEMPC_CTEMP_ID AND CTEMPC_A = 1
                WHERE CTEMP_A = 1;

    --- agregar entradas a la tabla de nomina de resultados del empleado por conceptos de nomina
    INSERT INTO NMEMP (NMEMP_ID, NMEMP_CTEMP_ID, NMEMP_NMCNP_ID, NMEMP_NMCNP_ISR_BF, NMEMP_NMCAL_ID, NMEMP_CNP_TP_ID, NMEMP_TOTAL, NMEMP_TPGRA, NMEMP_TPEXE, NMEMP_TDEDU)
                SELECT      NULL,        EMP_ID,       NMCNP_ID,       NMCNP_ISR_BF,  :NMCAL_NOM_ID,     NMCNP_TP_ID,           0,           0,           0,           0
                FROM NMCNP , NMINC
                WHERE NMCNP_A > 0 AND NMCNP_APL_ID = 1 AND NMCAL_NOM_ID = :NMCAL_NOM_ID
                ORDER BY EMP_ID, NMCNP_ID;
    --- agregar entradas a la tabla de nomina de resultados del empleado por conceptos individuales
    INSERT INTO NMEMP (NMEMP_ID, NMEMP_CTEMP_ID, NMEMP_NMCNP_ID, NMEMP_NMCNP_ISR_BF, NMEMP_NMCAL_ID, NMEMP_CNP_TP_ID, NMEMP_TOTAL, NMEMP_TPGRA, NMEMP_TPEXE, NMEMP_TDEDU)
                SELECT     NULL,         EMP_ID,       NMCNP_ID,       NMCNP_ISR_BF,   NMCAL_NOM_ID,     NMCNP_TP_ID,           0,           0,           0,           0
                FROM NMINC
                LEFT JOIN CTEMPD ON CTEMPD.CTEMPD_CTEMP_ID = NMINC.EMP_ID AND CTEMPD_A = 1
                LEFT JOIN NMCNP ON NMCNP.NMCNP_ID = CTEMPD.CTEMPD_NMCNP_ID 
                WHERE NMCNP_A > 0 AND NMCNP_APL_ID = 2 AND NMCAL_NOM_ID = :NMCAL_NOM_ID
                ORDER BY EMP_ID, NMCNP_ID;
END^
SET TERM ; ^

UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'ejercicio, para obtener la configuracion aplicable'
  where RDB$PARAMETER_NAME = 'NMCNF_ANIO' AND RDB$PROCEDURE_NAME = 'NME_CREAR_NMINC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'indice del periodo a calcular'
  where RDB$PARAMETER_NAME = 'NMCAL_NOM_ID' AND RDB$PROCEDURE_NAME = 'NME_CREAR_NMINC';
UPDATE RDB$PROCEDURE_PARAMETERS set RDB$DESCRIPTION = 'tipo de nomina'
  where RDB$PARAMETER_NAME = 'NMTP_ID' AND RDB$PROCEDURE_NAME = 'NME_CREAR_NMINC';

SET TERM ^ ;
ALTER PROCEDURE NME_DEL_INC (
    NOM_IDX Integer )
AS
BEGIN
/* ------------------------------------------------------------------------------------------------
    Develop: ANHE1 09012022 0119
    Purpose: para calcular periodo de nomina
------------------------------------------------------------------------------------------------ */
    EXECUTE STATEMENT 'DELETE FROM NMINC WHERE NMCAL_NOM_ID = ' || NOM_IDX;
    EXECUTE STATEMENT 'DELETE FROM NMEMP WHERE NMEMP_NMCAL_ID = ' || NOM_IDX;
END^
SET TERM ; ^


SET TERM ^ ;
ALTER PROCEDURE NM_UPDATE_CONF (
    NMCNF_ANIO Integer,
    NMCAL_NOM_ID Integer )
AS
DECLARE VARIABLE CNF_SMV Numeric(18,4); 
DECLARE VARIABLE CNF_UMAV Numeric(18,4);
DECLARE VARIABLE CNF_EEX_E Numeric(18,6);
DECLARE VARIABLE CNF_PED_E Numeric(18,6);
DECLARE VARIABLE CNF_PYB_E Numeric(18,6);
DECLARE VARIABLE CNF_IYV_E Numeric(18,6);
DECLARE VARIABLE CNF_CYV_E Numeric(18,6);
DECLARE VARIABLE CNF_ECF_P Numeric(18,6);
DECLARE VARIABLE CNF_EEX_P Numeric(18,6);
DECLARE VARIABLE CNF_PED_P Numeric(18,6);
DECLARE VARIABLE CNF_PYB_P Numeric(18,6);
DECLARE VARIABLE CNF_IYV_P Numeric(18,6);
DECLARE VARIABLE CNF_RDT_P Numeric(18,6);
DECLARE VARIABLE CNF_GPS_P Numeric(18,6);
DECLARE VARIABLE CNF_SAR_P Numeric(18,6);
DECLARE VARIABLE CNF_CYV_P Numeric(18,6);
DECLARE VARIABLE CNF_INF_P Numeric(18,6);
DECLARE VARIABLE CNF_TISR_ID integer;
DECLARE VARIABLE CNF_TSBD_ID integer;
BEGIN
/* -------------------------------------------------------------------------------------------------------------
    Develop: ANHE1-15/06/2022 21:03
    Purpose: creacion de la tabla temporal de trabajo
------------------------------------------------------------------------------------------------------------- */
    --- configuracion del periodo 
    SELECT  NMCNF_SMV,NMCNF_UMAV,NMCNF_TISR_ID, NMCNF_TSBD_ID, NMCNF_EEX_E, NMCNF_PED_E, NMCNF_PYB_E, NMCNF_IYV_E, NMCNF_CYV_E, NMCNF_ECF_P, NMCNF_EEX_P, NMCNF_PED_P, NMCNF_PYB_P, NMCNF_IYV_P, NMCNF_RDT_P, NMCNF_GPS_P, NMCNF_SAR_P, NMCNF_CYV_P, NMCNF_INF_P
    FROM NMCNF WHERE NMCNF_ANIO = :NMCNF_ANIO
        INTO :CNF_SMV, :CNF_UMAV, :CNF_TISR_ID,  :CNF_TSBD_ID,  :CNF_EEX_E,  :CNF_PED_E,  :CNF_PYB_E,  :CNF_IYV_E,  :CNF_CYV_E,  :CNF_ECF_P,  :CNF_EEX_P,  :CNF_PED_P,  :CNF_PYB_P,  :CNF_IYV_P,  :CNF_RDT_P,  :CNF_GPS_P,  :CNF_SAR_P,  :CNF_CYV_P,  :CNF_INF_P;

    --- agregar la lista de empleados a la tabla nminc (nomina incidencias)
    UPDATE NMINC SET 
      UMA = :CNF_UMAV, SM = :CNF_SMV, TISR_ID = :CNF_TISR_ID, TSBD_ID = :CNF_TSBD_ID, FEEX_E = :CNF_EEX_E, FPED_E = :CNF_PED_E, FPYB_E = :CNF_PYB_E, FIYV_E = :CNF_IYV_E, FCYV_E = :CNF_CYV_E, FECF_P = :CNF_ECF_P, FEEX_P = :CNF_EEX_P, FPED_P = :CNF_PED_P, FPYB_P = :CNF_PYB_P, FIYV_P = :CNF_IYV_P, FRDT_P = :CNF_RDT_P, FGPS_P = :CNF_GPS_P, FSAR_P = :CNF_SAR_P, FCYV_P = :CNF_CYV_P, FINF_P = :CNF_INF_P
	WHERE NMCAL_NOM_ID = :NMCAL_NOM_ID;

END^
SET TERM ; ^


UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dia festivo'  where RDB$FIELD_NAME = 'FECHA' and RDB$RELATION_NAME = 'CLDF';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'calendario de dias festivos'
where RDB$RELATION_NAME = 'CLDF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de llave'  where RDB$FIELD_NAME = 'CONF_KEY' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero o clave de grupo'  where RDB$FIELD_NAME = 'CONF_GRP' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'datos de la configuracion'  where RDB$FIELD_NAME = 'CONF_DATA' and RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'configuraciones de la empresa'
where RDB$RELATION_NAME = 'CONF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTDPT_ID' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion del departamento'  where RDB$FIELD_NAME = 'CTDPT_NOM' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cuenta contable'  where RDB$FIELD_NAME = 'CTDPT_CTCTBL' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clasificacion'  where RDB$FIELD_NAME = 'CTDPT_CLSF' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'centro de costos'  where RDB$FIELD_NAME = 'CTDPT_CCST' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de nuevo registro'  where RDB$FIELD_NAME = 'CTDPT_FN' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'CTDPT_USR_N' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'CTDPT_FM' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'CTDPT_USR_M' and RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'catalogo de departamentos'
where RDB$RELATION_NAME = 'CTDPT';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'catalogo de puestos y funciones'
where RDB$RELATION_NAME = 'CTDPTP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador principal de la tabla.'  where RDB$FIELD_NAME = 'CTEMP_ID' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Registro activo.'  where RDB$FIELD_NAME = 'CTEMP_A' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Indice con el directorio principal.'  where RDB$FIELD_NAME = 'CTEMP_DRCTR_ID' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave de control de empleado.'  where RDB$FIELD_NAME = 'CTEMP_CLV' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Registro federal de causantes.'  where RDB$FIELD_NAME = 'CTEMP_RFC' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'NumSeguridadSocial: opcional para la expresion del numero de seguridad social aplicable al trabajador.'  where RDB$FIELD_NAME = 'CTEMP_NSS' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Unidad medica familiar.'  where RDB$FIELD_NAME = 'CTEMP_UMF' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Cuenta FONACOT.'  where RDB$FIELD_NAME = 'CTEMP_FONAC' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave unica de registro.'  where RDB$FIELD_NAME = 'CTEMP_CURP' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'AFORE.'  where RDB$FIELD_NAME = 'CTEMP_AFORE' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave de entidad federativa de nacimiento.'  where RDB$FIELD_NAME = 'CTEMP_ENTFED' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Lugar de nacimiento.'  where RDB$FIELD_NAME = 'CTEMP_LGNAC' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Nacionalidad.'  where RDB$FIELD_NAME = 'CTEMP_NAC' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Nombre(s) del empleado.'  where RDB$FIELD_NAME = 'CTEMP_NOM' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Primer apellido.'  where RDB$FIELD_NAME = 'CTEMP_PAPE' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Segundo apellido.'  where RDB$FIELD_NAME = 'CTEMP_SAPE' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Correo electronico.'  where RDB$FIELD_NAME = 'CTEMP_MAIL' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Sitio web personal.'  where RDB$FIELD_NAME = 'CTEMP_SITIO' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Fecha de nacimiento.'  where RDB$FIELD_NAME = 'CTEMP_FNAC' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Genero.'  where RDB$FIELD_NAME = 'CTEMP_SEXO' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Estado civil.'  where RDB$FIELD_NAME = 'CTEMP_ECVL' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de telefono.'  where RDB$FIELD_NAME = 'CTEMP_TLFN' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de telefono 2, opcional.'  where RDB$FIELD_NAME = 'CTEMP_TLFN2' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'codigo postal del domicilio fiscal del receptor del comprobante'  where RDB$FIELD_NAME = 'CTEMP_DOMFIS' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de regimen fiscal, clave 065 - Sueldos y Salarios e Ingresos Asimilados a Salarios'  where RDB$FIELD_NAME = 'CTEMP_RGFSC' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Imagen del empleado.'  where RDB$FIELD_NAME = 'CTEMP_IMG' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'bandera para indicar si el registro ya fue validado (Nombre, RFC y Domicilio Fiscal)'  where RDB$FIELD_NAME = 'CTEMP_VSAT' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Fecha de registro.'  where RDB$FIELD_NAME = 'CTEMP_FN' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Usuario creo registro.'  where RDB$FIELD_NAME = 'CTEMP_USR_N' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Ultima fecha de modificaciones.'  where RDB$FIELD_NAME = 'CTEMP_FM' and RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Ultimo usuario que modifico.'  where RDB$FIELD_NAME = 'CTEMP_USR_M' and RDB$RELATION_NAME = 'CTEMP';
ALTER TABLE CTEMP ADD CTEMP_EDAD COMPUTED BY ((case when CTEMP_FNAC is not null then (datediff(year from CTEMP_FNAC to current_date )) else 0 end));
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Empleados: directorio de empleados, datos generales y fiscales'
where RDB$RELATION_NAME = 'CTEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTEMPB_ID' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTEMPB_A' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de empleados'  where RDB$FIELD_NAME = 'CTEMPB_CTEMP_ID' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cuenta verificada'  where RDB$FIELD_NAME = 'CTEMPB_VRFCD' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe del cargo maximo que se puede hacer a la cuenta'  where RDB$FIELD_NAME = 'CTEMPB_CRGMX' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'para expresar el Banco emisor del cheque, de acuerdo al catalogo publicado en la pagina de internet del SAT'  where RDB$FIELD_NAME = 'CTEMPB_CLV' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'referencia numerica'  where RDB$FIELD_NAME = 'CTEMPB_REFNUM' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'opcion tipo moneda adicional'  where RDB$FIELD_NAME = 'CTEMPB_MND' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'sucursal'  where RDB$FIELD_NAME = 'CTEMPB_SCRSL' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'referencia alfanumerica'  where RDB$FIELD_NAME = 'CTEMPB_REFALF' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'para expresar el numero de cuenta'  where RDB$FIELD_NAME = 'CTEMPB_NMCTA' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cuenta clabe'  where RDB$FIELD_NAME = 'CTEMPB_CLABE' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre corto definido en el listado del sat'  where RDB$FIELD_NAME = 'CTEMPB_NMCRT' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o razon social'  where RDB$FIELD_NAME = 'CTEMPB_BANCO' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fec. sist.'  where RDB$FIELD_NAME = 'CTEMPB_FN' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'usuario creo'  where RDB$FIELD_NAME = 'CTEMPB_USR_N' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de modificacion'  where RDB$FIELD_NAME = 'CTEMPB_FM' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultimo usuario que modifico'  where RDB$FIELD_NAME = 'CTEMPB_USR_M' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del beneficiario'  where RDB$FIELD_NAME = 'CTEMPB_BENEF' and RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'rfc del beneficiario'  where RDB$FIELD_NAME = 'CTEMPB_BRFC' and RDB$RELATION_NAME = 'CTEMPB';
CREATE INDEX IDX_CTEMPB_CTEMP_ID ON CTEMPB (CTEMPB_CTEMP_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Empleados: catalogo de cuentas bancarias relacionadas al empleado'
where RDB$RELATION_NAME = 'CTEMPB';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador principal de la tabla.'  where RDB$FIELD_NAME = 'CTEMPC_ID' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Registro activo.'  where RDB$FIELD_NAME = 'CTEMPC_A' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio de empleados'  where RDB$FIELD_NAME = 'CTEMPC_CTEMP_ID' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Indice del departamento.'  where RDB$FIELD_NAME = 'CTEMPC_DEPTOID' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Indice del puesto.'  where RDB$FIELD_NAME = 'CTEMPC_PSTID' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave de tipo de regimen.'  where RDB$FIELD_NAME = 'CTEMPC_TPREG' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave riesgo de puesto.'  where RDB$FIELD_NAME = 'CTEMPC_RSGPST' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave periodicidad de pago.'  where RDB$FIELD_NAME = 'CTEMPC_PRDDPG' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave metodo de pago.'  where RDB$FIELD_NAME = 'CTEMPC_MTDPG' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave tipo de contrato.'  where RDB$FIELD_NAME = 'CTEMPC_TPCNTR' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave de tipo de jornada.'  where RDB$FIELD_NAME = 'CTEMPC_TPJRN' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Clave banco.'  where RDB$FIELD_NAME = 'CTEMPC_CLVBNC' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de cuenta de banco.'  where RDB$FIELD_NAME = 'CTEMPC_CTABAN' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de cuenta interbancaria.'  where RDB$FIELD_NAME = 'CTEMPC_CLABE' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de cuenta de tarjeta de vales de despensa.'  where RDB$FIELD_NAME = 'CTEMPC_CTAVAL' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de sucursal bancaria.'  where RDB$FIELD_NAME = 'CTEMPC_SCRSL' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Tipo de cuenta bancaria.'  where RDB$FIELD_NAME = 'CTEMPC_TPCTA' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Registro patronal.'  where RDB$FIELD_NAME = 'CTEMPC_RGP' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Departamento.'  where RDB$FIELD_NAME = 'CTEMPC_DEPTO' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Puesto.'  where RDB$FIELD_NAME = 'CTEMPC_PUESTO' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Fecha de inicio de relacion laboral.'  where RDB$FIELD_NAME = 'CTEMPC_FCHREL' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Fecha de terminaci�n laborar.'  where RDB$FIELD_NAME = 'CTEMPC_FCHTERLAB' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Base de cotizacion.'  where RDB$FIELD_NAME = 'CTEMPC_BSCOT' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Salario base.'  where RDB$FIELD_NAME = 'CTEMPC_SB' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Salario base de cotizaci�n.'  where RDB$FIELD_NAME = 'CTEMPC_SBC' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Salario diario integrado.'  where RDB$FIELD_NAME = 'CTEMPC_SDI' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Salario base.'  where RDB$FIELD_NAME = 'CTEMPC_SD' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de jornadas de trabajo.'  where RDB$FIELD_NAME = 'CTEMPC_JRNTR' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Numero de horas de la jornadas de trabajo.'  where RDB$FIELD_NAME = 'CTEMPC_JRNHRS' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Fecha de registro.'  where RDB$FIELD_NAME = 'CTEMPC_FN' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Usuario creo registro.'  where RDB$FIELD_NAME = 'CTEMPC_USR_N' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Ultima fecha de modificaciones.'  where RDB$FIELD_NAME = 'CTEMPC_FM' and RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Ultimo usuario que modifico.'  where RDB$FIELD_NAME = 'CTEMPC_USR_M' and RDB$RELATION_NAME = 'CTEMPC';
CREATE INDEX IDX_CTEMPC_CTEMP_ID ON CTEMPC (CTEMPC_CTEMP_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Empleados: catalogo de contratos'
where RDB$RELATION_NAME = 'CTEMPC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'CTEMPD_ID' and RDB$RELATION_NAME = 'CTEMPD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'CTEMPD_A' and RDB$RELATION_NAME = 'CTEMPD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de empleados'  where RDB$FIELD_NAME = 'CTEMPD_CTEMP_ID' and RDB$RELATION_NAME = 'CTEMPD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla de conceptos'  where RDB$FIELD_NAME = 'CTEMPD_NMCNP_ID' and RDB$RELATION_NAME = 'CTEMPD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla de sistema'  where RDB$FIELD_NAME = 'CTEMPD_TBL_ID' and RDB$RELATION_NAME = 'CTEMPD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'importe fijo'  where RDB$FIELD_NAME = 'CTEMPD_IMPFJ' and RDB$RELATION_NAME = 'CTEMPD';
CREATE INDEX IDX_CTEMPD_CTEMP_ID ON CTEMPD (CTEMPD_CTEMP_ID);
CREATE INDEX IDX_CTEMPD_NMCNP_ID ON CTEMPD (CTEMPD_NMCNP_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Nomina: concepto de nomina aplicable al empleado'
where RDB$RELATION_NAME = 'CTEMPD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador principal de la tabla.'  where RDB$FIELD_NAME = 'ISR_ID' and RDB$RELATION_NAME = 'ISR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Anio al que pertenece la informacion del ISR.'  where RDB$FIELD_NAME = 'ISR_ANIO' and RDB$RELATION_NAME = 'ISR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Periodo del de ISR (1 = Diario, 7 = Semanal, 10 = Decenal, 15 = Quincenal, 30 = Mensual, 365 = Anual).'  where RDB$FIELD_NAME = 'ISR_PRD' and RDB$RELATION_NAME = 'ISR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador principal de la tabla.'  where RDB$FIELD_NAME = 'ISRC_ID' and RDB$RELATION_NAME = 'ISRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador de la tabla ISR.'  where RDB$FIELD_NAME = 'ISRC_ISR_ID' and RDB$RELATION_NAME = 'ISRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Limite inferior.'  where RDB$FIELD_NAME = 'ISRC_LI' and RDB$RELATION_NAME = 'ISRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Limite superior.'  where RDB$FIELD_NAME = 'ISRC_LS' and RDB$RELATION_NAME = 'ISRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Cuota fija.'  where RDB$FIELD_NAME = 'ISRC_CUOTA' and RDB$RELATION_NAME = 'ISRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Por ciento aplicable sobre el excendente del limite inferior.'  where RDB$FIELD_NAME = 'ISRC_EXC' and RDB$RELATION_NAME = 'ISRC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice '  where RDB$FIELD_NAME = 'NOM_ID' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el directorio de empleados'  where RDB$FIELD_NAME = 'EMP_ID' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de control interno del empleado'  where RDB$FIELD_NAME = 'EMP_CLV' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'incidencias'  where RDB$FIELD_NAME = 'INC' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario diario'  where RDB$FIELD_NAME = 'SD' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha actual'  where RDB$FIELD_NAME = 'FCC' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de alta del empleado'  where RDB$FIELD_NAME = 'FCA' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias por anio'  where RDB$FIELD_NAME = 'DXA' and RDB$RELATION_NAME = 'NMAGU';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias por mes'  where RDB$FIELD_NAME = 'DXM' and RDB$RELATION_NAME = 'NMAGU';
ALTER TABLE NMAGU ADD DLB COMPUTED BY ((DXA - INC));
ALTER TABLE NMAGU ADD SLM COMPUTED BY (CAST((SD * DXM) AS NUMERIC(18, 4)));
ALTER TABLE NMAGU ADD AGU COMPUTED BY (
CAST ((((SD * DAG)/DXA) * DLB) AS NUMERIC(18, 4))
);
ALTER TABLE NMAGU ADD SM COMPUTED BY (
    CAST ((SELECT SM_SALARIO FROM SM WHERE SM_ANIO = EXTRACT(YEAR FROM FCC))
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD BEXN COMPUTED BY (
    CAST (
        (SM * 30)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD BGRV COMPUTED BY (
    CAST (
        (SLM+((AGU-BEXN)/DXA) *DXM)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD BGRA COMPUTED BY (
    CAST (
        CASE WHEN (AGU-BEXN)<=0 THEN 0 ELSE AGU-BEXN END
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD ISRSA COMPUTED BY (
    CAST (
        CASE WHEN BGRA > 0 THEN
            (SELECT (((BGRA) - ISRC_LI) * (ISRC_EXC / 100)) + ISRC_CUOTA 
             FROM ISR, ISRC 
             WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
                 AND ISR_ANIO = EXTRACT(YEAR FROM FCC) 
                 AND ISR_PRD = 30
                 AND (BGRA) >= ISRC_LI 
                 AND (BGRA) <= ISRC_LS)
            ELSE 0 END
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD SBE COMPUTED BY (
    CAST (
        CASE WHEN BGRA > 0 THEN
            (SELECT SBSD_CNTDD FROM SBSD 
             LEFT JOIN ISR ON SBSD_ISR_ID = ISR_ID 
             WHERE (BGRA >= SBSD_PARA 
                AND BGRA <= SBSD_HASTA) 
                AND ISR_ANIO = EXTRACT(YEAR FROM FCC) 
                AND ISR_PRD = 30)
            ELSE 0 END
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD ISRS COMPUTED BY (
    CAST (
        CASE WHEN AGU > 0 THEN
            (SELECT (((SLM) - ISRC_LI) * (ISRC_EXC / 100)) + ISRC_CUOTA 
             FROM ISR, ISRC 
             WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
                 AND ISR_ANIO = EXTRACT(YEAR FROM FCC) 
                 AND ISR_PRD = 30
                 AND (SLM) >= ISRC_LI 
                 AND (SLM) <= ISRC_LS)
            ELSE 0 END
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD ISRA COMPUTED BY (
    CAST (
        CASE WHEN AGU > 0 THEN
            (SELECT (((AGU) - ISRC_LI) * (ISRC_EXC / 100)) + ISRC_CUOTA 
                 FROM ISR, ISRC 
                 WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
                 AND ISR_ANIO = EXTRACT(YEAR FROM FCC) 
                 AND ISR_PRD = 7
                 AND (AGU) >= ISRC_LI 
                 AND (AGU) <= ISRC_LS)
            ELSE 0 END
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMAGU ADD GRT COMPUTED BY (
    CAST (
        CASE WHEN AGU > 0 THEN
            (AGU - ISRSA)
            ELSE 0 END
        AS NUMERIC(18, 4))
    );
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'NMCNF_ID' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ejercicio'  where RDB$FIELD_NAME = 'NMCNF_ANIO' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tabla a utilizar para el calculo del ISR'  where RDB$FIELD_NAME = 'NMCNF_TISR_ID' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla a utilizar para subsidio al empleo'  where RDB$FIELD_NAME = 'NMCNF_TSBD_ID' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de periodicidad'  where RDB$FIELD_NAME = 'NMCNF_PRD_ID' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias por anio'  where RDB$FIELD_NAME = 'NMCNF_DXA' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de la semana'  where RDB$FIELD_NAME = 'NMCNF_DXS' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas del dia'  where RDB$FIELD_NAME = 'NMCNF_HD' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de pago'  where RDB$FIELD_NAME = 'NMCNF_DP' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario minimo vigente'  where RDB$FIELD_NAME = 'NMCNF_SMV' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'valor de la unidad de medida actualizada (uma)'  where RDB$FIELD_NAME = 'NMCNF_UMAV' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'NMCNF_USR_N' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'NMCNF_FN' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registros'  where RDB$FIELD_NAME = 'NMCNF_USR_M' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion'  where RDB$FIELD_NAME = 'NMCNF_FM' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'EEXE = Enfermedad y Maternidad, Especie Excedente (Empleado)'  where RDB$FIELD_NAME = 'NMCNF_EEX_E' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PEDE = Enfermedad y Maternidad, Prestaciones en Dinero (Empleado)'  where RDB$FIELD_NAME = 'NMCNF_PED_E' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PYBE = Enfermedad y Maternidad, Pensionados y Beneficiarios (Empleado)'  where RDB$FIELD_NAME = 'NMCNF_PYB_E' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'IYVE = Enfermedad y Maternidad, Invalidez y Vida (Empleado)'  where RDB$FIELD_NAME = 'NMCNF_IYV_E' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'CYVE = Enfermedad y Maternidad, Cesantia y Vejez (Empleado)'  where RDB$FIELD_NAME = 'NMCNF_CYV_E' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ECFP = Especie - Cuota Fija (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_ECF_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'EEXP = Especie - Excedente (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_EEX_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PEDP = Prestaciones en Dinero (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_PED_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PYBP = Pensionados y Beneficiarios (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_PYB_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'IYVP = Invalidez y Vida (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_IYV_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'RDTP = Riesgo de Trabajo (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_RDT_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'GPSP = Guarderias y Prestaciones Sociales (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_GPS_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'SARP = Seguro de Retiro (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_SAR_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'CYVP = Cesantia y Vejez (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_CYV_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'INFP = INFONAVIT (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'NMCNF_INF_P' and RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'Nomina: configuracion del periodo (parametros)'
where RDB$RELATION_NAME = 'NMCNF';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de tabla'  where RDB$FIELD_NAME = 'NMCNP_ID' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'NMCNP_A' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '1 = percepcion; 2 = deduccion'  where RDB$FIELD_NAME = 'NMCNP_TP_ID' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'aplicable a tipo de nomina 1=Ordinaria (normal) o 2=Extraordinaria (especial)'  where RDB$FIELD_NAME = 'NMCNP_TNM_ID' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de aplicacion (1=general | 2=individual)'  where RDB$FIELD_NAME = 'NMCNP_APL_ID' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'NMCNP_TBL_ID' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'visible en recibo impreso'  where RDB$FIELD_NAME = 'NMCNP_VIS' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de descripcion'  where RDB$FIELD_NAME = 'NMCNP_CLV' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave SAT'  where RDB$FIELD_NAME = 'NMCNP_CLVSAT' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion'  where RDB$FIELD_NAME = 'NMCNP_NOM' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'formula'  where RDB$FIELD_NAME = 'NMCNP_FORM' and RDB$RELATION_NAME = 'NMCNP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'NMEMP_ID' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del catalogo de empleados'  where RDB$FIELD_NAME = 'NMEMP_CTEMP_ID' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de concepto de nomina (percepciones o deducciones)'  where RDB$FIELD_NAME = 'NMEMP_NMCNP_ID' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '1 = percepcion; 2 = deduccion'  where RDB$FIELD_NAME = 'NMEMP_CNP_TP_ID' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'base'  where RDB$FIELD_NAME = 'NMEMP_NMCNP_ISR_BF' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con tabla de incidencias de la nomina'  where RDB$FIELD_NAME = 'NMEMP_NMCAL_ID' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tota del concepto, resultado de la formula'  where RDB$FIELD_NAME = 'NMEMP_TOTAL' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de percepcion gravado'  where RDB$FIELD_NAME = 'NMEMP_TPGRA' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de percepcion extento'  where RDB$FIELD_NAME = 'NMEMP_TPEXE' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de deducciones'  where RDB$FIELD_NAME = 'NMEMP_TDEDU' and RDB$RELATION_NAME = 'NMEMP';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador principal de la tabla.'  where RDB$FIELD_NAME = 'NMHRE_ID' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'NMHRE_A' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de concepto de nomina (percepciones o deducciones)'  where RDB$FIELD_NAME = 'NMHRE_NMCNP_ID' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el empleado'  where RDB$FIELD_NAME = 'NMHRE_EMP_ID' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el periodo de nomina'  where RDB$FIELD_NAME = 'NMHRE_NOM_ID' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha '  where RDB$FIELD_NAME = 'NMHRE_FEC' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'cantidad de horas'  where RDB$FIELD_NAME = 'NMHRE_CANT' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nota'  where RDB$FIELD_NAME = 'NMHRE_NOTA' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'NMHRE_USR_N' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'NMHRE_FN' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'NMHRE_USR_M' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de la ultima modificacion del registro'  where RDB$FIELD_NAME = 'NMHRE_FM' and RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'registro de horas extra'
where RDB$RELATION_NAME = 'NMHRE';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de nomina'  where RDB$FIELD_NAME = 'NMTP_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de periodos'  where RDB$FIELD_NAME = 'NMCAL_NOM_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del catalogo de empleados'  where RDB$FIELD_NAME = 'EMP_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del empleado'  where RDB$FIELD_NAME = 'EMP_CLV' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de pago'  where RDB$FIELD_NAME = 'FCC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de alta'  where RDB$FIELD_NAME = 'FIRL' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'bandera: 1 = indica aplicar premio por puntualidad y asistencia'  where RDB$FIELD_NAME = 'BPP' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas de la jornada'  where RDB$FIELD_NAME = 'HRJR' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas de reloj'  where RDB$FIELD_NAME = 'HRLJ' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas extra autorizadas'  where RDB$FIELD_NAME = 'HRAT' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de dias de incapacidad'  where RDB$FIELD_NAME = 'INC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'numero de dias de ausencia'  where RDB$FIELD_NAME = 'AUS' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de vacaciones tomadas por el trabajador'  where RDB$FIELD_NAME = 'VAC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descuento diario por pago de credito de vivienda (infonavit)'  where RDB$FIELD_NAME = 'INF' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'descuento por anticipo a salarios'  where RDB$FIELD_NAME = 'DASL' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario diario'  where RDB$FIELD_NAME = 'SD' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario diario integrado personalizado'  where RDB$FIELD_NAME = 'SDIP' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario base de cotizacion'  where RDB$FIELD_NAME = 'SBC' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'jornadas de trabajo'  where RDB$FIELD_NAME = 'JT' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'horas del dia'  where RDB$FIELD_NAME = 'HD' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias del periodo'  where RDB$FIELD_NAME = 'DP' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias del anio'  where RDB$FIELD_NAME = 'DXA' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias de aguinaldo'  where RDB$FIELD_NAME = 'DAG' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = '% prima vacacional'  where RDB$FIELD_NAME = 'PV' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'salario minimo vigente (SM)'  where RDB$FIELD_NAME = 'SM' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'unidad de medida actualizada vigente (UMA)'  where RDB$FIELD_NAME = 'UMA' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion de la tabla ISR'  where RDB$FIELD_NAME = 'TISR_ID' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla subsidio al empleo asociada al periodo'  where RDB$FIELD_NAME = 'TSBD_ID' and RDB$RELATION_NAME = 'NMINC';
ALTER TABLE NMINC ADD SM3 COMPUTED BY (
    CAST ((SM * 3)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMINC ADD UMA3 COMPUTED BY (
    CAST ((UMA * 3)
        AS NUMERIC(18, 4))
    );
ALTER TABLE NMINC ADD HREXT COMPUTED BY (
    (CASE WHEN ABS(HRLJ -HRJR)>HRAT THEN HRAT 
        ELSE ABS(HRLJ-HRJR) END) 
        );
ALTER TABLE NMINC ADD ADS COMPUTED BY (
    CAST(
        DATEDIFF(DAY FROM FIRL TO FCC)/DXA
    AS NUMERIC(18,4)
    ));
ALTER TABLE NMINC ADD DVC COMPUTED BY (
    CAST(
        CASE WHEN (ADS < 1 ) THEN 6 
            WHEN (ADS < 2 ) THEN 8 
            WHEN (ADS < 3 ) THEN 10 
            WHEN (ADS < 4 ) THEN 12 
            WHEN (ADS < 9 ) THEN 14 
            WHEN (ADS < 14 ) THEN 16 
            WHEN (ADS < 19 ) THEN 18 
            WHEN (ADS < 24 ) THEN 20 
            WHEN (ADS < 29 ) THEN 22 
            WHEN (ADS < 34 ) THEN 24 
    END AS NUMERIC(18,4)
    ));
ALTER TABLE NMINC ADD FSDI COMPUTED BY (
    CAST(
        (DXA + DAG + (DVC * PV))/DXA AS NUMERIC(18,4) 
    ));
ALTER TABLE NMINC ADD SDI COMPUTED BY ( 
    CAST( 
        CASE WHEN (SDIP = 0) THEN
                (SD * FSDI)
            ELSE
                SDIP
            END
    AS NUMERIC(18,2) 
    ));
ALTER TABLE NMINC ADD DT COMPUTED BY (
/* DIAS TRABAJADOS */
    CAST(
        (DP-(AUS+INC+VAC))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD TPGRV COMPUTED BY (
    CAST (
        (SELECT SUM(NMEMP_TPGRA) FROM NMEMP WHERE NMEMP_NMCAL_ID = NMCAL_NOM_ID AND NMEMP_CTEMP_ID = EMP_ID) AS NUMERIC(18,4)
        ));
ALTER TABLE NMINC ADD TPEXE COMPUTED BY (
    CAST (
        (SELECT SUM(NMEMP_TPEXE) FROM NMEMP WHERE NMEMP_NMCAL_ID = NMCAL_NOM_ID AND NMEMP_CTEMP_ID = EMP_ID) AS NUMERIC(18,4)
        ));
ALTER TABLE NMINC ADD TDEDU COMPUTED BY (
    CAST (
        (SELECT SUM(NMEMP_TDEDU) FROM NMEMP WHERE NMEMP_NMCAL_ID = NMCAL_NOM_ID AND NMEMP_CTEMP_ID = EMP_ID) AS NUMERIC(18,4)
        ));
ALTER TABLE NMINC ADD ISPT COMPUTED BY (
    CAST (
        (SELECT (((TPGRV) - ISRC_LI) * (ISRC_EXC / 100)) + ISRC_CUOTA 
         FROM ISR, ISRC 
         WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
             AND ISR_ID = TISR_ID
             AND (TPGRV) >= ISRC_LI 
             AND (TPGRV) <= ISRC_LS) AS NUMERIC(18,4)
        ));
ALTER TABLE NMINC ADD SUBE COMPUTED BY (
    CAST(
        (SELECT SBSD_CNTDD FROM SBSD 
        LEFT JOIN ISR ON SBSD_ISR_ID = ISR_ID 
        WHERE (TPGRV >= SBSD_PARA 
            AND TPGRV <= SBSD_HASTA) 
            AND ISR_ID = TSBD_ID)
        AS NUMERIC(18, 4)
    ));
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'EEXE = Enfermedad y Maternidad, Especie Excedente (Empleado) '  where RDB$FIELD_NAME = 'FEEX_E' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PEDE = Enfermedad y Maternidad, Prestaciones en Dinero (Empleado)'  where RDB$FIELD_NAME = 'FPED_E' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PYBE = Enfermedad y Maternidad, Pensionados y Beneficiarios (Empleado)'  where RDB$FIELD_NAME = 'FPYB_E' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'YVE = Enfermedad y Maternidad, Invalidez y Vida (Empleado)'  where RDB$FIELD_NAME = 'FIYV_E' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'CYVE = Enfermedad y Maternidad, Cesantia y Vejez (Empleado) '  where RDB$FIELD_NAME = 'FCYV_E' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ECFP = Especie - Cuota Fija (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FECF_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'EEXP = Especie - Excedente (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FEEX_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PEDP = Prestaciones en Dinero (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FPED_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'PYBP = Pensionados y Beneficiarios (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FPYB_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'IYVP = Invalidez y Vida (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FIYV_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'RDTP = Riesgo de Trabajo (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FRDT_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'GPSP = Guarderias y Prestaciones Sociales (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FGPS_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'SARP = Seguro de Retiro (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FSAR_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'CYVP = Cesantia y Vejez (CUOTAS IMSS Patron) '  where RDB$FIELD_NAME = 'FCYV_P' and RDB$RELATION_NAME = 'NMINC';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'NFP = INFONAVIT (CUOTAS IMSS Patron)'  where RDB$FIELD_NAME = 'FINF_P' and RDB$RELATION_NAME = 'NMINC';
ALTER TABLE NMINC ADD EEX_E COMPUTED BY (
/*-- Especie-Excedente Empleado (Enfermedad y Maternidad) --*/
    CAST(
        (
        IIF((SBC-UMA3)>0, 
                ((SBC-UMA3)*FEEX_E/100)* CAST(DT AS NUMERIC(18,2))
                ,0)
        )
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD PED_E COMPUTED BY (
/*-- Prestaciones en dinero Empleado (Enfermedad y Maternidad) --*/
    CAST(
        (SBC*(FPED_E/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD PYB_E COMPUTED BY (
/*-- Pensionados y Beneficiarios Empleado (Enfermedad y Maternidad) --*/
    CAST(
        (SBC*(FPYB_E/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IYV_E COMPUTED BY (
/*-- Invalidez y Vida Empleado (Enfermedad y Maternidad) --*/
    CAST(
        (SBC*(FIYV_E/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD CYV_E COMPUTED BY (
/*-- Cesant�a y Vejez Empleado (Enfermedad y Maternidad) --*/
    CAST(
        (SBC*(FCYV_E/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD ECF_P COMPUTED BY (
/*-- Especie Cuota Fija Patron (Enfermedad y Maternidad) --*/
    CAST(
        (UMA*(FECF_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD EEX_P COMPUTED BY (
/*-- Especie - Excedente Patron (Enfermedad y Maternidad) --*/
    CAST(
        IIF((SBC-UMA3)>0, ((SBC-UMA3)*(FEEX_P/100))*CAST(DT AS NUMERIC(18,2)),0)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD PED_P COMPUTED BY (
/*-- Prestaciones en Dinero Patron (Enfermedad y Maternidad) --*/
    CAST(
        (SBC*(FPED_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD PYB_P COMPUTED BY (
/*-- Pensionados y Beneficiarios / Patron (Enfermedad y Maternidad) --*/
    CAST(
        (SBC*(FPYB_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IYV_P COMPUTED BY (
/*-- Invalidez y Vida / Patron --*/
    CAST(
        (SBC*(FIYV_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD RDT_P COMPUTED BY (
/*-- Riesgo De Trabajo / Patron --*/
    CAST(
        (SBC*(FRDT_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD GPS_P COMPUTED BY (
/*-- Guarder�as y Prestaciones Sociales / Patron --*/
    CAST(
        (SBC*(FGPS_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD SAR_P COMPUTED BY (
/*-- Seguro de Retiro Sociales / Patron --*/
    CAST(
        (SBC*(FSAR_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD CYV_P COMPUTED BY (
/*-- Cesant�a y Vejez / Patron --*/
    CAST(
        (SBC*(FCYV_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD INF_P COMPUTED BY (
/*-- INFONAVIT / Patron --*/
    CAST(
        (SBC*(FINF_P/100)*CAST(DT AS NUMERIC(18,2)))
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IMSS_MENE COMPUTED BY (
/*-- IMSS MENSUAL EMPLEADO --*/
    CAST(
        (EEX_E+ PED_E + PYB_E + IYV_E)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IMSS_BIME COMPUTED BY (
/*-- IMSS BIMESTRAL EMPLEADO --*/
    CAST(
        (CYV_E)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IMSS_TOTE COMPUTED BY (
/*-- IMSS TOTAL EMPLEADO --*/
    CAST(
        (EEX_E+ PED_E + PYB_E + IYV_E + CYV_E)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IMSS_MENP COMPUTED BY (
/*-- IMSS MENSUAL PATRON --*/
    CAST(
        (ECF_P + EEX_P + PED_P + PYB_P + IYV_P + RDT_P + GPS_P)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IMSS_BIMP COMPUTED BY (
/*-- IMSS BIMESTRAL PATRON --*/
    CAST(
        (SAR_P + CYV_P)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IMSS_INFP COMPUTED BY (
/*-- INFONAVIT BIMESTRAL PATRON --*/
    CAST(
        (INF_P)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD IMSS_TOTP COMPUTED BY (
/*-- IMSS E INFONAVIT TOTAL PATRON --*/
    CAST(
        (ECF_P + EEX_P + PED_P + PYB_P + IYV_P + RDT_P + GPS_P + SAR_P + CYV_P + INF_P)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD FPRD COMPUTED BY (
/*-- FACTOR DEL PERIODO PARA CALCULAR LA BASE GRAVABLE MENSUAL (DIAS DEL ANIO / MESES DEL A�O / DIAS DE PAGO) 
    GRAVABLE DEL PERIODO * FACTOR DEL PERIODO = GRAVABLE MENSUAL
--*/
    CAST(
        (DXA/12)/DP
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD GRVMEN COMPUTED BY (
/* --- Gravable mensual (gravable del periodo por factor mensual) --*/
    CAST(
        (TPGRV * FPRD)
        AS NUMERIC(18, 4)
    ));
ALTER TABLE NMINC ADD ISRMES COMPUTED BY (
/* --- ISR DEL MES --*/
    CAST(
        (SELECT ((GRVMEN - ISRC_LI) * (ISRC_EXC / 100)) + ISRC_CUOTA 
         FROM ISR, ISRC 
         WHERE ISR.ISR_ID = ISRC.ISRC_ISR_ID 
             AND ISR_ID = 29
             AND GRVMEN >= ISRC_LI 
             AND GRVMEN <= ISRC_LS) AS NUMERIC(18,4)
    ));
ALTER TABLE NMINC ADD ISRPRD COMPUTED BY (
/* --- ISR DEL PERIODO PERO FALTA AGREGAR EL SUBSIDIO AL EMPLEO MENSUAL --*/
    CAST(
        (ISRMES / FPRD) AS NUMERIC(18,4)
    ));
ALTER TABLE NMINC ADD DXJ COMPUTED BY (
/* DIAS DEL PERIODO ENTRE JORNADAS DE TRABAJO */
    CAST ((DP / JT)
        AS NUMERIC(18, 4))
    );
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'NMPRD_ID' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'NMPRD_A' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de status de nomina'  where RDB$FIELD_NAME = 'NMPRD_STTS_ID' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre o descripcion'  where RDB$FIELD_NAME = 'NMPRD_NOM' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de inicio'  where RDB$FIELD_NAME = 'NMPRD_FCINI' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha final del periodo'  where RDB$FIELD_NAME = 'NMPRD_FCFIN' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de pago'  where RDB$FIELD_NAME = 'NMPRD_FCPGO' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de autorizacion'  where RDB$FIELD_NAME = 'NMPRD_FCAUT' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'dias del periodo'  where RDB$FIELD_NAME = 'NMPRD_DSPRD' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de periodo'  where RDB$FIELD_NAME = 'NMPRD_TPPRD' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de nomina'  where RDB$FIELD_NAME = 'NMPRD_TP_ID' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de percepciones'  where RDB$FIELD_NAME = 'NMPRD_TPER' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de deducciones'  where RDB$FIELD_NAME = 'NMPRD_TDED' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total del fondo de ahorro'  where RDB$FIELD_NAME = 'NMPRD_TFDA' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'total de vales de despensa'  where RDB$FIELD_NAME = 'NMPRD_TVDD' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que autoriza la nomina'  where RDB$FIELD_NAME = 'NMPRD_USR_A' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario que cancela nomina'  where RDB$FIELD_NAME = 'NMPRD_USR_C' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'NMPRD_USR_N' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'NMPRD_FN' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima clave del usuario que modifica el registro'  where RDB$FIELD_NAME = 'NMPRD_USR_M' and RDB$RELATION_NAME = 'NMPRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion'  where RDB$FIELD_NAME = 'NMPRD_FM' and RDB$RELATION_NAME = 'NMPRD';
CREATE INDEX IDX_NMPRD_STTS_ID ON NMPRD (NMPRD_STTS_ID);
CREATE INDEX IDX_NMPRD_TP_ID ON NMPRD (NMPRD_TP_ID);
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con la tabla de concepto de nomina (percepciones o deducciones)'  where RDB$FIELD_NAME = 'NMRD_NMCNP_ID' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el empleado'  where RDB$FIELD_NAME = 'NMRD_EMP_ID' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion con el periodo de nomina'  where RDB$FIELD_NAME = 'NMRD_NMPRD_ID' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'tipo de registro vacaciones = 15, incapacidad = 16, falta o ausencia =  17'  where RDB$FIELD_NAME = 'NMRD_CTTP_ID' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha '  where RDB$FIELD_NAME = 'NMRD_FEC' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nota'  where RDB$FIELD_NAME = 'NMRD_NOTA' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'NMRD_USR_N' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'NMRD_FN' and RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'registro de faltas, incapacidades, vacaciones'
where RDB$RELATION_NAME = 'NMRD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice'  where RDB$FIELD_NAME = 'RLSUSR_ID' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RLSUSR_A' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'marca del usuario maestro'  where RDB$FIELD_NAME = 'RLSUSR_M' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave de rol'  where RDB$FIELD_NAME = 'RLSUSR_CLV' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'nombre del rol'  where RDB$FIELD_NAME = 'RLSUSR_NMBR' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'RLSUSR_FN' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'RLSUSR_USR_N' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'RLSUSR_FM' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifica el registro'  where RDB$FIELD_NAME = 'RLSUSR_USR_M' and RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'roles de usuarios'
where RDB$RELATION_NAME = 'RLSUSR';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de la tabla'  where RDB$FIELD_NAME = 'RLUSRL_ID' and RDB$RELATION_NAME = 'RLUSRL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'registro activo'  where RDB$FIELD_NAME = 'RLUSRL_A' and RDB$RELATION_NAME = 'RLUSRL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del usuario'  where RDB$FIELD_NAME = 'RLUSRL_USR_ID' and RDB$RELATION_NAME = 'RLUSRL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice de relacion del ROL'  where RDB$FIELD_NAME = 'RLUSRL_RLSUSR_ID' and RDB$RELATION_NAME = 'RLUSRL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador principal de la tabla.'  where RDB$FIELD_NAME = 'SBSD_ID' and RDB$RELATION_NAME = 'SBSD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador de la tabla ISR.'  where RDB$FIELD_NAME = 'SBSD_ISR_ID' and RDB$RELATION_NAME = 'SBSD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Para ingresos de.'  where RDB$FIELD_NAME = 'SBSD_PARA' and RDB$RELATION_NAME = 'SBSD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Hasta ingresos de.'  where RDB$FIELD_NAME = 'SBSD_HASTA' and RDB$RELATION_NAME = 'SBSD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Cantidad de subsidio para el empleo diario.'  where RDB$FIELD_NAME = 'SBSD_CNTDD' and RDB$RELATION_NAME = 'SBSD';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Anio al que pertenece la informacion del ISR.'  where RDB$FIELD_NAME = 'SM_ANIO' and RDB$RELATION_NAME = 'SM';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Valor del salario minimo.'  where RDB$FIELD_NAME = 'SM_SALARIO' and RDB$RELATION_NAME = 'SM';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice'  where RDB$FIELD_NAME = 'UIPERFIL_ID' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del menu de opciones'  where RDB$FIELD_NAME = 'UIPERFIL_UIMENU_ID' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'indice del rol de usuario'  where RDB$FIELD_NAME = 'UIPERFIL_RLSUSR_ID' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'acciones permitidas'  where RDB$FIELD_NAME = 'UIPERFIL_ACTION' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'llave del menu'  where RDB$FIELD_NAME = 'UIPERFIL_KEY' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'fecha de creacion del registro'  where RDB$FIELD_NAME = 'UIPERFIL_FN' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del usuario creador del registro'  where RDB$FIELD_NAME = 'UIPERFIL_USR_N' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'ultima fecha de modificacion del registro'  where RDB$FIELD_NAME = 'UIPERFIL_FM' and RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'clave del ultimo usuario que modifico el registro'  where RDB$FIELD_NAME = 'UIPERFIL_USR_M' and RDB$RELATION_NAME = 'UIPERFIL';
CREATE INDEX UIPERFIL_RLSUSR_ID ON UIPERFIL (UIPERFIL_RLSUSR_ID);
CREATE INDEX UIPERFIL_UIMENU_ID ON UIPERFIL (UIPERFIL_UIMENU_ID);
UPDATE RDB$RELATIONS set
RDB$DESCRIPTION = 'permisos del perfil de usuario'
where RDB$RELATION_NAME = 'UIPERFIL';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Identificador principal de la tabla.'  where RDB$FIELD_NAME = 'UMA_ID' and RDB$RELATION_NAME = 'UMA';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Anio al que pertenece la informacion del UMA.'  where RDB$FIELD_NAME = 'UMA_ANIO' and RDB$RELATION_NAME = 'UMA';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Valor diario del UMA.'  where RDB$FIELD_NAME = 'UMA_DIARIO' and RDB$RELATION_NAME = 'UMA';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Valor mensual del UMA.'  where RDB$FIELD_NAME = 'UMA_MENSUAL' and RDB$RELATION_NAME = 'UMA';
UPDATE RDB$RELATION_FIELDS set RDB$DESCRIPTION = 'Valor anual del UMA.'  where RDB$FIELD_NAME = 'UMA_ANUAL' and RDB$RELATION_NAME = 'UMA';
GRANT EXECUTE
 ON PROCEDURE CALCULAR_ISR TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE CALCULAR_SBE TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE NME_CAL TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE NME_CALPD TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE NME_CALPD_INC TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE NME_CAL_INC TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE NME_CREAR_NMINC TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE NME_DEL_INC TO  SYSDBA;

GRANT EXECUTE
 ON PROCEDURE NM_UPDATE_CONF TO  SYSDBA;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CLDF TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CONF TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTDPT TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTDPTP TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTEMP TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTEMPB TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTEMPC TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON CTEMPD TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON ISR TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON ISRC TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMAGU TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMCNF TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMCNP TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMEMP TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMHRE TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMINC TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMPRD TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON NMRD TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RLSUSR TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON RLUSRL TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON SBSD TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON SM TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON UIPERFIL TO  SYSDBA WITH GRANT OPTION;

GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON UMA TO  SYSDBA WITH GRANT OPTION;

