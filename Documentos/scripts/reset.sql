/* -------------------------------------------------------------------------------------------------------------
    RESET (ELIMINACION DE DATOS DE EMPLEADOS Y PERIODOS)
------------------------------------------------------------------------------------------------------------- */
DELETE FROM CTEMP;
DELETE FROM CTEMPC;
DELETE FROM NMINC;
-- REINICIAR CONTADORES
SET GENERATOR GEN_CTEMP_ID TO 0;
SET GENERATOR GEN_CTEMPC_ID TO 0;
SET GENERATOR GEN_NMINC_ID TO 0;