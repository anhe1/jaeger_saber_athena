﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de contrato de empleado
    /// </summary>
    public class SqlEmpleadoContratoRepository : RepositoryMaster<ContratoModel>, ISqlEmpleadoContratoRepository {
        public SqlEmpleadoContratoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(ContratoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTEMPC ( CTEMPC_ID, CTEMPC_A, CTEMPC_CTEMP_ID, CTEMPC_DEPTOID, CTEMPC_PSTID, CTEMPC_PPRD_ID, CTEMPC_TPREG, CTEMPC_RSGPST, CTEMPC_PRDDPG, CTEMPC_MTDPG, CTEMPC_TPCNTR, CTEMPC_TPJRN, CTEMPC_CLVBNC, CTEMPC_CTABAN, CTEMPC_CLABE, CTEMPC_CTAVAL, CTEMPC_SCRSL, CTEMPC_TPCTA, CTEMPC_RGP, CTEMPC_DEPTO, CTEMPC_PUESTO, CTEMPC_FCHREL, CTEMPC_FCHTERLAB, CTEMPC_BSCOT, CTEMPC_SB, CTEMPC_SBC, CTEMPC_SDI, CTEMPC_SD, CTEMPC_JRNTR, CTEMPC_JRNHRS, CTEMPC_FN, CTEMPC_USR_N) " +
                                           "VALUES (@CTEMPC_ID,@CTEMPC_A,@CTEMPC_CTEMP_ID,@CTEMPC_DEPTOID,@CTEMPC_PSTID,@CTEMPC_PPRD_ID,@CTEMPC_TPREG,@CTEMPC_RSGPST,@CTEMPC_PRDDPG,@CTEMPC_MTDPG,@CTEMPC_TPCNTR,@CTEMPC_TPJRN,@CTEMPC_CLVBNC,@CTEMPC_CTABAN,@CTEMPC_CLABE,@CTEMPC_CTAVAL,@CTEMPC_SCRSL,@CTEMPC_TPCTA,@CTEMPC_RGP,@CTEMPC_DEPTO,@CTEMPC_PUESTO,@CTEMPC_FCHREL,@CTEMPC_FCHTERLAB,@CTEMPC_BSCOT,@CTEMPC_SB,@CTEMPC_SBC,@CTEMPC_SDI,@CTEMPC_SD,@CTEMPC_JRNTR,@CTEMPC_JRNHRS,@CTEMPC_FN,@CTEMPC_USR_N)"
            };
            sqlCommand.Parameters.AddWithValue("@CTEMPC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_DEPTOID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PSTID", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PPRD_ID", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPREG", item.ClaveTipoRegimen);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_RSGPST", item.ClaveRiesgoPuesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PRDDPG", item.ClavePeriodicidadPago);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_MTDPG", item.ClaveMetodoPago);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPCNTR", item.ClaveTipoContrato);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPJRN", item.ClaveTipoJornada);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CLVBNC", item.ClaveBanco);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CTABAN", item.NumeroCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CLABE", item.Clabe);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CTAVAL", item.Vales);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SCRSL", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPCTA", item.IdTipoCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_RGP", item.RegistroPatronal);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_DEPTO", item.Departamento);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PUESTO", item.Puesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_FCHREL", item.FecInicioRelLaboral);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_FCHTERLAB", item.FecTerminoRelLaboral);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_BSCOT", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SB", item.SalarioBase);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SBC", item.SalarioBaseCotizacion);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SDI", item.SalarioDiarioIntegrado);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SD", item.SalarioDiario);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_JRNTR", item.Jornadas);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_JRNHRS", item.Horas);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_USR_N", item.Creo);
            item.IdContrato = this.ExecuteScalar(sqlCommand);
            if (item.IdContrato > 0)
                return item.IdContrato;
            return 0;
        }

        public int Update(ContratoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTEMPC SET CTEMPC_A = @CTEMPC_A, CTEMPC_CTEMP_ID = @CTEMPC_CTEMP_ID, CTEMPC_DEPTOID = @CTEMPC_DEPTOID, CTEMPC_PSTID = @CTEMPC_PSTID, CTEMPC_PPRD_ID = @CTEMPC_PPRD_ID, CTEMPC_TPREG = @CTEMPC_TPREG, CTEMPC_RSGPST = @CTEMPC_RSGPST, CTEMPC_PRDDPG = @CTEMPC_PRDDPG, CTEMPC_MTDPG = @CTEMPC_MTDPG, CTEMPC_TPCNTR = @CTEMPC_TPCNTR, CTEMPC_TPJRN = @CTEMPC_TPJRN, CTEMPC_CLVBNC = @CTEMPC_CLVBNC, CTEMPC_CTABAN = @CTEMPC_CTABAN, CTEMPC_CLABE = @CTEMPC_CLABE, CTEMPC_CTAVAL = @CTEMPC_CTAVAL, CTEMPC_SCRSL = @CTEMPC_SCRSL, CTEMPC_TPCTA = @CTEMPC_TPCTA, CTEMPC_RGP = @CTEMPC_RGP, CTEMPC_DEPTO = @CTEMPC_DEPTO, CTEMPC_PUESTO = @CTEMPC_PUESTO, CTEMPC_FCHREL = @CTEMPC_FCHREL, CTEMPC_FCHTERLAB = @CTEMPC_FCHTERLAB, CTEMPC_BSCOT = @CTEMPC_BSCOT, CTEMPC_SB = @CTEMPC_SB, CTEMPC_SBC = @CTEMPC_SBC, CTEMPC_SDI = @CTEMPC_SDI, CTEMPC_SD = @CTEMPC_SD, CTEMPC_JRNTR = @CTEMPC_JRNTR, CTEMPC_JRNHRS = @CTEMPC_JRNHRS, CTEMPC_FM = @CTEMPC_FM, CTEMPC_USR_M = @CTEMPC_USR_M WHERE ((CTEMPC_ID = @CTEMPC_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTEMPC_ID", item.IdContrato);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_DEPTOID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PSTID", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PPRD_ID", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPREG", item.ClaveTipoRegimen);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_RSGPST", item.ClaveRiesgoPuesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PRDDPG", item.ClavePeriodicidadPago);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_MTDPG", item.ClaveMetodoPago);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPCNTR", item.ClaveTipoContrato);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPJRN", item.ClaveTipoJornada);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CLVBNC", item.ClaveBanco);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CTABAN", item.NumeroCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CLABE", item.Clabe);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_CTAVAL", item.Vales);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SCRSL", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_TPCTA", item.IdTipoCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_RGP", item.RegistroPatronal);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_DEPTO", item.Departamento);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_PUESTO", item.Puesto);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_FCHREL", item.FecInicioRelLaboral);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_FCHTERLAB", item.FecTerminoRelLaboral);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_BSCOT", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SB", item.SalarioBase);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SBC", item.SalarioDiarioIntegrado);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SDI", item.SalarioDiarioIntegrado);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_SD", item.SalarioDiario);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_JRNTR", item.Jornadas);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_JRNHRS", item.Horas);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CTEMPC_USR_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTEMPC WHERE CTEMPC_ID = @CTEMPC_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTEMPC_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public ContratoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CTEMPC WHERE CTEMPC_ID = @CTEMPC_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTEMPC_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        /// <summary>
        /// desactivar contrato
        /// </summary>
        public bool Remove(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE CTEMPC SET CTEMPC_A = 0 WHERE CTEMPC_ID = @CTEMPC_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTEMPC_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public IEnumerable<ContratoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CTEMPC "
            };
            return this.GetMapper(sqlCommand).ToList();
        }
        #endregion
        
        public IContratoModel Save(IContratoModel model) {
            if (model.IdContrato == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.IdContrato = this.Insert(model as ContratoModel);
            } else {
                model.Modifica = this.User;
                model.FechaModifica = DateTime.Now;
                this.Update(model as ContratoModel);
            }
            return model;
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1: class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM CTEMPC @wcondiciones"
            };

            if (typeof(T1) == typeof(ContratoDetailModel)) {
                sqlCommand.CommandText = @"SELECT * FROM CTEMPC LEFT JOIN CTEMP ON CTEMPC.CTEMPC_CTEMP_ID = CTEMP.CTEMP_ID WHERE CTEMP_A = 1 @condiciones";
            }
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
    }
}
