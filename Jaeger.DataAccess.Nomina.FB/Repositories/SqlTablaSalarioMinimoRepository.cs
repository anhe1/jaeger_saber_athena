﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de la tabla Salario Mínimo.
    /// </summary>
    public class SqlTablaSalarioMinimoRepository : RepositoryMaster<SalarioMinimoModel>, ISqlTablaSalarioMinimoRepository {
        public SqlTablaSalarioMinimoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = "DELETE FROM SM WHERE ((SM_ANIO = @p1))" };
            sqlCommand.Parameters.AddWithValue("@SM_ANIO", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(SalarioMinimoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "INSERT INTO SM (SM_ANIO, SM_SALARIO) VALUES (@SM_ANIO, @SM_SALARIO) RETURNING SM_ANIO"
            };
            sqlCommand.Parameters.AddWithValue("@SM_ANIO", item.Anio);
            sqlCommand.Parameters.AddWithValue("@SM_SALARIO", item.Valor);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(SalarioMinimoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE SM SET SM_SALARIO = @SM_SALARIO WHERE ((SM_ANIO = @SM_ANIO))"
            };
            sqlCommand.Parameters.AddWithValue("@SM_ANIO", item.Anio);
            sqlCommand.Parameters.AddWithValue("@SM_SALARIO", item.Valor);

            return this.ExecuteTransaction(sqlCommand);
        }

        public SalarioMinimoModel GetById(int index) {
            var sqlCommand = new FbCommand { CommandText = "SELECT FIRST 1 SM.* FROM SM WHERE ((SM_ANIO = @INDEX))" };
            sqlCommand.Parameters.AddWithValue("@INDEX", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<SalarioMinimoModel> GetList() {
            var sqlCommand = new FbCommand { CommandText = "SELECT * FROM SM ORDER BY SM_ANIO DESC" };
            return this.GetMapper(sqlCommand);
        }
        #endregion

        public SalarioMinimoModel Save(SalarioMinimoModel model) {
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }

        public SalarioMinimoModel GetByEjercicio(int year) {
            var sqlCommand = new FbCommand { CommandText = "SELECT FIRST 1 SM.* FROM SM WHERE ((SM_ANIO = @INDEX))" };
            sqlCommand.Parameters.AddWithValue("@INDEX", year);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }
    }
}

