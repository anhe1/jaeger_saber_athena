﻿using System;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de la tabla Registro de Ausencias.
    /// </summary>
    public class SqlRegistroAusenciasRepository : RepositoryMaster<RegistroAusenciasModel>, ISqlRegistroAusenciasRepository {
        public SqlRegistroAusenciasRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE NMVAC WHERE NMVAC_ID=@INDICE"
            };
            sqlCommand.Parameters.AddWithValue("@INDICE", index);
            return this.ExecuteScalar(sqlCommand) > 0;
        }

        public int Insert(RegistroAusenciasModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "INSERT INTO NMVAC (NMVAC_ID, NMVAC_A, NMVAC_NMCNP_ID, NMVAC_EMP_ID, NMVAC_NOM_ID, NMVAC_TP, NMVAC_FEC, NMVAC_CANT, NMVAC_NOTA, NMVAC_USR_N, NMVAC_FN" +
                                        "VALUES (@NMVAC_ID,@NMVAC_A,@NMVAC_NMCNP_ID,@NMVAC_EMP_ID,@NMVAC_NOM_ID,@NMVAC_TP,@NMVAC_FEC,@NMVAC_CANT,@NMVAC_NOTA,@NMVAC_USR_N,@NMVAC_FN) RETURNING NMVAC_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@NMVAC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMVAC_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMVAC_EMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@NMVAC_NOM_ID", item.IdNomina);
            sqlCommand.Parameters.AddWithValue("@NMVAC_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NMVAC_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NMVAC_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NMVAC_FN", item.FechaNuevo);

            return 0;
        }

        public int Update(RegistroAusenciasModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE NMVAC SET NMVAC_A=@NMVAC_A, NMVAC_NMCNP_ID=@NMVAC_NMCNP_ID, NMVAC_EMP_ID=@NMVAC_EMP_ID, NMVAC_NOM_ID=@NMVAC_NOM_ID, NMVAC_TP=@NMVAC_TP, NMVAC_FEC=@NMVAC_FEC, NMVAC_CANT=@NMVAC_CANT, NMVAC_NOTA=@NMVAC_NOTA, NMVAC_USR_M=@NMVAC_USR_M, NMVAC_FM=@NMVAC_FM WHERE NMVAC_ID = @INDICE;"
            };
            sqlCommand.Parameters.AddWithValue("@NMVAC_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMVAC_EMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@NMVAC_NOM_ID", item.IdNomina);
            sqlCommand.Parameters.AddWithValue("@NMVAC_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NMVAC_NOTA", item.Nota);
            return this.ExecuteTransaction(sqlCommand);
        }

        public RegistroAusenciasModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT NMVAC.* FROM NMVAC WHERE NMVAC_ID = @INDICE"
            };
            sqlCommand.Parameters.AddWithValue("@INDICE", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RegistroAusenciasModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<RegistroAusenciasModel> GetList() {
            return this.GetList<RegistroAusenciasModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT NMRD.* FROM NMRD @condiciones"
            };
            // DIAS FESTIVOS
            if (typeof(T1) == typeof(DiaFestivoModel)) {
                sqlCommand.CommandText = "SELECT CLDF.* FROM CLDF @wcondiciones";
            } else if (typeof(T1) == typeof(EmpleadoRegistroAusencias)) {
                sqlCommand.CommandText = "SELECT CTEMP.*, NMRD.* FROM CTEMP LEFT JOIN NMRD ON NMRD.NMRD_EMP_ID = CTEMP.CTEMP_ID @wcondiciones";
            } else if (typeof(T1) == typeof(EmpleadoVacacionesModel)) {
                sqlCommand.CommandText = @"SELECT * FROM CTEMP LEFT JOIN CTEMPC ON CTEMPC.CTEMPC_CTEMP_ID = CTEMP.CTEMP_ID AND CTEMPC.CTEMPC_A = 1 ";
            }
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IRegistroAusenciasModel Salveable(IRegistroAusenciasModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE OR INSERT INTO NMRD (NMRD_NMCNP_ID, NMRD_EMP_ID, NMRD_NMPRD_ID, NMRD_CTTP_ID, NMRD_FEC, NMRD_NOTA, NMRD_USR_N)" +
                                                  "VALUES(@NMRD_NMCNP_ID,@NMRD_EMP_ID,@NMRD_NMPRD_ID,@NMRD_CTTP_ID,@NMRD_FEC,@NMRD_NOTA,@NMRD_USR_N) MATCHING (NMRD_EMP_ID, NMRD_NMPRD_ID, NMRD_CTTP_ID, NMRD_FEC);"
            };
            item.Creo = this.User;
            sqlCommand.Parameters.AddWithValue("@NMRD_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMRD_EMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@NMRD_NMPRD_ID", item.IdNomina);
            sqlCommand.Parameters.AddWithValue("@NMRD_CTTP_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NMRD_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NMRD_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NMRD_USR_N", item.Creo);
            this.ExecuteNoQuery(sqlCommand);
            return item;
        }

        public IRegistroAusenciasModel Save(IRegistroAusenciasModel model) {
            return this.Salveable(model);
        }

        public List<IRegistroAusenciasModel> Save(List<IRegistroAusenciasModel> models) {
            foreach (var model in models) {
                this.Salveable(model);
            }
            return models;
        }
    }
}
