﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.ValueObjects;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de tabla de impuesto sobre la renta.
    /// </summary>
    public class SqlTablaImpuestoSobreRentaRepository : RepositoryMaster<TablaImpuestoSobreRentaModel>, ISqlTablaImpuestoSobreRentaRepository {
        public SqlTablaImpuestoSobreRentaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = "DELETE FROM ISR WHERE ((ISR_ID = @p1))" };
            sqlCommand.Parameters.AddWithValue("@ISR_ID", index);
            return true;
        }

        public int Insert(TablaImpuestoSobreRentaModel item) {
            var sqlCommand = new FbCommand { CommandText = "INSERT INTO ISR (ISR_ID, ISR_ANIO, ISR_PRD) VALUES (@ISR_ID, @ISR_ANIO, @ISR_PRD) RETURNING ISR_ID" };
            sqlCommand.Parameters.AddWithValue("@ISR_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@ISR_ANIO", item.Anio);
            sqlCommand.Parameters.AddWithValue("@ISR_PRD", item.Periodo);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(TablaImpuestoSobreRentaModel item) {
            var sqlCommand = new FbCommand { CommandText = "UPDATE ISR SET ISR_ID = @ISR_ID, ISR_ANIO = @ISR_ANIO, ISR_PRD = @ISR_PRD WHERE ((ISR_ID = @p4))" };
            sqlCommand.Parameters.AddWithValue("@ISR_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@ISR_ANIO", item.Anio);
            sqlCommand.Parameters.AddWithValue("@ISR_PRD", item.Periodo);
            return this.ExecuteTransaction(sqlCommand);
        }

        public int Insert(ImpuestoSobreRentaModel item) {
            var sqlCommand = new FbCommand { CommandText = "INSERT INTO ISRC (ISRC_ID, ISRC_ISR_ID, ISRC_LI, ISRC_LS, ISRC_CUOTA, ISRC_EXC) VALUES (@ISRC_ID, @ISRC_ISR_ID, @ISRC_LI, @ISRC_LS, @ISRC_CUOTA, @ISRC_EXC) RETURNING ISR_ID" };
            sqlCommand.Parameters.AddWithValue("@ISRC_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@ISRC_ISR_ID", item.SubId);
            sqlCommand.Parameters.AddWithValue("@ISRC_LI", item.LimiteInferior);
            sqlCommand.Parameters.AddWithValue("@ISRC_LS", item.LimiteSuperior);
            sqlCommand.Parameters.AddWithValue("@ISRC_CUOTA", item.CuotaFija);
            sqlCommand.Parameters.AddWithValue("@ISRC_EXC", item.PorcentajeExcedente);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(ImpuestoSobreRentaModel item) {
            var sqlCommand = new FbCommand { CommandText = "UPDATE ISRC SET ISRC_ID = @ISRC_ID, ISRC_ISR_ID = @ISRC_ISR_ID, ISRC_LI = @ISRC_LI, ISRC_LS = @ISRC_LS, ISRC_CUOTA = @ISRC_CUOTA, ISRC_EXC = @ISRC_EXC WHERE ((ISRC_ID = @p7))" };
            sqlCommand.Parameters.AddWithValue("@ISRC_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@ISRC_ISR_ID", item.SubId);
            sqlCommand.Parameters.AddWithValue("@ISRC_LI", item.LimiteInferior);
            sqlCommand.Parameters.AddWithValue("@ISRC_LS", item.LimiteSuperior);
            sqlCommand.Parameters.AddWithValue("@ISRC_CUOTA", item.CuotaFija);
            sqlCommand.Parameters.AddWithValue("@ISRC_EXC", item.PorcentajeExcedente);
            return this.ExecuteTransaction(sqlCommand);
        }

        public TablaImpuestoSobreRentaModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 ISR.* FROM ISR WHERE ISR_ID = @ISR_ID"
            };
            sqlCommand.Parameters.AddWithValue("@ISR_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        /// <summary>
        /// Obtiene una tabla de impuesto sobre la renta por año y periodo.
        /// </summary>
        /// <param name="year">año</param>
        /// <param name="periodo">periodo</param>
        /// <returns>ITablaImpuestoSobreRentaModel</returns>
        public TablaImpuestoSobreRentaModel GetById(int year, int periodo) {
            return this.GetList(new List<IConditional> { new Conditional("ISR_ANIO", year.ToString()), new Conditional("ISR_PRD", periodo.ToString()) }).FirstOrDefault();
        }

        public IEnumerable<TablaImpuestoSobreRentaModel> GetList() {
            var sqlCommand = new FbCommand { CommandText = "SELECT * FROM ISR ORDER BY ISR_ANIO DESC" };
            var _result = this.GetMapper(sqlCommand).ToList();
            if (_result != null) {
                var _ids = _result.Select(x => x.Id).ToArray();
                var _partes = this.GetList1(new List<IConditional> { new Conditional("ISRC_ISR_ID", string.Join(",", _ids), ConditionalTypeEnum.In) });
                if (_partes != null) {
                    for (int i = 0; i < _result.Count; i++) {
                        _result[i].Rangos = new BindingList<ImpuestoSobreRentaModel>(_partes.Where(it => it.SubId == _result[i].Id).ToList());
                    }
                }
            }
            return _result;
        }
        #endregion

        /// <summary>
        /// Obtiene una lista de tablas de impuesto sobre la renta.
        /// </summary>
        /// <param name="conditionals">condicionales</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<TablaImpuestoSobreRentaModel> GetList(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand { CommandText = "SELECT * FROM ISR @condiciones ORDER BY ISR_ANIO DESC" };
            sqlCommand = this.GetConditional(sqlCommand, conditionals);
            var _result = this.GetMapper(sqlCommand).ToList();
            if (_result != null) {
                var _ids = _result.Select(x => x.Id).ToArray();
                var _partes = this.GetList1(new List<IConditional> { new Conditional("ISRC_ISR_ID", string.Join(",", _ids), ConditionalTypeEnum.In) });
                if (_partes != null) {
                    for (int i = 0; i < _result.Count; i++) {
                        _result[i].Rangos = new BindingList<ImpuestoSobreRentaModel>(_partes.Where(it => it.SubId == _result[i].Id).ToList());
                    }
                }
            }
            return _result;
        }

        /// <summary>
        /// Guarda una tabla de impuesto sobre la renta.
        /// </summary>
        /// <param name="tabla">List</param>
        /// <returns>List</returns>
        public BindingList<TablaImpuestoSobreRentaModel> Save(BindingList<TablaImpuestoSobreRentaModel> tablas) {
            for (int i = 0; i < tablas.Count; i++) {
                tablas[i] = this.Save(tablas[i]);
            }
            return tablas;
        }

        private IEnumerable<ImpuestoSobreRentaModel> GetList1(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand { CommandText = "SELECT * FROM ISRC @condiciones" };
            sqlCommand = this.GetConditional(sqlCommand, conditionals);
            return this.GetMapper<ImpuestoSobreRentaModel>(sqlCommand);
        }

        private TablaImpuestoSobreRentaModel Save(TablaImpuestoSobreRentaModel tabla) {
            if (tabla.Id == 0) {
                tabla.Id = this.Insert(tabla);
                tabla.Rangos = this.Save(tabla.Rangos, tabla.Id);
            } else {
                this.Update(tabla);
                tabla.Rangos = this.Save(tabla.Rangos, tabla.Id);
            }
            return tabla;
        }

        private BindingList<ImpuestoSobreRentaModel> Save(BindingList<ImpuestoSobreRentaModel> rangos, int idTabla) {
            for (int i = 0; i < rangos.Count; i++) {
                rangos[i].SubId = idTabla;
                if (rangos[i].Id == 0)
                    this.Insert(rangos[i]);
                else
                    this.Update(rangos[i]);
            }
            return rangos;
        }
    }
}