﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    public class SqlEmpleadoRepository : RepositoryMaster<EmpleadoModel>, ISqlEmpleadoRepository {
        public SqlEmpleadoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(EmpleadoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO CTEMP ( CTEMP_ID, CTEMP_A, CTEMP_DRCTR_ID, CTEMP_CLV, CTEMP_RFC, CTEMP_NSS, CTEMP_UMF, CTEMP_FONAC, CTEMP_CURP, CTEMP_AFORE, CTEMP_ENTFED, CTEMP_LGNAC, CTEMP_NAC, CTEMP_NOM, CTEMP_PAPE, CTEMP_SAPE, CTEMP_MAIL, CTEMP_SITIO, CTEMP_FNAC, CTEMP_SEXO, CTEMP_ECVL, CTEMP_TLFN, CTEMP_TLFN2, CTEMP_IMG, CTEMP_FN, CTEMP_USR_N) 
                                           VALUES (@CTEMP_ID,@CTEMP_A,@CTEMP_DRCTR_ID,@CTEMP_CLV,@CTEMP_RFC,@CTEMP_NSS,@CTEMP_UMF,@CTEMP_FONAC,@CTEMP_CURP,@CTEMP_AFORE,@CTEMP_ENTFED,@CTEMP_LGNAC,@CTEMP_NAC,@CTEMP_NOM,@CTEMP_PAPE,@CTEMP_SAPE,@CTEMP_MAIL,@CTEMP_SITIO,@CTEMP_FNAC,@CTEMP_SEXO,@CTEMP_ECVL,@CTEMP_TLFN,@CTEMP_TLFN2,@CTEMP_IMG,@CTEMP_FN,@CTEMP_USR_N) RETURNING CTEMP_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@CTEMP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMP_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@CTEMP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTEMP_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@CTEMP_NSS", item.NSS);
            sqlCommand.Parameters.AddWithValue("@CTEMP_UMF", item.UMF);
            sqlCommand.Parameters.AddWithValue("@CTEMP_FONAC", item.FONACOT);
            sqlCommand.Parameters.AddWithValue("@CTEMP_CURP", item.CURP);
            sqlCommand.Parameters.AddWithValue("@CTEMP_AFORE", item.AFORE);
            sqlCommand.Parameters.AddWithValue("@CTEMP_ENTFED", item.IdEntidadFed);
            sqlCommand.Parameters.AddWithValue("@CTEMP_LGNAC", item.LugarNacimiento);
            sqlCommand.Parameters.AddWithValue("@CTEMP_NAC", item.Nacionalidad);
            sqlCommand.Parameters.AddWithValue("@CTEMP_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@CTEMP_PAPE", item.ApellidoPaterno);
            sqlCommand.Parameters.AddWithValue("@CTEMP_SAPE", item.ApellidoMaterno);
            sqlCommand.Parameters.AddWithValue("@CTEMP_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@CTEMP_SITIO", item.Sitio);
            sqlCommand.Parameters.AddWithValue("@CTEMP_FNAC", item.FecNacimiento);
            sqlCommand.Parameters.AddWithValue("@CTEMP_SEXO", item.IdGenero);
            sqlCommand.Parameters.AddWithValue("@CTEMP_ECVL", item.IdEdoCivil);
            sqlCommand.Parameters.AddWithValue("@CTEMP_TLFN", item.Telefono1);
            sqlCommand.Parameters.AddWithValue("@CTEMP_TLFN2", item.Telefono2);
            sqlCommand.Parameters.AddWithValue("@CTEMP_IMG", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTEMP_USR_N", item.Creo);
            item.IdEmpleado = this.ExecuteScalar(sqlCommand);
            if (item.IdEmpleado > 0)
                return item.IdEmpleado;
            return 0;
        }

        public int Update(EmpleadoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE CTEMP SET CTEMP_ID = @CTEMP_ID, CTEMP_A = @CTEMP_A, CTEMP_DRCTR_ID = @CTEMP_DRCTR_ID, CTEMP_CLV = @CTEMP_CLV, CTEMP_RFC = @CTEMP_RFC, CTEMP_NSS = @CTEMP_NSS, CTEMP_UMF = @CTEMP_UMF, CTEMP_FONAC = @CTEMP_FONAC, CTEMP_CURP = @CTEMP_CURP, CTEMP_AFORE = @CTEMP_AFORE, CTEMP_ENTFED = @CTEMP_ENTFED, CTEMP_LGNAC = @CTEMP_LGNAC, CTEMP_NAC = @CTEMP_NAC, CTEMP_NOM = @CTEMP_NOM, CTEMP_PAPE = @CTEMP_PAPE, CTEMP_SAPE = @CTEMP_SAPE, CTEMP_MAIL = @CTEMP_MAIL, CTEMP_SITIO = @CTEMP_SITIO, CTEMP_FNAC = @CTEMP_FNAC, CTEMP_SEXO = @CTEMP_SEXO, CTEMP_ECVL = @CTEMP_ECVL, CTEMP_TLFN = @CTEMP_TLFN, CTEMP_TLFN2 = @CTEMP_TLFN2, CTEMP_IMG = @CTEMP_IMG, CTEMP_FM = @CTEMP_FM, CTEMP_USR_M = @CTEMP_USR_M WHERE ((CTEMP_ID = @CTEMP_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@CTEMP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMP_DRCTR_ID", item.IdDirectorio);
            sqlCommand.Parameters.AddWithValue("@CTEMP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTEMP_RFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@CTEMP_NSS", item.NSS);
            sqlCommand.Parameters.AddWithValue("@CTEMP_UMF", item.UMF);
            sqlCommand.Parameters.AddWithValue("@CTEMP_FONAC", item.FONACOT);
            sqlCommand.Parameters.AddWithValue("@CTEMP_CURP", item.CURP);
            sqlCommand.Parameters.AddWithValue("@CTEMP_AFORE", item.AFORE);
            sqlCommand.Parameters.AddWithValue("@CTEMP_ENTFED", item.IdEntidadFed);
            sqlCommand.Parameters.AddWithValue("@CTEMP_LGNAC", item.LugarNacimiento);
            sqlCommand.Parameters.AddWithValue("@CTEMP_NAC", item.Nacionalidad);
            sqlCommand.Parameters.AddWithValue("@CTEMP_NOM", item.Nombre);
            sqlCommand.Parameters.AddWithValue("@CTEMP_PAPE", item.ApellidoPaterno);
            sqlCommand.Parameters.AddWithValue("@CTEMP_SAPE", item.ApellidoMaterno);
            sqlCommand.Parameters.AddWithValue("@CTEMP_MAIL", item.Correo);
            sqlCommand.Parameters.AddWithValue("@CTEMP_SITIO", item.Sitio);
            sqlCommand.Parameters.AddWithValue("@CTEMP_FNAC", item.FecNacimiento);
            sqlCommand.Parameters.AddWithValue("@CTEMP_SEXO", item.IdGenero);
            sqlCommand.Parameters.AddWithValue("@CTEMP_ECVL", item.IdEdoCivil);
            sqlCommand.Parameters.AddWithValue("@CTEMP_TLFN", item.Telefono1);
            sqlCommand.Parameters.AddWithValue("@CTEMP_TLFN2", item.Telefono2);
            sqlCommand.Parameters.AddWithValue("@CTEMP_IMG", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMP_FM", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTEMP_USR_M", item.Creo);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTEMP WHERE = @INDEX"
            };
            sqlCommand.Parameters.AddWithValue("@INDEX", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public EmpleadoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM CTEMP WHERE = @INDEX"
            };
            sqlCommand.Parameters.AddWithValue("@INDEX", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }
        #endregion

        public IEmpleadoDetailModel Save(IEmpleadoDetailModel model) {
            if (model.IdEmpleado == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdEmpleado = this.Insert(model as EmpleadoModel);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model as EmpleadoModel);
            }
            return model;
        }

        public IEnumerable<EmpleadoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM CTEMP"
            };
            return this.GetMapper(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CTEMP.*, CTEMPC.* FROM CTEMP LEFT JOIN CTEMPC ON CTEMP_ID = CTEMPC_CTEMP_ID AND CTEMPC_A = 1 @condiciones"
            };
            
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, conditionals)).ToList();
        }
    }
}
