﻿using System;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Services.Mapping;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de horas extra
    /// </summary>
    public class SqlNominaHorasExtraRepository : RepositoryMaster<RegistroHoraExtraModel>, ISqlNominaHorasExtraRepository {
        public SqlNominaHorasExtraRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE NMHRE WHERE NMHRE_ID=@INDICE"
            };
            sqlCommand.Parameters.AddWithValue("@INDICE", index);
            return this.ExecuteScalar(sqlCommand) > 0;
        }

        public int Insert(RegistroHoraExtraModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "INSERT INTO NMHRE (NMHRE_ID, NMHRE_A, NMHRE_NMCNP_ID, NMHRE_EMP_ID, NMHRE_NOM_ID, NMHRE_TP, NMHRE_FEC, NMHRE_CANT, NMHRE_NOTA, NMHRE_USR_N, NMHRE_FN)" +
                                        "VALUES (@NMHRE_ID,@NMHRE_A,@NMHRE_NMCNP_ID,@NMHRE_EMP_ID,@NMHRE_NOM_ID,@NMHRE_TP,@NMHRE_FEC,@NMHRE_CANT,@NMHRE_NOTA,@NMHRE_USR_N,@NMHRE_FN) RETURNING NMHRE_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@NMHRE_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMHRE_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NMHRE_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMHRE_EMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@NMHRE_NOM_ID", item.IdNomina);
            sqlCommand.Parameters.AddWithValue("@NMHRE_TP", item.IdTipoHora);
            sqlCommand.Parameters.AddWithValue("@NMHRE_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NMHRE_CANT", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@NMHRE_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NMHRE_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NMHRE_FN", item.FechaNuevo);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0) {
                return item.Id;
            }
            return 0;
        }

        public int Update(RegistroHoraExtraModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE NMHRE SET NMHRE_A=@NMHRE_A, NMHRE_NMCNP_ID=@NMHRE_NMCNP_ID, NMHRE_EMP_ID=@NMHRE_EMP_ID, NMHRE_NOM_ID=@NMHRE_NOM_ID, NMHRE_TP=@NMHRE_TP, NMHRE_FEC=@NMHRE_FEC, NMHRE_CANT=@NMHRE_CANT, NMHRE_NOTA=@NMHRE_NOTA, NMHRE_USR_M=@NMHRE_USR_M, NMHRE_FM=@NMHRE_FM WHERE NMHRE_ID = @INDICE;"
            };
            sqlCommand.Parameters.AddWithValue("@INDICE", item.Id);
            sqlCommand.Parameters.AddWithValue("@NMHRE_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NMHRE_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMHRE_EMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@NMHRE_NOM_ID", item.IdNomina);
            sqlCommand.Parameters.AddWithValue("@NMHRE_TP", item.IdTipoHora);
            sqlCommand.Parameters.AddWithValue("@NMHRE_FEC", item.Fecha);
            sqlCommand.Parameters.AddWithValue("@NMHRE_CANT", item.Cantidad);
            sqlCommand.Parameters.AddWithValue("@NMHRE_NOTA", item.Nota);
            sqlCommand.Parameters.AddWithValue("@NMHRE_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@NMHRE_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }
        
        public RegistroHoraExtraModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT NMHRE.* FROM NMHRE WHERE NMHRE_ID = @INDICE"
            };
            sqlCommand.Parameters.AddWithValue("@INDICE", index);
            var tabla = this.ExecuteReader(sqlCommand);
            var mapper = new DataNamesMapper<RegistroHoraExtraModel>();
            return mapper.Map(tabla).SingleOrDefault();
        }

        public IEnumerable<RegistroHoraExtraModel> GetList() {
            return this.GetList<RegistroHoraExtraModel>(new List<IConditional>());
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT NMHRE.* FROM NMHRE @condiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public IRegistroHoraExtraModel Save(IRegistroHoraExtraModel model) {
            if (model.Id == 0) {
                model.Creo = this.User;
                model.FechaNuevo = DateTime.Now;
                model.Id = this.Insert(model as RegistroHoraExtraModel);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model as RegistroHoraExtraModel);
            }
            return model;
        }
    }
}
