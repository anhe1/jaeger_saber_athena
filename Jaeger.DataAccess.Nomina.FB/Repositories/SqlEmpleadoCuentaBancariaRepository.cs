﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FireBird.Services;
using Jaeger.DataAccess.Abstractions;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de cuenta bancaria de empleado
    /// </summary>
    public class SqlEmpleadoCuentaBancariaRepository : RepositoryMaster<EmpleadoCuentaBancariaModel>, ISqlEmpleadoCuentaBancariaRepository {
        public SqlEmpleadoCuentaBancariaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM CTEMPB WHERE ((CTEMPB_ID = @CTEMPB_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTEMPB_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(EmpleadoCuentaBancariaModel item) {
            var sqlCommand = new FbCommand { CommandText = @"INSERT INTO CTEMPB (CTEMPB_ID, CTEMPB_A, CTEMPB_VRFCD, CTEMPB_CTEMP_ID, CTEMPB_CRGMX, CTEMPB_CLV, CTEMPB_REFNUM, CTEMPB_MND, CTEMPB_SCRSL, CTEMPB_TIPO, CTEMPB_REFALF, CTEMPB_NMCTA, CTEMPB_CLABE, CTEMPB_NMCRT, CTEMPB_BANCO, CTEMPB_BENEF, CTEMPB_BRFC, CTEMPB_FN, CTEMPB_USR_N) VALUES (@CTEMPB_ID, @CTEMPB_A, @CTEMPB_VRFCD, @CTEMPB_CTEMP_ID, @CTEMPB_CRGMX, @CTEMPB_CLV, @CTEMPB_REFNUM, @CTEMPB_MND, @CTEMPB_SCRSL, @CTEMPB_TIPO, @CTEMPB_REFALF, @CTEMPB_NMCTA, @CTEMPB_CLABE, @CTEMPB_NMCRT, @CTEMPB_BANCO, @CTEMPB_BENEF, CTEMPB_BRFC, @CTEMPB_FN, @CTEMPB_USR_N) RETURNING CTEMPB_ID" };
            sqlCommand.Parameters.AddWithValue("@CTEMPB_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_VRFCD", item.Verificado);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CRGMX", item.CargoMaximo);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_REFNUM", item.RefNumerica);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_MND", item.Moneda);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_SCRSL", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_TIPO", item.IdTipoCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_REFALF", item.RefAlfanumerica);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_NMCTA", item.NumCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CLABE", item.Clabe);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_NMCRT", item.InsitucionBancaria);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_BANCO", item.Banco);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_BENEF", item.Beneficiario);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_BRFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_USR_N", item.Creo);
            item.IdCuentaB = this.ExecuteScalar(sqlCommand);
            if (item.IdCuentaB > 0)
                return item.IdCuentaB;
            return 0;
        }

        public int Update(EmpleadoCuentaBancariaModel item) {
            var sqlCommand = new FbCommand { CommandText = @"UPDATE CTEMPB SET CTEMPB_A = @CTEMPB_A, CTEMPB_VRFCD = @CTEMPB_VRFCD, CTEMPB_CTEMP_ID = @CTEMPB_CTEMP_ID, CTEMPB_CRGMX = @CTEMPB_CRGMX, CTEMPB_CLV = @CTEMPB_CLV, CTEMPB_REFNUM = @CTEMPB_REFNUM, CTEMPB_MND = @CTEMPB_MND, CTEMPB_SCRSL = @CTEMPB_SCRSL, CTEMPB_TIPO = @CTEMPB_TIPO, CTEMPB_REFALF = @CTEMPB_REFALF, CTEMPB_NMCTA = @CTEMPB_NMCTA, CTEMPB_CLABE = @CTEMPB_CLABE, CTEMPB_NMCRT = @CTEMPB_NMCRT, CTEMPB_BANCO = @CTEMPB_BANCO, CTEMPB_BENEF = @CTEMPB_BENEF, CTEMPB_BRFC = @CTEMPB_BRFC, CTEMPB_FM = @CTEMPB_FM, CTEMPB_USR_M = @CTEMPB_USR_M WHERE ((CTEMPB_ID = @CTEMPB_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTEMPB_ID", item.IdCuentaB);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_VRFCD", item.Verificado);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CRGMX", item.CargoMaximo);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_REFNUM", item.RefNumerica);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_MND", item.Moneda);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_SCRSL", item.Sucursal);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_TIPO", item.IdTipoCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_REFALF", item.RefAlfanumerica);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_NMCTA", item.NumCuenta);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_CLABE", item.Clabe);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_NMCRT", item.InsitucionBancaria);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_BANCO", item.Banco);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_BENEF", item.Beneficiario);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_BRFC", item.RFC);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_FM", item.FechaMod);
            sqlCommand.Parameters.AddWithValue("@CTEMPB_USR_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }
        
        public EmpleadoCuentaBancariaModel GetById(int index) {
            var sqlCommand = new FbCommand { CommandText = @"SELECT FIRST 1 CTEMPB.* FROM CTEMPB WHERE ((CTEMPB_ID = @CTEMPB_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTEMPB_ID", index);
            return this.GetMapper<EmpleadoCuentaBancariaModel>(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<EmpleadoCuentaBancariaModel> GetList() {
            var sqlCommand = new FbCommand { CommandText = @"SELECT CTEMPB.* FROM CTEMPB" };
            return this.GetMapper(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> condiciones) where T1 : class, new() {
            var sqlCommand = new FbCommand { CommandText = @"SELECT CTEMPB.* FROM CTEMPB @wcondiciones" };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, condiciones)).ToList();
        }

        public EmpleadoCuentaBancariaModel Save(EmpleadoCuentaBancariaModel model) {
            if (model.IdCuentaB == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdCuentaB = this.Insert(model);
                model.SetModified = false;
            } else {
                model.Modifica = this.User;
                model.FechaMod = DateTime.Now;
                if (this.Update(model) > 0)
                    model.SetModified = false;
            }
            return model;
        }
    }
}