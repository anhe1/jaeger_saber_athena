﻿using System;
using FirebirdSql.Data.FirebirdClient;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// repositorio de calculo de nomina
    /// </summary>
    public class SqlNominaCalRepository : RepositoryMaster<NominaPeriodoEmpleadoModel>, ISqlNominaCalRepository {
        public SqlNominaCalRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) {
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM NMCAL WHERE ((NMCAL_ID = @index))" };
            sqlCommand.Parameters.AddWithValue("@index", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(NominaPeriodoEmpleadoModel item) {
            var sqlCommand = new FbCommand { CommandText = "INSERT INTO NMCAL (NMCAL_ID, NMCAL_PRD_ID, EMP_ID, EMP_CLV, FCC, FIRL, BPP, HRJR, HRLJ, HRAT, EFI, INC, AUS, VAC, INF, DASL, SD, SDIP, JT, DP, DXA, DAG, PV, FPPP, FFDA, FVDD, FFXC, PPRD_ID) " +
                                                                    "VALUES (@NMCAL_ID, @NMCAL_PRD_ID, @EMP_ID, @EMP_CLV, @FCC, @FIRL, @BPP, @HRJR, @HRLJ, @HRAT, @EFI, @INC, @AUS, @VAC, @INF, @DASL, @SD, @SDIP, @JT, @DP, @DXA, @DAG, @PV, @FPPP, @FFDA, @FVDD, @FFXC, @PPRD_ID)" };
            sqlCommand.Parameters.AddWithValue("@NMCAL_ID", DBNull.Value);
            //sqlCommand.Parameters.AddWithValue("@NMCAL_PRD_ID", item.IdProduccion);
            sqlCommand.Parameters.AddWithValue("@EMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@EMP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@FCC", item.FechaCal);
            sqlCommand.Parameters.AddWithValue("@FIRL", item.FechaIngreso);
            sqlCommand.Parameters.AddWithValue("@BPP", item.Puntualidad);
            sqlCommand.Parameters.AddWithValue("@HRJR", item.HorasJornadaTrabajo);
            sqlCommand.Parameters.AddWithValue("@HRLJ", item.HorasReloj);
            sqlCommand.Parameters.AddWithValue("@HRAT", item.HorasAutorizadas);
            sqlCommand.Parameters.AddWithValue("@EFI", item.Productividad);
            sqlCommand.Parameters.AddWithValue("@INC", item.Incapacidad);
            sqlCommand.Parameters.AddWithValue("@AUS", item.Ausencia);
            sqlCommand.Parameters.AddWithValue("@VAC", item.TotalVacaciones);
            sqlCommand.Parameters.AddWithValue("@INF", item.DescuentoInfonavit);
            sqlCommand.Parameters.AddWithValue("@DASL", item.DescuentoPrestamo);
            sqlCommand.Parameters.AddWithValue("@SD", item.SalarioDiario);
            sqlCommand.Parameters.AddWithValue("@SDIP", item.SDIP);
            sqlCommand.Parameters.AddWithValue("@JT", item.JornadasTrabajo);
            sqlCommand.Parameters.AddWithValue("@DP", item.DiasPeriodo);
            sqlCommand.Parameters.AddWithValue("@DXA", item.DiasXAnio);
            sqlCommand.Parameters.AddWithValue("@DAG", item.DiasAguinaldo);
            sqlCommand.Parameters.AddWithValue("@PV", item.PrimaVacacional);
            //sqlCommand.Parameters.AddWithValue("@FPPP", item.FactorPremioPuntualidad);
            //sqlCommand.Parameters.AddWithValue("@FFDA", item.FactorFondoAhorro);
            //sqlCommand.Parameters.AddWithValue("@FVDD", item.FactorValesDespensa);
            //sqlCommand.Parameters.AddWithValue("@FFXC", item.FFXC);
            //sqlCommand.Parameters.AddWithValue("@PPRD_ID", item.IdProduccion);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }
        
        public int Update(NominaPeriodoEmpleadoModel item) {
            //var sqlCommand = new FbCommand { CommandText = @"UPDATE NMCAL SET NMCAL_PRD_ID = @NMCAL_PRD_ID, EMP_ID = @EMP_ID, EMP_CLV = @EMP_CLV, FCC = @FCC, FIRL = @FIRL, BPP = @BPP, HRJR = @HRJR, HRLJ = @HRLJ, HRAT = @HRAT, EFI = @EFI, INC = @INC, AUS = @AUS, VAC = @VAC, INF = @INF, DASL = @DASL, SD = @SD, SDIP = @SDIP, JT = @JT, DP = @DP, DXA = @DXA, DAG = @DAG, PV = @PV, FPPP = @FPPP, FFDA = @FFDA, FVDD = @FVDD, FFXC = @FFXC, PPRD_ID = @PPRD_ID WHERE ((NMCAL_ID = @NMCAL_ID))" };
            var sqlCommand = new FbCommand { 
                CommandText = @"UPDATE NMINC SET FCC = @FCC, BPP = @BPP, HRLJ = @HRLJ, HRAT=@HRAT, INC = @INC, AUS = @AUS, VAC = @VAC WHERE NMCAL_ID = @NMCAL_ID"
            };
            item.FechaCal = DateTime.Now;
            sqlCommand.Parameters.AddWithValue("@NMCAL_ID", item.Id);
            //sqlCommand.Parameters.AddWithValue("@NMCAL_PRD_ID", item.IdProduccion);
            //sqlCommand.Parameters.AddWithValue("@EMP_ID", item.IdEmpleado);
            //sqlCommand.Parameters.AddWithValue("@EMP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@FCC", item.FechaCal);
            //sqlCommand.Parameters.AddWithValue("@FIRL", item.FechaIngreso);
            sqlCommand.Parameters.AddWithValue("@BPP", item.Puntualidad);
            //sqlCommand.Parameters.AddWithValue("@HRJR", item.HorasJornadaTrabajo);
            sqlCommand.Parameters.AddWithValue("@HRLJ", item.HorasReloj);
            sqlCommand.Parameters.AddWithValue("@HRAT", item.HorasAutorizadas);
            //sqlCommand.Parameters.AddWithValue("@EFI", item.Productividad);
            sqlCommand.Parameters.AddWithValue("@INC", item.Incapacidad);
            sqlCommand.Parameters.AddWithValue("@AUS", item.Ausencia);
            sqlCommand.Parameters.AddWithValue("@VAC", item.TotalVacaciones);
            //sqlCommand.Parameters.AddWithValue("@INF", item.DescuentoInfonavit);
            //sqlCommand.Parameters.AddWithValue("@DASL", item.DescuentoPrestamo);
            //sqlCommand.Parameters.AddWithValue("@SD", item.SalarioDiario);
            //sqlCommand.Parameters.AddWithValue("@SDIP", item.SDIP);
            //sqlCommand.Parameters.AddWithValue("@JT", item.JornadasTrabajo);
            //sqlCommand.Parameters.AddWithValue("@DP", item.DiasPeriodo);
            //sqlCommand.Parameters.AddWithValue("@DXA", item.DiasXAnio);
            //sqlCommand.Parameters.AddWithValue("@DAG", item.DiasAguinaldo);
            //sqlCommand.Parameters.AddWithValue("@PV", item.PrimaVacacional);
            //sqlCommand.Parameters.AddWithValue("@FPPP", item.FactorPremioPuntualidad);
            //sqlCommand.Parameters.AddWithValue("@FFDA", item.FactorFondoAhorro);
            //sqlCommand.Parameters.AddWithValue("@FVDD", item.FactorValesDespensa);
            //sqlCommand.Parameters.AddWithValue("@FFXC", item.FFXC);
            //sqlCommand.Parameters.AddWithValue("@PPRD_ID", item.IdProduccion);
            return this.ExecuteTransaction(sqlCommand);
        }

        public NominaPeriodoEmpleadoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 NMCAL.* FROM NMCAL"
            };
            throw new System.NotImplementedException();
        }

        public IEnumerable<NominaPeriodoEmpleadoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NMCAL"
            };
            return this.GetMapper(sqlCommand);
        }
        #endregion

        public IEnumerable<INominaPeriodoEmpleadoDetailModel> GetList(List<IConditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT CTEMP.*, NMINC.* FROM NMINC LEFT JOIN CTEMP ON NMINC.EMP_ID = CTEMP.CTEMP_ID @condiciones"
            };
            sqlCommand = this.GetConditional(sqlCommand, conditionals);
            return this.GetMapper<NominaPeriodoEmpleadoDetailModel>(sqlCommand);
        }

        public INominaPeriodoEmpleadoDetailModel Save(INominaPeriodoEmpleadoDetailModel model) {
            if (model.Id == 0) {
                this.Insert(model as NominaPeriodoEmpleadoDetailModel);
            } else {
                this.Update(model as NominaPeriodoEmpleadoDetailModel);
            }
            return model;
        }
    }
}
