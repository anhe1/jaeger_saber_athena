﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de conceptos de periodo
    /// </summary>
    public class SqlPeriodoConceptoRepository : RepositoryMaster<PeriodoConceptoModel>, ISqlPeriodoConceptoRepository {
        public SqlPeriodoConceptoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) {
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = "DELETE FROM NMEMP WHERE NMEMP_ID = @INDEX" };
            sqlCommand.Parameters.AddWithValue("@INDEX", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PeriodoConceptoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 NMEMP.* FROM NMPRD WHERE NMEMP_ID = @NMEMP_ID"
            };
            sqlCommand.Parameters.AddWithValue("@NMEMP_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<PeriodoConceptoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NMEMP"
            };
            return this.GetMapper<PeriodoConceptoModel>(sqlCommand);
        }

        public int Insert(PeriodoConceptoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"INSERT INTO NMEMP (NMEMP_ID, NMEMP_CTEMP_ID, NMEMP_NMCNP_ID, NMEMP_CNP_TP_ID, NMEMP_NMCNP_ISR_BF, NMEMP_NMCAL_ID, NMEMP_TOTAL, NMEMP_TPGRA, NMEMP_TPEXE, NMEMP_TDEDU) 
                                 VALUES (@NMEMP_ID, @NMEMP_CTEMP_ID, @NMEMP_NMCNP_ID, @NMEMP_CNP_TP_ID, @NMEMP_NMCNP_ISR_BF, @NMEMP_NMCAL_ID, @NMEMP_TOTAL, @NMEMP_TPGRA, @NMEMP_TPEXE, @NMEMP_TDEDU) RETURNING NMEMP_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NMEMP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMEMP_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@NMEMP_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMEMP_CNP_TP_ID", item.IdConceptoTipo);
            sqlCommand.Parameters.AddWithValue("@NMEMP_NMCNP_ISR_BF", item.IdBaseFiscal);
            sqlCommand.Parameters.AddWithValue("@NMEMP_NMCAL_ID", item.IdTablaIncidencias);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TPGRA", item.TPercepcionGravado);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TPEXE", item.TPercepcionExento);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TDEDU", item.TDeducciones);

            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(PeriodoConceptoModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"UPDATE NMEMP SET NMEMP_CTEMP_ID = @NMEMP_CTEMP_ID, NMEMP_NMCNP_ID = @NMEMP_NMCNP_ID, NMEMP_CNP_TP_ID = @NMEMP_CNP_TP_ID, NMEMP_NMCNP_ISR_BF = @NMEMP_NMCNP_ISR_BF, NMEMP_NMCAL_ID = @NMEMP_NMCAL_ID,
                    NMEMP_TOTAL = @NMEMP_TOTAL, NMEMP_TPGRA = @NMEMP_TPGRA, NMEMP_TPEXE = @NMEMP_TPEXE, NMEMP_TDEDU = @NMEMP_TDEDU WHERE NMEMP_ID = @index" };
            sqlCommand.Parameters.AddWithValue("@index", item.Id);
            sqlCommand.Parameters.AddWithValue("@NMEMP_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@NMEMP_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMEMP_CNP_TP_ID", item.IdConceptoTipo);
            sqlCommand.Parameters.AddWithValue("@NMEMP_NMCNP_ISR_BF", item.IdBaseFiscal);
            sqlCommand.Parameters.AddWithValue("@NMEMP_NMCAL_ID", item.IdTablaIncidencias);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TOTAL", item.Total);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TPGRA", item.TPercepcionGravado);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TPEXE", item.TPercepcionExento);
            sqlCommand.Parameters.AddWithValue("@NMEMP_TDEDU", item.TDeducciones);
            return this.ExecuteTransaction(sqlCommand);
        }
        #endregion

        public IEnumerable<PeriodoConceptoDetailModel> GetList(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT NMEMP.*, NMCNP.NMCNP_CLV, NMCNP.NMCNP_CLVSAT, NMCNP.NMCNP_NOM, NMCNP_FRMPG_ID
FROM NMEMP 
LEFT JOIN NMCNP ON NMEMP.NMEMP_NMCNP_ID = NMCNP.NMCNP_ID @condiciones
ORDER BY NMEMP.NMEMP_CTEMP_ID, NMEMP.NMEMP_NMCNP_ID"
            };

            sqlCommand = this.GetConditional(sqlCommand, conditionals);
            return this.GetMapper<PeriodoConceptoDetailModel>(sqlCommand);
        }
    }
}