﻿using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.Domain.Base.Builder;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de la tabla Puesto.
    /// </summary>
    public class SqlPuestoRepository : Abstractions.RepositoryMaster<PuestoModel>, ISqlPuestoRepository {
        public SqlPuestoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM CTDPTP WHERE (CTDPTP_ID = @CTDPTP_ID)" };
            sqlCommand.Parameters.AddWithValue("@CTDPTP_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public PuestoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 CTDPTP.* FROM CTDPTP WHERE CTDPTP_ID = @CTDPTP_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTDPTP_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public int Insert(PuestoModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"INSERT INTO CTDPTP (CTDPTP_ID, CTDPTP_A, CTDPTP_CTDPT_ID, CTDPTP_NOM, CTDPTP_SLDSG, CTDPTP_SLDMX, CTDPTP_RSGPST, CTDPTP_FN, CTDPTP_USR_N, CTDPTP_FM, CTDPTP_USR_M) 
                                 VALUES (@CTDPTP_ID, @CTDPTP_A, @CTDPTP_CTDPT_ID, @CTDPTP_NOM, @CTDPTP_SLDSG, @CTDPTP_SLDMX, @CTDPTP_RSGPST, @CTDPTP_FN, @CTDPTP_USR_N, @CTDPTP_FM, @CTDPTP_USR_M) RETURNING CTDPTP_ID"
            };
            sqlCommand.Parameters.AddWithValue("@CTDPTP_ID", System.DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_SLDSG", item.Sueldo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_SLDMX", item.SueldoMaximo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_RSGPST", item.RiesgoPuesto);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_USR_M", item.Modifica);
            item.IdPuesto = this.ExecuteScalar(sqlCommand);
            if (item.IdPuesto > 0)
                return item.IdPuesto;
            return 0;
        }

        public int Update(PuestoModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"UPDATE CTDPTP SET CTDPTP_ID = @CTDPTP_ID, CTDPTP_A = @CTDPTP_A, CTDPTP_NOM = @CTDPTP_NOM, CTDPTP_SLDSG = @CTDPTP_SLDSG, CTDPTP_SLDMX = @CTDPTP_SLDMX, CTDPTP_RSGPST = @CTDPTP_RSGPST, CTDPTP_FN = @CTDPTP_FN, CTDPTP_USR_N = @CTDPTP_USR_N, CTDPTP_FM = @CTDPTP_FM, CTDPTP_USR_M = @CTDPTP_USR_M, CTDPTP_CTDPT_ID = @CTDPTP_CTDPT_ID WHERE ((CTDPTP_ID = @p11))" 
            };
            sqlCommand.Parameters.AddWithValue("@CTDPTP_ID", item.IdPuesto);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_CTDPT_ID", item.IdDepartamento);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_SLDSG", item.Sueldo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_SLDMX", item.SueldoMaximo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_RSGPST", item.RiesgoPuesto);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_FM", item.FechaModifica);
            sqlCommand.Parameters.AddWithValue("@CTDPTP_USR_M", item.Modifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<PuestoModel> GetList() {
            var sqlCommand = new FbCommand { CommandText = @"SELECT * FROM CTDPTP" };
            return this.GetMapper(sqlCommand);
        }
        #endregion

        /// <summary>
        /// listado condicional
        /// </summary>
        /// <typeparam name="T1">objeto modelo</typeparam>
        /// <param name="conditionales">lista de condicionales</param>
        /// <returns>IEnumerable</returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionales) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CTDPTP.* FROM CTDPTP @condiciones"
            };

            return this.GetMapper<T1>(sqlCommand, conditionales).ToList();
        }

        public PuestoModel Save(PuestoModel model) {
            model.FechaNuevo = System.DateTime.Now;
            model.Creo = this.User;
            if (model.IdPuesto == 0) {
                model.IdPuesto = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}