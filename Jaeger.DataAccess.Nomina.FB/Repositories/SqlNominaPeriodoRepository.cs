﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de periodos de nomina
    /// </summary>
    public class SqlNominaPeriodoRepository : RepositoryMaster<NominaPeriodoModel>, ISqlNominaPeriodoRepository {
        public SqlNominaPeriodoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NMPRD WHERE ((NMPRD_ID = @NMPRD_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@NMPRD_ID", index);
            throw new NotImplementedException();
        }

        public int Update(NominaPeriodoModel item) {
            var sqlCommand = new FbCommand { CommandText = @"UPDATE NMPRD SET NMPRD_A = @NMPRD_A, NMPRD_STTS_ID = @NMPRD_STTS_ID, NMPRD_NOM = @NMPRD_NOM, NMPRD_FCINI = @NMPRD_FCINI, NMPRD_FCFIN = @NMPRD_FCFIN, NMPRD_FCPGO = @NMPRD_FCPGO, NMPRD_FCAUT = @NMPRD_FCAUT, NMPRD_DSPRD = @NMPRD_DSPRD, NMPRD_TPPRD = @NMPRD_TPPRD, NMPRD_TP_ID = @NMPRD_TP_ID, NMPRD_TPER = @NMPRD_TPER, NMPRD_TDED = @NMPRD_TDED, NMPRD_TFDA = @NMPRD_TFDA, NMPRD_TVDD = @NMPRD_TVDD, NMPRD_USR_A = @NMPRD_USR_A, NMPRD_USR_C = @NMPRD_USR_C, NMPRD_USR_M = @NMPRD_USR_M, NMPRD_FM = @NMPRD_FM WHERE ((NMPRD_ID = @NMPRD_ID))" };
            sqlCommand.Parameters.AddWithValue("@NMPRD_ID", item.IdPeriodo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@NMPRD_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FCINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FCFIN", item.FechaFinal);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FCPGO", item.FechaPago);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FCAUT", item.FechaAutoriza);
            sqlCommand.Parameters.AddWithValue("@NMPRD_DSPRD", item.DiasPeriodo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TPPRD", item.IdPeriodoTipo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TP_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TPER", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TDED", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TFDA", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TVDD", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMPRD_USR_A", item.Autoriza);
            sqlCommand.Parameters.AddWithValue("@NMPRD_USR_C", item.Cancela);
            sqlCommand.Parameters.AddWithValue("@NMPRD_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public int Insert(NominaPeriodoModel item) {
            var sqlCommand = new FbCommand { 
                CommandText = @"INSERT INTO NMPRD ( NMPRD_ID, NMPRD_A, NMPRD_STTS_ID, NMPRD_NOM, NMPRD_FCINI, NMPRD_FCFIN, NMPRD_FCPGO, NMPRD_DSPRD, NMPRD_TPPRD, NMPRD_TP_ID, NMPRD_USR_N, NMPRD_FN)
                                            VALUES (@NMPRD_ID,@NMPRD_A,@NMPRD_STTS_ID,@NMPRD_NOM,@NMPRD_FCINI,@NMPRD_FCFIN,@NMPRD_FCPGO,@NMPRD_DSPRD,@NMPRD_TPPRD,@NMPRD_TP_ID,@NMPRD_USR_N,@NMPRD_FN) RETURNING NMPRD_ID"
            };
            sqlCommand.Parameters.AddWithValue("@NMPRD_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMPRD_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_STTS_ID", item.IdStatus);
            sqlCommand.Parameters.AddWithValue("@NMPRD_NOM", item.Descripcion);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FCINI", item.FechaInicio);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FCFIN", item.FechaFinal);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FCPGO", item.FechaPago);
            sqlCommand.Parameters.AddWithValue("@NMPRD_DSPRD", item.DiasPeriodo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TPPRD", item.IdPeriodoTipo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_TP_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NMPRD_FN", item.FechaNuevo);

            item.IdPeriodo = this.ExecuteScalar(sqlCommand);
            if (item.IdPeriodo > 0)
                return item.IdPeriodo;
            return 0;
        }
        
        public NominaPeriodoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 NMPRD.* FROM NMPRD WHERE NMPRD_ID = @NMPRD_ID"
            };
            sqlCommand.Parameters.AddWithValue("@NMPRD_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<NominaPeriodoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NMPRD"
            };

            return this.GetMapper<NominaPeriodoDetailModel>(sqlCommand);
        }
        #endregion

        public INominaPeriodoDetailModel Save(INominaPeriodoDetailModel model) {
            if (model.IdPeriodo == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdPeriodo = this.Insert(model as NominaPeriodoDetailModel);
            } else {
                model.FechaModifica = DateTime.Now;
                model.Modifica = this.User;
                this.Update(model as NominaPeriodoDetailModel);
            }
            return model;
        }

        public IEnumerable<T1> GetLast<T1>() where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 NMPRD.* FROM NMPRD WHERE NMPRD_A = 1 ORDER BY NMPRD_ID DESC;"
            };
            return this.GetMapper<T1>(sqlCommand);
        }

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NMPRD @condiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        public int CrearPeriodo(INominaPeriodoDetailModel periodo) {
            var sqlCommand = new FbCommand {
                CommandText = "EXECUTE PROCEDURE NME_CREAR_NMINC(@PERIODO, @PERIODO_ID, @NMTP_ID)", 
                CommandType = System.Data.CommandType.StoredProcedure
            };
            sqlCommand.Parameters.AddWithValue("@PERIODO", periodo.FechaInicio.Year.ToString());
            sqlCommand.Parameters.AddWithValue("@PERIODO_ID", periodo.IdPeriodo.ToString());
            sqlCommand.Parameters.AddWithValue("@NMTP_ID", periodo.IdTipo.ToString());
            return this.ExecuteScalar(sqlCommand);
        }

        public int Calcular(INominaPeriodoDetailModel periodo) {
            var sqlCommand = new FbCommand {
                CommandText = "EXECUTE PROCEDURE NME_CAL_INC(@PERIODO)",
                CommandType = System.Data.CommandType.StoredProcedure
            };
            sqlCommand.Parameters.AddWithValue("@PERIODO", periodo.IdPeriodo.ToString());
            return this.ExecuteScalar(sqlCommand);
        }
    }
}