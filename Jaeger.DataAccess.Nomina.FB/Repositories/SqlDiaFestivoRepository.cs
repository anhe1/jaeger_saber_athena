﻿using System;
using FirebirdSql.Data.FirebirdClient;
using System.Collections.Generic;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de Dias Festivos
    /// </summary>
    public class SqlDiaFestivoRepository : RepositoryMaster<DiaFestivoModel>, ISqlDiaFestivoRepository {
        public SqlDiaFestivoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE FROM CLDF."
            };
            throw new NotImplementedException();
        }
     
        public int Update(DiaFestivoModel item) {
            throw new NotImplementedException();
        }

        public int Insert(DiaFestivoModel item) {
            throw new NotImplementedException();
        }

        public IEnumerable<DiaFestivoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CLDF.FECHA FROM CLDF WHERE EXTRACT(YEAR FROM CLDF.FECHA) = 2023 ORDER BY CLDF.FECHA ASC"
            };
            return this.GetMapper<DiaFestivoModel>(sqlCommand);
        }

        public DiaFestivoModel GetById(int index) {
            throw new NotImplementedException();
        }
        #endregion

        /// <summary>
        /// Obtiene una lista de dias festivos
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="conditionals"></param>
        /// <returns></returns>
        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT CLDF.* FROM CLDF @wcondiciones"
            };
            return this.GetMapper<T1>(sqlCommand, conditionals);
        }

        /// <summary>
        /// Guarda un dia festivo
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IDiaFestivoModel Save(IDiaFestivoModel model) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE FROM CLDF."
            };
            throw new NotImplementedException();
        }

        /// <summary>
        /// Guarda una lista de dias festivos
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public List<IDiaFestivoModel> Save(List<IDiaFestivoModel> models) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE FROM CLDF."
            };
            throw new NotImplementedException();
        }
    }
}
