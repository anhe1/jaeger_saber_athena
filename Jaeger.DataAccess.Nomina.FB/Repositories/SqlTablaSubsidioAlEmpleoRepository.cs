﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de la tabla Subsidio al Empleo.
    /// </summary>
    public class SqlTablaSubsidioAlEmpleoRepository : RepositoryMaster<TablaSubsidioAlEmpleoModel>, ISqlTablaSubsidioAlEmpleoRepository {
        public SqlTablaSubsidioAlEmpleoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion) : base(configuracion) {
        }

        #region CRUD
        public bool Delete(int index) {
            throw new NotImplementedException();
        }

        public int Insert(TablaSubsidioAlEmpleoModel item) {
            throw new NotImplementedException();
        }

        public int Update(TablaSubsidioAlEmpleoModel item) {
            throw new NotImplementedException();
        }

        public int Insert(Subsidio item) {
            var sqlCommand = new FbCommand { CommandText = @"INSERT INTO SBSD (SBSD_ID, SBSD_ISR_ID, SBSD_PARA, SBSD_HASTA, SBSD_CNTDD) VALUES (@SBSD_ID, @SBSD_ISR_ID, @SBSD_PARA, @SBSD_HASTA, @SBSD_CNTDD)" };
            sqlCommand.Parameters.AddWithValue("@SBSD_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@SBSD_ISR_ID", item.SubId);
            sqlCommand.Parameters.AddWithValue("@SBSD_PARA", item.IngresosDesde);
            sqlCommand.Parameters.AddWithValue("@SBSD_HASTA", item.IngresosHasta);
            sqlCommand.Parameters.AddWithValue("@SBSD_CNTDD", item.Cantidad);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(Subsidio item) {
            var sqlCommand = new FbCommand { CommandText = @"UPDATE SBSD SET SBSD_ID = @SBSD_ID, SBSD_ISR_ID = @SBSD_ISR_ID, SBSD_PARA = @SBSD_PARA, SBSD_HASTA = @SBSD_HASTA, SBSD_CNTDD = @SBSD_CNTDD WHERE ((SBSD_ID = @p6))" };
            sqlCommand.Parameters.AddWithValue("@SBSD_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@SBSD_ISR_ID", item.SubId);
            sqlCommand.Parameters.AddWithValue("@SBSD_PARA", item.IngresosDesde);
            sqlCommand.Parameters.AddWithValue("@SBSD_HASTA", item.IngresosHasta);
            sqlCommand.Parameters.AddWithValue("@SBSD_CNTDD", item.Cantidad);
            return this.ExecuteTransaction(sqlCommand);
        }

        public bool Delete(Subsidio item) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM SBSD WHERE ((SBSD_ID = @p1))" };
            sqlCommand.Parameters.AddWithValue("@SBSD_ID", item.Id);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public IEnumerable<TablaSubsidioAlEmpleoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM ISR ORDER BY ISR_ANIO DESC"
            };
            var _response = this.GetMapper<TablaSubsidioAlEmpleoModel>(sqlCommand).ToList();
            var _rangos = this.GetList1();
            if (_response != null) {
                if (_rangos != null) {
                    for (int i = 0; i < _response.Count(); i++) {
                        _response[i].Rangos = new List<ISubsidio>(_rangos.Where(it => it.SubId == _response[i].Id).ToList<ISubsidio>());
                    }
                }
            }
            return _response;

        }
        #endregion

        public IEnumerable<Subsidio> GetList1() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM SBSD"
            };
            return this.GetMapper<Subsidio>(sqlCommand);
        }
        
        public TablaSubsidioAlEmpleoModel GetById(int index) {
            throw new NotImplementedException();
        }

        public TablaSubsidioAlEmpleoModel GetById(int year, int periodo) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM ISR LEFT JOIN SBSD ON ISR_ID = SBSD_ISR_ID WHERE ISR_PRD = @PERIODO AND ISR_ANIO = @EJERCICIO"
            };
            sqlCommand.Parameters.AddWithValue("@PERIODO", periodo);
            sqlCommand.Parameters.AddWithValue("@EJERCICIO", year);
            var _response = this.GetMapper<Subsidio>(sqlCommand);
            return new TablaSubsidioAlEmpleoModel { Periodo = periodo, Anio = year, Rangos = new List<ISubsidio>(_response.ToList<ISubsidio>()) };
        }
    }
}