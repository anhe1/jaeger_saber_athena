﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de la tabla UMA.
    /// </summary>
    public class SqlTablaUMARepository : RepositoryMaster<UMAModel>, ISqlTablaUMARepository {
        public SqlTablaUMARepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "DELETE FROM UMA WHERE ((UMA_ID = @index))"
            };
            sqlCommand.Parameters.AddWithValue("@UMA_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }
        
        public int Insert(UMAModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "INSERT INTO UMA (UMA_ID, UMA_ANIO, UMA_DIARIO, UMA_MENSUAL, UMA_ANUAL) VALUES (@UMA_ID, @UMA_ANIO, @UMA_DIARIO, @UMA_MENSUAL, @UMA_ANUAL) RETURNING UMA_ID;"
            };
            sqlCommand.Parameters.AddWithValue("@UMA_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@UMA_ANIO", item.Anio);
            sqlCommand.Parameters.AddWithValue("@UMA_DIARIO", item.Diario);
            sqlCommand.Parameters.AddWithValue("@UMA_MENSUAL", item.Mensual);
            sqlCommand.Parameters.AddWithValue("@UMA_ANUAL", item.Anual);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(UMAModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE UMA SET UMA_ANIO = @UMA_ANIO, UMA_DIARIO = @UMA_DIARIO, UMA_MENSUAL = @UMA_MENSUAL, UMA_ANUAL = @UMA_ANUAL WHERE ((UMA_ID = @UMA_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@UMA_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@UMA_ANIO", item.Anio);
            sqlCommand.Parameters.AddWithValue("@UMA_DIARIO", item.Diario);
            sqlCommand.Parameters.AddWithValue("@UMA_MENSUAL", item.Mensual);
            sqlCommand.Parameters.AddWithValue("@UMA_ANUAL", item.Anual);
            return this.ExecuteTransaction(sqlCommand);
        }

        public IEnumerable<UMAModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM UMA ORDER BY UMA_ANIO DESC"
            };
            return this.GetMapper(sqlCommand);
        }

        public UMAModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT First 1 UMA.* FROM UMA"
            };
            sqlCommand.Parameters.AddWithValue("", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }
        #endregion

        public UMAModel GetByYear(int year) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT First 1 UMA.* FROM UMA WHERE UMA_ANIO = @YEAR"
            };
            sqlCommand.Parameters.AddWithValue("@YEAR", year);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public UMAModel Save(UMAModel model) {
            if (model.Id == 0) {
                model.Id = this.Insert(model);
            } else {
                this.Update(model);
            }
            return model;
        }
    }
}