﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Entities;
using Jaeger.Domain.Base.Builder;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de concepto de nomina
    /// </summary>
    public class SqlNominaConceptoRepository : RepositoryMaster<NominaConceptoModel>, ISqlNominaConceptoRepository {
        public SqlNominaConceptoRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = "DELETE FROM NMCNP WHERE ((NMCNP_ID = @NMCNP_ID))" };
            sqlCommand.Parameters.AddWithValue("@NMCNP_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(NominaConceptoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "INSERT INTO NMCNP (NMCNP_ID, NMCNP_A, NMCNP_TP_ID, NMCNP_TNM_ID, NMCNP_APL_ID, NMCNP_TBL_ID, NMCNP_FRMPG_ID, NMCNP_VIS, NMCNP_CLV, NMCNP_CLVSAT, NMCNP_NOM, NMCNP_FORM, NMCNP_ISR_BF, NMCNP_ISR_FORM, NMCNP_IMS_BF, NMCNP_IMS_FORM, NMCNP_USR_N, NMCNP_FN) " +
                                        "VALUES (@NMCNP_ID,@NMCNP_A,@NMCNP_TP_ID,@NMCNP_TNM_ID,@NMCNP_APL_ID,@NMCNP_TBL_ID,@NMCNP_FRMPG_ID,@NMCNP_VIS,@NMCNP_CLV,@NMCNP_CLVSAT,@NMCNP_NOM,@NMCNP_FORM,@NMCNP_ISR_BF,@NMCNP_ISR_FORM,@NMCNP_IMS_BF,@NMCNP_IMS_FORM,@NMCNP_USR_N,@NMCNP_FN) RETURNING NMCNP_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NMCNP_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMCNP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NMCNP_TP_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NMCNP_TNM_ID", item.IdTipoNomina);
            sqlCommand.Parameters.AddWithValue("@NMCNP_APL_ID", item.IdAplicacion);
            sqlCommand.Parameters.AddWithValue("@NMCNP_TBL_ID", item.IdTabla);
            sqlCommand.Parameters.AddWithValue("@NMCNP_FRMPG_ID", item.PagoEspecie);
            sqlCommand.Parameters.AddWithValue("@NMCNP_VIS", item.Visible);
            sqlCommand.Parameters.AddWithValue("@NMCNP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@NMCNP_CLVSAT", item.ClaveSAT);
            sqlCommand.Parameters.AddWithValue("@NMCNP_NOM", item.Concepto);
            sqlCommand.Parameters.AddWithValue("@NMCNP_FORM", item.Formula);
            sqlCommand.Parameters.AddWithValue("@NMCNP_ISR_BF", item.IdBaseISR);
            sqlCommand.Parameters.AddWithValue("@NMCNP_ISR_FORM", item.FormulaISR);
            sqlCommand.Parameters.AddWithValue("@NMCNP_IMS_BF", item.IdBaseIMS);
            sqlCommand.Parameters.AddWithValue("@NMCNP_IMS_FORM", item.FormulaIMS);
            sqlCommand.Parameters.AddWithValue("@NMCNP_USR_N", item.Creo);
            sqlCommand.Parameters.AddWithValue("@NMCNP_FN", item.FechaNuevo);

            item.IdConcepto = this.ExecuteScalar(sqlCommand);
            if (item.IdConcepto > 0)
                return item.IdConcepto;
            return 0;
        }

        public int Update(NominaConceptoModel item) {
            var sqlCommand = new FbCommand {
                CommandText = @"UPDATE NMCNP SET NMCNP_A = @NMCNP_A, NMCNP_TP_ID = @NMCNP_TP_ID, NMCNP_TNM_ID = @NMCNP_TNM_ID, NMCNP_APL_ID = @NMCNP_APL_ID, NMCNP_TBL_ID = @NMCNP_TBL_ID, NMCNP_FRMPG_ID = @NMCNP_FRMPG_ID, NMCNP_VIS = @NMCNP_VIS, NMCNP_CLV = @NMCNP_CLV, NMCNP_CLVSAT = @NMCNP_CLVSAT, NMCNP_NOM = @NMCNP_NOM, NMCNP_FORM = @NMCNP_FORM, NMCNP_ISR_BF = @NMCNP_ISR_BF, NMCNP_ISR_FORM = @NMCNP_ISR_FORM, NMCNP_IMS_BF = @NMCNP_IMS_BF, NMCNP_IMS_FORM = @NMCNP_IMS_FORM, NMCNP_USR_M = @NMCNP_USR_M, NMCNP_FM = @NMCNP_FM WHERE ((NMCNP_ID = @NMCNP_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@NMCNP_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@NMCNP_TP_ID", item.IdTipo);
            sqlCommand.Parameters.AddWithValue("@NMCNP_TNM_ID", item.IdTipoNomina);
            sqlCommand.Parameters.AddWithValue("@NMCNP_APL_ID", item.IdAplicacion);
            sqlCommand.Parameters.AddWithValue("@NMCNP_TBL_ID", item.IdTabla);
            sqlCommand.Parameters.AddWithValue("@NMCNP_FRMPG_ID", item.PagoEspecie);
            sqlCommand.Parameters.AddWithValue("@NMCNP_VIS", item.Visible);
            sqlCommand.Parameters.AddWithValue("@NMCNP_CLV", item.Clave);
            sqlCommand.Parameters.AddWithValue("@NMCNP_CLVSAT", item.ClaveSAT);
            sqlCommand.Parameters.AddWithValue("@NMCNP_NOM", item.Concepto);
            sqlCommand.Parameters.AddWithValue("@NMCNP_FORM", item.Formula);
            sqlCommand.Parameters.AddWithValue("@NMCNP_ISR_BF", item.IdBaseISR);
            sqlCommand.Parameters.AddWithValue("@NMCNP_ISR_FORM", item.FormulaISR);
            sqlCommand.Parameters.AddWithValue("@NMCNP_IMS_BF", item.IdBaseIMS);
            sqlCommand.Parameters.AddWithValue("@NMCNP_IMS_FORM", item.FormulaIMS);
            sqlCommand.Parameters.AddWithValue("@NMCNP_USR_M", item.Modifico);
            sqlCommand.Parameters.AddWithValue("@NMCNP_FM", item.FechaModifico);
            return this.ExecuteTransaction(sqlCommand);
        }

        public NominaConceptoModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT FISRT 1 NMCNP.* FROM NMCNP WHERE ((NMCNP_ID = @NMCNP_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@NMCNP_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<NominaConceptoModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM NMCNP"
            };

            return this.GetMapper(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM NMCNP @condiciones ORDER BY NMCNP_TP_ID, NMCNP_APL_ID, NMCNP_ID"
            };
            sqlCommand = this.GetConditional(sqlCommand, conditionals);
            return this.GetMapper<T1>(sqlCommand);
        }

        public IEnumerable<NominaConceptoDetailModel> GetList(List<Conditional> conditionals) {
            var sqlCommand = new FbCommand {
                CommandText = "SELECT * FROM NMCNP @condiciones ORDER BY NMCNP_TP_ID, NMCNP_APL_ID, NMCNP_ID"
            };
            sqlCommand = this.GetConditional(sqlCommand, conditionals);
            return this.GetMapper<NominaConceptoDetailModel>(sqlCommand);
        }

        public INominaConceptoDetailModel Save(INominaConceptoDetailModel model) {
            if (model.IdConcepto == 0) {
                model.FechaNuevo = DateTime.Now;
                model.Creo = this.User;
                model.IdConcepto = this.Insert(model as NominaConceptoDetailModel);
            } else {
                model.FechaModifico = DateTime.Now;
                model.Modifico = this.User;
                this.Update(model as NominaConceptoDetailModel);
            }
            return model;
        }
    }
}
