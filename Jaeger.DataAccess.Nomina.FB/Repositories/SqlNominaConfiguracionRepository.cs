﻿using System;
using System.Linq;
using System.Collections.Generic;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    /// <summary>
    /// Repositorio de configuracion de nomina
    /// </summary>
    public class SqlNominaConfiguracionRepository : RepositoryMaster<ConfiguracionModel>, ISqlNominaConfiguracionRepository {
        public SqlNominaConfiguracionRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public bool Delete(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"DELETE FROM NMCNF WHERE ((NMCNF_ID = @NMCNF_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@NMCNF_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public int Insert(ConfiguracionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "INSERT INTO NMCNF (NMCNF_ID, NMCNF_ANIO, NMCNF_TISR_ID, NMCNF_TSBD_ID, NMCNF_PRD_ID, NMCNF_DXA, NMCNF_DXS, NMCNF_HD, NMCNF_DP, NMCNF_SMV, NMCNF_UMAV, NMCNF_EEX_E, NMCNF_PED_E, NMCNF_PYB_E, NMCNF_IYV_E, NMCNF_CYV_E, NMCNF_ECF_P, NMCNF_EEX_P, NMCNF_PED_P, NMCNF_PYB_P, NMCNF_IYV_P, NMCNF_RDT_P, NMCNF_GPS_P, NMCNF_SAR_P, NMCNF_CYV_P, NMCNF_INF_P, NMCNF_FN, NMCNF_USR_N) " +
                                        "VALUES (@NMCNF_ID,@NMCNF_ANIO,@NMCNF_TISR_ID,@NMCNF_TSBD_ID,@NMCNF_PRD_ID,@NMCNF_DXA,@NMCNF_DXS,@NMCNF_HD,@NMCNF_DP,@NMCNF_SMV,@NMCNF_UMAV,@NMCNF_EEX_E,@NMCNF_PED_E,@NMCNF_PYB_E,@NMCNF_IYV_E,@NMCNF_CYV_E,@NMCNF_ECF_P,@NMCNF_EEX_P,@NMCNF_PED_P,@NMCNF_PYB_P,@NMCNF_IYV_P,@NMCNF_RDT_P,@NMCNF_GPS_P,@NMCNF_SAR_P,@NMCNF_CYV_P,@NMCNF_INF_P,@NMCNF_FN,@NMCNF_USR_N) RETURNING NMCNF_ID"
            };

            sqlCommand.Parameters.AddWithValue("@NMCNF_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@NMCNF_ANIO", item.Ejercicio);
            sqlCommand.Parameters.AddWithValue("@NMCNF_TISR_ID", item.IdTablaISR);
            sqlCommand.Parameters.AddWithValue("@NMCNF_TSBD_ID", item.IdTablaSubsidio);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PRD_ID", item.IdPeriodo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_DXA", item.DiasXAnio);
            sqlCommand.Parameters.AddWithValue("@NMCNF_DXS", item.DiasSemana);
            sqlCommand.Parameters.AddWithValue("@NMCNF_HD", item.HorasDia);
            sqlCommand.Parameters.AddWithValue("@NMCNF_DP", item.DiasPeriodo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_SMV", item.SalarioMinimo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_UMAV", item.UMA);

            sqlCommand.Parameters.AddWithValue("@NMCNF_EEX_E", item.ExcedenteE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PED_E", item.PrestacionDineroE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PYB_E", item.PensionadosYBeneficiariosE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_IYV_E", item.InvalidezYVidaE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_CYV_E", item.CesantiaYVejezE);

            sqlCommand.Parameters.AddWithValue("@NMCNF_ECF_P", item.CuotaFijaP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_EEX_P", item.ExcedenteP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PED_P", item.PrestacionDineroP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PYB_P", item.PensionadosYBeneficiariosP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_IYV_P", item.InvalidezYVidaP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_RDT_P", item.RiesgoTrabajo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_GPS_P", item.PrestacionesSociales);
            sqlCommand.Parameters.AddWithValue("@NMCNF_SAR_P", item.SeguroDeRetiro);
            sqlCommand.Parameters.AddWithValue("@NMCNF_CYV_P", item.CesantiaYVejezP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_INF_P", item.Infonavit);

            sqlCommand.Parameters.AddWithValue("@NMCNF_FN", item.FechaNuevo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_USR_N", item.Creo);

            item.IdConfiguracion = this.ExecuteScalar(sqlCommand);
            if (item.IdConfiguracion > 0)
                return item.IdConfiguracion;
            return 0;
        }

        public int Update(ConfiguracionModel item) {
            var sqlCommand = new FbCommand {
                CommandText = "UPDATE NMCNF SET NMCNF_ANIO = @NMCNF_ANIO, NMCNF_TISR_ID = @NMCNF_TISR_ID, NMCNF_TSBD_ID = @NMCNF_TSBD_ID, NMCNF_PRD_ID = @NMCNF_PRD_ID, NMCNF_DXA = @NMCNF_DXA, NMCNF_DXS = @NMCNF_DXS, " +
                "NMCNF_HD = @NMCNF_HD, NMCNF_DP = @NMCNF_DP, NMCNF_SMV = @NMCNF_SMV, NMCNF_UMAV = @NMCNF_UMAV, " +
                "NMCNF_EEX_E=@NMCNF_EEX_E, NMCNF_PED_E=@NMCNF_PED_E, NMCNF_PYB_E=@NMCNF_PYB_E, NMCNF_IYV_E=@NMCNF_IYV_E,NMCNF_CYV_E=@NMCNF_CYV_E," +
                "NMCNF_ECF_P=@NMCNF_ECF_P, NMCNF_EEX_P=@NMCNF_EEX_P, NMCNF_PED_P=@NMCNF_PED_P, NMCNF_PYB_P=@NMCNF_PYB_P, NMCNF_IYV_P=@NMCNF_IYV_P, NMCNF_RDT_P=@NMCNF_RDT_P, NMCNF_GPS_P=@NMCNF_GPS_P, NMCNF_SAR_P=@NMCNF_SAR_P, NMCNF_CYV_P=@NMCNF_CYV_P, NMCNF_INF_P=@NMCNF_INF_P, " +
                "NMCNF_USR_M = @NMCNF_USR_M, NMCNF_FM = @NMCNF_FM " +
                "WHERE ((NMCNF_ID = @NMCNF_ID))"
            };

            sqlCommand.Parameters.AddWithValue("@NMCNF_ID", item.IdConfiguracion);
            sqlCommand.Parameters.AddWithValue("@NMCNF_ANIO", item.Ejercicio);
            sqlCommand.Parameters.AddWithValue("@NMCNF_TISR_ID", item.IdTablaISR);
            sqlCommand.Parameters.AddWithValue("@NMCNF_TSBD_ID", item.IdTablaSubsidio);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PRD_ID", item.IdPeriodo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_DXA", item.DiasXAnio);
            sqlCommand.Parameters.AddWithValue("@NMCNF_DXS", item.DiasSemana);
            sqlCommand.Parameters.AddWithValue("@NMCNF_HD", item.HorasDia);
            sqlCommand.Parameters.AddWithValue("@NMCNF_DP", item.DiasPeriodo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_SMV", item.SalarioMinimo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_UMAV", item.UMA);

            sqlCommand.Parameters.AddWithValue("@NMCNF_EEX_E", item.ExcedenteE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PED_E", item.PrestacionDineroE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PYB_E", item.PensionadosYBeneficiariosE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_IYV_E", item.InvalidezYVidaE);
            sqlCommand.Parameters.AddWithValue("@NMCNF_CYV_E", item.CesantiaYVejezE);

            sqlCommand.Parameters.AddWithValue("@NMCNF_ECF_P", item.CuotaFijaP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_EEX_P", item.ExcedenteP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PED_P", item.PrestacionDineroP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_PYB_P", item.PensionadosYBeneficiariosP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_IYV_P", item.InvalidezYVidaP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_RDT_P", item.RiesgoTrabajo);
            sqlCommand.Parameters.AddWithValue("@NMCNF_GPS_P", item.PrestacionesSociales);
            sqlCommand.Parameters.AddWithValue("@NMCNF_SAR_P", item.SeguroDeRetiro);
            sqlCommand.Parameters.AddWithValue("@NMCNF_CYV_P", item.CesantiaYVejezP);
            sqlCommand.Parameters.AddWithValue("@NMCNF_INF_P", item.Infonavit);

            sqlCommand.Parameters.AddWithValue("@NMCNF_USR_M", item.Modifica);
            sqlCommand.Parameters.AddWithValue("@NMCNF_FM", item.FechaModifica);
            return this.ExecuteTransaction(sqlCommand);
        }

        public ConfiguracionModel GetById(int index) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 NMCNF.* FROM NMCNF WHERE ((NMCNF_ID = @NMCNF_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@NMCNF_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IConfiguracionDetailModel GetBy1d(int id) {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT FIRST 1 NMCNF.* FROM NMCNF WHERE ((NMCNF_ID = @NMCNF_ID))"
            };
            sqlCommand.Parameters.AddWithValue("@NMCNF_ID", id);
            return this.GetMapper<ConfiguracionDetailModel>(sqlCommand).FirstOrDefault() as IConfiguracionDetailModel;
        }

        public IEnumerable<ConfiguracionModel> GetList() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NMCNF"
            };
            return this.GetMapper(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> conditionals) where T1 : class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT * FROM NMCNF @wcondiciones"
            };

            sqlCommand = ExpressionTool.Where(sqlCommand, conditionals);
            return this.GetMapper<T1>(sqlCommand);
        }

        public IConfiguracionDetailModel Save(IConfiguracionDetailModel item) {
            if (item.IdConfiguracion == 0) {
                item.FechaNuevo = DateTime.Now;
                item.Creo = this.User;
                item.IdConfiguracion = this.Insert(item as ConfiguracionDetailModel);
            } else {
                item.FechaModifica = DateTime.Now;
                item.Modifica = this.User;
                this.Update(item as ConfiguracionDetailModel);
            }
            return item;
        }
    }
}