﻿using System;
using System.Collections.Generic;
using System.Linq;
using FirebirdSql.Data.FirebirdClient;
using Jaeger.DataAccess.Abstractions;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.Domain.Base.Builder;
using Jaeger.DataAccess.FireBird.Services;

namespace Jaeger.DataAccess.FB.Nomina.Repositories {
    public class SqlEmpleadoConceptoNominaRepository : RepositoryMaster<EmpleadoConceptoNominaModel>, ISqlEmpleadoConceptoNominaRepository {
        public SqlEmpleadoConceptoNominaRepository(Domain.DataBase.Contracts.IDataBaseConfiguracion configuracion, string user) : base(configuracion) {
            this.User = user;
        }

        #region CRUD
        public int Insert(EmpleadoConceptoNominaModel item) {
            var sqlCommand = new FbCommand { CommandText = @"INSERT INTO CTEMPD (CTEMPD_ID, CTEMPD_A, CTEMPD_CTEMP_ID, CTEMPD_NMCNP_ID, CTEMPD_TBL_ID, CTEMPD_IMPFJ) VALUES (@CTEMPD_ID, @CTEMPD_A, @CTEMPD_CTEMP_ID, @CTEMPD_NMCNP_ID, @CTEMPD_TBL_ID, @CTEMPD_IMPFJ) RETURNING CTEMPD_ID" };
            sqlCommand.Parameters.AddWithValue("@CTEMPD_ID", DBNull.Value);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_TBL_ID", item.IdTabla);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_IMPFJ", item.ImporteGravado);
            item.Id = this.ExecuteScalar(sqlCommand);
            if (item.Id > 0)
                return item.Id;
            return 0;
        }

        public int Update(EmpleadoConceptoNominaModel item) {
            var sqlCommand = new FbCommand { CommandText = @"UPDATE CTEMPD SET CTEMPD_A = @CTEMPD_A, CTEMPD_CTEMP_ID = @CTEMPD_CTEMP_ID, CTEMPD_NMCNP_ID = @CTEMPD_NMCNP_ID, CTEMPD_TBL_ID = @CTEMPD_TBL_ID, CTEMPD_IMPFJ = @CTEMPD_IMPFJ WHERE (CTEMPD_ID = @CTEMPD_ID)" };
            sqlCommand.Parameters.AddWithValue("@CTEMPD_ID", item.Id);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_A", item.Activo);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_CTEMP_ID", item.IdEmpleado);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_NMCNP_ID", item.IdConcepto);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_TBL_ID", item.IdTabla);
            sqlCommand.Parameters.AddWithValue("@CTEMPD_IMPFJ", item.ImporteGravado);
            return this.ExecuteTransaction(sqlCommand);
        }
        
        public bool Delete(int index) {
            var sqlCommand = new FbCommand { CommandText = @"DELETE FROM CTEMPD WHERE ((CTEMPD_ID = @CTEMPD_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTEMPD_ID", index);
            return this.ExecuteTransaction(sqlCommand) > 0;
        }

        public EmpleadoConceptoNominaModel GetById(int index) {
            var sqlCommand = new FbCommand { CommandText = @"SELECT FIRST 1 CTEMPD.* FROM CTEMPD WHERE ((CTEMPD_ID = @CTEMPD_ID))" };
            sqlCommand.Parameters.AddWithValue("@CTEMPD_ID", index);
            return this.GetMapper(sqlCommand).FirstOrDefault();
        }

        public IEnumerable<EmpleadoConceptoNominaModel> GetList() {
            var sqlCommand = new FbCommand { 
                CommandText = @"SELECT CTEMPD.* FROM CTEMPD WHERE ((CTEMPD_ID = @CTEMPD_ID))" 
            };
            return this.GetMapper(sqlCommand);
        }
        #endregion

        public IEnumerable<T1> GetList<T1>(List<IConditional> condiciones) where T1: class, new() {
            var sqlCommand = new FbCommand {
                CommandText = @"SELECT CTEMPD.* FROM CTEMPD @wcondiciones"
            };
            return this.GetMapper<T1>(ExpressionTool.Where(sqlCommand, condiciones)).ToList();
        }

        public EmpleadoConceptoNominaModel Save(EmpleadoConceptoNominaModel item) {
            if (item.Id == 0) {
                item.Id = this.Insert(item);
            } else {
                this.Update(item);
            }
            return item;
        }
    }
}