﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Service;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Forms;
using Jaeger.Domain.Base.Abstractions;

namespace Jaeger.UI.Nomina.Forms.Parametros {
    public partial class TablaISRForm : RadForm {
        private ITablaISRService service;
        private BindingList<TablaImpuestoSobreRentaModel> tabla;
        protected internal Domain.Base.ValueObjects.UIAction _permisos;
        public TablaISRForm(UIMenuElement menuElement) {
            InitializeComponent();
        }

        private void TablaISR_Load(object sender, EventArgs e) {
            this.service = new TablaISRService();
            this.ToolBar.Guardar.Click += this.ToolBarButtonGuardar_Click;
            this.ToolBar.Actualizar.Click += this.ToolBarButtonActualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBarButtonCerrar_Click;

            this.ToolBar.Actualizar.PerformClick();
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void ToolBarButtonGuardar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Guardar)) {
                espera.Text = "Guardando ...";
                espera.ShowDialog(this);
            }
            this.CreateBinding();
        }

        private void ToolBarButtonActualizar_Click(object sender, EventArgs e) {
            using (Waiting2Form espera = new Waiting2Form(this.Actualizar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            var _periodos = this.gridPeriodo.Columns["Periodo"] as GridViewComboBoxColumn;
            _periodos.DataSource = CommonService.GetPeriodos();
            this.CreateBinding();
        }

        private void CreateBinding() {
            this.gridPeriodo.DataSource = this.tabla;
            this.gridRangos.DataSource = this.tabla;
            this.gridRangos.DataMember = "Rangos";
        }

        private void Actualizar() {
            this.tabla = new BindingList<TablaImpuestoSobreRentaModel>(this.service.GetList());
            if (this.tabla == null)
                this.tabla = new BindingList<TablaImpuestoSobreRentaModel>();
        }

        private void Guardar() {
            //this.tabla = this.service.Save(this.tabla);
        }
    }
}
