﻿namespace Jaeger.UI.Nomina.Forms.Empleados
{
    partial class EmpleadoCatalogoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Jaeger.Domain.Base.ValueObjects.UIAction uiAction1 = new Jaeger.Domain.Base.ValueObjects.UIAction();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EmpleadoCatalogoForm));
            this.TEmpleado = new Jaeger.UI.Nomina.Forms.Empleados.EmpleadoGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TEmpleado
            // 
            this.TEmpleado.Caption = "";
            this.TEmpleado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEmpleado.Location = new System.Drawing.Point(0, 0);
            this.TEmpleado.Name = "TEmpleado";
            uiAction1.Agregar = false;
            uiAction1.Autorizar = false;
            uiAction1.Cancelar = false;
            uiAction1.Editar = false;
            uiAction1.Exportar = false;
            uiAction1.Importar = false;
            uiAction1.Imprimir = false;
            uiAction1.Remover = false;
            uiAction1.Reporte = false;
            uiAction1.Status = false;
            this.TEmpleado.Permisos = uiAction1;
            this.TEmpleado.ShowActualizar = true;
            this.TEmpleado.ShowAutorizar = false;
            this.TEmpleado.ShowAutosuma = false;
            this.TEmpleado.ShowCerrar = true;
            this.TEmpleado.ShowEditar = true;
            this.TEmpleado.ShowExportarExcel = false;
            this.TEmpleado.ShowFiltro = true;
            this.TEmpleado.ShowHerramientas = true;
            this.TEmpleado.ShowImagen = false;
            this.TEmpleado.ShowImprimir = false;
            this.TEmpleado.ShowNuevo = true;
            this.TEmpleado.ShowRemover = true;
            this.TEmpleado.ShowSeleccionMultiple = true;
            this.TEmpleado.Size = new System.Drawing.Size(1175, 455);
            this.TEmpleado.TabIndex = 0;
            // 
            // EmpleadoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1175, 455);
            this.Controls.Add(this.TEmpleado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EmpleadoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Empleados: Expediente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.CatalogoEmpleados_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private EmpleadoGridControl TEmpleado;
    }
}
