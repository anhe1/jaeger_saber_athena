﻿using System;
using Jaeger.UI.Nomina.Builder;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    internal class EmpleadoGridControl : Common.Forms.GridStandarControl {
        public GridViewTemplate gridContrato = new GridViewTemplate();
        public GridViewTemplate gridBanco = new GridViewTemplate();
        
        public EmpleadoGridControl() : base() {

        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            using (IEmpleadoGridViewBuilder build = new EmpleadoGridViewBuilder()) {
                this.GridData.Columns.AddRange(build.Templetes().Master().Build());
            }
        }

        public override void GridData_RowsChanged(object sender, GridViewCollectionChangedEventArgs e) {
            //if (e.Template.Caption == this.gridContrato.Caption) {
            //    var rowData = e.ParentRow.DataBoundItem as EmpleadoDetailModel;
            //    if (rowData != null) {
            //        if (rowData.Contratos != null) {
            //            foreach (var item in rowData.Contratos) {
            //                var row = e.Template.Rows.NewRow();
            //                row.Cells["IdEmpleado"].Value = item.IdEmpleado;
            //                row.Cells["Activo"].Value = item.Activo;
            //                row.Cells["ClaveTipoRegimen"].Value = item.ClaveTipoRegimen;
            //                row.Cells["ClaveRiesgoPuesto"].Value = item.ClaveRiesgoPuesto;
            //                row.Cells["ClavePeriodicidadPago"].Value = item.ClavePeriodicidadPago;
            //                row.Cells["ClaveMetodoPago"].Value = item.ClaveMetodoPago;
            //                row.Cells["ClaveTipoContrato"].Value = item.ClaveTipoContrato;
            //                row.Cells["ClaveTipoJornada"].Value = item.ClaveTipoJornada;
            //                row.Cells["RegistroPatronal"].Value = item.RegistroPatronal;
            //                row.Cells["Departamento"].Value = item.Departamento;
            //                row.Cells["FecInicioRelLaboral"].Value = item.FecInicioRelLaboral;
            //                row.Cells["FecTerminoRelLaboral"].Value = item.FecTerminoRelLaboral;
            //                row.Cells["SalarioBase"].Value = item.SalarioBase;
            //                row.Cells["SalarioBaseCotizacion"].Value = item.SalarioBaseCotizacion;
            //                row.Cells["SalarioDiario"].Value = item.SalarioDiario;
            //                row.Cells["Jornadas"].Value = item.Jornadas;
            //                e.SourceCollection.Add(row);
            //            }
            //        }
            //    }
            //} else if (e.Template.Caption == this.gridBanco.Caption) {
            //    var rowData = e.ParentRow.DataBoundItem as EmpleadoDetailModel;
            //    if (rowData != null) {
            //        if (rowData.Cuentas != null) {
            //            foreach (var item in rowData.Cuentas) {
            //                var row = e.Template.Rows.NewRow();
            //                row.Cells["Activo"].Value = item.Activo;
            //                row.Cells["Verificado"].Value = item.Verificado;
            //                row.Cells["IdTipoCuenta"].Value = item.IdTipoCuenta;
            //                row.Cells["Banco"].Value = item.Banco;
            //                row.Cells["Clave"].Value = item.Clave;
            //                row.Cells["InsitucionBancaria"].Value = item.InsitucionBancaria;
            //                row.Cells["NumeroDeCuenta"].Value = item.NumCuenta;
            //                row.Cells["Creo"].Value = item.Creo;
            //                row.Cells["FechaNuevo"].Value = item.FechaNuevo;
            //                e.SourceCollection.Add(row);
            //            }
            //        }
            //    }
            //}
        }
    }
}
