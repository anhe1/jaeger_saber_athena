﻿using System;
using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    internal class ContratoGridControl : Common.Forms.GridStandarControl {
        public ContratoGridControl() : base() {

        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            using (IContratoGridViewBuilder build = new ContratoGridViewBuilder()) {
                this.GridData.Columns.AddRange(build.Templetes().Master().Build());
            }
        }
    }
}
