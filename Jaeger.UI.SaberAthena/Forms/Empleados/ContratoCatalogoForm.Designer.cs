﻿namespace Jaeger.UI.Nomina.Forms.Empleados {
    partial class ContratoCatalogoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.ExpressionFormattingObject expressionFormattingObject1 = new Telerik.WinControls.UI.ExpressionFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContratoCatalogoForm));
            this.TContrato = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            this.gridViewEmpleados = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TContrato
            // 
            this.TContrato.Dock = System.Windows.Forms.DockStyle.Top;
            this.TContrato.Etiqueta = "";
            this.TContrato.Location = new System.Drawing.Point(0, 0);
            this.TContrato.Name = "TContrato";
            this.TContrato.ReadOnly = false;
            this.TContrato.ShowActualizar = true;
            this.TContrato.ShowAutorizar = false;
            this.TContrato.ShowCerrar = true;
            this.TContrato.ShowEditar = true;
            this.TContrato.ShowExportarExcel = false;
            this.TContrato.ShowFiltro = true;
            this.TContrato.ShowGuardar = false;
            this.TContrato.ShowHerramientas = false;
            this.TContrato.ShowImagen = false;
            this.TContrato.ShowImprimir = false;
            this.TContrato.ShowNuevo = true;
            this.TContrato.ShowRemover = true;
            this.TContrato.Size = new System.Drawing.Size(800, 30);
            this.TContrato.TabIndex = 4;
            // 
            // gridViewEmpleados
            // 
            this.gridViewEmpleados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridViewEmpleados.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridViewEmpleados.MasterTemplate.AutoGenerateColumns = false;
            gridViewTextBoxColumn1.DataType = typeof(int);
            gridViewTextBoxColumn1.FieldName = "IdContrato";
            gridViewTextBoxColumn1.FormatString = "{0:0000#}";
            gridViewTextBoxColumn1.HeaderText = "N. Contrato";
            gridViewTextBoxColumn1.Name = "IdContrato";
            gridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn1.Width = 75;
            expressionFormattingObject1.ApplyToRow = true;
            expressionFormattingObject1.CellBackColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            expressionFormattingObject1.Expression = "Activo = false";
            expressionFormattingObject1.Name = "Inactivo";
            expressionFormattingObject1.RowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            expressionFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            gridViewCheckBoxColumn1.ConditionalFormattingObjectList.Add(expressionFormattingObject1);
            gridViewCheckBoxColumn1.FieldName = "Activo";
            gridViewCheckBoxColumn1.HeaderText = "Activo";
            gridViewCheckBoxColumn1.Name = "Activo";
            gridViewCheckBoxColumn1.VisibleInColumnChooser = false;
            gridViewTextBoxColumn2.FieldName = "IdEmpleado";
            gridViewTextBoxColumn2.FormatString = "{0:0000#}";
            gridViewTextBoxColumn2.HeaderText = "N. Emplado";
            gridViewTextBoxColumn2.Name = "IdEmpleado1";
            gridViewTextBoxColumn3.Expression = "";
            gridViewTextBoxColumn3.FieldName = "Clave";
            gridViewTextBoxColumn3.HeaderText = "Clave";
            gridViewTextBoxColumn3.Name = "Clave";
            gridViewTextBoxColumn3.Width = 80;
            gridViewTextBoxColumn4.FieldName = "ApellidoPaterno";
            gridViewTextBoxColumn4.HeaderText = "A. Paterno";
            gridViewTextBoxColumn4.Name = "ApellidoPaterno";
            gridViewTextBoxColumn4.Width = 140;
            gridViewTextBoxColumn5.FieldName = "ApellidoMaterno";
            gridViewTextBoxColumn5.HeaderText = "A. Materno";
            gridViewTextBoxColumn5.Name = "ApellidoMaterno";
            gridViewTextBoxColumn5.Width = 140;
            gridViewTextBoxColumn6.FieldName = "Nombre";
            gridViewTextBoxColumn6.HeaderText = "Nombre";
            gridViewTextBoxColumn6.Name = "Nombre";
            gridViewTextBoxColumn6.Width = 140;
            gridViewTextBoxColumn7.FieldName = "RFC";
            gridViewTextBoxColumn7.HeaderText = "RFC";
            gridViewTextBoxColumn7.Name = "rfc";
            gridViewTextBoxColumn7.Width = 115;
            gridViewTextBoxColumn8.FieldName = "CURP";
            gridViewTextBoxColumn8.HeaderText = "CURP";
            gridViewTextBoxColumn8.Name = "Curp";
            gridViewTextBoxColumn8.Width = 135;
            gridViewTextBoxColumn9.FieldName = "Departamento";
            gridViewTextBoxColumn9.HeaderText = "Departamento";
            gridViewTextBoxColumn9.Name = "Departamento";
            gridViewTextBoxColumn9.Width = 180;
            gridViewTextBoxColumn10.FieldName = "Puesto";
            gridViewTextBoxColumn10.HeaderText = "Puesto";
            gridViewTextBoxColumn10.Name = "Puesto";
            gridViewTextBoxColumn10.Width = 120;
            gridViewTextBoxColumn11.DataType = typeof(decimal);
            gridViewTextBoxColumn11.FieldName = "SalarioDiario";
            gridViewTextBoxColumn11.FormatString = "{0:n2}";
            gridViewTextBoxColumn11.HeaderText = "SD";
            gridViewTextBoxColumn11.Name = "SalarioDiario";
            gridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn11.Width = 75;
            gridViewTextBoxColumn12.DataType = typeof(decimal);
            gridViewTextBoxColumn12.FieldName = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn12.FormatString = "{0:n2}";
            gridViewTextBoxColumn12.HeaderText = "SDI";
            gridViewTextBoxColumn12.Name = "SalarioDiarioIntegrado";
            gridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn12.Width = 75;
            gridViewTextBoxColumn13.DataType = typeof(int);
            gridViewTextBoxColumn13.FieldName = "JornadasTrabajo";
            gridViewTextBoxColumn13.HeaderText = "J. Trabajo";
            gridViewTextBoxColumn13.Name = "JornadasTrabajo";
            gridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn14.FieldName = "Correo";
            gridViewTextBoxColumn14.HeaderText = "Correo";
            gridViewTextBoxColumn14.Name = "Correo";
            gridViewTextBoxColumn14.Width = 145;
            gridViewTextBoxColumn15.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn15.FieldName = "FechaNuevo";
            gridViewTextBoxColumn15.FormatString = "{0:dd MMM yy}";
            gridViewTextBoxColumn15.HeaderText = "Fc. Sist.";
            gridViewTextBoxColumn15.Name = "FechaNuevo";
            gridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn15.Width = 75;
            gridViewTextBoxColumn16.FieldName = "Creo";
            gridViewTextBoxColumn16.HeaderText = "Creó";
            gridViewTextBoxColumn16.Name = "Creo";
            gridViewTextBoxColumn16.Width = 65;
            gridViewTextBoxColumn17.FieldName = "Modifica";
            gridViewTextBoxColumn17.HeaderText = "Modifica";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "Modifica";
            gridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn17.Width = 65;
            gridViewTextBoxColumn18.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn18.FieldName = "FechaModifica";
            gridViewTextBoxColumn18.HeaderText = "Fec. Mod.";
            gridViewTextBoxColumn18.IsVisible = false;
            gridViewTextBoxColumn18.Name = "FechaModifica";
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn18.Width = 75;
            this.gridViewEmpleados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.gridViewEmpleados.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridViewEmpleados.Name = "gridViewEmpleados";
            this.gridViewEmpleados.Size = new System.Drawing.Size(800, 420);
            this.gridViewEmpleados.TabIndex = 12;
            // 
            // ContratoCatalogoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gridViewEmpleados);
            this.Controls.Add(this.TContrato);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ContratoCatalogoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Empleados: Contratos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ContratoCatalogoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Forms.ToolBarStandarControl TContrato;
        internal Telerik.WinControls.UI.RadGridView gridViewEmpleados;
    }
}