﻿using System;
using System.ComponentModel;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Service;
using Jaeger.Domain.Base.Abstractions;
using Jaeger.Domain.Nomina.Entities;
using Telerik.WinControls.Enumerations;
using System.Windows.Forms;
using Telerik.WinControls;
using Jaeger.Domain.Nomina.Contracts;

namespace Jaeger.UI.Nomina.Forms.Empleados {
    public partial class ContratoCatalogoForm : RadForm {
        protected IEmpleadoContratoService service;
        private BindingList<IContratoDetailModel> contratos;
        private Domain.Base.ValueObjects.UIAction _permisos;

        public ContratoCatalogoForm(UIMenuElement menuElement) {
            InitializeComponent();
            this._permisos = new Domain.Base.ValueObjects.UIAction(menuElement.Permisos);
        }

        private void ContratoCatalogoForm_Load(object sender, EventArgs e) {
            this.service = new EmpleadoContratoService();
            this.gridViewEmpleados.TelerikGridCommon();

            this.TContrato.Nuevo.Enabled = this._permisos.Agregar;
            this.TContrato.Editar.Enabled = this._permisos.Editar;
            this.TContrato.Remover.Enabled = this._permisos.Remover;

            this.TContrato.Nuevo.Click += this.TContrato_Nuevo_Click;
            this.TContrato.Editar.Click += this.TContrato_Editar_Click;
            this.TContrato.Remover.Click += this.TContrato_Remover_Click;
            this.TContrato.Actualizar.Click += this.TContrato_Actualizar_Click;
            this.TContrato.Filtro.Click += this.TContrato_Filtro_Click;
            this.TContrato.Cerrar.Click += this.TContrato_Cerrar_Click;
        }

        #region barra de herramientas
        private void TContrato_Nuevo_Click(object sender, EventArgs e) {
            var _editar = new EmpleadoContratoForm(null);
            _editar.ShowDialog(this);
        }

        private void TContrato_Remover_Click(object sender, EventArgs e) {
            if (this.gridViewEmpleados.CurrentRow != null) {
                var _seleccionado = this.gridViewEmpleados.CurrentRow.DataBoundItem as ContratoDetailModel;
                if (_seleccionado != null) {
                    if (_seleccionado.Activo) {
                        if (RadMessageBox.Show(this, Properties.Resources.Msg_Empleado_Remover, "Atención", MessageBoxButtons.YesNo, RadMessageIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
                            if (this.service.Remove(_seleccionado.IdContrato)) {
                                this.gridViewEmpleados.Rows.Remove(this.gridViewEmpleados.CurrentRow);
                            }
                        } else {

                        }
                    }
                }
            }
        }

        private void TContrato_Editar_Click(object sender, EventArgs e) {
            var _seleccionado = this.gridViewEmpleados.CurrentRow.DataBoundItem as ContratoDetailModel;
            if (_seleccionado != null) {
                var _editar = new EmpleadoContratoForm(_seleccionado);
                _editar.ShowDialog(this);
            }
        }
        private void TContrato_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.Text = "Consultando...";
                espera.ShowDialog(this);
            }
            this.gridViewEmpleados.DataSource = this.contratos;
        }

        private void TContrato_Filtro_Click(object sender, EventArgs e) {
            this.gridViewEmpleados.ShowFilteringRow = this.TContrato.Filtro.ToggleState != ToggleState.On;
            if (this.gridViewEmpleados.ShowFilteringRow == false)
                this.gridViewEmpleados.FilterDescriptors.Clear();
        }

        private void TContrato_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.contratos = this.service.GetList(true);
        }
        #endregion
    }
}
