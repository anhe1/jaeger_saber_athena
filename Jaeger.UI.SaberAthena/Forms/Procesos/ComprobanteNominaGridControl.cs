﻿using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public class ComprobanteNominaGridControl : UI.Common.Forms.GridCommonControl {
        public ComprobanteNominaGridControl() : base() {
            IComprobanteGridBuilder view = new ComprobanteGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }
    }
}
