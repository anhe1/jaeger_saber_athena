﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Nomina.Builder;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public class PeriodoGridControl : RadGridView {
        public RadContextMenu MenuContextual = new RadContextMenu();
        protected internal RadMenuItem AgregarVacaciones = new RadMenuItem { Text = "Vacaciones" };
        protected internal RadMenuItem AgregarAusencias = new RadMenuItem { Text = "Ausencias" };
        protected internal RadMenuItem AgregarHorasExtra = new RadMenuItem { Text = "Horas Extra" };

        public GridViewTemplate GridPercepciones;
        public GridViewTemplate GridDeducciones;
        public GridViewTemplate GridVacaciones;
        public GridViewTemplate GridAusencias;
        public GridViewTemplate GridHorasExtra;

        public PeriodoGridControl() : base() {
            this.MenuContextual.Items.Add(this.AgregarHorasExtra);
            this.MenuContextual.Items.Add(this.AgregarAusencias);
            this.MenuContextual.Items.Add(this.AgregarVacaciones);
            this.ContextMenuOpening += this.GridData_ContextMenuOpening;
        }

        public void CreateView() {
            RadGridView radGridView = this;
            radGridView.AutoGenerateColumns = false;
            radGridView.AllowAddNewRow = false;
            radGridView.AllowEditRow = true;
            radGridView.AllowDeleteRow = false;
            radGridView.EnableFiltering = true;
            radGridView.ShowGroupPanel = false;
            radGridView.ShowFilteringRow = false;
            radGridView.EnableAlternatingRowColor = true;
            radGridView.AutoSizeRows = false;
            radGridView.AllowRowResize = false;

            this.GridPercepcionesView();
            this.GridDeduccionesView();
            this.GridHorasExtraView();
            this.MasterTemplate.Templates.AddRange(this.GridPercepciones, this.GridDeducciones, this.GridHorasExtra);

            IPeriodoGridViewBuilder view = new PeriodoGridViewBuilder();
            this.MasterTemplate.Columns.AddRange(view.Templetes().Master().Build());
            this.GridPercepciones.Columns.AddRange(view.Templetes().Percepciones().Build());
            this.GridDeducciones.Columns.AddRange(view.Templetes().Deducciones().Build());

            this.MasterTemplate.SearchRowPosition = SystemRowPosition.Bottom;
            this.MasterTemplate.SummaryRowsBottom.Add(new GridViewSummaryRowItem(new GridViewSummaryItem[] {
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "SueldosSalarios"},
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "TotalPercepciones" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "TotalPercepcionesGravado" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "TotalPercepcionesExento" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "TotalDeducciones" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "PercepcionEspecial" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "DescuentoPresatamo" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "DescuentoInfonavit" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "FondoAhorro" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "ValesDespensa" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "SueldoNeto" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "Sueldo" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "ISR" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "SubSidio" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "ISRRetener" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "CuotasIMSS" },
                new GridViewSummaryItem{ Aggregate = GridAggregateFunction.Sum, AggregateExpression = null, FormatString = "{0:n}", Name = "CuotasIMSSPatronal" } }
            ));

            this.GridPercepciones.SummaryRowsBottom.Add(new GridViewSummaryRowItem(new GridViewSummaryItem[] {
                new GridViewSummaryItem {
                    Aggregate = GridAggregateFunction.Sum,
                    AggregateExpression = null,
                    FormatString = "{0:n}",
                    Name = "ImporteGravado"
                },
                new GridViewSummaryItem {
                    Aggregate = GridAggregateFunction.Sum,
                    AggregateExpression = null,
                    FormatString = "{0:n}",
                    Name = "ImporteExento"
                }}
            ));


            this.GridDeducciones.SummaryRowsBottom.Add(new GridViewSummaryRowItem(new GridViewSummaryItem[] {
                new GridViewSummaryItem {
                    Aggregate = GridAggregateFunction.Sum,
                    AggregateExpression = null,
                    FormatString = "{0:n}",
                    Name = "ImporteGravado"
                }}
            ));

            this.GridPercepciones.HierarchyDataProvider = new GridViewEventDataProvider(this.GridPercepciones);
            this.GridDeducciones.HierarchyDataProvider = new GridViewEventDataProvider(this.GridDeducciones);
            this.GridHorasExtra.HierarchyDataProvider = new GridViewEventDataProvider(this.GridHorasExtra);
            this.RowSourceNeeded += new GridViewRowSourceNeededEventHandler(this.GridData_RowSourceNeeded);
            this.Rows.CollectionChanged += this.Rows_CollectionChanged;
        }

        public virtual void GridPercepcionesView() {
            this.GridPercepciones = new GridViewTemplate() {
                Caption = "Percepciones",
                AllowAddNewRow = false,
                AutoGenerateColumns = false,
                AllowDeleteRow = false,
                AllowEditRow = false,
                ShowGroupedColumns = false,
                EnableAlternatingRowColor = true,
                AllowRowResize = false
            };
        }

        public virtual void GridDeduccionesView() {
            this.GridDeducciones = new GridViewTemplate() {
                Caption = "Deducciones",
                AllowAddNewRow = false,
                AutoGenerateColumns = false,
                AllowDeleteRow = false,
                AllowEditRow = false,
                ShowGroupedColumns = false,
                EnableAlternatingRowColor = true,
                AllowRowResize = false
            };
        }

        public virtual void GridHorasExtraView() {
            this.GridHorasExtra = new GridViewTemplate() {
                Caption = "Horas Extra",
                AutoGenerateColumns = false,
                AllowAddNewRow = false,
                AllowEditRow = true,
                AllowDeleteRow = false,
                EnableFiltering = true,
                ShowFilteringRow = false,
                EnableAlternatingRowColor = true,
                AllowRowResize = false
            };
            IHorasExtraGridViewBuilder view = new HorasExtraGridViewBuilder();
            this.GridHorasExtra.Columns.AddRange(view.Templetes().Simple().Build());
        }

        private void Rows_CollectionChanged(object sender, Telerik.WinControls.Data.NotifyCollectionChangedEventArgs e) {
            if (e.Action == Telerik.WinControls.Data.NotifyCollectionChangedAction.ItemChanged) {
                var _seleccionado = this.CurrentRow.DataBoundItem as NominaPeriodoEmpleadoDetailModel;
                if (_seleccionado != null) {
                    _seleccionado.IsChange = true;
                }
            }
            Console.WriteLine(e.Action);
        }

        private void GridData_RowSourceNeeded(object sender, GridViewRowSourceNeededEventArgs e) {
            if (e.Template.Caption == this.GridPercepciones.Caption) {
                var rowView = e.ParentRow.DataBoundItem as NominaPeriodoEmpleadoDetailModel;
                if (rowView != null) {
                    if (rowView.Conceptos != null) {
                        foreach (var item in rowView.Conceptos) {
                            if (item.IdConceptoTipo == 1 && item.Total > 0) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["Clave"].Value = item.Clave;
                                row.Cells["Tipo"].Value = item.Tipo;
                                row.Cells["Concepto"].Value = item.Concepto;
                                row.Cells["ImporteExento"].Value = item.TPercepcionExento;
                                row.Cells["ImporteGravado"].Value = item.TPercepcionGravado;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.GridDeducciones.Caption) {
                var rowView = e.ParentRow.DataBoundItem as NominaPeriodoEmpleadoDetailModel;
                if (rowView != null) {
                    if (rowView.Conceptos != null) {
                        foreach (var item in rowView.Conceptos) {
                            if (item.IdConceptoTipo == 2) {
                                GridViewRowInfo row = e.Template.Rows.NewRow();
                                row.Cells["Clave"].Value = item.Clave;
                                row.Cells["Tipo"].Value = item.Tipo;
                                row.Cells["Concepto"].Value = item.Concepto;
                                row.Cells["ImporteGravado"].Value = item.TDeducciones;
                                e.SourceCollection.Add(row);
                            }
                        }
                    }
                }
            } else if (e.Template.Caption == this.GridHorasExtra.Caption) {
                var rowView = e.ParentRow.DataBoundItem as NominaPeriodoEmpleadoDetailModel;
                if (rowView != null) {
                    if (rowView.HorasExtra != null) {
                        foreach (var item in rowView.HorasExtra) {
                            GridViewRowInfo row = e.Template.Rows.NewRow();
                            row.Cells["Fecha"].Value = item.Fecha;
                            row.Cells["Cantidad"].Value = item.Cantidad;
                            e.SourceCollection.Add(row);
                        }
                    }
                }
            }
        }

        public virtual void GridData_ContextMenuOpening(object sender, ContextMenuOpeningEventArgs e) {
            if (e.ContextMenuProvider is GridHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridRowHeaderCellElement) {
            } else if (e.ContextMenuProvider is GridFilterCellElement) {
            } else if (e.ContextMenuProvider is GridDataCellElement) {
                if (this.CurrentRow.ViewInfo.ViewTemplate == this.MasterTemplate) {
                    e.ContextMenu = this.MenuContextual.DropDown;
                }
            }
        }
    }
}
