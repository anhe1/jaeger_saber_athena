﻿using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.WinControls.UI;
using Jaeger.Domain.Nomina.Entities;
using Jaeger.UI.Common.Services;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Service;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class PeriodoAbrirForm : RadForm {
        #region declaraciones
        protected INominaPeriodoService service;
        private List<NominaPeriodoModel> currentPeriodos;
        #endregion

        #region eventos
        public void OnSeleccionar(NominaPeriodoModel e) {
            if (this.PeriodoSeleccionado != null) {
                this.PeriodoSeleccionado(this, e);
            }
        }

        public event EventHandler<NominaPeriodoModel> PeriodoSeleccionado;
        #endregion

        #region formulario
        public PeriodoAbrirForm() {
            InitializeComponent();
        }

        private void NominaAbrir_Load(object sender, EventArgs e) {
            this.service = new NominaPeriodoService();
            this.radGridPeriodos.TelerikGridCommon();
            this.radGridPeriodos.ShowFilteringRow = true;
            this.radGridPeriodos.AllowEditRow = true;

            var comboStatus = this.radGridPeriodos.Columns["IdStatus"] as GridViewComboBoxColumn;
            comboStatus.DisplayMember = "Descripcion";
            comboStatus.ValueMember = "Id";
            comboStatus.DataSource = CommonService.GetPeriodoStatus();

            var comboPeriodoTipo = this.radGridPeriodos.Columns["IdPeriodoTipo"] as GridViewComboBoxColumn;
            comboPeriodoTipo.DisplayMember = "Descripcion";
            comboPeriodoTipo.ValueMember = "Id";
            comboPeriodoTipo.DataSource = CommonService.GetPeriodos();

            this.ToolBar.Editar.Click += this.TPeriodo_Editar_Click;
            this.ToolBar.Actualizar.Click += this.TPeriodo_Actualizar_Click;
            this.ToolBar.Cerrar.Click += this.ToolBarButtonCerrar_Click;
        }
        #endregion

        #region barra de herramientas
        private void TPeriodo_Editar_Click(object sender, EventArgs e) {
            if (this.radGridPeriodos.CurrentRow != null) {
                var _seleccionado = this.radGridPeriodos.CurrentRow.DataBoundItem as NominaPeriodoModel;
                if (_seleccionado != null) {
                    this.OnSeleccionar(_seleccionado);
                } else {
                    this.OnSeleccionar(null);
                }
                this.Close();
            }
        }

        private void TPeriodo_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Common.Forms.Waiting1Form(this.Consultar)) {
                espera.Text = "Consultando ...";
                espera.ShowDialog(this);
            }
            this.radGridPeriodos.DataSource = this.currentPeriodos;
        }

        private void ToolBarButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }
        #endregion

        #region metodos privados
        private void Consultar() {
            this.currentPeriodos = this.service.GetList(this.ToolBar.GetMes(), this.ToolBar.GetEjercicio()).ToList();
        }
        #endregion
    }
}
