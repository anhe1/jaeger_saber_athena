﻿namespace Jaeger.UI.Nomina.Forms.Procesos
{
    partial class ConceptoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.Clave = new Telerik.WinControls.UI.RadTextBox();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.Ocultar = new Telerik.WinControls.UI.RadCheckBox();
            this.Generales = new Telerik.WinControls.UI.RadGroupBox();
            this.ConceptoTipo = new Telerik.WinControls.UI.RadDropDownList();
            this.checkActivo = new Telerik.WinControls.UI.RadCheckBox();
            this.TipoNomina = new Telerik.WinControls.UI.RadDropDownList();
            this.PagoEspecie = new Telerik.WinControls.UI.RadCheckBox();
            this.Aplicacion = new Telerik.WinControls.UI.RadDropDownList();
            this.ClaveSAT = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.Formula = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.ISR = new Telerik.WinControls.UI.RadGroupBox();
            this.ConceptoBaseFormula = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.ConceptoBase = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.TConcepto = new Jaeger.UI.Common.Forms.ToolBarStandarControl();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ocultar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Generales)).BeginInit();
            this.Generales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoTipo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkActivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagoEspecie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aplicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSAT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSAT.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSAT.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Formula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ISR)).BeginInit();
            this.ISR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoBaseFormula)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(462, 33);
            this.Encabezado.TabIndex = 103;
            this.Encabezado.TabStop = false;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(16, 22);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(35, 18);
            this.radLabel2.TabIndex = 105;
            this.radLabel2.Text = "Clave:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 150);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(58, 18);
            this.radLabel3.TabIndex = 106;
            this.radLabel3.Text = "Clave SAT:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(16, 47);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(67, 18);
            this.radLabel4.TabIndex = 107;
            this.radLabel4.Text = "Descripción:";
            // 
            // Clave
            // 
            this.Clave.Location = new System.Drawing.Point(94, 21);
            this.Clave.Name = "Clave";
            this.Clave.NullText = "P/D 00#";
            this.Clave.Size = new System.Drawing.Size(61, 20);
            this.Clave.TabIndex = 111;
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(94, 46);
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(346, 20);
            this.Descripcion.TabIndex = 113;
            // 
            // Ocultar
            // 
            this.Ocultar.Location = new System.Drawing.Point(248, 150);
            this.Ocultar.Name = "Ocultar";
            this.Ocultar.Size = new System.Drawing.Size(192, 18);
            this.Ocultar.TabIndex = 117;
            this.Ocultar.Text = "Ocultar en representación impresa";
            // 
            // Generales
            // 
            this.Generales.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.Generales.Controls.Add(this.ConceptoTipo);
            this.Generales.Controls.Add(this.Clave);
            this.Generales.Controls.Add(this.checkActivo);
            this.Generales.Controls.Add(this.radLabel2);
            this.Generales.Controls.Add(this.TipoNomina);
            this.Generales.Controls.Add(this.PagoEspecie);
            this.Generales.Controls.Add(this.Aplicacion);
            this.Generales.Controls.Add(this.Ocultar);
            this.Generales.Controls.Add(this.Descripcion);
            this.Generales.Controls.Add(this.ClaveSAT);
            this.Generales.Controls.Add(this.radLabel3);
            this.Generales.Controls.Add(this.radLabel4);
            this.Generales.Controls.Add(this.radLabel1);
            this.Generales.Controls.Add(this.Formula);
            this.Generales.Controls.Add(this.radLabel7);
            this.Generales.Controls.Add(this.radLabel8);
            this.Generales.Dock = System.Windows.Forms.DockStyle.Top;
            this.Generales.HeaderText = "Generales";
            this.Generales.Location = new System.Drawing.Point(0, 63);
            this.Generales.Name = "Generales";
            this.Generales.Size = new System.Drawing.Size(462, 184);
            this.Generales.TabIndex = 118;
            this.Generales.Text = "Generales";
            // 
            // ConceptoTipo
            // 
            this.ConceptoTipo.DisplayMember = "Descripcion";
            this.ConceptoTipo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ConceptoTipo.Location = new System.Drawing.Point(273, 21);
            this.ConceptoTipo.Name = "ConceptoTipo";
            this.ConceptoTipo.NullText = "Selecciona";
            this.ConceptoTipo.Size = new System.Drawing.Size(97, 20);
            this.ConceptoTipo.TabIndex = 128;
            this.ConceptoTipo.ValueMember = "ID";
            this.ConceptoTipo.Validated += new System.EventHandler(this.ConceptoTipo_Validated);
            // 
            // checkActivo
            // 
            this.checkActivo.Location = new System.Drawing.Point(389, 22);
            this.checkActivo.Name = "checkActivo";
            this.checkActivo.Size = new System.Drawing.Size(51, 18);
            this.checkActivo.TabIndex = 117;
            this.checkActivo.Text = "Activo";
            // 
            // TipoNomina
            // 
            this.TipoNomina.DataMember = "Id";
            this.TipoNomina.DisplayMember = "Descripcion";
            this.TipoNomina.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoNomina.Location = new System.Drawing.Point(94, 124);
            this.TipoNomina.Name = "TipoNomina";
            this.TipoNomina.NullText = "Selecciona";
            this.TipoNomina.Size = new System.Drawing.Size(122, 20);
            this.TipoNomina.TabIndex = 125;
            // 
            // PagoEspecie
            // 
            this.PagoEspecie.Location = new System.Drawing.Point(248, 124);
            this.PagoEspecie.Name = "PagoEspecie";
            this.PagoEspecie.Size = new System.Drawing.Size(101, 18);
            this.PagoEspecie.TabIndex = 117;
            this.PagoEspecie.Text = "Pago en especie";
            // 
            // Aplicacion
            // 
            this.Aplicacion.DataMember = "Id";
            this.Aplicacion.DisplayMember = "Descripcion";
            this.Aplicacion.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.Aplicacion.Location = new System.Drawing.Point(94, 98);
            this.Aplicacion.Name = "Aplicacion";
            this.Aplicacion.NullText = "Selecciona";
            this.Aplicacion.Size = new System.Drawing.Size(122, 20);
            this.Aplicacion.TabIndex = 126;
            this.Aplicacion.ValueMember = "Id";
            // 
            // ClaveSAT
            // 
            this.ClaveSAT.AutoSizeDropDownToBestFit = true;
            this.ClaveSAT.DisplayMember = "Clave";
            this.ClaveSAT.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // ClaveSAT.NestedRadGridView
            // 
            this.ClaveSAT.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.ClaveSAT.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClaveSAT.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ClaveSAT.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.ClaveSAT.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.ClaveSAT.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.ClaveSAT.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.ClaveSAT.EditorControl.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn1.FieldName = "Clave";
            gridViewTextBoxColumn1.HeaderText = "Clave";
            gridViewTextBoxColumn1.Name = "Clave";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 100;
            this.ClaveSAT.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.ClaveSAT.EditorControl.MasterTemplate.EnableGrouping = false;
            this.ClaveSAT.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.ClaveSAT.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.ClaveSAT.EditorControl.Name = "NestedRadGridView";
            this.ClaveSAT.EditorControl.ReadOnly = true;
            this.ClaveSAT.EditorControl.ShowGroupPanel = false;
            this.ClaveSAT.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.ClaveSAT.EditorControl.TabIndex = 0;
            this.ClaveSAT.Location = new System.Drawing.Point(94, 150);
            this.ClaveSAT.Name = "ClaveSAT";
            this.ClaveSAT.Size = new System.Drawing.Size(100, 20);
            this.ClaveSAT.TabIndex = 120;
            this.ClaveSAT.TabStop = false;
            this.ClaveSAT.Text = "...";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(16, 98);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(60, 18);
            this.radLabel1.TabIndex = 124;
            this.radLabel1.Text = "Aplicación:";
            // 
            // Formula
            // 
            this.Formula.Location = new System.Drawing.Point(94, 72);
            this.Formula.Name = "Formula";
            this.Formula.Size = new System.Drawing.Size(346, 20);
            this.Formula.TabIndex = 114;
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(16, 73);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(49, 18);
            this.radLabel7.TabIndex = 108;
            this.radLabel7.Text = "Formula:";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(16, 124);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(76, 18);
            this.radLabel8.TabIndex = 104;
            this.radLabel8.Text = "T. de Nómina:";
            // 
            // ISR
            // 
            this.ISR.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.ISR.Controls.Add(this.ConceptoBaseFormula);
            this.ISR.Controls.Add(this.radLabel6);
            this.ISR.Controls.Add(this.ConceptoBase);
            this.ISR.Controls.Add(this.radLabel5);
            this.ISR.Dock = System.Windows.Forms.DockStyle.Top;
            this.ISR.HeaderText = "ISR";
            this.ISR.Location = new System.Drawing.Point(0, 247);
            this.ISR.Name = "ISR";
            this.ISR.Size = new System.Drawing.Size(462, 79);
            this.ISR.TabIndex = 0;
            this.ISR.Text = "ISR";
            // 
            // ConceptoBaseFormula
            // 
            this.ConceptoBaseFormula.Location = new System.Drawing.Point(94, 47);
            this.ConceptoBaseFormula.MaxLength = 150;
            this.ConceptoBaseFormula.Name = "ConceptoBaseFormula";
            this.ConceptoBaseFormula.Size = new System.Drawing.Size(346, 20);
            this.ConceptoBaseFormula.TabIndex = 129;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(16, 48);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(49, 18);
            this.radLabel6.TabIndex = 128;
            this.radLabel6.Text = "Formula:";
            // 
            // ConceptoBase
            // 
            this.ConceptoBase.DisplayMember = "Descripcion";
            this.ConceptoBase.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ConceptoBase.Location = new System.Drawing.Point(94, 21);
            this.ConceptoBase.Name = "ConceptoBase";
            this.ConceptoBase.NullText = "Selecciona";
            this.ConceptoBase.Size = new System.Drawing.Size(122, 20);
            this.ConceptoBase.TabIndex = 127;
            this.ConceptoBase.ValueMember = "ID";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(16, 21);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(48, 18);
            this.radLabel5.TabIndex = 126;
            this.radLabel5.Text = "Importe:";
            // 
            // TConcepto
            // 
            this.TConcepto.Dock = System.Windows.Forms.DockStyle.Top;
            this.TConcepto.Etiqueta = "";
            this.TConcepto.Location = new System.Drawing.Point(0, 33);
            this.TConcepto.Name = "TConcepto";
            this.TConcepto.ShowActualizar = true;
            this.TConcepto.ShowAutorizar = false;
            this.TConcepto.ShowCerrar = true;
            this.TConcepto.ShowEditar = false;
            this.TConcepto.ShowExportarExcel = false;
            this.TConcepto.ShowFiltro = false;
            this.TConcepto.ShowGuardar = true;
            this.TConcepto.ShowHerramientas = false;
            this.TConcepto.ShowImagen = false;
            this.TConcepto.ShowImprimir = false;
            this.TConcepto.ShowNuevo = false;
            this.TConcepto.ShowRemover = false;
            this.TConcepto.Size = new System.Drawing.Size(462, 30);
            this.TConcepto.TabIndex = 192;
            
            // 
            // ConceptoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 331);
            this.Controls.Add(this.ISR);
            this.Controls.Add(this.Generales);
            this.Controls.Add(this.TConcepto);
            this.Controls.Add(this.Encabezado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConceptoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Concepto de nómina";
            this.Load += new System.EventHandler(this.ConceptoNominaForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Clave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ocultar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Generales)).EndInit();
            this.Generales.ResumeLayout(false);
            this.Generales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoTipo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkActivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PagoEspecie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Aplicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSAT.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSAT.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaveSAT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Formula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ISR)).EndInit();
            this.ISR.ResumeLayout(false);
            this.ISR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoBaseFormula)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConceptoBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox Clave;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadCheckBox Ocultar;
        private Telerik.WinControls.UI.RadGroupBox Generales;
        private Telerik.WinControls.UI.RadGroupBox ISR;
        private Telerik.WinControls.UI.RadTextBox Formula;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadMultiColumnComboBox ClaveSAT;
        private Telerik.WinControls.UI.RadCheckBox checkActivo;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadDropDownList Aplicacion;
        private Telerik.WinControls.UI.RadDropDownList TipoNomina;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Common.Forms.ToolBarStandarControl TConcepto;
        private Telerik.WinControls.UI.RadCheckBox PagoEspecie;
        private Telerik.WinControls.UI.RadTextBox ConceptoBaseFormula;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadDropDownList ConceptoBase;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList ConceptoTipo;
    }
}
