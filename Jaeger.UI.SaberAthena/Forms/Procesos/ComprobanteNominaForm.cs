﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class ComprobanteNominaForm : RadForm {
        protected internal INominaControlService _Service;
        protected internal List<ComprobanteNominaSingleModel> _DataSource;

        public ComprobanteNominaForm() {
            InitializeComponent();
        }

        private void ComprobanteNominaForm_Load(object sender, EventArgs e) {
            this.TNomina.Actualizar.Click += TNomina_Actualizar_Click;
            this.TNomina.Cerrar.Click += this.TNomina_Cerrar_Click;
        }

        private void TNomina_Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
            this.TNomina.GridData.DataSource = _DataSource;
        }

        private void TNomina_Cerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void Consultar() {
            var query = Aplication.Nomina.Services.NominaControlService.Query().Year(this.TNomina.GetEjercicio()).Month(this.TNomina.GetPeriodo());
            this._DataSource = this._Service.GetList<ComprobanteNominaSingleModel>(query.Build()).ToList();
        }
    }
}
