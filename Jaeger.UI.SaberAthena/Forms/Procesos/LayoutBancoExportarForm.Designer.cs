﻿namespace Jaeger.UI.Nomina.Forms {
    partial class LayoutBancoExportarForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LayoutBancoExportarForm));
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.checkBoxRFC = new Telerik.WinControls.UI.RadCheckBox();
            this.fechaAplicacion = new Telerik.WinControls.UI.RadDateTimePicker();
            this.textBoxGrupoAfinidad = new Telerik.WinControls.UI.RadTextBox();
            this.textBoxNumeroCuenta = new Telerik.WinControls.UI.RadTextBox();
            this.comboBoxLayout = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButtonExportar = new Telerik.WinControls.UI.RadButton();
            this.radButtonCerrar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxRFC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fechaAplicacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxGrupoAfinidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNumeroCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonExportar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonCerrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.checkBoxRFC);
            this.radGroupBox1.Controls.Add(this.fechaAplicacion);
            this.radGroupBox1.Controls.Add(this.textBoxGrupoAfinidad);
            this.radGroupBox1.Controls.Add(this.textBoxNumeroCuenta);
            this.radGroupBox1.Controls.Add(this.comboBoxLayout);
            this.radGroupBox1.Controls.Add(this.radLabel3);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.radLabel2);
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Información de Layout";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(247, 271);
            this.radGroupBox1.TabIndex = 1;
            this.radGroupBox1.Text = "Información de Layout";
            // 
            // checkBoxRFC
            // 
            this.checkBoxRFC.Location = new System.Drawing.Point(16, 239);
            this.checkBoxRFC.Name = "checkBoxRFC";
            this.checkBoxRFC.Size = new System.Drawing.Size(153, 18);
            this.checkBoxRFC.TabIndex = 4;
            this.checkBoxRFC.Text = "Incluir RFC del beneficiario";
            // 
            // fechaAplicacion
            // 
            this.fechaAplicacion.Location = new System.Drawing.Point(16, 199);
            this.fechaAplicacion.Name = "fechaAplicacion";
            this.fechaAplicacion.Size = new System.Drawing.Size(215, 20);
            this.fechaAplicacion.TabIndex = 3;
            this.fechaAplicacion.TabStop = false;
            this.fechaAplicacion.Text = "sábado, 19 de diciembre de 2020";
            this.fechaAplicacion.Value = new System.DateTime(2020, 12, 19, 0, 18, 16, 649);
            // 
            // textBoxGrupoAfinidad
            // 
            this.textBoxGrupoAfinidad.Location = new System.Drawing.Point(16, 149);
            this.textBoxGrupoAfinidad.Name = "textBoxGrupoAfinidad";
            this.textBoxGrupoAfinidad.NullText = "Grupo Afonidad (Banbajio)";
            this.textBoxGrupoAfinidad.Size = new System.Drawing.Size(215, 20);
            this.textBoxGrupoAfinidad.TabIndex = 2;
            // 
            // textBoxNumeroCuenta
            // 
            this.textBoxNumeroCuenta.Location = new System.Drawing.Point(16, 99);
            this.textBoxNumeroCuenta.Name = "textBoxNumeroCuenta";
            this.textBoxNumeroCuenta.NullText = "Núm. de Cuenta";
            this.textBoxNumeroCuenta.Size = new System.Drawing.Size(215, 20);
            this.textBoxNumeroCuenta.TabIndex = 2;
            // 
            // comboBoxLayout
            // 
            this.comboBoxLayout.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "Banbajio";
            radListDataItem2.Text = "Bancomer";
            this.comboBoxLayout.Items.Add(radListDataItem1);
            this.comboBoxLayout.Items.Add(radListDataItem2);
            this.comboBoxLayout.Location = new System.Drawing.Point(16, 49);
            this.comboBoxLayout.Name = "comboBoxLayout";
            this.comboBoxLayout.NullText = "Selecciona";
            this.comboBoxLayout.Size = new System.Drawing.Size(215, 20);
            this.comboBoxLayout.TabIndex = 1;
            this.comboBoxLayout.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.comboBoxLayout_SelectedIndexChanged);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(16, 175);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(108, 18);
            this.radLabel3.TabIndex = 0;
            this.radLabel3.Text = "Fecha de Aplicación:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(16, 125);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(83, 18);
            this.radLabel4.TabIndex = 0;
            this.radLabel4.Text = "Grupo afinidad:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(16, 75);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(90, 18);
            this.radLabel2.TabIndex = 0;
            this.radLabel2.Text = "Núm. de Cuenta:";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(16, 25);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(114, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Selecciona un Layout:";
            // 
            // radButtonExportar
            // 
            this.radButtonExportar.Location = new System.Drawing.Point(125, 277);
            this.radButtonExportar.Name = "radButtonExportar";
            this.radButtonExportar.Size = new System.Drawing.Size(110, 24);
            this.radButtonExportar.TabIndex = 4;
            this.radButtonExportar.Text = "Exportar";
            this.radButtonExportar.Click += new System.EventHandler(this.radButtonExportar_Click);
            // 
            // radButtonCerrar
            // 
            this.radButtonCerrar.Location = new System.Drawing.Point(9, 277);
            this.radButtonCerrar.Name = "radButtonCerrar";
            this.radButtonCerrar.Size = new System.Drawing.Size(110, 24);
            this.radButtonCerrar.TabIndex = 4;
            this.radButtonCerrar.Text = "Cerrar";
            this.radButtonCerrar.Click += new System.EventHandler(this.radButtonCerrar_Click);
            // 
            // BancoLayoutExportarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(247, 308);
            this.Controls.Add(this.radButtonExportar);
            this.Controls.Add(this.radGroupBox1);
            this.Controls.Add(this.radButtonCerrar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BancoLayoutExportarForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Exportar";
            this.Load += new System.EventHandler(this.BancoLayoutExportarForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkBoxRFC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fechaAplicacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxGrupoAfinidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxNumeroCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonExportar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButtonCerrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadButton radButtonExportar;
        private Telerik.WinControls.UI.RadButton radButtonCerrar;
        private Telerik.WinControls.UI.RadDateTimePicker fechaAplicacion;
        private Telerik.WinControls.UI.RadTextBox textBoxGrupoAfinidad;
        private Telerik.WinControls.UI.RadTextBox textBoxNumeroCuenta;
        private Telerik.WinControls.UI.RadDropDownList comboBoxLayout;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadCheckBox checkBoxRFC;
    }
}
