﻿using System;
using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public class RegistroVacacionesGridControl : Common.Forms.GridCommonControl {
        public RegistroVacacionesGridControl() : base() { }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            IRegistroVacacionesGridViewBuilder view = new RegistroVacacionesGridViewBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
        }
    }
}
