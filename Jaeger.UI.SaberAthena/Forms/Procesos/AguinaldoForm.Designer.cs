﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class AguinaldoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn2 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject3 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject4 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem12 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem13 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem14 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem15 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem16 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem17 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem18 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem19 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem20 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem21 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem22 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            this.gridEmpleados = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutosuma = false;
            this.ToolBar.ShowCancelar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowEjercicio = true;
            this.ToolBar.ShowExportarExcel = true;
            this.ToolBar.ShowFiltro = true;
            this.ToolBar.ShowHerramientas = true;
            this.ToolBar.ShowImprimir = true;
            this.ToolBar.ShowNuevo = true;
            this.ToolBar.ShowPeriodo = false;
            this.ToolBar.Size = new System.Drawing.Size(1088, 30);
            this.ToolBar.TabIndex = 4;
            // 
            // gridEmpleados
            // 
            this.gridEmpleados.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEmpleados.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            this.gridEmpleados.MasterTemplate.AllowAddNewRow = false;
            gridViewTextBoxColumn18.DataType = typeof(int);
            gridViewTextBoxColumn18.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn18.FieldName = "IdEmpleado";
            gridViewTextBoxColumn18.FormatString = "{0:0000#}";
            gridViewTextBoxColumn18.HeaderText = "Núm";
            gridViewTextBoxColumn18.Name = "IdEmpleado";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn19.FieldName = "Clave";
            gridViewTextBoxColumn19.HeaderText = "Clave";
            gridViewTextBoxColumn19.Name = "Clave";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.Width = 60;
            gridViewTextBoxColumn20.FieldName = "Nombre";
            gridViewTextBoxColumn20.HeaderText = "Nombre";
            gridViewTextBoxColumn20.Name = "Nombre";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.Width = 220;
            gridViewDateTimeColumn2.FieldName = "FecInicioRelLaboral";
            gridViewDateTimeColumn2.FormatString = "{0:dd MMM yyyy}";
            gridViewDateTimeColumn2.HeaderText = "Fec. Alta";
            gridViewDateTimeColumn2.Name = "FecInicioRelLaboral";
            gridViewDateTimeColumn2.Width = 75;
            gridViewTextBoxColumn21.DataType = typeof(decimal);
            gridViewTextBoxColumn21.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn21.FieldName = "DiasLaborados";
            gridViewTextBoxColumn21.FormatString = "{0:N2}";
            gridViewTextBoxColumn21.HeaderText = "Dias Laborados";
            gridViewTextBoxColumn21.Name = "DiasLaborados";
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn21.WrapText = true;
            gridViewTextBoxColumn22.DataType = typeof(decimal);
            gridViewTextBoxColumn22.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn22.FieldName = "SalarioDiario";
            gridViewTextBoxColumn22.FormatString = "{0:n}";
            gridViewTextBoxColumn22.HeaderText = "Salario Diario";
            gridViewTextBoxColumn22.Name = "SalarioDiario";
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn22.Width = 60;
            gridViewTextBoxColumn22.WrapText = true;
            gridViewTextBoxColumn23.DataType = typeof(decimal);
            gridViewTextBoxColumn23.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn23.FieldName = "SalarioMensual";
            gridViewTextBoxColumn23.FormatString = "{0:n}";
            gridViewTextBoxColumn23.HeaderText = "Salario Mensual";
            gridViewTextBoxColumn23.Name = "SalarioMensual";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn23.Width = 80;
            gridViewTextBoxColumn23.WrapText = true;
            gridViewTextBoxColumn24.DataType = typeof(decimal);
            gridViewTextBoxColumn24.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn24.FieldName = "DiasAguinaldo";
            gridViewTextBoxColumn24.FormatString = "{0:N2}";
            gridViewTextBoxColumn24.HeaderText = "Días Aguinaldo";
            gridViewTextBoxColumn24.MaxLength = 10;
            gridViewTextBoxColumn24.Name = "DiasAguinaldo";
            gridViewTextBoxColumn24.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn25.DataType = typeof(decimal);
            gridViewTextBoxColumn25.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn25.FieldName = "Importe";
            gridViewTextBoxColumn25.FormatString = "{0:n}";
            gridViewTextBoxColumn25.HeaderText = "Aguinaldo";
            gridViewTextBoxColumn25.Name = "Aguinaldo";
            gridViewTextBoxColumn25.ReadOnly = true;
            gridViewTextBoxColumn25.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn25.Width = 80;
            gridViewTextBoxColumn25.WrapText = true;
            gridViewTextBoxColumn26.DataType = typeof(decimal);
            gridViewTextBoxColumn26.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn26.FieldName = "BaseGravable";
            gridViewTextBoxColumn26.FormatString = "{0:n}";
            gridViewTextBoxColumn26.HeaderText = "Base Gravable";
            gridViewTextBoxColumn26.Name = "BaseGravable";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn26.Width = 80;
            gridViewTextBoxColumn26.WrapText = true;
            gridViewTextBoxColumn27.DataType = typeof(decimal);
            gridViewTextBoxColumn27.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn27.FieldName = "BaseExento";
            gridViewTextBoxColumn27.FormatString = "{0:n}";
            gridViewTextBoxColumn27.HeaderText = "Base Exento";
            gridViewTextBoxColumn27.Name = "BaseExento";
            gridViewTextBoxColumn27.ReadOnly = true;
            gridViewTextBoxColumn27.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn27.Width = 80;
            gridViewTextBoxColumn27.WrapText = true;
            gridViewTextBoxColumn28.DataType = typeof(decimal);
            gridViewTextBoxColumn28.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn28.FieldName = "BaseGravada";
            gridViewTextBoxColumn28.FormatString = "{0:n}";
            gridViewTextBoxColumn28.HeaderText = "Base Gravada";
            gridViewTextBoxColumn28.Name = "BaseGravada";
            gridViewTextBoxColumn28.ReadOnly = true;
            gridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn28.Width = 80;
            gridViewTextBoxColumn28.WrapText = true;
            gridViewTextBoxColumn29.DataType = typeof(decimal);
            gridViewTextBoxColumn29.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn29.FieldName = "ISR";
            gridViewTextBoxColumn29.FormatString = "{0:n}";
            gridViewTextBoxColumn29.HeaderText = "ISR Ag + Sld";
            gridViewTextBoxColumn29.Name = "ISR";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn29.Width = 80;
            gridViewTextBoxColumn29.WrapText = true;
            gridViewTextBoxColumn30.DataType = typeof(decimal);
            gridViewTextBoxColumn30.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn30.FieldName = "Subsidio";
            gridViewTextBoxColumn30.FormatString = "{0:n}";
            gridViewTextBoxColumn30.HeaderText = "Subsidio";
            gridViewTextBoxColumn30.Name = "Subsidio";
            gridViewTextBoxColumn30.ReadOnly = true;
            gridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn30.Width = 80;
            gridViewTextBoxColumn30.WrapText = true;
            conditionalFormattingObject3.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.CellForeColor = System.Drawing.Color.Red;
            conditionalFormattingObject3.ConditionType = Telerik.WinControls.UI.ConditionTypes.Less;
            conditionalFormattingObject3.Name = "Valor Negativo ISR Salario (+Aguinaldo)";
            conditionalFormattingObject3.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject3.TValue1 = "0";
            gridViewTextBoxColumn31.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            gridViewTextBoxColumn31.DataType = typeof(decimal);
            gridViewTextBoxColumn31.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn31.FieldName = "ISRSalarioMasAguinaldo";
            gridViewTextBoxColumn31.FormatString = "{0:n}";
            gridViewTextBoxColumn31.HeaderText = "ISR Salario + Aguinaldo";
            gridViewTextBoxColumn31.Name = "ISRSalarioMasAguinaldo";
            gridViewTextBoxColumn31.ReadOnly = true;
            gridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn31.Width = 80;
            gridViewTextBoxColumn31.WrapText = true;
            conditionalFormattingObject4.CellBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.CellForeColor = System.Drawing.Color.Red;
            conditionalFormattingObject4.ConditionType = Telerik.WinControls.UI.ConditionTypes.Less;
            conditionalFormattingObject4.Name = "Valor Negativo ISR Salario";
            conditionalFormattingObject4.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.RowForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject4.TValue1 = "0";
            gridViewTextBoxColumn32.ConditionalFormattingObjectList.Add(conditionalFormattingObject4);
            gridViewTextBoxColumn32.DataType = typeof(decimal);
            gridViewTextBoxColumn32.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn32.FieldName = "ISRSueldo";
            gridViewTextBoxColumn32.FormatString = "{0:n}";
            gridViewTextBoxColumn32.HeaderText = "ISR Salario";
            gridViewTextBoxColumn32.Name = "ISRSueldo";
            gridViewTextBoxColumn32.ReadOnly = true;
            gridViewTextBoxColumn32.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn32.Width = 80;
            gridViewTextBoxColumn32.WrapText = true;
            gridViewTextBoxColumn33.DataType = typeof(decimal);
            gridViewTextBoxColumn33.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn33.FieldName = "ISRAguinaldo";
            gridViewTextBoxColumn33.FormatString = "{0:n}";
            gridViewTextBoxColumn33.HeaderText = "ISR Aguinaldo";
            gridViewTextBoxColumn33.Name = "ISRAguinaldo";
            gridViewTextBoxColumn33.ReadOnly = true;
            gridViewTextBoxColumn33.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn33.Width = 80;
            gridViewTextBoxColumn33.WrapText = true;
            gridViewTextBoxColumn34.DataType = typeof(decimal);
            gridViewTextBoxColumn34.ExcelExportType = Telerik.WinControls.UI.Export.DisplayFormatType.Fixed;
            gridViewTextBoxColumn34.FieldName = "AguinaldoPagar";
            gridViewTextBoxColumn34.FormatString = "{0:n}";
            gridViewTextBoxColumn34.HeaderText = "Gratificación";
            gridViewTextBoxColumn34.Name = "AguinaldoPagar";
            gridViewTextBoxColumn34.ReadOnly = true;
            gridViewTextBoxColumn34.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn34.Width = 80;
            gridViewTextBoxColumn34.WrapText = true;
            this.gridEmpleados.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewDateTimeColumn2,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32,
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34});
            gridViewSummaryItem12.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem12.AggregateExpression = null;
            gridViewSummaryItem12.FormatString = "{0:N2}";
            gridViewSummaryItem12.Name = "SalarioMensual";
            gridViewSummaryItem13.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem13.AggregateExpression = null;
            gridViewSummaryItem13.FormatString = "{0:N2}";
            gridViewSummaryItem13.Name = "Aguinaldo";
            gridViewSummaryItem14.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem14.AggregateExpression = null;
            gridViewSummaryItem14.FormatString = "{0:N2}";
            gridViewSummaryItem14.Name = "BaseGravable";
            gridViewSummaryItem15.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem15.AggregateExpression = null;
            gridViewSummaryItem15.FormatString = "{0:N2}";
            gridViewSummaryItem15.Name = "BaseExento";
            gridViewSummaryItem16.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem16.AggregateExpression = null;
            gridViewSummaryItem16.FormatString = "{0:N2}";
            gridViewSummaryItem16.Name = "BaseGravada";
            gridViewSummaryItem17.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem17.AggregateExpression = null;
            gridViewSummaryItem17.FormatString = "{0:N2}";
            gridViewSummaryItem17.Name = "ISR";
            gridViewSummaryItem18.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem18.AggregateExpression = null;
            gridViewSummaryItem18.FormatString = "{0:N2}";
            gridViewSummaryItem18.Name = "Subsidio";
            gridViewSummaryItem19.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem19.AggregateExpression = null;
            gridViewSummaryItem19.FormatString = "{0:N2}";
            gridViewSummaryItem19.Name = "ISRSalarioMasAguinaldo";
            gridViewSummaryItem20.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem20.AggregateExpression = null;
            gridViewSummaryItem20.FormatString = "{0:N2}";
            gridViewSummaryItem20.Name = "ISRSueldo";
            gridViewSummaryItem21.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem21.AggregateExpression = null;
            gridViewSummaryItem21.FormatString = "{0:N2}";
            gridViewSummaryItem21.Name = "ISRAguinaldo";
            gridViewSummaryItem22.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Sum;
            gridViewSummaryItem22.AggregateExpression = null;
            gridViewSummaryItem22.FormatString = "{0:N2}";
            gridViewSummaryItem22.Name = "AguinaldoPagar";
            this.gridEmpleados.MasterTemplate.SummaryRowsTop.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem12,
                gridViewSummaryItem13,
                gridViewSummaryItem14,
                gridViewSummaryItem15,
                gridViewSummaryItem16,
                gridViewSummaryItem17,
                gridViewSummaryItem18,
                gridViewSummaryItem19,
                gridViewSummaryItem20,
                gridViewSummaryItem21,
                gridViewSummaryItem22}));
            this.gridEmpleados.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridEmpleados.Name = "gridEmpleados";
            this.gridEmpleados.ShowGroupPanel = false;
            this.gridEmpleados.Size = new System.Drawing.Size(1088, 545);
            this.gridEmpleados.TabIndex = 5;
            // 
            // AguinaldoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 575);
            this.Controls.Add(this.gridEmpleados);
            this.Controls.Add(this.ToolBar);
            this.Name = "AguinaldoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Aguinaldo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.AguinaldoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Common.Forms.ToolBarCommonControl ToolBar;
        private Telerik.WinControls.UI.RadGridView gridEmpleados;
    }
}