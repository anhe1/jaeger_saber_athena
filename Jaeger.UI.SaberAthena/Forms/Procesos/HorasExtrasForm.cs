﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Service;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class HorasExtrasForm : RadForm {
        protected internal INominaHorasExtraService _horasExtraService;
        protected internal BindingList<IRegistroHoraExtraModel> _DataSource;

        public HorasExtrasForm() {
            InitializeComponent();
        }

        private void HorasExtrasForm_Load(object sender, EventArgs e) {
            this.THorasExtra.Nuevo.Click += THorasExtra_Nuevo_Click;
            this.THorasExtra.Actualizar.Click += Actualizar_Click;
        }

        private void THorasExtra_Nuevo_Click(object sender, EventArgs e) {
            
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this._horasExtraService = new NominaHorasExtraService();
        }

        protected virtual void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting1Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
        }

        protected virtual void Consultar() {
            var query = NominaHorasExtraService.Query().IdNomina(0).Build();
            this._DataSource= new BindingList<IRegistroHoraExtraModel>(
                this._horasExtraService.GetList<RegistroHoraExtraModel>(query).ToList<IRegistroHoraExtraModel>());
        }
    }
}
