﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class EmpleadoAguinaldoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.TEmpleado = new Jaeger.UI.Nomina.Forms.Procesos.AguinaldoGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TEmpleado
            // 
            this.TEmpleado.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TEmpleado.Location = new System.Drawing.Point(0, 0);
            this.TEmpleado.Name = "TEmpleado";
            this.TEmpleado.PDF = null;
            this.TEmpleado.ShowActualizar = true;
            this.TEmpleado.ShowAutosuma = false;
            this.TEmpleado.ShowCancelar = false;
            this.TEmpleado.ShowCerrar = true;
            this.TEmpleado.ShowEditar = true;
            this.TEmpleado.ShowEjercicio = true;
            this.TEmpleado.ShowExportarExcel = false;
            this.TEmpleado.ShowFiltro = true;
            this.TEmpleado.ShowHerramientas = false;
            this.TEmpleado.ShowImprimir = false;
            this.TEmpleado.ShowItem = false;
            this.TEmpleado.ShowNuevo = true;
            this.TEmpleado.ShowPeriodo = true;
            this.TEmpleado.ShowSeleccionMultiple = true;
            this.TEmpleado.Size = new System.Drawing.Size(1016, 520);
            this.TEmpleado.TabIndex = 0;
            // 
            // EmpleadoAguinaldoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 520);
            this.Controls.Add(this.TEmpleado);
            this.Name = "EmpleadoAguinaldoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Aguinaldo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EmpleadoAguinaldoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AguinaldoGridControl TEmpleado;
    }
}