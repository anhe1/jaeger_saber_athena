﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class PeriodoNuevoForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Encabezado = new System.Windows.Forms.PictureBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.Descripcion = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.label12 = new Telerik.WinControls.UI.RadLabel();
            this.FecInicio = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.FecFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.FecPago = new Telerik.WinControls.UI.RadDateTimePicker();
            this.TipoPeriodo = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.TipoNomina = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.BCancelar = new Telerik.WinControls.UI.RadButton();
            this.BAceptar = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FecInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FecFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FecPago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoPeriodo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BCancelar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BAceptar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // Encabezado
            // 
            this.Encabezado.BackColor = System.Drawing.Color.White;
            this.Encabezado.Dock = System.Windows.Forms.DockStyle.Top;
            this.Encabezado.Location = new System.Drawing.Point(0, 0);
            this.Encabezado.Name = "Encabezado";
            this.Encabezado.Size = new System.Drawing.Size(395, 47);
            this.Encabezado.TabIndex = 104;
            this.Encabezado.TabStop = false;
            // 
            // radLabel2
            // 
            this.radLabel2.BackColor = System.Drawing.Color.White;
            this.radLabel2.Location = new System.Drawing.Point(12, 12);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(280, 18);
            this.radLabel2.TabIndex = 106;
            this.radLabel2.Text = "Ingresa la información necesaria para el nuevo período";
            // 
            // Descripcion
            // 
            this.Descripcion.Location = new System.Drawing.Point(107, 64);
            this.Descripcion.MaxLength = 65;
            this.Descripcion.Name = "Descripcion";
            this.Descripcion.Size = new System.Drawing.Size(266, 20);
            this.Descripcion.TabIndex = 115;
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 65);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(67, 18);
            this.radLabel4.TabIndex = 114;
            this.radLabel4.Text = "Descripción:";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(12, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 18);
            this.label12.TabIndex = 117;
            this.label12.Text = "Fecha de Inicio:";
            // 
            // FecInicio
            // 
            this.FecInicio.Location = new System.Drawing.Point(107, 90);
            this.FecInicio.Name = "FecInicio";
            this.FecInicio.Size = new System.Drawing.Size(185, 20);
            this.FecInicio.TabIndex = 116;
            this.FecInicio.TabStop = false;
            this.FecInicio.Text = "lunes, 21 de enero de 2019";
            this.FecInicio.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 174);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 116);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(79, 18);
            this.radLabel1.TabIndex = 119;
            this.radLabel1.Text = "Fecha de Final:";
            // 
            // FecFinal
            // 
            this.FecFinal.Location = new System.Drawing.Point(107, 116);
            this.FecFinal.Name = "FecFinal";
            this.FecFinal.Size = new System.Drawing.Size(185, 20);
            this.FecFinal.TabIndex = 118;
            this.FecFinal.TabStop = false;
            this.FecFinal.Text = "lunes, 21 de enero de 2019";
            this.FecFinal.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 174);
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(12, 142);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(82, 18);
            this.radLabel3.TabIndex = 121;
            this.radLabel3.Text = "Fecha de Pago:";
            // 
            // FecPago
            // 
            this.FecPago.Location = new System.Drawing.Point(107, 142);
            this.FecPago.Name = "FecPago";
            this.FecPago.Size = new System.Drawing.Size(185, 20);
            this.FecPago.TabIndex = 120;
            this.FecPago.TabStop = false;
            this.FecPago.Text = "lunes, 21 de enero de 2019";
            this.FecPago.Value = new System.DateTime(2019, 1, 21, 13, 51, 43, 174);
            // 
            // TipoPeriodo
            // 
            this.TipoPeriodo.DataMember = "Id";
            this.TipoPeriodo.DisplayMember = "Descripcion";
            this.TipoPeriodo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoPeriodo.Location = new System.Drawing.Point(107, 194);
            this.TipoPeriodo.Name = "TipoPeriodo";
            this.TipoPeriodo.NullText = "Selecciona";
            this.TipoPeriodo.Size = new System.Drawing.Size(122, 20);
            this.TipoPeriodo.TabIndex = 125;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(12, 194);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(48, 18);
            this.radLabel5.TabIndex = 124;
            this.radLabel5.Text = "Periodo:";
            // 
            // TipoNomina
            // 
            this.TipoNomina.DataMember = "Id";
            this.TipoNomina.DisplayMember = "Descripcion";
            this.TipoNomina.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.TipoNomina.Location = new System.Drawing.Point(107, 168);
            this.TipoNomina.Name = "TipoNomina";
            this.TipoNomina.NullText = "Selecciona";
            this.TipoNomina.Size = new System.Drawing.Size(122, 20);
            this.TipoNomina.TabIndex = 123;
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(12, 168);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(89, 18);
            this.radLabel6.TabIndex = 122;
            this.radLabel6.Text = "Tipo de Nómina:";
            // 
            // BCancelar
            // 
            this.BCancelar.Location = new System.Drawing.Point(270, 259);
            this.BCancelar.Name = "BCancelar";
            this.BCancelar.Size = new System.Drawing.Size(113, 23);
            this.BCancelar.TabIndex = 126;
            this.BCancelar.Text = "Cancelar";
            this.BCancelar.Click += new System.EventHandler(this.BCancelar_Click);
            // 
            // BAceptar
            // 
            this.BAceptar.Location = new System.Drawing.Point(270, 230);
            this.BAceptar.Name = "BAceptar";
            this.BAceptar.Size = new System.Drawing.Size(113, 23);
            this.BAceptar.TabIndex = 127;
            this.BAceptar.Text = "Crear";
            this.BAceptar.Click += new System.EventHandler(this.BAceptar_Click);
            // 
            // PeriodoNuevoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 289);
            this.ControlBox = false;
            this.Controls.Add(this.BCancelar);
            this.Controls.Add(this.BAceptar);
            this.Controls.Add(this.TipoPeriodo);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.TipoNomina);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.FecPago);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.FecFinal);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.FecInicio);
            this.Controls.Add(this.Descripcion);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.Encabezado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PeriodoNuevoForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PeriodoNuevoForm";
            this.Load += new System.EventHandler(this.PeriodoNuevoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Encabezado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Descripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FecInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FecFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FecPago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoPeriodo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BCancelar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BAceptar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Encabezado;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadTextBox Descripcion;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel label12;
        private Telerik.WinControls.UI.RadDateTimePicker FecInicio;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker FecFinal;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadDateTimePicker FecPago;
        private Telerik.WinControls.UI.RadDropDownList TipoPeriodo;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadDropDownList TipoNomina;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        internal Telerik.WinControls.UI.RadButton BCancelar;
        internal Telerik.WinControls.UI.RadButton BAceptar;
    }
}