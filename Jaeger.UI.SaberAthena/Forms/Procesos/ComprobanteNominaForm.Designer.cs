﻿
namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class ComprobanteNominaForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.PanelIzquierdo = new Telerik.WinControls.UI.RadCollapsiblePanel();
            this.GrupoBusqueda = new Telerik.WinControls.UI.RadGroupBox();
            this.ButtonBuscar = new Telerik.WinControls.UI.RadButton();
            this.Empleados = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PorEmpleado = new Telerik.WinControls.UI.RadCheckBox();
            this.Departamentos = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PorDepartamento = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.FechaFinal = new Telerik.WinControls.UI.RadDateTimePicker();
            this.FechaInicial = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ButtonSplitFechas = new Telerik.WinControls.UI.RadSplitButton();
            this.PorRangoFechas = new Telerik.WinControls.UI.RadRadioButton();
            this.IdDocumento = new Telerik.WinControls.UI.RadTextBox();
            this.PorIdDocumento = new Telerik.WinControls.UI.RadRadioButton();
            this.Nominas = new Telerik.WinControls.UI.RadMultiColumnComboBox();
            this.PorNomina = new Telerik.WinControls.UI.RadCheckBox();
            this.BarraEstado = new Telerik.WinControls.UI.RadStatusStrip();
            this.BarraEstadoLabel = new Telerik.WinControls.UI.RadLabelElement();
            this.TNomina = new Jaeger.UI.Nomina.Forms.ComprobanteNominaGridControl();
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).BeginInit();
            this.PanelIzquierdo.PanelContainer.SuspendLayout();
            this.PanelIzquierdo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).BeginInit();
            this.GrupoBusqueda.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorEmpleado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFinal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSplitFechas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorRangoFechas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorIdDocumento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorNomina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelIzquierdo
            // 
            this.PanelIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelIzquierdo.ExpandDirection = Telerik.WinControls.UI.RadDirection.Right;
            this.PanelIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.PanelIzquierdo.Name = "PanelIzquierdo";
            this.PanelIzquierdo.OwnerBoundsCache = new System.Drawing.Rectangle(0, 0, 150, 648);
            // 
            // PanelIzquierdo.PanelContainer
            // 
            this.PanelIzquierdo.PanelContainer.Controls.Add(this.GrupoBusqueda);
            this.PanelIzquierdo.PanelContainer.Size = new System.Drawing.Size(347, 567);
            this.PanelIzquierdo.Size = new System.Drawing.Size(375, 569);
            this.PanelIzquierdo.TabIndex = 1;
            // 
            // GrupoBusqueda
            // 
            this.GrupoBusqueda.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.GrupoBusqueda.Controls.Add(this.ButtonBuscar);
            this.GrupoBusqueda.Controls.Add(this.Empleados);
            this.GrupoBusqueda.Controls.Add(this.PorEmpleado);
            this.GrupoBusqueda.Controls.Add(this.Departamentos);
            this.GrupoBusqueda.Controls.Add(this.PorDepartamento);
            this.GrupoBusqueda.Controls.Add(this.radLabel3);
            this.GrupoBusqueda.Controls.Add(this.radLabel2);
            this.GrupoBusqueda.Controls.Add(this.FechaFinal);
            this.GrupoBusqueda.Controls.Add(this.FechaInicial);
            this.GrupoBusqueda.Controls.Add(this.radLabel1);
            this.GrupoBusqueda.Controls.Add(this.ButtonSplitFechas);
            this.GrupoBusqueda.Controls.Add(this.PorRangoFechas);
            this.GrupoBusqueda.Controls.Add(this.IdDocumento);
            this.GrupoBusqueda.Controls.Add(this.PorIdDocumento);
            this.GrupoBusqueda.Controls.Add(this.Nominas);
            this.GrupoBusqueda.Controls.Add(this.PorNomina);
            this.GrupoBusqueda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GrupoBusqueda.HeaderText = "Búsqueda";
            this.GrupoBusqueda.Location = new System.Drawing.Point(0, 0);
            this.GrupoBusqueda.Name = "GrupoBusqueda";
            this.GrupoBusqueda.Size = new System.Drawing.Size(347, 567);
            this.GrupoBusqueda.TabIndex = 0;
            this.GrupoBusqueda.Text = "Búsqueda";
            // 
            // ButtonBuscar
            // 
            this.ButtonBuscar.Location = new System.Drawing.Point(210, 382);
            this.ButtonBuscar.Name = "ButtonBuscar";
            this.ButtonBuscar.Size = new System.Drawing.Size(110, 24);
            this.ButtonBuscar.TabIndex = 15;
            this.ButtonBuscar.Text = "Buscar";
            // 
            // Empleados
            // 
            // 
            // Empleados.NestedRadGridView
            // 
            this.Empleados.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Empleados.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Empleados.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Empleados.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Empleados.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Empleados.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Empleados.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Empleados.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Empleados.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Empleados.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.Empleados.EditorControl.Name = "NestedRadGridView";
            this.Empleados.EditorControl.ReadOnly = true;
            this.Empleados.EditorControl.ShowGroupPanel = false;
            this.Empleados.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Empleados.EditorControl.TabIndex = 0;
            this.Empleados.Location = new System.Drawing.Point(14, 340);
            this.Empleados.Name = "Empleados";
            this.Empleados.NullText = "Empleado";
            this.Empleados.Size = new System.Drawing.Size(306, 20);
            this.Empleados.TabIndex = 14;
            this.Empleados.TabStop = false;
            // 
            // PorEmpleado
            // 
            this.PorEmpleado.Location = new System.Drawing.Point(14, 316);
            this.PorEmpleado.Name = "PorEmpleado";
            this.PorEmpleado.Size = new System.Drawing.Size(90, 18);
            this.PorEmpleado.TabIndex = 13;
            this.PorEmpleado.Text = "Por Empleado";
            // 
            // Departamentos
            // 
            // 
            // Departamentos.NestedRadGridView
            // 
            this.Departamentos.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Departamentos.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Departamentos.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Departamentos.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Departamentos.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Departamentos.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Departamentos.EditorControl.MasterTemplate.AllowColumnChooser = false;
            this.Departamentos.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Departamentos.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Departamentos.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.Departamentos.EditorControl.Name = "NestedRadGridView";
            this.Departamentos.EditorControl.ReadOnly = true;
            this.Departamentos.EditorControl.ShowGroupPanel = false;
            this.Departamentos.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Departamentos.EditorControl.TabIndex = 0;
            this.Departamentos.Location = new System.Drawing.Point(14, 290);
            this.Departamentos.Name = "Departamentos";
            this.Departamentos.NullText = "Departamento";
            this.Departamentos.Size = new System.Drawing.Size(306, 20);
            this.Departamentos.TabIndex = 12;
            this.Departamentos.TabStop = false;
            // 
            // PorDepartamento
            // 
            this.PorDepartamento.Location = new System.Drawing.Point(14, 266);
            this.PorDepartamento.Name = "PorDepartamento";
            this.PorDepartamento.Size = new System.Drawing.Size(113, 18);
            this.PorDepartamento.TabIndex = 11;
            this.PorDepartamento.Text = "Por Departamento";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(14, 232);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(19, 18);
            this.radLabel3.TabIndex = 10;
            this.radLabel3.Text = "Al:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(14, 204);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(25, 18);
            this.radLabel2.TabIndex = 9;
            this.radLabel2.Text = "Del:";
            // 
            // FechaFinal
            // 
            this.FechaFinal.Location = new System.Drawing.Point(45, 230);
            this.FechaFinal.Name = "FechaFinal";
            this.FechaFinal.Size = new System.Drawing.Size(208, 20);
            this.FechaFinal.TabIndex = 8;
            this.FechaFinal.TabStop = false;
            this.FechaFinal.Text = "jueves, 1 de noviembre de 2018";
            this.FechaFinal.Value = new System.DateTime(2018, 11, 1, 22, 10, 26, 820);
            // 
            // FechaInicial
            // 
            this.FechaInicial.Location = new System.Drawing.Point(45, 204);
            this.FechaInicial.Name = "FechaInicial";
            this.FechaInicial.Size = new System.Drawing.Size(208, 20);
            this.FechaInicial.TabIndex = 7;
            this.FechaInicial.TabStop = false;
            this.FechaInicial.Text = "jueves, 1 de noviembre de 2018";
            this.FechaInicial.Value = new System.DateTime(2018, 11, 1, 22, 10, 26, 820);
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(14, 174);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(73, 18);
            this.radLabel1.TabIndex = 6;
            this.radLabel1.Text = "Por Fecha de:";
            // 
            // ButtonSplitFechas
            // 
            this.ButtonSplitFechas.Location = new System.Drawing.Point(112, 174);
            this.ButtonSplitFechas.Name = "ButtonSplitFechas";
            this.ButtonSplitFechas.Size = new System.Drawing.Size(110, 24);
            this.ButtonSplitFechas.TabIndex = 5;
            this.ButtonSplitFechas.Text = "Selecciona";
            // 
            // PorRangoFechas
            // 
            this.PorRangoFechas.Location = new System.Drawing.Point(14, 150);
            this.PorRangoFechas.Name = "PorRangoFechas";
            this.PorRangoFechas.Size = new System.Drawing.Size(120, 18);
            this.PorRangoFechas.TabIndex = 4;
            this.PorRangoFechas.Text = "Por rango de fechas";
            // 
            // IdDocumento
            // 
            this.IdDocumento.Location = new System.Drawing.Point(14, 114);
            this.IdDocumento.MaxLength = 36;
            this.IdDocumento.Name = "IdDocumento";
            this.IdDocumento.NullText = "UUID";
            this.IdDocumento.Size = new System.Drawing.Size(306, 20);
            this.IdDocumento.TabIndex = 3;
            // 
            // PorIdDocumento
            // 
            this.PorIdDocumento.Location = new System.Drawing.Point(14, 90);
            this.PorIdDocumento.Name = "PorIdDocumento";
            this.PorIdDocumento.Size = new System.Drawing.Size(131, 18);
            this.PorIdDocumento.TabIndex = 2;
            this.PorIdDocumento.Text = "Por Folio Fiscal (UUID)";
            // 
            // Nominas
            // 
            this.Nominas.AutoSizeDropDownHeight = true;
            this.Nominas.AutoSizeDropDownToBestFit = true;
            this.Nominas.DisplayMember = "Descripcion";
            this.Nominas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // Nominas.NestedRadGridView
            // 
            this.Nominas.EditorControl.BackColor = System.Drawing.SystemColors.Window;
            this.Nominas.EditorControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Nominas.EditorControl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Nominas.EditorControl.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.Nominas.EditorControl.MasterTemplate.AllowAddNewRow = false;
            this.Nominas.EditorControl.MasterTemplate.AllowCellContextMenu = false;
            this.Nominas.EditorControl.MasterTemplate.AllowColumnChooser = false;
            gridViewTextBoxColumn1.FieldName = "IdControl";
            gridViewTextBoxColumn1.HeaderText = "Núm.";
            gridViewTextBoxColumn1.Name = "IdControl";
            gridViewTextBoxColumn2.FieldName = "Descripcion";
            gridViewTextBoxColumn2.HeaderText = "Descripción";
            gridViewTextBoxColumn2.Name = "Descripcion";
            gridViewTextBoxColumn2.Width = 260;
            gridViewTextBoxColumn3.FieldName = "TipoNomina";
            gridViewTextBoxColumn3.HeaderText = "Tipo";
            gridViewTextBoxColumn3.Name = "TipoNomina";
            gridViewTextBoxColumn4.FieldName = "FechaSubida";
            gridViewTextBoxColumn4.FormatString = "{0:d}";
            gridViewTextBoxColumn4.HeaderText = "Fecha";
            gridViewTextBoxColumn4.Name = "FechaSubida";
            gridViewTextBoxColumn4.Width = 85;
            this.Nominas.EditorControl.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.Nominas.EditorControl.MasterTemplate.EnableGrouping = false;
            this.Nominas.EditorControl.MasterTemplate.ShowFilteringRow = false;
            this.Nominas.EditorControl.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.Nominas.EditorControl.Name = "NestedRadGridView";
            this.Nominas.EditorControl.ReadOnly = true;
            this.Nominas.EditorControl.ShowGroupPanel = false;
            this.Nominas.EditorControl.Size = new System.Drawing.Size(240, 150);
            this.Nominas.EditorControl.TabIndex = 0;
            this.Nominas.Location = new System.Drawing.Point(14, 53);
            this.Nominas.Name = "Nominas";
            this.Nominas.NullText = "Selecciona";
            this.Nominas.Size = new System.Drawing.Size(306, 20);
            this.Nominas.TabIndex = 1;
            this.Nominas.TabStop = false;
            this.Nominas.ValueMember = "IdControl";
            // 
            // PorNomina
            // 
            this.PorNomina.Location = new System.Drawing.Point(14, 29);
            this.PorNomina.Name = "PorNomina";
            this.PorNomina.Size = new System.Drawing.Size(80, 18);
            this.PorNomina.TabIndex = 0;
            this.PorNomina.Text = "Por Nómina";
            // 
            // BarraEstado
            // 
            this.BarraEstado.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BarraEstadoLabel});
            this.BarraEstado.Location = new System.Drawing.Point(375, 543);
            this.BarraEstado.Name = "BarraEstado";
            this.BarraEstado.Size = new System.Drawing.Size(724, 26);
            this.BarraEstado.TabIndex = 3;
            // 
            // BarraEstadoLabel
            // 
            this.BarraEstadoLabel.Name = "BarraEstadoLabel";
            this.BarraEstado.SetSpring(this.BarraEstadoLabel, false);
            this.BarraEstadoLabel.Text = "Listo.";
            this.BarraEstadoLabel.TextWrap = true;
            this.BarraEstadoLabel.UseCompatibleTextRendering = false;
            // 
            // TNomina
            // 
            this.TNomina.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TNomina.Location = new System.Drawing.Point(375, 0);
            this.TNomina.Name = "TNomina";
            this.TNomina.PDF = null;
            this.TNomina.ShowActualizar = true;
            this.TNomina.ShowAutosuma = false;
            this.TNomina.ShowCancelar = false;
            this.TNomina.ShowCerrar = true;
            this.TNomina.ShowEditar = true;
            this.TNomina.ShowEjercicio = true;
            this.TNomina.ShowExportarExcel = false;
            this.TNomina.ShowFiltro = true;
            this.TNomina.ShowHerramientas = false;
            this.TNomina.ShowImprimir = false;
            this.TNomina.ShowItem = false;
            this.TNomina.ShowNuevo = true;
            this.TNomina.ShowPeriodo = true;
            this.TNomina.ShowSeleccionMultiple = true;
            this.TNomina.Size = new System.Drawing.Size(724, 543);
            this.TNomina.TabIndex = 4;
            // 
            // ComprobanteNominaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 569);
            this.Controls.Add(this.TNomina);
            this.Controls.Add(this.BarraEstado);
            this.Controls.Add(this.PanelIzquierdo);
            this.Name = "ComprobanteNominaForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Recibo de Nómina";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ComprobanteNominaForm_Load);
            this.PanelIzquierdo.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PanelIzquierdo)).EndInit();
            this.PanelIzquierdo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GrupoBusqueda)).EndInit();
            this.GrupoBusqueda.ResumeLayout(false);
            this.GrupoBusqueda.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Empleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorEmpleado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Departamentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorDepartamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFinal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonSplitFechas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorRangoFechas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorIdDocumento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas.EditorControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Nominas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PorNomina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarraEstado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadCollapsiblePanel PanelIzquierdo;
        private Telerik.WinControls.UI.RadGroupBox GrupoBusqueda;
        private Telerik.WinControls.UI.RadButton ButtonBuscar;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Empleados;
        private Telerik.WinControls.UI.RadCheckBox PorEmpleado;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Departamentos;
        private Telerik.WinControls.UI.RadCheckBox PorDepartamento;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDateTimePicker FechaFinal;
        private Telerik.WinControls.UI.RadDateTimePicker FechaInicial;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadSplitButton ButtonSplitFechas;
        private Telerik.WinControls.UI.RadRadioButton PorRangoFechas;
        private Telerik.WinControls.UI.RadTextBox IdDocumento;
        private Telerik.WinControls.UI.RadRadioButton PorIdDocumento;
        private Telerik.WinControls.UI.RadMultiColumnComboBox Nominas;
        private Telerik.WinControls.UI.RadCheckBox PorNomina;
        private Telerik.WinControls.UI.RadStatusStrip BarraEstado;
        private Telerik.WinControls.UI.RadLabelElement BarraEstadoLabel;
        private ComprobanteNominaGridControl TNomina;
    }
}