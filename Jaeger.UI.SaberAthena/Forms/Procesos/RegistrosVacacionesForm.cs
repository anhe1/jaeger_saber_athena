﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Service;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class RegistrosVacacionesForm : RadForm {
        protected internal INominaVacacionesService _Service;
        public RegistrosVacacionesForm() {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this._Service = new NominaVacacionesService();
        }

        private void RegistrosVacacionesForm_Load(object sender, EventArgs e) {
            this.TRegistro.Nuevo.Click += TRegistro_Nuevo_Click;
        }

        private void TRegistro_Nuevo_Click(object sender, EventArgs e) {
            throw new NotImplementedException();
        }
    }
}
