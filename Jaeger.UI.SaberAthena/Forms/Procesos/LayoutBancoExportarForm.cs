﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Forms {
    public partial class LayoutBancoExportarForm : RadForm {

        public LayoutBancoExportarForm() {
            InitializeComponent();

        }

        private void BancoLayoutExportarForm_Load(object sender, EventArgs e) {
            //if (this.beneficiarios.Count <= 0) {
            //    RadMessageBox.Show(this, "No existen registros para exportar", "Atención", MessageBoxButtons.OK, RadMessageIcon.Exclamation);
            //    this.Close();
            //}
        }

        private void radButtonExportar_Click(object sender, EventArgs e) {
            var saveFile = new SaveFileDialog { Filter = "*.txt|*.txt", FileName = "layout_" + DateTime.Now.ToString("ddmmyy") + ".txt" };
            if (saveFile.ShowDialog(this) == DialogResult.OK) {
               // this.service.Exportar(this.beneficiarios, this.comboBoxLayout.Text, this.textBoxNumeroCuenta.Text, this.textBoxGrupoAfinidad.Text, saveFile.FileName, this.fechaAplicacion.Value, this.checkBoxRFC.Checked);
            }
        }

        private void radButtonCerrar_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void comboBoxLayout_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e) {

        }
    }
}
