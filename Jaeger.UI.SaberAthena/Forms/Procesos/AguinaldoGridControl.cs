﻿using System;
using Jaeger.UI.Nomina.Builder;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    internal class AguinaldoGridControl : Common.Forms.GridCommonControl {
        public AguinaldoGridControl() : base() {
            this.Load += AguinaldoGridControl_Load;
        }

        private void AguinaldoGridControl_Load(object sender, EventArgs e) {
            this.ShowNuevo = false;
            this.ShowEditar = false;
            this.ShowHerramientas = true;
            this.ShowPeriodo =  false;
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this.CreateView();
        }

        public virtual void CreateView() {
            IAguinaldoGridBuilder view = new AguinaldoGridBuilder();
            this.GridData.Columns.AddRange(view.Templetes().Master().Build());
            this.GridData.AllowEditRow = true;
        }
    }
}
