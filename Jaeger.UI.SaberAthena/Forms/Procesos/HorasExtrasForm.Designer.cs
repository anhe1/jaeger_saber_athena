﻿namespace Jaeger.UI.Nomina.Forms.Procesos {
    partial class HorasExtrasForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.THorasExtra = new Jaeger.UI.Nomina.Forms.Procesos.HorasExtraGridControl();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // THorasExtra
            // 
            this.THorasExtra.Dock = System.Windows.Forms.DockStyle.Fill;
            this.THorasExtra.Location = new System.Drawing.Point(0, 0);
            this.THorasExtra.Name = "THorasExtra";
            this.THorasExtra.PDF = null;
            this.THorasExtra.ShowActualizar = true;
            this.THorasExtra.ShowAutosuma = false;
            this.THorasExtra.ShowCancelar = false;
            this.THorasExtra.ShowCerrar = true;
            this.THorasExtra.ShowEditar = true;
            this.THorasExtra.ShowEjercicio = true;
            this.THorasExtra.ShowExportarExcel = false;
            this.THorasExtra.ShowFiltro = true;
            this.THorasExtra.ShowHerramientas = false;
            this.THorasExtra.ShowImprimir = false;
            this.THorasExtra.ShowItem = false;
            this.THorasExtra.ShowNuevo = true;
            this.THorasExtra.ShowPeriodo = true;
            this.THorasExtra.ShowSeleccionMultiple = true;
            this.THorasExtra.Size = new System.Drawing.Size(800, 450);
            this.THorasExtra.TabIndex = 0;
            // 
            // HorasExtrasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.THorasExtra);
            this.Name = "HorasExtrasForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Horas Extras";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.HorasExtrasForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private HorasExtraGridControl THorasExtra;
    }
}