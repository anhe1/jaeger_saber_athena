﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Forms;
using Jaeger.Aplication.Nomina.Contracts;
using Jaeger.Aplication.Nomina.Service;
using Jaeger.Domain.Nomina.Contracts;
using Jaeger.Domain.Nomina.Entities;

namespace Jaeger.UI.Nomina.Forms.Procesos {
    public partial class RegistroVacacionesForm : RadForm {
        protected internal INominaRegistroDiasService _Service;
        protected internal BindingList<IRegistroDiasModel> _DataSource;

        public RegistroVacacionesForm() {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e) {
            base.OnLoad(e);
            this._Service = new NominaRegistroDiasService();
        }

        private void RegistroVacacionesForm_Load(object sender, EventArgs e) {
            this.TRegistro.Nuevo.Enabled = true;
            this.TRegistro.Nuevo.Click += TRegistro_Nuevo_Click;
            this.TRegistro.Actualizar.Click += Actualizar_Click;
        }

        private void TRegistro_Nuevo_Click(object sender, EventArgs e) {
            using (var d0 = new RegistroVacacionesEmpleadoForm(_Service)) {
                d0.ShowDialog(this);
            }
        }

        private void Actualizar_Click(object sender, EventArgs e) {
            using (var espera = new Waiting2Form(this.Consultar)) {
                espera.ShowDialog(this);
            }
        }

        protected internal virtual void Consultar() {
            var query = NominaRegistroDiasService.Query().IdEmpleado().Tipo(Domain.Nomina.ValueObjects.DiasTipoEnum.Vacaciones).Build();
            this._DataSource = new BindingList<IRegistroDiasModel>(
                this._Service.GetList<RegistroDiasModel>(query).ToList<IRegistroDiasModel>()
                );
        }
    }
}
