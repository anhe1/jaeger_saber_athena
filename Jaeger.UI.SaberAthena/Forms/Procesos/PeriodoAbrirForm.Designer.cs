﻿namespace Jaeger.UI.Nomina.Forms.Procesos
{
    partial class PeriodoAbrirForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewComboBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radGridPeriodos = new Telerik.WinControls.UI.RadGridView();
            this.ToolBar = new Jaeger.UI.Common.Forms.ToolBarCommonControl();
            ((System.ComponentModel.ISupportInitialize)(this.radGridPeriodos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridPeriodos.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGridPeriodos
            // 
            this.radGridPeriodos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridPeriodos.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn17.FieldName = "Id";
            gridViewTextBoxColumn17.HeaderText = "Id";
            gridViewTextBoxColumn17.IsVisible = false;
            gridViewTextBoxColumn17.Name = "Id";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.VisibleInColumnChooser = false;
            gridViewTextBoxColumn18.FieldName = "IdStatus";
            gridViewTextBoxColumn18.HeaderText = "Estado";
            gridViewTextBoxColumn18.Name = "IdStatus";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.Width = 65;
            gridViewTextBoxColumn19.FieldName = "Descripcion";
            gridViewTextBoxColumn19.HeaderText = "Descripción";
            gridViewTextBoxColumn19.Name = "Descripcion";
            gridViewTextBoxColumn19.ReadOnly = true;
            gridViewTextBoxColumn19.Width = 215;
            gridViewTextBoxColumn20.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn20.FieldName = "FechaInicio";
            gridViewTextBoxColumn20.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn20.HeaderText = "Fec. Inicio";
            gridViewTextBoxColumn20.Name = "FechaInicio";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn20.Width = 85;
            gridViewTextBoxColumn21.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn21.FieldName = "FechaFinal";
            gridViewTextBoxColumn21.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn21.HeaderText = "Fec. Final";
            gridViewTextBoxColumn21.Name = "FechaFin";
            gridViewTextBoxColumn21.ReadOnly = true;
            gridViewTextBoxColumn21.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn21.Width = 85;
            gridViewTextBoxColumn22.DataType = typeof(System.DateTime);
            gridViewTextBoxColumn22.FieldName = "FechaPago";
            gridViewTextBoxColumn22.FormatString = "{0:dd MMM yyyy}";
            gridViewTextBoxColumn22.HeaderText = "Fec. Pago";
            gridViewTextBoxColumn22.Name = "FechaPago";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            gridViewTextBoxColumn22.Width = 85;
            gridViewTextBoxColumn23.DataType = typeof(int);
            gridViewTextBoxColumn23.FieldName = "DiasPeriodo";
            gridViewTextBoxColumn23.HeaderText = "Días";
            gridViewTextBoxColumn23.Name = "DiasPeriodo";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            gridViewTextBoxColumn24.FieldName = "IdPeriodoTipo";
            gridViewTextBoxColumn24.HeaderText = "Tipo";
            gridViewTextBoxColumn24.Name = "IdPeriodoTipo";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.Width = 65;
            this.radGridPeriodos.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19,
            gridViewTextBoxColumn20,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewTextBoxColumn24});
            this.radGridPeriodos.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.radGridPeriodos.Name = "radGridPeriodos";
            this.radGridPeriodos.Size = new System.Drawing.Size(713, 250);
            this.radGridPeriodos.TabIndex = 1;
            // 
            // ToolBar
            // 
            this.ToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBar.Location = new System.Drawing.Point(0, 0);
            this.ToolBar.Name = "ToolBar";
            this.ToolBar.ShowActualizar = true;
            this.ToolBar.ShowAutosuma = false;
            this.ToolBar.ShowCancelar = false;
            this.ToolBar.ShowCerrar = true;
            this.ToolBar.ShowEditar = true;
            this.ToolBar.ShowEjercicio = true;
            this.ToolBar.ShowFiltro = false;
            this.ToolBar.ShowHerramientas = false;
            this.ToolBar.ShowImprimir = false;
            this.ToolBar.ShowNuevo = false;
            this.ToolBar.ShowPeriodo = true;
            this.ToolBar.Size = new System.Drawing.Size(713, 30);
            this.ToolBar.TabIndex = 2;
            
            // 
            // NominaPeriodoAbrirForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 280);
            this.Controls.Add(this.radGridPeriodos);
            this.Controls.Add(this.ToolBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NominaPeriodoAbrirForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Abrir Nómina";
            this.Load += new System.EventHandler(this.NominaAbrir_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridPeriodos.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridPeriodos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView radGridPeriodos;
        private Common.Forms.ToolBarCommonControl ToolBar;
    }
}
