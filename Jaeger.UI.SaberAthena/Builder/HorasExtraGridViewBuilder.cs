﻿using Jaeger.UI.Common.Builder;
using System;
using Telerik.WinControls.UI;

namespace Jaeger.UI.Nomina.Builder {
    public class HorasExtraGridViewBuilder : GridViewBuilder, IGridViewBuilder, IHorasExtraGridViewBuilder, IHorasExtraTempletesGridViewBuilder, IHorasExtraColumnsGridViewBuilder {
        public HorasExtraGridViewBuilder() : base() { }

        public IHorasExtraTempletesGridViewBuilder Templetes() { return this; }
        public IHorasExtraTempletesGridViewBuilder Master() {
            this._Columns.Clear();
            this.Id().Activo().IdConcepto().IdNomina().IdEmpleado().IdTipo().Fecha().Cantidad().Creo().FechaNuevo();
            return this;
        }

        public IHorasExtraTempletesGridViewBuilder Simple() {
            this._Columns.Clear();
            this.Fecha().Cantidad();
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder Id() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Id",
                HeaderText = "Id",
                Name = "Id",
                IsVisible = false
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder Activo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                Name = "Activo",
                IsVisible = false
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder IdConcepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdConcepto",
                HeaderText = "IdConcepto",
                Name = "IdConcepto",
                IsVisible = true
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder IdNomina() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdNomina",
                HeaderText = "IdNomina",
                Name = "IdNomina",
                IsVisible = false
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder IdEmpleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdEmpleado",
                HeaderText = "IdEmpleado",
                Name = "IdEmpleado",
                IsVisible = true
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder IdTipo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdTipoHora",
                HeaderText = "IdTipoHora",
                Name = "IdTipoHora",
                IsVisible = true
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder Fecha() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Fecha",
                HeaderText = "Fecha",
                Name = "Fecha",
                IsVisible = true,
                Width = 75,
                DataType = typeof(DateTime),
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                FormatString = this.FormatStringDate
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder Cantidad() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Cantidad",
                HeaderText = "Cantidad",
                Name = "Cantidad",
                IsVisible = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75,
                FormatString = this.FormatStringNumber
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                IsVisible = true
            });
            return this;
        }

        public IHorasExtraColumnsGridViewBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "FechaNuevo",
                HeaderText = "FechaNuevo",
                Name = "FechaNuevo",
                IsVisible = true
            });
            return this;
        }
    }
}
