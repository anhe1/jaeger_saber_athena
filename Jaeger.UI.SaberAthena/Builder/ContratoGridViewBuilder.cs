﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class ContratoGridViewBuilder : GridViewBuilder, IContratoGridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, IContratoTempleteGridViewBuilder, IContratoColumnsGridViewBuilder {
        public ContratoGridViewBuilder() : base() {

        }
        public IContratoTempleteGridViewBuilder Templetes() {
            return this;
        }

        public IContratoTempleteGridViewBuilder Master() {
            this._Columns.Clear();
            this.IdEmpleado().Activo().ClaveTipoRegimen().ClaveRiesgoPuesto().ClavePeriodicidadPago().ClaveMetodoPago().ClaveTipoContrato().ClaveTipoJornada().RegistroPatronal().Departamento().Puesto().FecInicioRelLaboral().
FecTerminoRelLaboral().SalarioBase().SalarioBaseCotizacion().SalarioDiario().Jornadas();
            return this;
        }

        public IContratoTempleteGridViewBuilder Master1() {
            this._Columns.Clear();
            this.IdContrato().Activo().IdEmpleado().Clave();
            return this;
        }

        public IContratoColumnsGridViewBuilder IdContrato() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdContrato",
                HeaderText = "IdContrato",
                IsVisible = true,
                Name = "IdContrato",
                FormatString = "{0:0000#}",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder IdEmpleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "IdEmpleado",
                HeaderText = "IdEmpleado",
                IsVisible = false,
                Name = "IdEmpleado",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(string),
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 80
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder Activo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo"
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder ClaveTipoRegimen() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveTipoRegimen",
                HeaderText = "Régimen",
                Name = "ClaveTipoRegimen"
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder ClaveRiesgoPuesto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveRiesgoPuesto",
                HeaderText = "R. Puesto",
                Name = "ClaveRiesgoPuesto"
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder ClavePeriodicidadPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClavePeriodicidadPago",
                HeaderText = "P. Pago",
                Name = "ClavePeriodicidadPago"
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder ClaveMetodoPago() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveMetodoPago",
                HeaderText = "M. Pago",
                Name = "ClaveMetodoPago"
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder ClaveTipoContrato() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveTipoContrato",
                HeaderText = "T. Contrato",
                Name = "ClaveTipoContrato",
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder ClaveTipoJornada() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "ClaveTipoJornada",
                HeaderText = "T. Jornada",
                Name = "ClaveTipoJornada"
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder RegistroPatronal() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RegistroPatronal",
                HeaderText = "Reg. Patronal",
                Name = "RegistroPatronal"
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder Departamento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Departamento",
                HeaderText = "Departamento",
                Name = "Departamento",
                Width = 150
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder Puesto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Puesto",
                HeaderText = "Puesto",
                Name = "Puesto",
                Width = 150
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder FecInicioRelLaboral() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FecInicioRelLaboral",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "FecInicioRelLaboral",
                Name = "FecInicioRelLaboral",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder FecTerminoRelLaboral() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FecTerminoRelLaboral",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "FecTerminoRelLaboral",
                Name = "FecTerminoRelLaboral",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder SalarioBase() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SalarioBase",
                FormatString = "{0:N2}",
                HeaderText = "SalarioBase",
                Name = "SalarioBase",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder SalarioBaseCotizacion() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SalarioBaseCotizacion",
                FormatString = "{0:N4}",
                HeaderText = "SalarioBaseCotizacion",
                Name = "SalarioBaseCotizacion",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder SalarioDiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SalarioDiario",
                FormatString = "{0:N2}",
                HeaderText = "SalarioDiario",
                Name = "SalarioDiario",
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IContratoColumnsGridViewBuilder Jornadas() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Jornadas",
                FormatString = "{0:N0}",
                HeaderText = "Jornadas",
                Name = "Jornadas",
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
            });
            return this;
        }
    }
}
