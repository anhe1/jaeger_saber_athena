﻿using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class ConceptoGridBuilder : GridViewBuilder, IGridViewBuilder, IConceptoGridBuilder, IConceptoColumnsGridBuilder, IConceptoTempletesGridBuilder {
        public IConceptoTempletesGridBuilder Templetes() {
            return this;
        }

        public IConceptoTempletesGridBuilder Master() {
            this._Columns.Clear();
            Activo().IdTipo().IdAplicacion().Clave().ClaveSAT().Concepto().Formula().Ocultar().PagoEspecie().IdBaseISR();
            return this;
        }

        public IConceptoColumnsGridBuilder Activo() {
            var activo = new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Status",
                Name = "Activo",
            };
            var conditionalFormattingObject1 = new ConditionalFormattingObject {
                ApplyToRow = true,
                CellBackColor = System.Drawing.Color.Empty,
                CellForeColor = System.Drawing.Color.Empty,
                Name = "Activo",
                RowBackColor = System.Drawing.Color.Empty,
                RowFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0))),
                RowForeColor = System.Drawing.Color.Gray,
                TValue1 = "False",
                TValue2 = "False"
            };
            activo.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            this._Columns.Add(activo);
            return this;
        }

        public IConceptoColumnsGridBuilder IdTipo() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdTipo",
                HeaderText = "Tipo",
                Name = "IdTipo",
                Width = 85
            });
            return this;
        }

        public IConceptoColumnsGridBuilder IdAplicacion() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdAplicacion",
                HeaderText = "Aplicación",
                Name = "IdAplicacion",
                Width = 75
            });
            return this;
        }

        public IConceptoColumnsGridBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave"
            });
            return this;
        }

        public IConceptoColumnsGridBuilder ClaveSAT() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ClaveSAT",
                HeaderText = "Cl. SAT",
                Name = "ClaveSAT",
            });
            return this;
        }

        public IConceptoColumnsGridBuilder Concepto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Concepto",
                HeaderText = "Concepto",
                Name = "Concepto",
                Width = 250
            });
            return this;
        }

        public IConceptoColumnsGridBuilder Formula() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Formula",
                HeaderText = "Fórmula",
                Name = "Formula",
                Width = 250
            });
            return this;
        }

        public IConceptoColumnsGridBuilder Ocultar() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Ocultar",
                HeaderText = "Ocultar",
                Name = "Ocultar"
            });
            return this;
        }

        public IConceptoColumnsGridBuilder PagoEspecie() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "PagoEspecie",
                HeaderText = "P. Especie",
                Name = "PagoEspecie",
                DataType = typeof(int)
            });
            return this;
        }

        public IConceptoColumnsGridBuilder IdBaseISR() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdBaseISR",
                HeaderText = "Grava ISR",
                Name = "IdBaseISR",
                Width = 85
            });
            return this;
        }
    }
}
