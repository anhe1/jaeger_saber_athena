﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IComprobanteGridBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IComprobanteTempletesGridBuilder Templetes();
    }

    public interface IComprobanteTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IComprobanteTempletesGridBuilder Master();
    }

    public interface IComprobanteColumnsGridBuilder : IGridViewColumnsBuild {
        IComprobanteColumnsGridBuilder IdNomina();
        IComprobanteColumnsGridBuilder IdDirectorio();
        IComprobanteColumnsGridBuilder IdComprobante();
        IComprobanteColumnsGridBuilder NoEmpleado();
        IComprobanteColumnsGridBuilder ReceptorNombre();
        IComprobanteColumnsGridBuilder ReceptorRFC();
        IComprobanteColumnsGridBuilder Departamento();
        IComprobanteColumnsGridBuilder Puesto();
        IComprobanteColumnsGridBuilder ClaveRiesgoPuesto();
        IComprobanteColumnsGridBuilder ClaveTipoRegimen();
        IComprobanteColumnsGridBuilder ClaveTipoContrato();
        IComprobanteColumnsGridBuilder ClaveTipoJornada();
        IComprobanteColumnsGridBuilder ClavePeriricidadPago();
        IComprobanteColumnsGridBuilder FechaPago();
        IComprobanteColumnsGridBuilder FechaInicialPago();
        IComprobanteColumnsGridBuilder FechaFinalPago();
        IComprobanteColumnsGridBuilder NumDiasPagados();
        IComprobanteColumnsGridBuilder FechaInicioRelLaboral();
        IComprobanteColumnsGridBuilder Antiguedad();
        IComprobanteColumnsGridBuilder SalarioBaseCotApor();
        IComprobanteColumnsGridBuilder SalarioDiarioIntegrado();
        IComprobanteColumnsGridBuilder PercepcionTotalGravado();
        IComprobanteColumnsGridBuilder PercepcionTotalExento();
        IComprobanteColumnsGridBuilder DeduccionTotalGravado();
        IComprobanteColumnsGridBuilder DeduccionTotalExento();
        IComprobanteColumnsGridBuilder DescuentoIncapacidad();
        IComprobanteColumnsGridBuilder HorasExtra();
        IComprobanteColumnsGridBuilder Total();
        IComprobanteColumnsGridBuilder ClaveTipoNomina();
        IComprobanteColumnsGridBuilder IdDocumento();
        IComprobanteColumnsGridBuilder Estado();
        IComprobanteColumnsGridBuilder FechaEstado();
        IComprobanteColumnsGridBuilder FechaTimbre();
        IComprobanteColumnsGridBuilder Correo();
        IComprobanteColumnsGridBuilder UrlFileXML();
        IComprobanteColumnsGridBuilder UrlFilePDF();
    }
}
