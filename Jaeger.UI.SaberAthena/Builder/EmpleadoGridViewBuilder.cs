﻿using System;
using System.Drawing;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class EmpleadoGridViewBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IGridViewColumnsBuild, IDisposable,
        IEmpleadoGridViewBuilder, IEmpleadoTempleteGridViewBuilder, IEmpleadoColumnsGridViewBuilder {

        public EmpleadoGridViewBuilder() : base() { }

        public IEmpleadoTempleteGridViewBuilder Templetes() {
            return this;
        }

        public IEmpleadoTempleteGridViewBuilder Master() {
            this._Columns.Clear();
            this.Activo().IdEmpleado().Clave().ApellidoPaterno().ApellidoMaterno().Nombre().RFC().CURP().Depatamento().Puesto().SalarioDiario().SalarioDiarioIntegrado().JornadasTrabajo().Correo().FechaNuevo();
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Activo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "Activo",
                Name = "Activo",
                VisibleInColumnChooser = false
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder IdEmpleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "IdEmpleado",
                FormatString = "{0:0000#}",
                HeaderText = "Núm. ",
                Name = "IdEmpleado"
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 80
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder ApellidoPaterno() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ApellidoPaterno",
                HeaderText = "A. Paterno",
                Name = "ApellidoPaterno",
                Width = 140
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder ApellidoMaterno() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "ApellidoMaterno",
                HeaderText = "A. Materno",
                Name = "ApellidoMaterno",
                Width = 140
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Nombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre",
                Name = "Nombre",
                Width = 140
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder RFC() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "RFC",
                HeaderText = "RFC",
                Name = "rfc",
                Width = 115
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder CURP() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "CURP",
                HeaderText = "CURP",
                Name = "Curp",
                Width = 135
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Depatamento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Departamento",
                HeaderText = "Departamento",
                IsVisible = false,
                Name = "Departamento",
                Width = 180
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Puesto() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Puesto",
                HeaderText = "Puesto",
                IsVisible = false,
                Name = "Puesto",
                Width = 120
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder SalarioDiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SalarioDiario",
                FormatString = "{0:n2}",
                HeaderText = "SD",
                IsVisible = false,
                Name = "SalarioDiario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder SalarioDiarioIntegrado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                FieldName = "SalarioDiarioIntegrado",
                FormatString = "{0:n2}",
                HeaderText = "SDI",
                IsVisible = false,
                Name = "SalarioDiarioIntegrado",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 75
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder JornadasTrabajo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                FieldName = "JornadasTrabajo",
                HeaderText = "J. Trabajo",
                IsVisible = false,
                Name = "JornadasTrabajo",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Correo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Correo",
                HeaderText = "Correo",
                Name = "Correo",
                Width = 145
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder FechaNuevo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(System.DateTime),
                FieldName = "FechaNuevo",
                FormatString = "{0:dd MMM yy}",
                HeaderText = "Fc. Sist.",
                Name = "FechaNuevo",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creó",
                Name = "Creo",
                Width = 65,
                TextAlignment = ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder Modifica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Modifica",
                HeaderText = "Modifica",
                IsVisible = false,
                Name = "Modifica",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 65
            });
            return this;
        }

        public IEmpleadoColumnsGridViewBuilder FechaModifica() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(DateTime),
                FieldName = "FechaModifica",
                HeaderText = "Fec. Mod.",
                IsVisible = false,
                Name = "FechaModifica",
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = 75
            });
            return this;
        }
    }
}
