﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IConceptoGridBuilder : IGridViewBuilder {
        IConceptoTempletesGridBuilder Templetes();
    }

    public interface IConceptoTempletesGridBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IConceptoTempletesGridBuilder Master();
    }

    public interface IConceptoColumnsGridBuilder : IGridViewColumnsBuild {
        IConceptoColumnsGridBuilder Activo();
        IConceptoColumnsGridBuilder IdTipo();
        IConceptoColumnsGridBuilder IdAplicacion();
        IConceptoColumnsGridBuilder Clave();
        IConceptoColumnsGridBuilder ClaveSAT();
        IConceptoColumnsGridBuilder Concepto();
        IConceptoColumnsGridBuilder Formula();
        IConceptoColumnsGridBuilder Ocultar();
        IConceptoColumnsGridBuilder PagoEspecie();
        IConceptoColumnsGridBuilder IdBaseISR();
    }
}
