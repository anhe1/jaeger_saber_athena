﻿using System;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public interface IContratoGridViewBuilder : IGridViewBuilder, IGridViewTempleteBuild, IDisposable {
        IContratoTempleteGridViewBuilder Templetes();
    }

    public interface IContratoTempleteGridViewBuilder : IGridViewTempleteBuild, IGridViewBuilder {
        IContratoTempleteGridViewBuilder Master();
    }

    public interface IContratoColumnsGridViewBuilder : IGridViewColumnsBuild {
        IContratoColumnsGridViewBuilder IdEmpleado();

        IContratoColumnsGridViewBuilder Clave();

        IContratoColumnsGridViewBuilder Activo();

        IContratoColumnsGridViewBuilder ClaveTipoRegimen();

        IContratoColumnsGridViewBuilder ClaveRiesgoPuesto();

        IContratoColumnsGridViewBuilder ClavePeriodicidadPago();

        IContratoColumnsGridViewBuilder ClaveMetodoPago();

        IContratoColumnsGridViewBuilder ClaveTipoContrato();

        IContratoColumnsGridViewBuilder ClaveTipoJornada();

        IContratoColumnsGridViewBuilder RegistroPatronal();

        IContratoColumnsGridViewBuilder Departamento();

        IContratoColumnsGridViewBuilder Puesto();

        IContratoColumnsGridViewBuilder FecInicioRelLaboral();

        IContratoColumnsGridViewBuilder FecTerminoRelLaboral();

        IContratoColumnsGridViewBuilder SalarioBase();

        IContratoColumnsGridViewBuilder SalarioBaseCotizacion();

        IContratoColumnsGridViewBuilder SalarioDiario();

        IContratoColumnsGridViewBuilder Jornadas();
    }
}
