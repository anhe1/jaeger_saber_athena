﻿using System.Drawing;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Export;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class AguinaldoGridBuilder : GridViewBuilder, IGridViewBuilder, IAguinaldoGridBuilder, IAguinaldoTempletesGridBuilder, IAguinaldoColumnsGridBuilder {
        public AguinaldoGridBuilder() : base() { }

        public IAguinaldoTempletesGridBuilder Templetes() {
            return this;
        }

        public IAguinaldoTempletesGridBuilder Master() {
            this._Columns.Clear();
            this.IdEmpleado().Clave().Nombre().FechaInicioLaboral().DiasLaborados().Ausencias().SalarioDiario().SalarioMensual().DiasAguinaldo().Importe().BaseGravable().BaseExento().BaseGravada().ISR()
                .Subsidio().ISRSalarioMasAguinaldo().ISRSueldo().ISRAguinaldo().AguinaldoPagar();
            return this;
        }

        #region columnas
        public IAguinaldoColumnsGridBuilder IdEmpleado() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(int),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "IdEmpleado",
                FormatString = "{0:0000#}",
                HeaderText = "Núm",
                Name = "IdEmpleado",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleCenter,
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                ReadOnly = true,
                Width = 60
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder Nombre() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Nombre",
                HeaderText = "Nombre",
                Name = "Nombre",
                ReadOnly = true,
                Width = 220
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder FechaInicioLaboral() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FecInicioRelLaboral",
                FormatString = "{0:dd MMM yyyy}",
                HeaderText = "Fec. Alta",
                Name = "FecInicioRelLaboral",
                Width = 75
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder DiasLaborados() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "DiasLaborados",
                FormatString = "{0:N0}",
                HeaderText = "Dias Laborados",
                Name = "DiasLaborados",
                TextAlignment = ContentAlignment.MiddleCenter,
                WrapText = true,
                ReadOnly = false
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder Ausencias() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "Ausencias",
                FormatString = "{0:N0}",
                HeaderText = "Ausencias",
                Name = "Ausencias",
                TextAlignment = ContentAlignment.MiddleCenter,
                WrapText = true,
                Width = 60,
                ReadOnly = false
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder SalarioDiario() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "SalarioDiario",
                FormatString = "{0:n}",
                HeaderText = "Salario Diario",
                Name = "SalarioDiario",
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 60,
                WrapText = true
            }
         );
            return this;
        }

        public IAguinaldoColumnsGridBuilder SalarioMensual() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "SalarioMensual",
                FormatString = "{0:n}",
                HeaderText = "Salario Mensual",
                Name = "SalarioMensual",
                ReadOnly = true,
                TextAlignment = System.Drawing.ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder DiasAguinaldo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "DiasAguinaldo",
                FormatString = "{0:N0}",
                HeaderText = "Días Aguinaldo",
                MaxLength = 10,
                Name = "DiasAguinaldo",
                TextAlignment = ContentAlignment.MiddleCenter
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder Importe() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "Importe",
                FormatString = "{0:n}",
                HeaderText = "Aguinaldo",
                Name = "Aguinaldo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder BaseGravable() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "BaseGravable",
                FormatString = "{0:n}",
                HeaderText = "Base Gravable",
                Name = "BaseGravable",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder BaseExento() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "BaseExento",
                FormatString = "{0:n}",
                HeaderText = "Base Exento",
                Name = "BaseExento",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder BaseGravada() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "BaseGravada",
                FormatString = "{0:n}",
                HeaderText = "Base Gravada",
                Name = "BaseGravada",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder ISR() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "ISR",
                FormatString = "{0:n}",
                HeaderText = "ISR Ag + Sld",
                Name = "ISR",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder Subsidio() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "Subsidio",
                FormatString = "{0:n}",
                HeaderText = "Subsidio",
                Name = "Subsidio",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder ISRSalarioMasAguinaldo() {
            var columna = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "ISRSalarioMasAguinaldo",
                FormatString = "{0:n}",
                HeaderText = "ISR Salario + Aguinaldo",
                Name = "ISRSalarioMasAguinaldo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            };
            var conditionalFormattingObject3 = new ConditionalFormattingObject {
                CellBackColor = Color.Empty,
                CellForeColor = Color.Red,
                ConditionType = ConditionTypes.Less,
                Name = "Valor Negativo ISR Salario (+Aguinaldo)",
                RowBackColor = Color.Empty,
                RowForeColor = Color.Empty,
                TValue1 = "0"
            };
            columna.ConditionalFormattingObjectList.Add(conditionalFormattingObject3);
            return this;
        }

        public IAguinaldoColumnsGridBuilder ISRSueldo() {
            var columna = new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "ISRSueldo",
                FormatString = "{0:n}",
                HeaderText = "ISR Salario",
                Name = "ISRSueldo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            };
            var conditionalFormattingObject4 = new ConditionalFormattingObject() {
                CellBackColor = Color.Empty,
                CellForeColor = Color.Red,
                ConditionType = ConditionTypes.Less,
                Name = "Valor Negativo ISR Salario",
                RowBackColor = Color.Empty,
                RowForeColor = Color.Empty,
                TValue1 = "0"
            };
            columna.ConditionalFormattingObjectList.Add(conditionalFormattingObject4);
            return this;
        }

        public IAguinaldoColumnsGridBuilder ISRAguinaldo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "ISRAguinaldo",
                FormatString = "{0:n}",
                HeaderText = "ISR Aguinaldo",
                Name = "ISRAguinaldo",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true
            });
            return this;
        }

        public IAguinaldoColumnsGridBuilder AguinaldoPagar() {
            this._Columns.Add(new GridViewTextBoxColumn {
                DataType = typeof(decimal),
                ExcelExportType = DisplayFormatType.Fixed,
                FieldName = "AguinaldoPagar",
                FormatString = "{0:n}",
                HeaderText = "Gratificación",
                Name = "AguinaldoPagar",
                ReadOnly = true,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = 80,
                WrapText = true,
            });
            return this;
        }
        #endregion
    }
}
