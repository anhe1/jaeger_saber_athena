﻿using System;
using Telerik.WinControls.UI;
using Jaeger.UI.Common.Builder;

namespace Jaeger.UI.Nomina.Builder {
    public class CuentaBancoGridViewBuilder : GridViewBuilder, IGridViewBuilder, IGridViewTempleteBuild, IDisposable, ICuentaBancoGridViewBuilder, ICuentaBancariaTempleteGridViewBuilder, ICuentaBancariaColumnsGridViewBuilder {
        public ICuentaBancariaTempleteGridViewBuilder Templetes() {
            return this;
        }

        public ICuentaBancariaTempleteGridViewBuilder Master() {
            this.Activo().Verificado().TipoCuenta().Banco().Clave().InstitucionBancaria().NumeroCuenta().Creo().FechaNuevo();
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder Activo() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Activo",
                HeaderText = "A",
                Name = "Activo"
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder Verificado() {
            this._Columns.Add(new GridViewCheckBoxColumn {
                FieldName = "Verificado",
                HeaderText = "Verificado",
                Name = "Verificado"
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder TipoCuenta() {
            this._Columns.Add(new GridViewComboBoxColumn {
                FieldName = "IdTipoCuenta",
                HeaderText = "IdTipoCuenta",
                Name = "IdTipoCuenta"
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder Banco() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Banco",
                HeaderText = "Banco",
                Name = "Banco"
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder Clave() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Clave",
                HeaderText = "Clave",
                Name = "Clave",
                Width = 110
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder InstitucionBancaria() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "InsitucionBancaria",
                HeaderText = "Insitución Bancaria",
                Name = "InsitucionBancaria",
                Width = 200
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder NumeroCuenta() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "NumeroDeCuenta",
                HeaderText = "Núm Cuenta",
                Name = "NumeroDeCuenta",
                Width = 100
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder Creo() {
            this._Columns.Add(new GridViewTextBoxColumn {
                FieldName = "Creo",
                HeaderText = "Creo",
                Name = "Creo",
                Width = 75
            });
            return this;
        }

        public ICuentaBancariaColumnsGridViewBuilder FechaNuevo() {
            this._Columns.Add(new GridViewDateTimeColumn {
                FieldName = "FechaNuevo",
                HeaderText = "Fec. Sist.",
                Name = "FechaNuevo",
                Width = 75
            });
            return this;
        }
    }
}
